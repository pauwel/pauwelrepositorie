Public Class CVoyage

    Private _id As Integer, _voyagenbr As String
    Private _kade As String
    Private _omschrijving As String
    Private _transID As Integer
    Private _is_afgewerkt As Boolean
    Private _kostID

    Public Property Id() As Integer
        Get
            Return Me._id
        End Get
        Set(ByVal value As Integer)
            Me._id = value
        End Set
    End Property

    Public Property VoyageNbr() As String
        Get
            Return Me._voyagenbr
        End Get
        Set(ByVal value As String)
            Me._voyagenbr = value
        End Set
    End Property

    Public Property Kade() As String
        Get
            Return Me._kade
        End Get
        Set(ByVal value As String)
            Me._kade = value
        End Set
    End Property

    Public Property KostID() As Integer
        Get
            Return Me._kostID
        End Get
        Set(ByVal value As Integer)
            Me._kostID = value
        End Set
    End Property

    Public Property Omschrijving() As String
        Get
            Return Me._omschrijving
        End Get
        Set(ByVal value As String)
            Me._omschrijving = value
        End Set
    End Property
    'Public Property TransId() As Integer
    '    Get
    '        Return Me._transID
    '    End Get
    '    Set(ByVal value As Integer)
    '        Me._transID = value
    '    End Set
    'End Property

    Public Property IsAfgewerkt() As Boolean
        Get
            Return Me._is_afgewerkt
        End Get
        Set(ByVal value As Boolean)
            Me._is_afgewerkt = value
        End Set
    End Property
End Class
