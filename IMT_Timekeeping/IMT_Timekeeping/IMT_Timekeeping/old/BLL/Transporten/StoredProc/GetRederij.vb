
Imports DAL

Public Class GetRederij


    Private _Adapter As TransportTableAdapters.getByRederijTableAdapter
    Protected ReadOnly Property Adapter() As TransportTableAdapters.getByRederijTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New TransportTableAdapters.getByRederijTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property



    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetTransportMiddelByRederij(ByVal rederijID As Integer) As DAL.Transport.getByRederijDataTable
        Return Me.Adapter.GetData(rederijID)

    End Function


End Class
