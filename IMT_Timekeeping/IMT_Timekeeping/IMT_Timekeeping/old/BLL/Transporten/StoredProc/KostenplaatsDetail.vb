
Imports DAL


Public Class KostenplaatsDetail
    Private _Adapter As DAL.SPtransportenTableAdapters.GetKostenplaatsDetailTableAdapter

    Protected ReadOnly Property Adapter() As DAL.SPtransportenTableAdapters.GetKostenplaatsDetailTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New DAL.SPtransportenTableAdapters.GetKostenplaatsDetailTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetKostenplatsDetail() As DAL.SPtransporten.GetKostenplaatsDetailDataTable
        Return Me.Adapter.GetData()
    End Function


End Class
