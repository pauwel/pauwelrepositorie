Option Compare Text
Imports DAL

Public Class SP_Transporten


    Private _Adapter As DAL.SPtransportenTableAdapters.GetTransportMiddelenTableAdapter

    Protected ReadOnly Property Adapter() As DAL.SPtransportenTableAdapters.GetTransportMiddelenTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New DAL.SPtransportenTableAdapters.GetTransportMiddelenTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetTransportMiddel() As DAL.SPtransporten.GetTransportMiddelenDataTable
        Return Me.Adapter.GetData()
    End Function
End Class


