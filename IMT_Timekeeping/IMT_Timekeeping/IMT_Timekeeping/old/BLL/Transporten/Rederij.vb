Option Compare Text
Imports DAL

Public Class Rederij


    Private _adapter As DAL.TransportTableAdapters.tblRederijenTableAdapter
    Protected ReadOnly Property Adapter() As DAL.TransportTableAdapters.tblRederijenTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New DAL.TransportTableAdapters.tblRederijenTableAdapter
            End If
            Return _adapter
        End Get

    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetRederijen() As DAL.Transport.tblRederijenDataTable
        Return Me.Adapter.GetData
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function InsertRederij(ByVal rederijNaam As String)
        Me.Adapter.Insert(rederijNaam, True)
        Return True
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function DeleteRederij(ByVal RederijID As Integer)
        Try
            Return Me.Adapter.Delete(RederijID)
        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try


    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function UpdateRederij(ByVal RederijID As Integer, ByVal RederijNaam As String)
        Me.Adapter.Update(RederijNaam, True, RederijID)
        Return True
    End Function



End Class
