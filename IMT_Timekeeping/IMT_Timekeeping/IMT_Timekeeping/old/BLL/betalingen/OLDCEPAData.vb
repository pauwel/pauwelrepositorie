

Imports DAL

Public Class OLDCEPAData

    Private _adapter As betalingenTableAdapters.GETCEPAPreviousValidatedDataTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As betalingenTableAdapters.GETCEPAPreviousValidatedDataTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New betalingenTableAdapters.GETCEPAPreviousValidatedDataTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function GetBetalingdatums() As betalingen.GETCEPAPreviousValidatedDataDataTable

        Return Adapter.GetData

    End Function

End Class
