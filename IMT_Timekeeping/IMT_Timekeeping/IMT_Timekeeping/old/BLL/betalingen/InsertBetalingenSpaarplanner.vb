
Imports DAL

Public Class InsertBetalingenSpaarplanner
    Private _adapter As betalingenTableAdapters.Betaling = Nothing

    Protected ReadOnly Property Adapter() As betalingenTableAdapters.Betaling
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New betalingenTableAdapters.Betaling
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function InsertBetalingSpaarplanner(ByVal datum As DateTime, ByVal gevraagdbedrag As Decimal, ByVal shiftid As Integer, ByVal werknemerid As Integer, ByVal stdloon As Decimal)
        Me.Adapter.InsertBetalingSpaarplanner(datum, gevraagdbedrag, shiftid, werknemerid, stdloon)
        Return True
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
        Public Function getsaldo(ByVal werknemerid As Integer) As Decimal
        Return Me.Adapter.GetSaldoByWerknemer(werknemerid, 0)

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
           Public Function getstdloon(ByVal werknemerid As Integer, ByVal shiftid As Integer) As Decimal
        Return Me.Adapter.GetMinLoonSpaarplanner(werknemerid, shiftid, 0)

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
          Public Function CheckReedsBetaald(ByVal werknemerid As Integer, ByVal datum As DateTime) As Boolean

        If Me.Adapter.CheckBetalingInput(datum, werknemerid) = 0 Then
            Return False
        Else
            Return True
        End If
    
    End Function


    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
       Function InsertBetalingsregels(ByVal wachtid As Integer) As Boolean
        Return Adapter.InsertBetalingsRegels(wachtid)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
      Function DeleteBetalingsregel(ByVal betaalid As Integer) As Boolean
        Return Adapter.Deletebetaalregel(betaalid)
    End Function

End Class
