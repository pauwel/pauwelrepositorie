
Option Compare Text
Imports DAL

Public Class Kosten
    Private _Adapter As prestatiesTableAdapters.tblKostenPlaatsTableAdapter
    Protected ReadOnly Property Adapter() As prestatiesTableAdapters.tblKostenPlaatsTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New prestatiesTableAdapters.tblKostenPlaatsTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetKosten() As prestaties.tblKostenPlaatsDataTable
        Return Me.Adapter.GetData()
    End Function
End Class
