
Option Compare Text
Imports dal

Public Class Feestdagen


    Private _adapter As prestatiesTableAdapters.tblFeestdagenTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As prestatiesTableAdapters.tblFeestdagenTableAdapter
        Get
            If _adapter Is Nothing Then
                _adapter = New prestatiesTableAdapters.tblFeestdagenTableAdapter
            End If

            Return _adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function getdata() As prestaties.tblFeestdagenDataTable
        Return adapter.getdata
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function InsertFeestdagen(ByVal feestdag As String, ByVal datum As DateTime) As Boolean


       

        If CheckFeestdag(datum.Date) Then
            MsgBox("Deze datum is reeds in gebruik")
        Else
            Return Adapter.Insert(feestdag, datum.Date)

        End If

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function UpdateFeestdagen(ByVal feestdag As String, ByVal datum As DateTime, ByVal id As Integer) As Boolean

        Return Adapter.Update(feestdag, datum.Date, id)
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function deleteFeestdagen(ByVal id As Integer) As Boolean

        Return Adapter.Delete(id)
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function CheckFeestdag(ByVal datum As DateTime) As Boolean

        Dim aantal As Integer = Adapter.CheckFeestdag(datum)
        If aantal = 0 Then
            Return False
        Else
            Return True
        End If
    End Function


End Class
