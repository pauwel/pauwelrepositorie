
Imports DAL

Public Class Voyagedetail

    Private _Adapter As SPtransportenTableAdapters.GetVoyageDetailsTableAdapter
    Protected ReadOnly Property Adapter() As SPtransportenTableAdapters.GetVoyageDetailsTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New SPtransportenTableAdapters.GetVoyageDetailsTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetVoyageDeatil(ByVal status As Int16) As SPtransporten.GetVoyageDetailsDataTable
        Return Me.Adapter.GetData(status)
    End Function
End Class
