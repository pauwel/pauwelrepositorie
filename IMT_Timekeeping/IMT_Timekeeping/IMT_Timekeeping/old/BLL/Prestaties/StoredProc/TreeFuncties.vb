
Option Compare Text
Imports DAL


Public Class TreeFuncties
    Private _Adapter As PLanningSPTableAdapters.GetAvailableFunctiesTableAdapter
    Protected ReadOnly Property Adapter() As PLanningSPTableAdapters.GetAvailableFunctiesTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New PLanningSPTableAdapters.GetAvailableFunctiesTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetTreeFuncties() As PLanningSP.GetAvailableFunctiesDataTable
        Return Me.Adapter.GetData()
    End Function
End Class
