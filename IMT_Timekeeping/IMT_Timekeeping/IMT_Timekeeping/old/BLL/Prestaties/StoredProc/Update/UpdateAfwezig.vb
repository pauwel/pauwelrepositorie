
Imports DAL


Public Class UpdateAfwezig

    Private _Adapter As UpdatePrestatiesTableAdapters.UpdateAfwezig = Nothing
    Protected ReadOnly Property Adapter() As UpdatePrestatiesTableAdapters.UpdateAfwezig
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New UpdatePrestatiesTableAdapters.UpdateAfwezig
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function SetAfwezigheid(ByVal afwezig As Boolean, ByVal reden As String, ByVal gevalideerd As Boolean, ByVal wordtuitbetaald As Boolean, ByVal id As Integer) As Boolean
        Return Me.Adapter.UpdatePrestaties_Afwezigheid(afwezig, reden, gevalideerd, wordtuitbetaald, id)
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function UpdateprestatieNaInput(ByVal prestatieid As Integer, ByVal machineid As Integer, ByVal wordtuitbetaald As Boolean, ByVal wachtid As Integer)
        Return Me.Adapter.UpdatePrestatieNaInput(prestatieid, machineid, wordtuitbetaald, wachtid)
    End Function

End Class



