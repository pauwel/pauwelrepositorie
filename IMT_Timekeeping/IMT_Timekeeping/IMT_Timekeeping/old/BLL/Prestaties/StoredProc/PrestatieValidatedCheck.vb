Option Compare Text
Imports DAL
Public Class PrestatieValidatedCheck

    Private _adapter As DAL.prestatiesTableAdapters.IsPrestatieValidatedTableAdapter
    Public ReadOnly Property Adapter() As DAL.prestatiesTableAdapters.IsPrestatieValidatedTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New DAL.prestatiesTableAdapters.IsPrestatieValidatedTableAdapter
            End If
            Return _adapter
        End Get
    End Property

    ''' <summary>
    ''' Nakijken of een bepaalde prestatie reeds gevalideerd is
    ''' </summary>
    ''' <param name="wachtID"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Nakijken in mapping tabel tussen prestaties en wachtbestand.  Als waarde daar bestaat is het gevalideerd</remarks>
    Public Function isValidated(ByVal wachtID As Integer) As Boolean
        Return Me.Adapter.Fill(wachtID)
    End Function

End Class
