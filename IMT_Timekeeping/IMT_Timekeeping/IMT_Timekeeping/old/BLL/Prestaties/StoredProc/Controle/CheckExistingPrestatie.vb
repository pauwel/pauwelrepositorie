

Imports dal

Public Class CheckExistingPrestatie


    Private _adapter As ControleTableAdapters.CheckExistingPrestatie
    Private ReadOnly Property adapter() As ControleTableAdapters.CheckExistingPrestatie
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New ControleTableAdapters.CheckExistingPrestatie
            End If
            Return Me._adapter
        End Get

    End Property


    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function CheckExisting(ByVal datum As DateTime, ByVal wnid As Int16, ByVal shiftid As Int16, ByVal machid As Int16) As Boolean
        adapter.CheckExistingPrestatie(datum, wnid, shiftid, machid)
    End Function

End Class
