
Option Compare Text
Imports DAL
Public Class FunctiesPerMachine
    Private _Adapter As PLanningSPTableAdapters.GetFunctiesPerMachineTableAdapter
    Protected ReadOnly Property Adapter() As PLanningSPTableAdapters.GetFunctiesPerMachineTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New PLanningSPTableAdapters.GetFunctiesPerMachineTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetFunctiesPerMachine() As PLanningSP.GetFunctiesPerMachineDataTable
        Return Me.Adapter.GetData()
    End Function
End Class
