
Imports DAL

Public Class GetPrestatieByDate

    Private _Adapter As PLanningSPTableAdapters.GetPrestatieByDateTableAdapter
    Protected ReadOnly Property Adapter() As PLanningSPTableAdapters.GetPrestatieByDateTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New PLanningSPTableAdapters.GetPrestatieByDateTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetPrestatieByDate(ByVal datum As DateTime, ByVal voyageid As Integer, ByVal shiftid As Integer) As PLanningSP.GetPrestatieByDateDataTable
        Return Me.Adapter.GetData(datum, voyageid, shiftid)
    End Function
End Class
