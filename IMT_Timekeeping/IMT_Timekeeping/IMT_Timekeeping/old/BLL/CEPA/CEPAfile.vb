
Imports System.IO
Imports System.Text

Public Class CEPAfile


    Private _Adapter As DAL.betalingenTableAdapters.GETCEPABetalingenTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL.betalingenTableAdapters.GETCEPABetalingenTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New DAL.betalingenTableAdapters.GETCEPABetalingenTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetData() As DAL.betalingen.GETCEPABetalingenDataTable
        Return Me.Adapter.GetData(datum)
    End Function


    Private _prestatieperiode As DateTime
    Public Property Presatieperiode() As DateTime
        Get
            Return _prestatieperiode
        End Get
        Set(ByVal value As DateTime)
            _prestatieperiode = value
        End Set
    End Property


    Private _datum As DateTime
    Public Property Datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)
            _datum = value.Date
        End Set
    End Property


    Private Function header() As String
        Dim head As New StringBuilder
        Dim d As DateTime = Datum 'DateTime.Now

        With head
            .Append("H")
            .Append("B")
            .Append("0371")
            .Append(datumwaarde(d))

            .Append("0622")
            .Append("0000")
            .Append(Space(1))
            .Append("IMT" & Space(37))
            .Append("Kipdorp" & Space(18))
            .Append("57")
            .Append(Space(3))
            .Append("2000")
            .Append("Antwerpen" & Space(16))
            .Append(Space(3))
            .Append("N")
        End With
        Return head.ToString

    End Function


    Private _volgnummer As Integer
    Public Property Volgnummer() As Integer
        Get
            Return _volgnummer
        End Get
        Set(ByVal value As Integer)
            _volgnummer = value
        End Set
    End Property



    Private _lonen As Decimal
    Public Property lonen() As Decimal
        Get
            Return _lonen
        End Get
        Set(ByVal value As Decimal)
            _lonen = value
        End Set
    End Property


    Private _premie As Decimal
    Public Property premie() As Decimal
        Get
            Return _premie
        End Get
        Set(ByVal value As Decimal)
            _premie = value
        End Set
    End Property


    Private _kledij As Decimal
    Public Property Kledij() As Decimal
        Get
            Return _kledij
        End Get
        Set(ByVal value As Decimal)
            _kledij = value
        End Set
    End Property


    Private _somLoonboek As Long
    Public Property somLoonboek() As Long
        Get
            Return _somLoonboek
        End Get
        Set(ByVal value As Long)
            _somLoonboek = value
        End Set
    End Property

    Private _borderelsomLoon As Decimal
    Public Property borderelsomloon() As Decimal
        Get
            Return _borderelsomLoon
        End Get
        Set(ByVal value As Decimal)
            _borderelsomLoon = value
        End Set
    End Property


    Private _borderelsompremie As Decimal
    Public Property borderelsompremie() As Decimal
        Get
            Return _borderelsompremie
        End Get
        Set(ByVal value As Decimal)
            _borderelsompremie = value
        End Set
    End Property

    Private _borderelsomkledij As Decimal
    Public Property borderelsomkledij() As Decimal
        Get
            Return _borderelsomkledij
        End Get
        Set(ByVal value As Decimal)
            _borderelsomkledij = value
        End Set
    End Property



    Function body() As DataTable
        Dim d As DateTime = Datum 'DateTime.Now

        Dim data As DataTable
        data = GetData()


        Dim content As New DataTable
        With content
            lonen = 0
            premie = 0
            Kledij = 0
            somLoonboek = 0
            Volgnummer = 0
            .Columns.Add("id")
            .Columns.Add("content")

            For Each r As DataRow In GetData.Rows 'data.Rows
                Dim dr As DataRow = content.NewRow
                'ID aanmaken
                Try
                    Dim b As Decimal = 0
                    If String.IsNullOrEmpty(r("bedrag").ToString) = False Then
                        b = r("bedrag")
                    End If
                    If b <> 0 Then

                        Dim id As New StringBuilder
                        With id
                            .Append("D")
                            Dim nr As String = Nothing
                            Volgnummer += 1
                            Select Case Volgnummer
                                Case Is < 10 : nr = "00000" & Volgnummer
                                Case Is < 100 : nr = "0000" & Volgnummer
                                Case Is < 1000 : nr = "000" & Volgnummer
                                Case Is < 10000 : nr = "00" & Volgnummer
                                Case Is < 100000 : nr = "0" & Volgnummer
                                Case Else : nr = Volgnummer

                            End Select
                            .Append(nr)
                            .Append("0622")
                            .Append("0000")
                            .Append("0") 'loonkostcode
                        End With

                        'content
                        Dim c As New StringBuilder
                        Dim lb As String = r("loonboek")
                        lb = lb.Replace("/", "")

                        lb = lb.Replace(" ", "")

                        Dim werkboek As String = r("loonboek")
                        werkboek = werkboek.Substring(0, werkboek.IndexOf("/"))
                        somLoonboek += Convert.ToInt64(werkboek)

                        Select Case Len(lb)
                            Case 4 : lb = "000000" + lb
                            Case 5 : lb = "00000" + lb
                            Case 6 : lb = "0000" + lb
                            Case 7 : lb = "000" + lb
                            Case 8 : lb = "00" + lb
                            Case 9 : lb = "0" & lb
                            Case 10 : lb = lb
                        End Select
                        With c
                            .Append(lb)
                            .Append(datumwaarde(r("datum")))
                            If String.IsNullOrEmpty(r("cepafunctiecode").ToString) Then
                                .Append(Space(1))
                            Else
                                .Append(r("cepafunctiecode"))
                            End If

                            .Append(shiften(r("shiftid")))

                            If r("bedragcomment") = "stdloon" Then
                                .Append(vergoedingscode(True, False))
                                .Append(Uren)
                                .Append("J+")
                                .Append(stdbedrag(r("bedrag")))
                                'som van lonen voor weergave in totaal rij
                                lonen += r("bedrag")

                                .Append("+")
                                .Append(supplement(0))
                                .Append("+")
                                .Append("00000000") 'Misen => wordt NIET gebruikt bij IMT = vergoeding voor averij,....

                                .Append(attest("000"))
                                .Append("+")
                                .Append(kledijvergoeding(0))

                            ElseIf r("bedragcomment") = "supplement" Then
                                .Append(vergoedingscode(False, False))
                                .Append(Uren)
                                .Append("N+")
                                .Append(stdbedrag(0))
                                .Append("+")
                                .Append(supplement(r("bedrag")))
                                'som van premie voor weergave in totaal rij
                                premie += r("bedrag")

                                .Append("+")
                                .Append("00000000") 'Misen => wordt NIET gebruikt bij IMT = vergoeding voor averij,....

                                .Append(attest(0))
                                .Append("+")
                                .Append(kledijvergoeding(0))


                            Else
                                .Append(vergoedingscode(False, True))
                                .Append(Uren)
                                .Append("N+")
                                .Append(stdbedrag(0))
                                .Append("+")
                                .Append(supplement(0))
                                .Append("+")
                                .Append("00000000") 'Misen => wordt NIET gebruikt bij IMT = vergoeding voor averij,....

                                .Append(attest(r("cepacode").ToString))
                                .Append("+")
                                If r("bedrag") Is DBNull.Value Then
                                    .Append(kledijvergoeding(0))
                                Else
                                    .Append(kledijvergoeding(r("bedrag")))
                                    Kledij += r("bedrag")
                                End If
                            End If



                            .Append(Space(33))
                        End With

                        Dim ident As String = id.ToString
                        Dim inhoud As String = c.ToString
                        dr("id") = ident
                        dr("content") = inhoud

                        content.Rows.Add(dr)
                    End If
                Catch ex As InvalidCastException

                End Try



            Next

        End With




        Return content
    End Function



    Function Totalen() As String
        Dim t As New StringBuilder

        With t
            .Append("T")
            Dim nr As String = Nothing

            Select Case Volgnummer
                Case Is < 10 : nr = "00000" & Volgnummer
                Case Is < 100 : nr = "0000" & Volgnummer
                Case Is < 1000 : nr = "000" & Volgnummer
                Case Is < 10000 : nr = "00" & Volgnummer
                Case Is < 100000 : nr = "0" & Volgnummer
                Case Else : nr = Volgnummer

            End Select
            .Append(nr)
            .Append("+")
            Dim l As String = Nothing
            borderelsomloon = lonen
            lonen = stdbedrag(lonen)
            Select Case lonen
                Case Is < 10 : l = "000000000000" & lonen
                Case Is < 100 : l = "00000000000" & lonen
                Case Is < 1000 : l = "0000000000" & lonen
                Case Is < 10000 : l = "000000000" & lonen
                Case Is < 100000 : l = "00000000" & lonen
                Case Is < 1000000 : l = "0000000" & lonen
                Case Is < 10000000 : l = "000000" & lonen
                Case Is < 100000000 : l = "00000" & lonen
                Case Is < 1000000000 : l = "0000" & lonen
                Case Is < 10000000000 : l = "000" & lonen
                Case Is < 100000000000 : l = "00" & lonen
                Case Is < 1000000000000 : l = "0" & lonen
                Case Else : l = lonen

            End Select
            .Append(l)
            .Append("+")
            Dim p As String = Nothing
            borderelsompremie = premie
            premie = supplement(premie)
            Select Case premie
                Case Is < 10 : p = "000000000000" & premie
                Case Is < 100 : p = "00000000000" & premie
                Case Is < 1000 : p = "0000000000" & premie
                Case Is < 10000 : p = "000000000" & premie
                Case Is < 100000 : p = "00000000" & premie
                Case Is < 1000000 : p = "0000000" & premie
                Case Is < 10000000 : p = "000000" & premie
                Case Is < 100000000 : p = "00000" & premie
                Case Is < 1000000000 : p = "0000" & premie
                Case Is < 10000000000 : p = "000" & premie
                Case Is < 100000000000 : p = "00" & premie
                Case Is < 1000000000000 : p = "0" & premie
                Case Else : p = premie

            End Select

            .Append(p)
            .Append("+")
            .Append("0000000000000") 'Misen => wordt NIET gebruikt bij IMT = vergoeding voor averij,....
            .Append("+")
            Dim k As String = Nothing
            borderelsomkledij=Kledij
            Kledij = kledijvergoeding(Kledij)
            Select Case Kledij
                Case Is < 10 : k = "000000000000" & Kledij
                Case Is < 100 : k = "00000000000" & Kledij
                Case Is < 1000 : k = "0000000000" & Kledij
                Case Is < 10000 : k = "000000000" & Kledij
                Case Is < 100000 : k = "00000000" & Kledij
                Case Is < 1000000 : k = "0000000" & Kledij
                Case Is < 10000000 : k = "000000" & Kledij
                Case Is < 100000000 : k = "00000" & Kledij
                Case Is < 1000000000 : k = "0000" & Kledij
                Case Is < 10000000000 : k = "000" & Kledij
                Case Is < 100000000000 : k = "00" & Kledij
                Case Is < 1000000000000 : k = "0" & Kledij
                Case Else : k = Kledij

            End Select
            .Append(k)
            .Append(Space(1))


            Dim somLB As String = Nothing
            Select Case somLoonboek
                Case Is < 10 : somLB = "0000000000" & somLoonboek.ToString
                Case Is < 100 : somLB = "000000000" & somLoonboek.ToString
                Case Is < 1000 : somLB = "00000000" & somLoonboek.ToString
                Case Is < 10000 : somLB = "0000000" & somLoonboek.ToString
                Case Is < 100000 : somLB = "000000" & somLoonboek.ToString
                Case Is < 1000000 : somLB = "00000" & somLoonboek.ToString
                Case Is < 10000000 : somLB = "0000" & somLoonboek.ToString
                Case Is < 100000000 : somLB = "000" & somLoonboek.ToString
                Case Is < 1000000000 : somLB = "00" & somLoonboek.ToString
                Case Is < 10000000000 : somLB = "0" & somLoonboek.ToString
                Case Else : somLB = somLoonboek.ToString
            End Select

            .Append(somLB)
            .Append("00000000")
            .Append(Space(45))

        End With

        Return t.ToString
    End Function




    Public Enum ploegen As Integer
        dag = 0
        vroege = 1
        late = 2
        nacht = 3
    End Enum

    Private Function datumwaarde(ByVal datum As DateTime) As String
        Dim d As String = Nothing
        If datum.Day < 10 Then
            d = "0" & datum.Day
        Else
            d = datum.Day
        End If

        If datum.Month < 10 Then
            d = d & "0" & datum.Month
        Else
            d = d & datum.Month
        End If

        d = d & datum.Year

        Return d
    End Function

    Private Function kledijvergoeding(ByVal kledij As Decimal) As String
        Dim k As String = Nothing
        k = kledij

        Dim dec As Decimal = 0.01
        'controle op het decimaal character
        Dim dm As String = dec
        If dm.IndexOf(",") = 0 Then
            'punt als decimaalteken
            k = k.Replace(".", "")
        Else
            'komma als decimaal teken
            k = k.Replace(",", "")
        End If

      

        Select Case Len(k)
            Case 0 : k = "00000000"
            Case 1 : k = "0000000" & k.ToString
            Case 2 : k = "000000" & k.ToString
            Case 3 : k = "00000" & k.ToString
            Case 4 : k = "0000" & k.ToString
            Case 5 : k = "000" & k.ToString
            Case 6 : k = "00" & k.ToString
            Case 7 : k = "0" & k.ToString
            Case 8 : k = k
        End Select

        Return k
    End Function

    Private Function attest(ByVal cepacode As String) As String
        Dim a As String = Nothing
        Select Case Len(cepacode)
            Case 0 : a = "000"
            Case 1 : a = "00" + cepacode
            Case 2 : a = "0" + cepacode
            Case 3 : a = cepacode
            Case Else : a = Mid(cepacode, Len(cepacode) - 2)
        End Select
        Return a
    End Function

    Private Function stdbedrag(ByVal b As Decimal) As String
        Dim l As String = b

        Dim dec As Decimal = 0.01
        'controle op het decimaal character
        Dim dm As String = dec
        If dm.IndexOf(",") = 0 Then
            'punt als decimaalteken
            l = l.Replace(".", "")
        Else
            'komma als decimaal teken
            l = l.Replace(",", "")
        End If

        Select Case Len(l)
            Case 0 : l = "000000000"
            Case 1 : l = "00000000" + l
            Case 2 : l = "0000000" + l
            Case 3 : l = "000000" + l
            Case 4 : l = "00000" + l
            Case 5 : l = "0000" + l
            Case 6 : l = "000" + l
            Case 7 : l = "00" + l
            Case 8 : l = "0" + l
            Case Else : l = l
        End Select
        Return l
    End Function

    Private Function supplement(ByVal b As Decimal) As String
        Dim l As String = b

        Dim dec As Decimal = 0.01
        'controle op het decimaal character
        Dim dm As String = dec
        If dm.IndexOf(",") = 0 Then
            'punt als decimaalteken
            l = l.Replace(".", "")
        Else
            'komma als decimaal teken
            l = l.Replace(",", "")
        End If
        Select Case Len(l)
            Case 0 : l = "00000000"
            Case 1 : l = "0000000" + l
            Case 2 : l = "000000" + l
            Case 3 : l = "00000" + l
            Case 4 : l = "0000" + l
            Case 5 : l = "000" + l
            Case 6 : l = "00" + l
            Case 7 : l = "0" + l
            Case 8 : l = l
            Case Else : l = l
        End Select
        Return l
    End Function

    Private Function vergoedingscode(ByVal stdloon As Boolean, ByVal kledijvergoeding As Boolean) As String
        If stdloon Then
            Return "1000"
        ElseIf kledijvergoeding Then
            Return "5120"
        Else
            Return "2000"
        End If

    End Function

    Private Function Uren() As String
        Return "0000"

    End Function
    Private Function shiften(ByVal shiftid As Integer) As String

        Dim s As String = Nothing
        Select Case shiftid
            Case 36 : s = "0" & ploegen.dag
            Case 43 : s = ploegen.vroege & Space(1)
            Case 38 : s = ploegen.late & Space(1)
            Case 39 : s = ploegen.nacht & Space(1)

        End Select
        Return s
    End Function


    Private _path As String
    Public Property path() As String
        Get
            Return _path
        End Get
        Set(ByVal value As String)
            _path = value
        End Set
    End Property



    Sub schrijver()

        If path <> "" Or path Is Nothing = False Then
            Dim memstr As New MemoryStream

            Dim wr As New StreamWriter(memstr)
            wr.WriteLine(header)
            body()
            For Each r As DataRow In body.Rows
                wr.WriteLine(r("id") & Space(9) & r("content"))
            Next

            wr.WriteLine(Totalen)


            wr.Flush()
            Dim f As FileStream = File.Create(path)
            memstr.WriteTo(f)

            wr.Close()
            f.Close()
            memstr.Close()



        End If





    End Sub


    Sub borderel()
        If path <> "" Or path Is Nothing = False Then
            Dim memstr As New MemoryStream

            Dim wr As New StreamWriter(memstr)
            wr.WriteLine(hoofding)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(36) & "Borderel rechtstreeks looninput" & Space(15) & "NR: 00374" & Space(3) & "*")
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Firma / dept" & Space(50) & "Kontaktpersoon" & Space(14) & "*")
            wr.WriteLine("*" & Space(4) & "0622 /  00  " & Space(50) & "Van De Sande G" & Space(14) & "*")
            wr.WriteLine("*" & Space(66) & "Franssens H" & Space(17) & "*")
            wr.WriteLine("*" & Space(66) & "Tel: 03/5401900" & Space(13) & "*")
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(hoofding)



            wr.WriteLine("*" & Space(4) & "Datum opmaak info-drager:" & Space(20) & bordereldatum(Datum))
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Soort informatiedrager  :" & Space(20) & "Transmissie")
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Aantal detaillijnen     :" & Space(20) & Volgnummer)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Totaal werkboeknummers  :" & Space(20) & somLoonboek)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Totaal bedrag lonen     :" & Space(20) & borderelsomloon)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Totaal bedrag premie    :" & Space(20) & borderelsompremie)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Totaal bedrag misen     :" & Space(20) & "0")
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Totaal bedrag kledij    :" & Space(20) & borderelsomkledij)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(4) & "Prestatieperiode        :" & Space(20) & bordereldatum(prestatiedatum(Presatieperiode)))
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(hoofding)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)

            wr.WriteLine("*" & Space(30) & "*" & Space(5) & "aangifte firma" & Space(11) & "*" & Space(5) & "ontvangst loondienst" & Space(7) & "*")
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(30) & "*" & Space(5) & "IMT           " & Space(11) & "*" & Space(5) & "CEPA                " & Space(7) & "*")
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(30) & "*****************************************************************")
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(5) & "Datum" & Space(20) & "*" & Space(30) & "*" & Space(32) & "*")
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(5) & "Handtekening" & Space(13) & "*" & Space(30) & "*" & Space(32) & "*")
            wr.WriteLine(legelijn)
            wr.WriteLine("*" & Space(5) & "Firmastempel" & Space(13) & "*" & Space(30) & "*" & Space(32) & "*")
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)
            wr.WriteLine(legelijn)

            wr.WriteLine(hoofding)
            wr.Flush()
            Dim f As FileStream = File.Create(path)
            memstr.WriteTo(f)

            wr.Close()
            f.Close()
            memstr.Close()



        End If
    End Sub

    Private Function bordereldatum(ByVal datum As Date) As String
        Dim dag As String = Nothing, maand As String = Nothing
        If datum.Day < 10 Then
            dag = "0" & datum.Day
        Else
            dag = datum.Day
        End If
        If maand < 10 Then
            maand = "0" & datum.Month
        Else
            maand = datum.Month
        End If
        Return dag & "/" & maand & "/" & datum.Year
    End Function

    Private Function hoofding() As String
        Dim h As String = Nothing
        For i As Integer = 1 To 96
            h = h & "*"
        Next
        Return h
    End Function

    Private Function legelijn() As String
        Dim ll As String = Nothing
        ll = "*" & Space(94) & "*"
        Return ll
    End Function
    Private Function prestatiedatum(ByVal d As Date) As Date

        Dim i As Integer = 0
        'Select Case d.DayOfWeek
        '    Case DayOfWeek.Friday : i = -13
        '    Case DayOfWeek.Thursday : i = -12
        '    Case DayOfWeek.Wednesday : i = -11
        '    Case DayOfWeek.Tuesday : i = -10
        '    Case DayOfWeek.Monday : i = -9
        '    Case DayOfWeek.Sunday : i = -8
        '    Case DayOfWeek.Saturday : i = -7

        'End Select

        prestatiedatum = DateAdd(DateInterval.Day, i, d)
        Return prestatiedatum
    End Function
End Class
