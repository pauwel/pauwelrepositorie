
Imports DAL

Public Class GetMinLoon
    Private _Adapter As SpaarplannersTableAdapters.GetMinLoonSpaarplannerTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SpaarplannersTableAdapters.GetMinLoonSpaarplannerTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New SpaarplannersTableAdapters.GetMinLoonSpaarplannerTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetMinLoon(ByVal catid As Integer, ByVal shiftid As Integer) As String
        Try
            Dim r As DataRow = Adapter.GetData(catid, shiftid).Rows(0)
            Return r("basis")
        Catch ex As Exception
            Return Nothing
        End Try


    End Function
End Class
