
Imports DAL

Public Class GetCategory
    Private _Adapter As SpaarplannersTableAdapters.GetCategoryTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SpaarplannersTableAdapters.GetCategoryTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New SpaarplannersTableAdapters.GetCategoryTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetCategory(ByVal werknemersid As Int16) As String
        Try
            Dim r As DataRow = Adapter.GetData(werknemersid).Rows(0)
            Return r("catid")
        Catch ex As Exception
            Return Nothing
        End Try


    End Function
End Class
