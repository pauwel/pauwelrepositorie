
Imports DAL

Public Class checkBetalingSpaarplanners
    Private _Adapter As SpaarplannersTableAdapters.CheckBetalingSpaarplannersTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SpaarplannersTableAdapters.CheckBetalingSpaarplannersTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New  SpaarplannersTableAdapters.CheckBetalingSpaarplannersTableAdapter
            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function OKBetalingSpaarplanner(ByVal datum As DateTime, ByVal werknemersid As Int16) As Boolean


        Try
            Dim r As DataRow = Adapter.GetData(datum, werknemersid).Rows(0)
            If r("status") = "wachtbestand" Then
                Return True
            Else : Return False
            End If
        Catch ex As Exception
            Return False
        End Try


    End Function
End Class
