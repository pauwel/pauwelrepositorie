Imports DAL

Public Class BLLWerknemersSP
    Private _Adapter As DAL._SPWerknemersTableAdapters.Werknemers_GetByTypeTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL._SPWerknemersTableAdapters.Werknemers_GetByTypeTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL._SPWerknemersTableAdapters.Werknemers_GetByTypeTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetWerknemers(ByVal WNID As Integer) As DAL._SPWerknemers.Werknemers_GetByTypeDataTable
        Return Adapter.GetData(WNID)
    End Function
End Class




