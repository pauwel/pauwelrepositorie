
Imports DAL

Public Class BLLLoonkostSP
    Private _Adapter As DAL._SPWerknemersTableAdapters.LoonkostGetDataTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL._SPWerknemersTableAdapters.LoonkostGetDataTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL._SPWerknemersTableAdapters.LoonkostGetDataTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetLoonkost() As DAL._SPWerknemers.LoonkostGetDataDataTable
        Return Adapter.GetData()
    End Function
End Class
