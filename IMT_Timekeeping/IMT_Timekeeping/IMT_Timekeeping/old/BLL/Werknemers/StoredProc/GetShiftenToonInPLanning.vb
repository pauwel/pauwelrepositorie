
Imports DAL

Public Class GetShiftenToonInPLanning

    Private _Adapter As DAL._SPWerknemersTableAdapters.GetDataByToonInPLanningTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL._SPWerknemersTableAdapters.GetDataByToonInPLanningTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL._SPWerknemersTableAdapters.GetDataByToonInPLanningTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetShiften() As DAL._SPWerknemers.GetDataByToonInPLanningDataTable
        Return Adapter.GetData()
    End Function
End Class
