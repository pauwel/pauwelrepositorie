

Option Compare Text
Imports DAL

Public Class Shiften
    Private _Adapter As WerknemersTableAdapters.tblShiftenTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As WerknemersTableAdapters.tblShiftenTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New WerknemersTableAdapters.tblShiftenTableAdapter
            End If

            Return _Adapter
        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetShift() As DAL.Werknemers.tblShiftenDataTable
        Try
            Return Adapter.GetData
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function UpdateShift(ByVal omschrijving As String, ByVal vergoeding As Decimal, ByVal sort As Integer, ByVal tooninplanning As Boolean, ByVal verwijderen As Boolean, ByVal id As Int32) As Boolean
        Try

            Adapter.Update(omschrijving, vergoeding, sort, tooninplanning, verwijderen, id)
            Return True


        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Insert, True)> _
            Public Function InsertShift(ByVal omschrijving As String, ByVal vergoeding As Decimal, ByVal sort As Integer, ByVal tooninplanning As Boolean, ByVal verwijderen As Boolean) As Boolean
        Try
            If isUnique(omschrijving) Then
                Adapter.Insert(omschrijving, vergoeding, sort, tooninplanning, verwijderen)
                Return True
            Else
                MsgBox("Deze shift bestaat reeds")
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteShift(ByVal id As Integer) As Boolean
        Try
            Return Adapter.Delete(id)


        Catch sqlex As SqlClient.SqlException
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", sqlex)
        Catch ex As Exception
            Return False
        End Try

    End Function


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetVergoedingByID(ByVal id As Integer) As Decimal
        Return Adapter.GetVergoedingByID(id)
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function isUnique(ByVal shift As String) As Decimal
        If Adapter.Is_unique(shift) = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetShiftnaam(ByVal shiftid As Integer) As String
        Return Adapter.GETSHIFTNAAM(shiftid)
    End Function
End Class
