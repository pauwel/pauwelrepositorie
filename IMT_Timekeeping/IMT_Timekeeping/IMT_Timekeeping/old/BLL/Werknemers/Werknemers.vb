
Option Compare Text

Imports DAL
Imports System.Data.SqlTypes
Public Class Werknemers

    Private _Adapter As WerknemersTableAdapters.tblWerknemersTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As WerknemersTableAdapters.tblWerknemersTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New WerknemersTableAdapters.tblWerknemersTableAdapter
            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetWerknemers() As DAL.Werknemers.tblWerknemersDataTable
        Return Adapter.GetData
    End Function

   

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Insert, True)> _
           Public Function InsertWerknemers(ByVal loonboek As String, ByVal naam As String, ByVal voornaam As String, ByVal geboorte As DateTime, ByVal adres As String, ByVal postcode As String, _
    ByVal gemeente As String, ByVal land As Int32, ByVal tel As String, ByVal mobiel As String, ByVal mail As String, ByVal datumvast As DateTime, ByVal spaarplan As Boolean, ByVal datumspaarplan As DateTime, _
    ByVal catid As Int32, ByVal typewn As Int32) As Boolean



        Adapter.Insert(loonboek, naam, voornaam, geboorte.Date, adres, postcode, _
            gemeente, land, tel, mobiel, mail, datumvast.Date, spaarplan, datumspaarplan.Date, catid, typewn)
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function UpdateWerknemers(ByVal loonboek As String, ByVal naam As String, ByVal voornaam As String, ByVal geboortedatum As DateTime, ByVal adres As String, ByVal postcode As String, _
    ByVal gemeente As String, ByVal land As Int32, ByVal tel As String, ByVal mobiel As String, ByVal mail As String, ByVal datumvast As DateTime, ByVal spaarplan As Boolean, ByVal datumspaarplan As DateTime, _
    ByVal catid As Int32, ByVal typewn As Int32, ByVal id As Integer) As Boolean

        Adapter.Update(loonboek, naam, voornaam, geboortedatum.Date, adres, postcode, _
            gemeente, land, tel, mobiel, mail, datumvast.Date, spaarplan, datumspaarplan.Date, catid, typewn, id)

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function deleteWerknemers(ByVal id As Integer) As Boolean
        Try
            Adapter.Delete(id)

        Catch ex As SqlClient.SqlException
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try


    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function LoonboekIsUnique(ByVal loonboek As String) As Boolean

        Dim ok As Nullable(Of Integer) = Adapter.LoonboekIsUnique(loonboek)
        If ok.HasValue Then
            If ok.Value > 0 Then
                Return False
            Else
                Return True
            End If

        Else
            Return True
        End If
    End Function


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function GetLeeftijd(ByVal werknemersid As Integer, ByVal prestatiedatum As DateTime) As Integer

        If IsDate(Adapter.GetGeboortedatum(werknemersid)) Then
            Dim leeftijd As Integer
            leeftijd = DateDiff(DateInterval.Year, Adapter.GetGeboortedatum(werknemersid), prestatiedatum)

            If leeftijd > 80 Then
                Return 0
            Else
                Return leeftijd
            End If
        End If
    End Function


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function CheckSpaarplan(ByVal werknemersid As Integer) As Boolean

        Dim ok As Boolean = False
        Try
            ok = Adapter.CheckSpaarplan(werknemersid)
        Catch ex As Exception
            ok = False
        End Try
        Return ok
        
    End Function


    
End Class
