Option Compare Text
Imports DAL

Public Class TypeWerknemers
    Private _Adapter As WerknemersTableAdapters.tblTypeWerknemerTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As WerknemersTableAdapters.tblTypeWerknemerTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New WerknemersTableAdapters.tblTypeWerknemerTableAdapter
            End If

            Return _Adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetWerknemersType() As DAL.Werknemers.tblTypeWerknemerDataTable
        Return Adapter.GetData
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function UpdateWerknemersType(ByVal stelsel As String, ByVal sort As Integer, ByVal id As Int32, ByVal vastindienst As Boolean) As Boolean
        Try

            Adapter.Update(stelsel, vastindienst, sort, id)
            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Insert, True)> _
            Public Function InsertWerknemersType(ByVal stelsel As String, ByVal sort As Integer, ByVal vastindienst As Boolean) As Boolean
        Try

            If isUnique(stelsel) Then
                Adapter.Insert(stelsel, vastindienst, sort)
                Return True
            Else
                MsgBox("Dit type bestaat reeds")
                Return False

            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteWerknemersType(ByVal id As Integer) As Boolean
        Try
            Return Adapter.Delete(id)

        Catch sqlex As SqlClient.SqlException
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", sqlex)
        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function IsVast(ByVal id As Integer) As Boolean
        Try
            Return Adapter.isVast(id)
        Catch ex As Exception
            Return False
        End Try

    End Function
    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
           Public Function isUnique(ByVal stelsel As String) As Boolean

        If Adapter.Is_Unique(stelsel) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
