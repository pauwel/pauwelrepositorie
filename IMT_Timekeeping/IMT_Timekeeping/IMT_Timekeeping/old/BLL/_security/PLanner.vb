

Imports DAL
Public Class PLanner

    Private _adapter As SecurityTableAdapters.Security_PlannerTableAdapter = Nothing

    Protected ReadOnly Property Adapter() As SecurityTableAdapters.Security_PlannerTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SecurityTableAdapters.Security_PlannerTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function getPlannerControls() As Security.Security_PlannerDataTable
        Return Adapter.GetData()
    End Function
End Class
