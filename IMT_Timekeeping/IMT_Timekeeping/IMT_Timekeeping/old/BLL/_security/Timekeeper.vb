

Imports DAL
Public Class Timekeeper


    Private _adapter As SecurityTableAdapters.Security_TimekeeperTableAdapter = Nothing

    Protected ReadOnly Property Adapter() As SecurityTableAdapters.Security_TimekeeperTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SecurityTableAdapters.Security_TimekeeperTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function getTimekeeperControls() As Security.Security_TimekeeperDataTable
        Return Adapter.GetData()
    End Function
End Class
