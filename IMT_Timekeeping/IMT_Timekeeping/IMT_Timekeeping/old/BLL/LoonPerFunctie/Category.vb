
Option Compare Text

Imports DAL

Public Class Category
    Private _Adapter As LoonPerFunctieTableAdapters.tblCategoriesTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As LoonPerFunctieTableAdapters.tblCategoriesTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New LoonPerFunctieTableAdapters.tblCategoriesTableAdapter
            End If

            Return _Adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetCategories() As LoonPerFunctie.tblCategoriesDataTable
        Return Adapter.GetData
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function UpdateCategory(ByVal code As String, ByVal omschrijving As String, ByVal CEPACode As String, ByVal cepafunctiecode As String, ByVal id As Int32) As Boolean
        Try


            Adapter.Update(code, omschrijving, CEPACode, cepafunctiecode, id)
            Return True



        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Insert, True)> _
            Public Function InsertCategory(ByVal code As String, ByVal omschrijving As String, ByVal CEPACode As String, ByVal cepafunctiecode As String) As Boolean
        Try
            If isUnique(code) Then
                Adapter.Insert(code, omschrijving, CEPACode, cepafunctiecode)
                Return True
            Else
                MsgBox("Deze category bestaat reeds")
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteCategory(ByVal id As Integer) As Boolean
        Try
            Return Adapter.Delete(id)

        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try

    End Function
    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function isUnique(ByVal code As String) As Boolean

        Dim x As Integer = Adapter.is_Unique(code)

        If x = 0 Then
            Return True
        Else
            Return (False)
        End If

    End Function
End Class
