
Imports DAL

Public Class GetLoonPerFunctieDetail


    Private _Adapter As vwLoonPFTableAdapters.GetLoonPerFunctieDetailsTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As vwLoonPFTableAdapters.GetLoonPerFunctieDetailsTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New vwLoonPFTableAdapters.GetLoonPerFunctieDetailsTableAdapter
            End If

            Return _Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetLoonPerFunctie(ByVal Shiftid As Integer) As vwLoonPF.GetLoonPerFunctieDetailsDataTable
        Return Adapter.GetData(Shiftid)
    End Function
End Class

