Public Class LoonkostByWerknemer
    Private _Adapter As DAL._SPWerknemersTableAdapters.GetLoonkostByWerknemerTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL._SPWerknemersTableAdapters.GetLoonkostByWerknemerTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL._SPWerknemersTableAdapters.GetLoonkostByWerknemerTableAdapter

            End If
            Return _Adapter

        End Get
    End Property

    ''' <summary>
    ''' 
    ''' Loonkost op basis van werknemer ophalen 
    ''' </summary>
    ''' <param name="WerknemerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetLoonkost(ByVal WerknemerID) As DAL._SPWerknemers.GetLoonkostByWerknemerDataTable
        Return Adapter.GetData(WerknemerID)
    End Function
End Class
