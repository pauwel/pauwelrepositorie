
Imports DAL


Public Class UpdateLoonperfunctie


    Private _adapter As LoonPerFunctieTableAdapters.SPloonperfunctie = Nothing
    Public ReadOnly Property adapter() As LoonPerFunctieTableAdapters.SPloonperfunctie
        Get
            If _adapter Is Nothing Then
                _adapter = New LoonPerFunctieTableAdapters.SPloonperfunctie
            End If
            Return _adapter
        End Get

    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Function UpdateLPF(ByVal stdloon As Decimal, ByVal OU As Decimal, ByVal Supplement As Decimal, ByVal eindejaar As Decimal, _
    ByVal datum As DateTime, ByVal supplementdubbeleshift As Decimal, ByVal shiftid As Integer, ByVal catid As Integer, ByVal functieid As Integer)
        Return adapter.UpdateLoonPerFunctie(stdloon, OU, Supplement, eindejaar, _
         supplementdubbeleshift, datum, shiftid, catid, functieid)

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
        Function CheckMachineFunctie(ByVal functieid As Integer) As Integer
        Return adapter.CheckMachineFunctie(functieid)

    End Function



End Class
