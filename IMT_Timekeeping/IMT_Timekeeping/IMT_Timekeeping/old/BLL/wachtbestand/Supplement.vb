Public Class Supplement
    Dim LPF As BLL.LoonPerFuncties = New BLL.LoonPerFuncties
    ''' <summary>
    ''' Constructor - nieuw Supplement object aanmaken 
    ''' </summary>
    ''' <param name="wr">De wachtregel waarop het supplement moet geboekt worden</param>
    ''' <remarks></remarks>
    Sub New(ByVal wr As Wachtregel)
        Me.Wachtregel = wr

    End Sub
    ''' <summary>
    ''' Het bedrag berekenen.  
    ''' Dit wordt berekent op basis van het aantal uren en het standaardloon dat vasthangt aan de LoonID van de wachtregel. 
    ''' </summary>
    ''' <param name="uren">Het aantal uren waarop het bedrag moet berekent worden </param>
    ''' <returns>Het berekende bedrag</returns>
    ''' <remarks></remarks>
    Public Function berekenBedrag(ByVal uren As Decimal) As Decimal

        If UrenInvoer Then
            Dim loongegevens As DataRow = LPF.GetByID(Me.Wachtregel.loonID)(0)
            Dim stdLoon = loongegevens("stdLoon")
            Dim basis As Decimal = Math.Round((stdLoon / 7.25), 2, MidpointRounding.AwayFromZero)
            basis = Math.Round(basis * 1.5, 2, MidpointRounding.AwayFromZero)
            Return Math.Round(basis * Me.Uren, 2, MidpointRounding.AwayFromZero)
        End If
        Return False
    End Function
    ''' <summary>
    ''' Controle van de verplichte properties om definitief te mogen worden opgeslagen 
    ''' </summary>
    ''' <returns>True/False afhankelijk of validatie geslaagd is</returns>
    ''' <remarks></remarks>
    Public Function validate() As Boolean
        If Me.CepaVerplicht And String.IsNullOrEmpty(Trim(Me.CepaCode)) Then
            Return False
        End If
        If Me.Bedrag = 0 Then
            Return False
        End If
        If Me.Loontype < 30 And String.IsNullOrEmpty(Me.Comment) Then
            Return False
        End If
        Return True
    End Function
    ''' <summary>
    ''' Toevoegen van het supplement aan wachtregel 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub save()
        If Me.validate Then
            Me.Wachtregel.addDetail(Me.Bedrag, Timekeeping.Register.LoonDetailReden.User, Me.Comment, Me.Loontype, Me.CepaCode, Me.Uren)
        Else
            Throw New Exception("Niet alle vereiste velden werden correct ingevuld")
        End If
    End Sub

    ''' <summary>
    ''' Het loontype of soort supplement
    ''' </summary>
    ''' <remarks></remarks>
    Private _loontype As Integer
    Public Property Loontype() As Integer
        Get
            Return _loontype
        End Get
        Set(ByVal value As Integer)
            _loontype = value
            Dim LT As New BLL.LoonType

            'controle of CEPA code verplicht is 
            Me._CepaVerplicht = LT.CheckCepaVerplicht(Me.Loontype) 

            'controle of het supplement in uren moet ingegeven worden 
            Me._urenInvoer = LT.CheckUrenInvoer(Me.Loontype)

        End Set
    End Property

    ''' <summary>
    ''' Extra commentaar
    ''' </summary>
    ''' <remarks></remarks>
    Private _comment As String
    Public Property Comment() As String
        Get
            Return _comment
        End Get
        Set(ByVal value As String)
            _comment = value
        End Set
    End Property

    ''' <summary>
    ''' Aantal uren van het supplement 
    ''' Moet enkel een waarde hebben voor bepaalde loontype's 
    ''' </summary>
    ''' <remarks></remarks>
    Private _uren As Decimal
    Public Property Uren() As Decimal
        Get
            Return _uren
        End Get
        Set(ByVal value As Decimal)
            _uren = value
            Me.Bedrag = Me.berekenBedrag(Me.Uren)
        End Set
    End Property

    ''' <summary>
    ''' Het bedrag van het supplement
    ''' </summary>
    ''' <remarks></remarks>
    Private _bedrag As Decimal
    Public Property Bedrag() As Decimal
        Get
            Return _bedrag
        End Get
        Set(ByVal value As Decimal)
            _bedrag = value
        End Set
    End Property

    ''' <summary>
    ''' De CEPA code die vermeld moet worden bij het supplement 
    ''' Deze is niet voor alle loontypes verplicht.  
    ''' </summary>
    ''' <remarks></remarks>
    Private _cepaCode As String
    Public Property CepaCode() As String
        Get
            Return _cepaCode
        End Get
        Set(ByVal value As String)
            _cepaCode = value
        End Set
    End Property



    ''' <summary>
    ''' Bepaald of de CEPA code verplicht is voor dit soort supplement
    ''' </summary>
    ''' <remarks></remarks>
    Private _CepaVerplicht As Boolean
    Public Property CepaVerplicht() As Boolean
        Get
            Return _CepaVerplicht
        End Get
        Set(ByVal value As Boolean)
            _CepaVerplicht = value
        End Set
    End Property

    ''' <summary>
    ''' Bepaald of voor dit soort de waarde al dan niet in uren moet ingevuld
    ''' </summary>
    ''' <remarks></remarks>
    Private _urenInvoer As Boolean
    Public Property UrenInvoer() As Boolean
        Get
            Return _urenInvoer
        End Get
        Set(ByVal value As Boolean)
            _urenInvoer = value
        End Set
    End Property


    ''' <summary>
    ''' Het wachtregel object waaraan het supplement zal worden toegevoegd 
    ''' </summary>
    ''' <remarks></remarks>
    Private _wachtregel As BLL.Wachtregel
    Public Property Wachtregel() As BLL.Wachtregel
        Get
            Return _wachtregel
        End Get
        Set(ByVal value As BLL.Wachtregel)
            _wachtregel = value
        End Set
    End Property

End Class
