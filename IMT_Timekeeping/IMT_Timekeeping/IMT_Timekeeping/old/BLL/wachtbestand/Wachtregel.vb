Option Explicit On
Imports DAL
Imports System.Data.SqlTypes.SqlDateTime

''' <summary>
''' Deze klasse bevat de gegevens van een bepaalde wachtregel, en een aantal methoden om details toe te voegen of uit te lezen 
''' </summary>
''' <remarks></remarks>
Public Class Wachtregel



    ''' <summary>
    ''' ID van het object
    ''' </summary>
    ''' <remarks></remarks>
    Private _wachtID As Nullable(Of Integer) = Nothing
    Public Property WachtID() As Nullable(Of Integer)
        Get
            Return _wachtID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _wachtID = value
        End Set
    End Property


    ''' <summary>
    ''' Het resterende saldo van deze wachtregel / wachtbriefje
    ''' Wordt gevoed door de som van alle detaillijnen voor dit record 
    ''' </summary>
    ''' <remarks></remarks>
    Private _saldo As Decimal
    Public ReadOnly Property Saldo() As Decimal
        Get
            Dim saldoCalc As BLL.Saldo = New BLL.Saldo
            Me._saldo = saldoCalc.GetSaldo(Me.WachtID)

            Return _saldo
        End Get
    End Property

    ''' <summary>
    ''' Een nieuw record aanmaken 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        'nieuwe aanmaken
        'Me.DatumUit = nothing
    End Sub

    ''' <summary>
    ''' Een bestaande regel uit de database halen
    ''' </summary>
    ''' <param name="wachtID">De wachtID die wordt opgehaald</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal wachtID As Integer)
        Me.loadData(wachtID)
    End Sub
    ''' <summary>
    ''' Gegevens voor huidige me.wachtID inladen 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub loadData()
        Dim da As BLL.GetWachtRegelById = New BLL.GetWachtRegelById
        Dim r As DataRow = da.Getdata(Me.WachtID.Value).Rows(0)
        Me._validated = r("validated")
        Me.loonID = r("LoonID")
        Me.ShiftID = r("ShiftID")
        Me.Datum = r("Datum")
        Me.FunctieID = r("FunctieID")
        Me.VoyageID = r("voyageID")
        Me.Is_feestdag = r("is_feestdag")
        Me.Is_weekend = r("is_weekend")
        Me.WachtTypeID = r("WachtTypeId")
        If r("datumIn") Is DBNull.Value Then
            Me.DatumIn = Nothing
        Else
            Me.DatumIn = r("datumIn")
        End If
        If r("datumUit") Is DBNull.Value Then
            Me.DatumUit = Nothing
        Else
            Me.DatumUit = r("datumUit")
        End If
        Me.WordtUitbetaald = r("wordtUitbetaald")
        Me.loonID = r("LoonID")
        Me.GewerkteTijd = r("GewerkteTijd")
        Me.gewerkteShift = r("gewerkteShiftID")
        Me.Loonboek = r("loonboek")
        Me.Wbbrief = r("WBBrief")

        If r("is_afwezig") Is DBNull.Value Then
            Me.isAfwezig = False
        Else
            Me.isAfwezig = r("is_afwezig")
        End If

        If r("RedenAfwezigheid") Is DBNull.Value Then
            Me.redenAfwezigheid = ""
        Else
            Me.redenAfwezigheid = r("RedenAfwezigheid")
        End If


        'werknemer info 
        Me.WerknemerID = r("WerknemerID")
        Me.Voornaam = r("Voornaam")
        Me.Naam = r("Naam")

    End Sub
    ''' <summary>
    ''' Gegevens ophalen adhv wachtID.  
    ''' </summary>
    ''' <param name="wachtID">ID van het wachtbestand</param>
    ''' <remarks></remarks>
    ''' 

    Protected Sub loadData(ByVal wachtID As Integer)
        Me.WachtID = wachtID
        Me.loadData()
    End Sub

    ''' <summary>
    ''' Detaillijn aan het wachtbestand record (=briefje) toevoegen 
    ''' </summary>
    ''' <param name="bedrag"></param>
    ''' <param name="reden"></param>
    ''' <param name="loontype"></param>
    ''' <param name="RelatedWachtID">De wachtID waaraan dit record gerelateerd is</param>
    ''' <remarks></remarks>
    Public Sub addDetail(ByVal bedrag As Double, ByVal reden As Integer, ByVal commentaar As String, ByVal loontype As Integer, ByVal cepacode As String, ByVal uren As Decimal, ByVal RelatedWachtID As Integer)
        Dim nieuwsaldo As Decimal = Me.Saldo + bedrag
        If nieuwsaldo < 0 Then
            Throw New Exception("Saldo ontoereikend")
        Else
            Dim loondetail As BLL.Loondetail = New BLL.Loondetail
            loondetail.insertLoonDetail(Me.WachtID, bedrag, reden, DateTime.Now.Date, commentaar, loontype, cepacode, uren, RelatedWachtID)

        End If

    End Sub

    ''' <summary>
    ''' Detaillijn aan het wachtbestand record (=briefje) toevoegen 
    ''' </summary>
    ''' <param name="bedrag"></param>
    ''' <param name="reden"></param>
    ''' <param name="loontype"></param>
    ''' <remarks></remarks>
    Public Sub addDetail(ByVal bedrag As Double, ByVal reden As Integer, ByVal commentaar As String, ByVal loontype As Integer, ByVal cepacode As String, ByVal uren As Decimal)
        Me.addDetail(bedrag, reden, commentaar, loontype, cepacode, uren, Nothing)
    End Sub

    ''' <summary>
    ''' De detailregels van het huidige object in een datatable
    ''' </summary>
    Private _detailregels As DAL.wachtbestand.GetLoondetailsByWachtidDataTable

    ''' <summary>
    ''' De detailregels van het huidige object opvragen
    ''' </summary>
    ''' <returns>Datatable met detailregels of Nothing indien het een nieuwe regel is die nog niet werd opgeslagen.  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Detailregels() As DataTable
        Get
            If Not Me.WachtID.HasValue Then 'als object nog geen wachtID heeft, dus nog niet in db zit -> Nothing terugsturen
                Return Nothing
            End If
            Dim detailadapter As DAL.wachtbestandTableAdapters.GetLoondetailsByWachtidTableAdapter = New DAL.wachtbestandTableAdapters.GetLoondetailsByWachtidTableAdapter
            Me._detailregels = detailadapter.GetData(WachtID)
            Return _detailregels
        End Get
    End Property


    ''' <summary>
    ''' De detailregels van het huidige object in een datatable
    ''' </summary>
    Private _detailregelsGrouped As DAL.wachtbestand.GetLoonDetailsGroupedDataTable
    ''' <summary>
    ''' De detailregels van het huidige object opvragen, gegroepeerd per loontype
    ''' </summary>
    ''' <returns>Datatable met detailregels of Nothing indien het een nieuwe regel is die nog niet werd opgeslagen.  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DetailregelsGrouped() As DataTable
        Get
            If Not Me.WachtID.HasValue Then 'als object nog geen wachtID heeft, dus nog niet in db zit -> Nothing terugsturen
                Return Nothing
            End If
            Dim detailadapter As DAL.wachtbestandTableAdapters.GetLoonDetailsGroupedTableAdapter = New DAL.wachtbestandTableAdapters.GetLoonDetailsGroupedTableAdapter
            Me._detailregelsGrouped = detailadapter.GetData(WachtID)
            Return _detailregelsGrouped
        End Get
    End Property

#Region "Standaard detail regels"
    ' hier gaan we functies zetten die voor de standaard detail regels zorgen (bv code30 berekenen) 

    Public Sub addExtraSupplementen(ByVal bedrag As Decimal, ByVal supplement As String, _
        ByVal supplementID As Integer, ByVal cepacode As String, ByVal commentaar As String, ByVal uren As Decimal)

        Me.addDetail(bedrag, 1, commentaar, supplementID, cepacode, uren)
    End Sub
#End Region




#Region "properties"

    ''' <summary>
    ''' LoonID voor deze wachtregel.  
    ''' </summary>
    ''' <remarks></remarks>
    Private _loonID As Integer
    Public Property loonID() As Integer
        Get
            Return _loonID
        End Get
        Set(ByVal value As Integer)
            _loonID = value
        End Set
    End Property

    Private _loonboek As String
    Public Property Loonboek() As String
        Get
            Return _loonboek
        End Get
        Set(ByVal value As String)
            _loonboek = value
        End Set
    End Property

    Private _validated As Boolean
    Public Property Validated() As Boolean
        Get
            Return _validated
        End Get
        Set(ByVal value As Boolean)
            _validated = value
            Dim wrdb As BLL.UpdateWachtregels = New BLL.UpdateWachtregels
            wrdb.setValidated(Me.WachtID, value)
        End Set
    End Property


    Private _naam As String
    Public Property Naam() As String
        Get
            Return _naam
        End Get
        Set(ByVal value As String)
            _naam = value
        End Set
    End Property


    Private _voornaam As String
    Public Property Voornaam() As String
        Get
            Return _voornaam
        End Get
        Set(ByVal value As String)
            _voornaam = value
        End Set
    End Property


    Private _prestatieid As Integer
    Public Property Prestatieid() As Integer
        Get
            Return _prestatieid
        End Get
        Set(ByVal value As Integer)
            _prestatieid = value
        End Set
    End Property


    Private _wbbrief As String
    Public Property Wbbrief() As String
        Get
            Return _wbbrief
        End Get
        Set(ByVal value As String)
            _wbbrief = value
        End Set
    End Property

    Private _datum As DateTime
    Public Property Datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)
            Me._datum = value
        End Set
    End Property


    Private _shiftID As Integer
    Public Property ShiftID() As Integer
        Get
            Return _shiftID
        End Get
        Set(ByVal value As Integer)
            _shiftID = value
        End Set
    End Property


    Private _functieID As Integer
    Public Property FunctieID() As Integer
        Get
            Return _functieID
        End Get
        Set(ByVal value As Integer)
            _functieID = value
        End Set
    End Property


    Private _voyageID As Integer
    Public Property VoyageID() As Integer
        Get
            Return _voyageID
        End Get
        Set(ByVal value As Integer)
            _voyageID = value
        End Set
    End Property

    Private _gewerkteTijd As Decimal
    Public Property GewerkteTijd() As Decimal
        Get
            Return _gewerkteTijd
        End Get
        Set(ByVal value As Decimal)
            _gewerkteTijd = value
        End Set
    End Property


    Private _gewerkteShift As Integer
    Public Property gewerkteShift() As Integer
        Get
            Return _gewerkteShift
        End Get
        Set(ByVal value As Integer)
            _gewerkteShift = value
        End Set
    End Property

    Private _isWeekend As Boolean
    Public Property Is_weekend() As Boolean
        Get
            Return _isWeekend
        End Get
        Set(ByVal value As Boolean)
            _isWeekend = value
        End Set
    End Property


    Private _isFeestdag As Boolean
    Public Property Is_feestdag() As Boolean
        Get
            Return _isFeestdag
        End Get
        Set(ByVal value As Boolean)
            _isFeestdag = value
        End Set
    End Property


    Private _datumIn As Nullable(Of DateTime)
    Public Property DatumIn() As Nullable(Of DateTime)
        Get
            Return _datumIn
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _datumIn = value
        End Set
    End Property

    Private _datumUit As Nullable(Of DateTime)
    Public Property DatumUit() As Nullable(Of DateTime)
        Get
            Return _datumUit
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _datumUit = value
        End Set
    End Property


    Private _wachtTypeID As Integer
    Public Property WachtTypeID() As Integer
        Get
            Return _wachtTypeID
        End Get
        Set(ByVal value As Integer)
            _wachtTypeID = value
        End Set
    End Property


    Private _werknemerID As Integer
    Public Property WerknemerID() As Integer
        Get
            Return _werknemerID
        End Get
        Set(ByVal value As Integer)
            _werknemerID = value
        End Set
    End Property

    Private _wordtUitbetaald As Boolean
    Public Property WordtUitbetaald() As Boolean
        Get
            Return _wordtUitbetaald
        End Get
        Set(ByVal value As Boolean)
            _wordtUitbetaald = value
        End Set
    End Property


    Private _isAfwezig As Boolean
    Public Property isAfwezig() As Boolean
        Get
            Return _isAfwezig
        End Get
        Set(ByVal value As Boolean)
            _isAfwezig = value
        End Set
    End Property

    Private _redenAfwezigheid As String
    Public Property redenAfwezigheid() As String
        Get
            Return _redenAfwezigheid
        End Get
        Set(ByVal value As String)
            _redenAfwezigheid = value
        End Set
    End Property

#End Region

End Class