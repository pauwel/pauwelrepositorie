﻿Public Class Overboeken

    Public Sub New(ByVal source As BLL.Wachtregel)
        Me.SourceWachtRegel = source
    End Sub


    Public Sub boeken()


        If Me.SourceWachtRegel Is Nothing Then
            Throw New Exception("Geen bronbriefje opgegeven")
        End If

        Dim details As DataTable = Me.SourceWachtRegel.DetailregelsGrouped
        For Each det As DataRow In details.Rows
            If det("loontypeid") = Timekeeping.Register.Loontypes.Overuren Then
                Dim bedrag As Decimal = det("bedrag")
                'Dim uren As Decimal = det("Uren")
                Dim uren As Decimal = Nothing

                If bedrag > 0 Then
                    Me.write(bedrag, det("loontypeid"), uren)
                End If

            End If
        Next
    End Sub

    Private Sub write(ByVal bedrag As Decimal, ByVal loontype As Integer, ByVal uren As Decimal)
        If Me.NewWachtRegel Is Nothing Then
            Me.createWachtregel()
        End If
        Me.SourceWachtRegel.addDetail(bedrag * -1, Timekeeping.Register.LoonDetailReden.User, "Overboeking naar " & Me.NewWachtRegel.Wbbrief, loontype, Nothing, uren, Me.NewWachtRegel.WachtID)
        Me.NewWachtRegel.addDetail(bedrag, Timekeeping.Register.LoonDetailReden.User, "Overboeking van " & Me.SourceWachtRegel.Wbbrief, loontype, Nothing, uren, Me.SourceWachtRegel.WachtID)
    End Sub
    ''' <summary>
    ''' Een nieuwe prestatie/wachtregel (loonbriefje) aanmaken om de overuren op te boeken
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub createWachtregel()
        Dim schrijver As New BLL.WachtRegelWriter
        ' de meeste gegevens nemen we over van het bronbriefje
        With schrijver
            .datum = Me.SourceWachtRegel.Datum
            .functieid = Me.SourceWachtRegel.FunctieID
            .machineid = 0
            .ploeg = 0
            .shiftid = Me.SourceWachtRegel.ShiftID

            .gewerkteshiftid = Me.SourceWachtRegel.gewerkteShift
            .voyageid = Me.SourceWachtRegel.VoyageID
            .werknemerid = Me.SourceWachtRegel.WerknemerID
            .wordtuitbetaald = False

            .weekend(.shiftid, .datum)
            .feestdag(.datum, .shiftid)
            .wachtypeid = Timekeeping.Register.WachtTypes.Prestatie
            .Validated = True

        End With
        schrijver.store(True)
        Dim nieuw As Integer = schrijver.wachtid
        Me.NewWachtRegel = New BLL.Wachtregel(nieuw)
    End Sub

    Private _sourceWachtRegel As BLL.Wachtregel
    Public Property SourceWachtRegel() As BLL.Wachtregel
        Get
            Return _sourceWachtRegel
        End Get
        Set(ByVal value As BLL.Wachtregel)
            _sourceWachtRegel = value
        End Set
    End Property


    Private _newWachtRegel As BLL.Wachtregel
    Public Property NewWachtRegel() As BLL.Wachtregel
        Get
            Return _newWachtRegel
        End Get
        Set(ByVal value As BLL.Wachtregel)
            _newWachtRegel = value
        End Set
    End Property

End Class
