
Imports DAL

Public Class DeleteValidated


    Private _Adapter As DAL.DeleteValidatedTableAdapters.Delete
    Protected ReadOnly Property Adapter() As DAL.DeleteValidatedTableAdapters.Delete
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL.DeleteValidatedTableAdapters.Delete
            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteValidated(ByVal wachtid As Integer)
        Return Adapter.DeleteValidatedRecord(wachtid)
    End Function

End Class
