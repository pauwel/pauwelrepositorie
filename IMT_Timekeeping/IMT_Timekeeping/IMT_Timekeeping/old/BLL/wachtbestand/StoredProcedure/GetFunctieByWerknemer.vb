
Imports DAL

Public Class GetFunctieByWerknemer

    Private _adapter As SPWachtbestandTableAdapters.GetFunctieByWerknemerTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetFunctieByWerknemerTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SPWachtbestandTableAdapters.GetFunctieByWerknemerTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Getdata(ByVal werknemerid As Integer) As SPWachtbestand.GetFunctieByWerknemerDataTable
        Return Me.Adapter.GetData(werknemerid)
    End Function

End Class
