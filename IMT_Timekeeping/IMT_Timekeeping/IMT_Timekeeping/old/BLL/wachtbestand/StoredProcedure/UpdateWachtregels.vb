

Imports DAL

''' <summary>
''' Klasse om aanpassingen aan een wachtregel weg te schrijven
''' </summary>
''' <remarks></remarks>
Public Class UpdateWachtregels

    Private _adapter As DAL.WachtregelsTableAdapters.WachtregelsTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL.WachtregelsTableAdapters.WachtregelsTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New WachtregelsTableAdapters.WachtregelsTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property
    ''' <summary>
    ''' Updaten van wachtregel
    ''' </summary>
    ''' <param name="wachtID"></param>
    ''' <param name="voyageid"></param>
    ''' <param name="isweekend"></param>
    ''' <param name="isfeestdag"></param>
    ''' <param name="werknemerid"></param>
    ''' <param name="functieid"></param>
    ''' <param name="shiftid"></param>
    ''' <param name="gewerkteShiftID"></param>
    ''' <param name="wordtuitbetaald"></param>
    ''' <param name="is_afwezig"></param>
    ''' <param name="redenAfwezigheid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function UpdateWachtregel(ByVal wachtID As Integer, ByVal voyageid As Integer, ByVal isweekend As Boolean, ByVal isfeestdag As Boolean, _
                             ByVal werknemerid As Integer, ByVal functieid As Integer, ByVal shiftid As Integer, ByVal gewerkteShiftID As Integer, _
                            ByVal wordtuitbetaald As Boolean, ByVal is_afwezig As Boolean, ByVal redenAfwezigheid As String)
    
        'update naar database 
        Return Me.Adapter.UpdatePrestatie(wachtID, voyageid, isweekend, isfeestdag, werknemerid, _
                                         functieid, shiftid, gewerkteShiftID, wordtuitbetaald, is_afwezig, redenAfwezigheid)



    End Function
    ''' <summary>
    ''' Een prestatie/wachtid al dan niet op gevalideerd zetten 
    ''' </summary>
    ''' <param name="wachtID"></param>
    ''' <param name="validated">standaardwaarde = true</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function setValidated(ByVal wachtID As Integer, ByVal validated As Boolean)
        Return Me.Adapter.PrestatieSetValidated(wachtID, validated)
    End Function
    ''' <summary>
    ''' De prestatie/wachtregel wordt als gevalideerd gemarkeerd
    ''' </summary>
    ''' <param name="wachtID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
       Public Function setValidated(ByVal wachtID As Integer)
        Dim validated As Boolean = True
        Return Me.Adapter.PrestatieSetValidated(wachtID, validated)
    End Function
End Class
