
Imports DAL
Public Class GetAllBestanddata

    Private _Adapter As SPWachtbestandTableAdapters.GetAllBestanddataTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetAllBestanddataTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New SPWachtbestandTableAdapters.GetAllBestanddataTableAdapter
            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function Getdata(ByVal begin As DateTime, ByVal eind As DateTime) As SPWachtbestand.GetAllBestanddataDataTable

        Return Adapter.GetData(begin, eind)

    End Function


End Class
