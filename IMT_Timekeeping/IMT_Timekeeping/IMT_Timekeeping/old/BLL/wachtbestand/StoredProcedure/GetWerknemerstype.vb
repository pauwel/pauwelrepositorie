Option Compare Text

Imports DAL

Public Class GetWerknemerstype

    Private _adapter As SPWachtbestandTableAdapters.GetWerkNemersTypeTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetWerkNemersTypeTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SPWachtbestandTableAdapters.GetWerkNemersTypeTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Getdata(ByVal werknemersid As Integer) As Integer

        For Each r As DataRow In Me.Adapter.GetData(werknemersid).Rows
            Dim i As Integer = 0
            i = r("TypeWerknemerID").ToString
            Return i
        Next

        Me.Adapter.GetData(werknemersid)
    End Function
End Class
