Public Class Saldo


    Private _adapter As DAL.SPWachtbestandTableAdapters.GetSaldoByWachtID = Nothing
    Public ReadOnly Property Adapter() As DAL.SPWachtbestandTableAdapters.GetSaldoByWachtID
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New DAL.SPWachtbestandTableAdapters.GetSaldoByWachtID
            End If

            Return _adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Function GetSaldo(ByVal wachtID As Integer) As Decimal
        Try
            Return Me.Adapter.GetSaldoByWachtID(wachtID, 0)
        Catch ex As Exception
            Return 0
        End Try

    End Function

End Class
