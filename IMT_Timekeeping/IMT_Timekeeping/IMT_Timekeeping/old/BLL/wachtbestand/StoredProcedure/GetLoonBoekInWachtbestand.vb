Option Compare Text
Imports DAL

Public Class GetLoonBoekInWachtbestand

    Private _Adapter As SPWachtbestandTableAdapters.GetLoonboekInWachtbestandTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetLoonboekInWachtbestandTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New SPWachtbestandTableAdapters.GetLoonboekInWachtbestandTableAdapter
            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function Getdata() As SPWachtbestand.GetLoonboekInWachtbestandDataTable

        Return Adapter.GetData

    End Function




End Class
