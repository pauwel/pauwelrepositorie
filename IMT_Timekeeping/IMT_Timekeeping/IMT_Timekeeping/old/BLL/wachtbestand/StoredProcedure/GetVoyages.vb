
Imports DAL

Public Class GetVoyages

    Private _adapter As SPWachtbestandTableAdapters.GetVoyagesTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetVoyagesTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SPWachtbestandTableAdapters.GetVoyagesTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Getdata() As SPWachtbestand.GetVoyagesDataTable
        Return Me.Adapter.GetData()
    End Function
End Class
