

Imports DAL


Public Class InsertWachtregels


    Private _adapter As InsertWachtregelTableAdapters.InsertWachtregels = Nothing
    Protected ReadOnly Property Adapter() As InsertWachtregelTableAdapters.InsertWachtregels
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New InsertWachtregelTableAdapters.InsertWachtregels
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function InsertData(ByVal datum As DateTime, ByVal voyageid As Integer, ByVal gewerktetijd As Decimal, _
    ByVal isweekend As Boolean, ByVal isfeestdag As Boolean, ByVal wachtypeid As Integer, ByVal werknemerid As Integer, _
     ByVal functieid As Integer, ByVal shiftid As Integer, ByVal gewerkteshiftid As Integer, ByVal wordtuitbetaald As Boolean, _
    ByVal machineid As Integer, ByVal ploeg As String, ByVal validated As Boolean)

        Return Me.InsertData(datum, voyageid, gewerktetijd, isweekend, isfeestdag, wachtypeid, _
                     werknemerid, functieid, shiftid, gewerkteshiftid, wordtuitbetaald, machineid, ploeg, validated, False)

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
   Public Function InsertData(ByVal datum As DateTime, ByVal voyageid As Integer, ByVal gewerktetijd As Decimal, _
   ByVal isweekend As Boolean, ByVal isfeestdag As Boolean, ByVal wachtypeid As Integer, ByVal werknemerid As Integer, _
    ByVal functieid As Integer, ByVal shiftid As Integer, ByVal gewerkteshiftid As Integer, ByVal wordtuitbetaald As Boolean, _
   ByVal machineid As Integer, ByVal ploeg As String, ByVal validated As Boolean, ByVal forceInsert As Boolean)
        Dim canInsert As Boolean = False
        If forceInsert = True Then
            canInsert = True
        Else
            canInsert = Not CheckExisting(datum, werknemerid, gewerkteshiftid, machineid)
        End If

        If (canInsert) Then
            Return Me.Adapter.InsertPrestatie(datum, voyageid, gewerktetijd, isweekend, isfeestdag, wachtypeid, werknemerid, _
                 functieid, shiftid, gewerkteshiftid, wordtuitbetaald, machineid, ploeg, validated)
        Else
            Return False
        End If
    End Function


    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function DeletePrestatie(ByVal id As Integer)
        Try
            Return Adapter.DeletePrestatie(id)
        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function CheckExisting(ByVal datum As DateTime, ByVal wnid As Integer, ByVal shiftid As Integer, ByVal machid As Integer) As Boolean

        If Timekeeping.Register.Shiften.dag = shiftid Then
            Dim RValue As Integer = Adapter.CheckExistingPrestatie(datum, wnid, shiftid, machid)
            If RValue = 0 Then
                'zoeken op vroege shift
                Dim v As Integer = Adapter.CheckExistingPrestatie(datum, wnid, Timekeeping.Register.Shiften.vroege, machid)
                If v = 0 Then
                    Return False
                Else
                    MsgBox("een identieke (werknemer en / of machine ) regel werd reeds ingepland met de vroege shift", MsgBoxStyle.Information)
                    Return True
                End If
            Else
                MsgBox("een identieke (werknemer en / of machine )  regel werd reeds ingepland met de dag shift", MsgBoxStyle.Information)
                Return True
            End If
        ElseIf Timekeeping.Register.Shiften.vroege = shiftid Then
            Dim RValue As Integer = Adapter.CheckExistingPrestatie(datum, wnid, shiftid, machid)
            If RValue = 0 Then
                'zoeken op vroege shift
                Dim v As Integer = Adapter.CheckExistingPrestatie(datum, wnid, Timekeeping.Register.Shiften.dag, machid)
                If v = 0 Then
                    Return False
                Else
                    MsgBox("een identieke (werknemer en / of machine ) regel werd reeds ingepland met de dag shift", MsgBoxStyle.Information)
                    Return True
                End If
            Else
                MsgBox("een identieke (werknemer en / of machine )  regel werd reeds ingepland met de vroege shift", MsgBoxStyle.Information)
                Return True
            End If
        Else
            Dim RValue As Integer = Adapter.CheckExistingPrestatie(datum, wnid, shiftid, machid)
            If RValue = 0 Then
                Return False
            Else
                MsgBox("een identieke (werknemer en / of machine )  regel werd reeds ingepland in de geselecteeerde shift", MsgBoxStyle.Information)
                Return True
            End If
        End If


    End Function
End Class
