
Public Class Splitter


#Region "Properties"


    Private _prestatieid As Integer
    Public Property Prestatieid() As Integer
        Get
            Return _prestatieid
        End Get
        Set(ByVal value As Integer)
            _prestatieid = value
        End Set
    End Property


    Private _Datum As DateTime
    Public Property Datum() As DateTime
        Get
            Return _Datum
        End Get
        Set(ByVal value As DateTime)
            _Datum = value
        End Set
    End Property


    Private _VoyageID As Integer
    Public Property VoyageID() As Integer
        Get
            Return _VoyageID
        End Get
        Set(ByVal value As Integer)
            _VoyageID = value
        End Set
    End Property



    Private _voyageto As Integer
    Public Property VoyageTo() As Integer
        Get
            Return _voyageto
        End Get
        Set(ByVal value As Integer)
            _voyageto = value
        End Set
    End Property


    Private _inputUren As Decimal
    Public Property inputUren() As Decimal
        Get
            Return _inputUren
        End Get
        Set(ByVal value As Decimal)
            If Uren - value > 0 Then
                _inputUren = value

            Else
                '  MsgBox("oeps")
            End If

        End Set
    End Property



    Private _Uren As Decimal
    Public Property Uren() As Decimal
        Get
            Return _Uren
        End Get
        Set(ByVal value As Decimal)
            _Uren = value
        End Set
    End Property




    Private _MachineID As Integer
    Public Property MachineID() As Integer
        Get
            Return _MachineID
        End Get
        Set(ByVal value As Integer)
            _MachineID = value
        End Set
    End Property

    Private _MachineKost As Decimal
    Public Property MachineKost() As Decimal
        Get
            Return _MachineKost
        End Get
        Set(ByVal value As Decimal)
            _MachineKost = value
        End Set
    End Property




    Private _ploeg As String
    Public Property ploeg() As String
        Get
            Return _ploeg
        End Get
        Set(ByVal value As String)
            _ploeg = value
        End Set
    End Property

    Private _wachtID As Integer
    Public Property wachtID() As Integer
        Get
            Return _wachtID
        End Get
        Set(ByVal value As Integer)
            _wachtID = value
        End Set
    End Property


    Private _GewerkteShiftID As Integer
    Public Property GewerkteShiftID() As Integer
        Get
            Return _GewerkteShiftID
        End Get
        Set(ByVal value As Integer)
            _GewerkteShiftID = value
        End Set
    End Property



#End Region

    Sub SetProperties()

        Dim sp As New SplitPrestatieGetPrestatiedetails
        Dim r As DataRow = sp.GetPresatieregel(Prestatieid)

        With Me
            .GewerkteShiftID = r("GewerkteShiftID").ToString
            .MachineID = r("MachineID").ToString
            .MachineKost = r("MachineKost").ToString
            .ploeg = r("ploeg").ToString
            .Uren = r("Uren").ToString
            .VoyageID = r("VoyageID").ToString
            .MachineKost = r("Machinekost").ToString

            Try
                .wachtID = r("wachtID").ToString
            Catch ex As InvalidCastException
                'ingeval van NULL waarde in de database
            End Try

            .Datum = r("Datum").ToString

        End With

    End Sub

    Sub insert()

        Dim sp As New BLL.SplitPrestatie
        With Me
            Dim n_uren As Decimal = newUren()
            Dim r_uren As Decimal = restUren()
            Dim n_machinekost As Decimal = newMachinekost()
            Dim r_machinekost As Decimal = restMachinekost()

            If n_uren > 0 And r_uren > 0 Then
                sp.Split(.MachineID, .Datum, n_uren, .ploeg, .VoyageTo, .wachtID, .Prestatieid, .GewerkteShiftID, r_uren, n_machinekost, r_machinekost)

            Else
                MsgBox("overdracht van uren is incorrect, controleer!", MsgBoxStyle.Critical)
            End If

        End With

    End Sub

    Private Function newUren() As Decimal
        Return (Uren - inputUren)
    End Function
    Private Function restUren() As Decimal
        Return (Uren - (Uren - inputUren))
    End Function

    Private Function newMachinekost() As Decimal
        Return (MachineKost / Uren * (Uren - inputUren))
    End Function
    Private Function restMachinekost() As Decimal
        Return (MachineKost - (MachineKost / Uren * (Uren - inputUren)))
    End Function



End Class
