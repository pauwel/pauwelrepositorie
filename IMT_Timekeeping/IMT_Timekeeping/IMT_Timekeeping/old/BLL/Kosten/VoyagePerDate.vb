
Imports DAL
Public Class VoyagePerDate
    Private _Adapter As KostenTableAdapters.GETprestatieVoyagePerDatumTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.GETprestatieVoyagePerDatumTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.GETprestatieVoyagePerDatumTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetVoyages(ByVal datum As DateTime, ByVal shiftid As Integer) As DAL.Kosten.GETprestatieVoyagePerDatumDataTable
        Return Me.Adapter.GetData(datum, shiftid)
    End Function

End Class
