

Imports DAL
Public Class ShiftenMachinesPerDate


    Private _Adapter As KostenTableAdapters.GetShiftenMachinesByDateTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.GetShiftenMachinesByDateTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.GetShiftenMachinesByDateTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetShiften(ByVal datum As DateTime) As DAL.Kosten.GetShiftenMachinesByDateDataTable
        Return Me.Adapter.GetData(datum)
    End Function

End Class

