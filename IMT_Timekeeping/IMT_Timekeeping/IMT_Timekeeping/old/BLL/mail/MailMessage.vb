
Imports System.Configuration
Imports System.Net.Mail
Imports system.text
Imports System.DirectoryServices
Imports System.Data.SqlClient
Imports DAL





Public Class MailMessage



    Private mailbody As New StringBuilder

    Public Sub sendmail()

        Dim regel As New BLL.mailrule

        If regel.TimekeeperMail Then
            If ValueChanged() Then

                Dim SQLMAIL As Boolean = False
                SQLMAIL = ConfigurationManager.AppSettings("DBMAIL")

                If Not SQLMAIL Then
                    Dim host As String = ConfigurationManager.AppSettings("SMTP")
                    If String.IsNullOrEmpty(host) = False Then
                        Dim SMTP As New SmtpClient(host)
                        Dim m As New BLL.Mail

                        Dim sb As New StringBuilder

                        sb.Append("De gegevens van werknemer " & Werknemer & " gepland op " & Datum.Date & " werden gewijzigd: ")
                        Dim from As New MailAddress(ConfigurationManager.AppSettings("MailFrom"))
                        Dim mailtje As New StringBuilder

                        'mailtje.Append("De gegevens van werknemer " & Werknemer & " gepland op " & Datum.Date & " werden gewijzigd: ")
                        mailtje.Append(mailbody)

                        If String.IsNullOrEmpty(from.ToString) = False Then
                            For Each r As DataRow In m.GetMailadres.Rows

                                Dim mail As New System.Net.Mail.MailMessage(from.ToString, r("mailadres").ToString, sb.ToString, mailtje.ToString)

                                Try
                                    mail.IsBodyHtml = True
                                    SMTP.Send(mail)
                                Catch ex As SmtpException
                                    'failed sending -- retry after 5 seconds
                                    Threading.Thread.Sleep(5000)
                                    SMTP.Send(mail)
                                End Try


                            Next

                        Else
                            MsgBox("Geen geldige afzender gevonden, controleer het config bestand", MsgBoxStyle.Critical)
                        End If

                    Else
                        MsgBox("Geen SMTP host gevonden, controleer het config bestand", MsgBoxStyle.Critical)
                    End If

                Else
                    Dim mailtje As New StringBuilder

                    ' mailtje.Append("De gegevens van werknemer " & Werknemer & " gepland op " & Datum.Date & " werden gewijzigd: ")
                    mailtje.Append(mailbody)

                    Dim m As New BLL.Mail
                    For Each r As DataRow In m.GetMailadres.Rows

                        Dim con As New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings(0).ConnectionString.ToString)
                        Dim com As New SqlClient.SqlCommand
                        com.CommandType = CommandType.StoredProcedure
                        com.CommandText = "spd_send_tk_mail"
                        com.Connection = con

                       

                        com.Parameters.Add("@r", SqlDbType.Char, 100).Value = r("mailadres").ToString
                        com.Parameters.Add("@b", SqlDbType.Char, 1000).Value = mailtje.ToString
                        com.Parameters.Add("@titel", SqlDbType.Char, 100).Value = "De gegevens van werknemer " & Werknemer & " gepland op " & Datum.Date & " werden gewijzigd: "


                        If con.State = ConnectionState.Closed Then
                            con.Open()
                        End If

                        com.ExecuteNonQuery()


                        con.Close()


                    Next

                End If

            Else

            End If

        End If



    End Sub

    Dim _wachtid As Integer
    Property Wachtid() As Integer
        Get
            Return _wachtid
        End Get
        Set(ByVal value As Integer)
            _wachtid = value
        End Set
    End Property

    Private _wn As String
    Public Property Werknemer() As String
        Get
            Return _wn
        End Get
        Set(ByVal value As String)
            _wn = value
        End Set
    End Property


    Private _datum As DateTime
    Public Property Datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)
            _datum = value
        End Set
    End Property


    Private _orgWeekdag As Boolean
    Public Property orgWeekdag() As Boolean
        Get
            Return _orgWeekdag
        End Get
        Set(ByVal value As Boolean)
            _orgWeekdag = value
        End Set
    End Property


    Private _newWeekdag As Boolean
    Public Property newWeekdag() As Boolean
        Get
            Return _newWeekdag
        End Get
        Set(ByVal value As Boolean)
            _newWeekdag = value
        End Set
    End Property


    Private _orgFeestdag As Boolean
    Public Property OrgFeestdag() As Boolean
        Get
            Return _orgFeestdag
        End Get
        Set(ByVal value As Boolean)
            _orgFeestdag = value
        End Set
    End Property


    Private _newFeestdag As Boolean
    Public Property newFeestdag() As Boolean
        Get
            Return _newFeestdag
        End Get
        Set(ByVal value As Boolean)
            _newFeestdag = value
        End Set

    End Property


    Private _orgAfwezig As Boolean
    Public Property orgAfwezig() As Boolean
        Get
            Return _orgAfwezig
        End Get
        Set(ByVal value As Boolean)
            _orgAfwezig = value
        End Set
    End Property


    Private _newAfwezig As Boolean
    Public Property NewAfwezig() As Boolean
        Get
            Return _newAfwezig
        End Get
        Set(ByVal value As Boolean)
            _newAfwezig = value
        End Set
    End Property


    Private _orgShift As String
    Public Property orgShift() As String
        Get
            Return _orgShift
        End Get
        Set(ByVal value As String)
            _orgShift = value
        End Set
    End Property


    Private _newShift As String
    Public Property newShift() As String
        Get
            Return _newShift
        End Get
        Set(ByVal value As String)
            _newShift = value
        End Set
    End Property


    Private _orgFunctie As String
    Public Property orgFunctie() As String
        Get
            Return _orgFunctie
        End Get
        Set(ByVal value As String)
            _orgFunctie = value
        End Set
    End Property


    Private _newFunctie As String
    Public Property newFunctie() As String
        Get
            Return _newFunctie
        End Get
        Set(ByVal value As String)
            _newFunctie = value
        End Set
    End Property



    Private _orgVoyage As String
    Public Property orgVoyage() As String
        Get
            Return _orgVoyage
        End Get
        Set(ByVal value As String)
            _orgVoyage = value
        End Set
    End Property


    Private _newVoyage As String
    Public Property newVoyage() As String
        Get
            Return _newVoyage
        End Get
        Set(ByVal value As String)
            _newVoyage = value
        End Set
    End Property



    Private _orgReden As String
    Public Property orgReden() As String
        Get
            Return _orgReden
        End Get
        Set(ByVal value As String)
            _orgReden = value
        End Set
    End Property


    Private _newReden As String
    Public Property newReden() As String
        Get
            Return _newReden
        End Get
        Set(ByVal value As String)
            _newReden = value
        End Set
    End Property

    Dim _dta As DataTable
    Property dta() As DataTable
        Get
            Return _dta
        End Get
        Set(ByVal value As DataTable)
            _dta = value
        End Set
    End Property


    Private Function ValueChanged() As Boolean

        mailbody.Remove(0, mailbody.Length)

        Dim changed As Boolean = False

        If orgAfwezig <> NewAfwezig Then
            mailbody.Append("AFWEZIG werd gewijzigd van " & orgAfwezig & " naar " & NewAfwezig & "<br>")

            If NewAfwezig Then
                mailbody.Append(" REDEN voor afwezigheid: " & newReden & "<br>")
            End If
            changed = True
        End If
        If OrgFeestdag <> newFeestdag Then
            mailbody.Append(" FEESTDAG werd gewijzigd van " & OrgFeestdag & " naar " & newFeestdag & "<br>")
            changed = True
        End If
        If orgWeekdag <> newWeekdag Then
            mailbody.Append(" WEEKDAG werd gewijzigd van " & orgWeekdag & " naar " & newWeekdag & "<br>")
            changed = True
        End If



        If String.IsNullOrEmpty(newShift) = False Then
            If orgShift <> newShift Then
                mailbody.Append(" SHIFT werd gewijzigd van " & orgShift & " naar " & newShift & "<br>")
                changed = True
            End If
        End If

        If String.IsNullOrEmpty(newFunctie) = False Then
            If orgFunctie <> newFunctie Then
                mailbody.Append(" FUNCTIE werd gewijzigd van " & orgFunctie & " naar " & newFunctie & "<br>")
                changed = True
            End If
        End If

        If String.IsNullOrEmpty(newVoyage) = False Then
            If orgVoyage <> newVoyage Then
                mailbody.Append(" VOYAGE werd gewijzigd van " & orgVoyage & " naar " & newVoyage & "<br>")
                changed = True
            End If
        End If






        Dim com As New SqlCommand
        Dim con As New SqlConnection
        con.ConnectionString = ConfigurationManager.ConnectionStrings(0).ConnectionString

        com.Connection = con
        com.CommandType = CommandType.StoredProcedure
        com.CommandText = "GETMailSuplementen"
        Dim p As New SqlParameter
        p.ParameterName = "@wachtid"
        p.Value = Wachtid


        com.Parameters.Add(p)

        con.Open()


        Dim adapter As SqlDataAdapter = New SqlDataAdapter(com)

        Dim table As DataTable = New DataTable
        adapter.Fill(table)

        If table.Rows.Count > 0 Then
            Dim tb As StringBuilder = Nothing




            mailbody.Append("<br><u>" & " Extra supplementen werden toegevoegd  " & "</u><p>")
            For Each dr As DataRow In table.Rows

                Dim uren As Decimal = Math.Round(dr("uren"), 2)
                Dim bedrag As Decimal = Math.Round(dr("bedrag"), 2)


                mailbody.Append("| omschrijving: " & dr("Omschrijving").ToString)
                mailbody.Append("| uren: " & uren.ToString)
                mailbody.Append("| bedrag: " & bedrag.ToString)
                mailbody.Append("| commentaar: " & dr("commentaar").ToString & "<p>")
            Next
            changed = True


            table.Dispose()
            adapter.Dispose()
        End If

        Return changed
    End Function


End Class
