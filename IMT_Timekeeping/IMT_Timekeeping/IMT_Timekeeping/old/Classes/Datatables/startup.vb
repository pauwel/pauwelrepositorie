
Imports DAL

Public Class startup


    Private _cat As DataTable, _catdisplay As String, _catvalue As String

    Dim _functie As DataTable, _functiedisplay As String, _functievalue As String
    Dim _employeestype As DataTable, _employeestypedisplay As String, _employeestypevalue As String
    Private _employees As DataTable, _employeedisplay As String, _employeevalue As String
    Private _land As DataTable, _landdisplay As String, _landvalue As String

    Private _rederij As DataTable
    Public Property rederij() As DataTable
        Get
            Return _rederij
        End Get
        Set(ByVal value As DataTable)
            _rederij = value
        End Set
    End Property

    Private _rederijDisplayMember As String
    Public Property rederijDisplayMember() As String
        Get
            Return _rederijDisplayMember
        End Get
        Set(ByVal value As String)
            _rederijDisplayMember = value
        End Set
    End Property


    Private _rederijValueMember As String
    Public Property rederijValueMember() As String
        Get
            Return _rederijValueMember
        End Get
        Set(ByVal value As String)
            _rederijValueMember = value
        End Set
    End Property


    Public Property category() As DataTable
        Get
            Return _cat
        End Get
        Set(ByVal value As DataTable)
            _cat = value
        End Set
    End Property
    'displaymember
    Public Property categorydisplaymember() As String
        Get
            Return _catdisplay
        End Get
        Set(ByVal value As String)
            _catdisplay = value
        End Set
    End Property
    'valuemember
    Public Property categoryvaluemember() As String
        Get
            Return _catvalue
        End Get
        Set(ByVal value As String)
            _catvalue = value
        End Set
    End Property


    Public Property functies() As DataTable
        Get
            Return _functie
        End Get
        Set(ByVal value As DataTable)
            _functie = value
        End Set
    End Property
    'displaymember
    Public Property functiesdisplaymember() As String
        Get
            Return _functiedisplay
        End Get
        Set(ByVal value As String)
            _functiedisplay = value
        End Set
    End Property
    'valuemember
    Public Property functiesvaluemember() As String
        Get
            Return _functievalue
        End Get
        Set(ByVal value As String)
            _functievalue = value
        End Set
    End Property


    Private _shift As DataTable
    Public Property shift() As DataTable
        Get
            Return _shift
        End Get
        Set(ByVal value As DataTable)
            _shift = value
        End Set
    End Property


    Private _shiftdisplaymember As String
    Public Property shiftdisplaymember() As String
        Get
            Return _shiftdisplaymember
        End Get
        Set(ByVal value As String)
            _shiftdisplaymember = value
        End Set
    End Property



    Private _shiftvaluemember As String
    Public Property shiftvaluemember() As String
        Get
            Return _shiftvaluemember
        End Get
        Set(ByVal value As String)
            _shiftvaluemember = value
        End Set
    End Property


    Public Property employeetypes() As DataTable
        Get
            Return _employeestype
        End Get
        Set(ByVal value As DataTable)
            _employeestype = value
        End Set
    End Property

    'displaymember
    Public Property employeetypesdisplaymember() As String
        Get
            Return _employeestypedisplay
        End Get
        Set(ByVal value As String)
            _employeestypedisplay = value
        End Set
    End Property
    'valuemember
    Public Property employeetypesvaluemember() As String
        Get
            Return _employeestypevalue
        End Get
        Set(ByVal value As String)
            _employeestypevalue = value
        End Set
    End Property

    Public Property employees() As DataTable
        Get
            Return _employees
        End Get
        Set(ByVal value As DataTable)
            _employees = value
        End Set
    End Property

    'displaymember
    Public Property employeesdisplaymember() As String
        Get
            Return _employeedisplay
        End Get
        Set(ByVal value As String)
            _employeedisplay = value
        End Set
    End Property
    'valuemember
    Public Property employeesvaluemember() As String
        Get
            Return _employeevalue
        End Get
        Set(ByVal value As String)
            _employeevalue = value
        End Set
    End Property
    Public Property land() As DataTable
        Get
            Return _land
        End Get
        Set(ByVal value As DataTable)
            _land = value
        End Set
    End Property

    'displaymember
    Public Property landdisplaymember() As String
        Get
            Return _landdisplay
        End Get
        Set(ByVal value As String)
            _landdisplay = value
        End Set
    End Property
    'valuemember
    Public Property landvaluemember() As String
        Get
            Return _landvalue
        End Get
        Set(ByVal value As String)
            _landvalue = value
        End Set
    End Property


    Private _machines As DataTable
    Public Property Machines() As DataTable
        Get
            Return _machines
        End Get
        Set(ByVal value As DataTable)
            _machines = value
        End Set
    End Property

    Private _machineDisplaymember As String
    Public Property MachineDisplayMember() As String
        Get
            Return _machineDisplaymember
        End Get
        Set(ByVal value As String)
            _machineDisplaymember = value
        End Set
    End Property


    Private _MachineValueMember As String
    Public Property MachineValueMember() As String
        Get
            Return _MachineValueMember
        End Get
        Set(ByVal value As String)
            _MachineValueMember = value
        End Set
    End Property



End Class
