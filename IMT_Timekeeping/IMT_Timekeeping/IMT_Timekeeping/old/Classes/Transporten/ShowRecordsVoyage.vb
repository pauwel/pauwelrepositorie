

Public Class ShowRecordsVoyage


    Private _status As Boolean
    Public Property Status() As Boolean
        Get
            Return _status
        End Get
        Set(ByVal value As Boolean)
            _status = value
        End Set
    End Property



    ''' <summary>
    ''' indien vlag=0 => alle gegevens tonen
    ''' vlag=1 => selectie via status
    ''' </summary>
    ''' <remarks></remarks>
    Private _vlag As Integer
    Public Property Vlag() As Integer
        Get
            Return _vlag
        End Get
        Set(ByVal value As Integer)
            _vlag = value
        End Set
    End Property




End Class
