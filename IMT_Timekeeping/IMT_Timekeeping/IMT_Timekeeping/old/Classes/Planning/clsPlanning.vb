

Option Compare Text

Public Class clsPlanning



#Region "Prestatieinput"
    Private _functie As Int32, _voyage As Int32
    Public Property SelectedFunctie() As Int32
        Get
            Return _functie
        End Get
        Set(ByVal value As Int32)
            _functie = value

        End Set
    End Property

    Public Property SelectedVoyage() As Int32
        Get
            Return _voyage
        End Get
        Set(ByVal value As Int32)
            _voyage = value

        End Set
    End Property


    Private _shift As Integer
    Public Property Shift() As Integer
        Get
            Return _shift
        End Get
        Set(ByVal value As Integer)
            _shift = value
        End Set
    End Property


    Private _onlyMachines As Boolean
    Public Property OnlyMachines() As Boolean
        Get
            Return _onlyMachines
        End Get
        Set(ByVal value As Boolean)
            _onlyMachines = value
        End Set
    End Property


    Private _Werkenemer As Integer
    Public Property Werknemer() As Integer
        Get
            Return _Werkenemer
        End Get
        Set(ByVal value As Integer)
            _Werkenemer = value
        End Set
    End Property


    Private _Machine As Integer
    Public Property Machine() As Integer
        Get
            Return _Machine
        End Get
        Set(ByVal value As Integer)
            _Machine = value
        End Set
    End Property


    Private _griddata As DataTable
    Public Property GridData() As DataTable
        Get
            Return _griddata
        End Get
        Set(ByVal value As DataTable)
            _griddata = value
        End Set
    End Property

    Private _ShiftInPLanning As DataTable
    Public Property ShiftInPLanning() As DataTable
        Get
            Return _ShiftInPLanning
        End Get
        Set(ByVal value As DataTable)
            _ShiftInPLanning = value
        End Set
    End Property



    Private _selectedVoyages As DataTable
    Public Property SelectedVoyages() As DataTable
        Get
            Return _selectedVoyages
        End Get
        Set(ByVal value As DataTable)
            _selectedVoyages = value
        End Set
    End Property




    Public Function ValidatePrestatie() As Boolean
        'twee mogelijkheden
        'enkel machine = geen functie en geen werknemer geselecteerd
        If OnlyMachines Then
            If Shift > 0 And SelectedVoyage > 0 And Machine > 0 Then
                Return True
            Else
                MsgBox("Shift, voyage en machine zijn verplichte velden", MsgBoxStyle.Information)
                Return False
            End If

        Else
            'indien werknemer met of zonder machine
            If Shift > 0 And SelectedVoyage > 0 And SelectedFunctie > 0 And Werknemer > 0 Then
                Return True
            Else
                MsgBox("Shift, voyage, functie en werknemer zijn verplichte velden", MsgBoxStyle.Information)
                Return False
            End If
        End If


    End Function

#End Region

#Region "CheckVergoeding"

    'Indien nachtploeg op vrijdag ==> zaterdag vergoeding
    'Indien nachtploeg op zaterdag ==> zondag vergoeding
    'Indien nachtploeg op dagvoor feestdag ==> feestdag vergoeding



    Private _prestatiedatum As DateTime
    Public Property Prestatiedatum() As DateTime
        Get
            Return _prestatiedatum
        End Get
        Set(ByVal value As DateTime)

            _prestatiedatum = value.Date
            Typedag()


        End Set
    End Property



    Private _ploeg As Integer
    Public Property Ploeg() As Integer
        Get
            Return _ploeg
        End Get
        Set(ByVal value As Integer)

            _ploeg = value
            Typedag()
            
        End Set
    End Property



    Private _weekdag As Boolean
    Public Property Weekdag() As Boolean
        Get

            Return _weekdag
        End Get
        Set(ByVal value As Boolean)
            _weekdag = value
        End Set

    End Property


    Private _weekend As Boolean
    Public Property weekend() As Boolean
        Get

            Return _weekend
        End Get
        Set(ByVal value As Boolean)
            _weekend = value
        End Set
    End Property


    Private _feestdag As Boolean
    Public Property Feestdag() As Boolean
        Get

            Return _feestdag
        End Get
        Set(ByVal value As Boolean)
            _feestdag = value
        End Set
    End Property



    Private Sub Typedag()

        Weekdag = False
        weekend = False
        Feestdag = False
        Dim volgendedag As DateTime = DateAdd(DateInterval.Day, 1, Prestatiedatum)

        If (clBLL_feestdagen.CheckFeestdag(volgendedag) And Ploeg = 39) Or _
                 clBLL_feestdagen.CheckFeestdag(Prestatiedatum) Then
            Feestdag = True

            If Prestatiedatum.DayOfWeek = DayOfWeek.Friday And Ploeg = 39 Or _
                Prestatiedatum.DayOfWeek = DayOfWeek.Saturday Or _
                Prestatiedatum.DayOfWeek = DayOfWeek.Sunday Then
                weekend = True
            Else
                Weekdag = True
            End If

        ElseIf Ploeg = 39 And Prestatiedatum.DayOfWeek = DayOfWeek.Friday Then
            weekend = True
            Weekdag = True

        ElseIf Prestatiedatum.DayOfWeek = DayOfWeek.Saturday Or _
            Prestatiedatum.DayOfWeek = DayOfWeek.Sunday Then
            weekend = True
        Else
            Weekdag = True
        End If



    End Sub

    



    Function Vergoeding() As Integer

        Dim volgendedag As DateTime = DateAdd(DateInterval.Day, 1, Prestatiedatum)
        'eerst controle dat volgende dag GEEN feestag is

        If (clBLL_feestdagen.CheckFeestdag(volgendedag) And Ploeg = 39) Or _
         clBLL_feestdagen.CheckFeestdag(Prestatiedatum) Then
            'nacht + volgende dag = feestdag = feestdagvergoeding // tijdens feestdag altijd feestdag vergoeding

            Return 42

        ElseIf Ploeg = 39 And volgendedag.DayOfWeek = DayOfWeek.Friday Then
            'nacht + vrijdag = zaterdagvergoeding
            Return 40
        ElseIf Ploeg = 39 And volgendedag.DayOfWeek = DayOfWeek.Saturday Or _
            volgendedag.DayOfWeek = DayOfWeek.Sunday Then
            'nacht + zaterdag = zondagvergoeding// zondag altijd zondag vergoeding (ook nachtploeg)
            Return 41
        End If

    End Function


#End Region


#Region "Spaarplanner"


    Function IsSpaarplanner() As Boolean
        Dim w As New BLL.Werknemers

        If w.CheckSpaarplan(Werknemer) Then
            Return True
        Else
            Return False
        End If

    End Function



#End Region

End Class
