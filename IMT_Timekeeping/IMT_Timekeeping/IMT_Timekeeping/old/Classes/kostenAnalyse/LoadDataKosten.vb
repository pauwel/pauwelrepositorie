

Imports System.Threading
Public Class LoadDataKosten


    Private _shift As DataTable
    Public Property PrestatieShiften() As DataTable
        Get
            Return _shift
        End Get
        Set(ByVal value As DataTable)
            _shift = value
        End Set
    End Property


    Private _voyagesTo As DataTable
    Public Property VoyagesTo() As DataTable
        Get
            Return _voyagesTo
        End Get
        Set(ByVal value As DataTable)
            _voyagesTo = value
        End Set
    End Property


    Private _voyages As DataTable
    Public Property Prestatievoyages() As DataTable
        Get
            Return _voyages
        End Get
        Set(ByVal value As DataTable)
            _voyages = value
        End Set
    End Property

    Private _wnTo As DataTable
    Public Property WerknemersTO() As DataTable
        Get
            Return _wnTo
        End Get
        Set(ByVal value As DataTable)
            _wnTo = value
        End Set
    End Property


    Private _wn As DataTable
    Public Property Werknemers() As DataTable
        Get
            Return _wn
        End Get
        Set(ByVal value As DataTable)
            _wn = value
        End Set
    End Property



    Private _selectedVoyage As Integer
    Public Property SelectedVoyage() As Integer
        Get
            Return _selectedVoyage
        End Get
        Set(ByVal value As Integer)
            _selectedVoyage = value
        End Set
    End Property



    Private _machines As Boolean
    Public Property MAchinesOnly() As Boolean
        Get
            Return _machines
        End Get
        Set(ByVal value As Boolean)
            _machines = value
        End Set
    End Property


    Private _uren As Decimal
    Public Property Uren() As Decimal
        Get
            Return _uren
        End Get
        Set(ByVal value As Decimal)
            _uren = value
        End Set
    End Property


    Private _datum As DateTime
    Public Property Datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)


            _datum = value

            'start a new thread to execute the task asynchronously
            thExecuteTaskAsync = New Thread(AddressOf loadshift)
            thExecuteTaskAsync.Start()
            While thExecuteTaskAsync.ThreadState = ThreadState.Running
                Thread.Sleep(0)
            End While

        End Set
    End Property

    Private _shiftselected As Integer
    Public Property Shift() As Integer
        Get
            Return _shiftselected
        End Get
        Set(ByVal value As Integer)
            If _shiftselected <> value Then
                _shiftselected = value

                'start a new thread to execute the task asynchronously
                thExecuteTaskAsync = New Thread(AddressOf loadvoyages)
                thExecuteTaskAsync.Start()
                While thExecuteTaskAsync.ThreadState = ThreadState.Running
                    Thread.Sleep(0)
                End While

            End If
        End Set
    End Property

    Private _voyageselected As Integer
    Public Property VoyageSelected() As Integer
        Get
            Return _voyageselected
        End Get
        Set(ByVal value As Integer)

            _voyageselected = value

            'start a new thread to execute the task asynchronously
            thExecuteTaskAsync = New Thread(AddressOf loadWerknemers)
            thExecuteTaskAsync.Start()
            While thExecuteTaskAsync.ThreadState = ThreadState.Running
                Thread.Sleep(0)
            End While


        End Set
    End Property

    Private _voyageTOselected As Integer
    Public Property VoyageToSelected() As Integer
        Get
            Return _voyageTOselected
        End Get
        Set(ByVal value As Integer)

            _voyageTOselected = value

            'start a new thread to execute the task asynchronously
            thExecuteTaskAsync = New Thread(AddressOf loadWerknemersTo)
            thExecuteTaskAsync.Start()
            While thExecuteTaskAsync.ThreadState = ThreadState.Running
                Thread.Sleep(0)
            End While


        End Set
    End Property
    Dim thExecuteTaskAsync As Thread = Nothing

    Private Sub StartExecuteTaskAsync()
        'clear existing thread
        If Not thExecuteTaskAsync Is Nothing Then
            thExecuteTaskAsync.Abort()
            thExecuteTaskAsync.Join()
            thExecuteTaskAsync = Nothing
        End If
       
    End Sub

    Private Sub loadshift()

        Try
            If MAchinesOnly Then
                Dim loadshift As New BLL.ShiftenMachinesPerDate
                PrestatieShiften = loadshift.GetShiften(Datum.Date)
            Else
                Dim loadShift As New BLL.ShiftPerDate
                PrestatieShiften = loadShift.GetShiften(Datum.Date)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub SelectVoyageto()
        Dim voyageto As New BLL.VoyageTo
        VoyagesTo = voyageto.GetVoyagesTo(SelectedVoyage)
    End Sub

    Sub loadvoyages()

        Try
            Dim loadPloeg As New BLL.VoyagePerDate
            Prestatievoyages = loadPloeg.GetVoyages(Datum.Date, Shift)


        Catch ex As Exception

        End Try

    End Sub
    'Laden van machines indien checkbox alleen machines is aangevinkt
    Private Sub loadWerknemers()

        Try
            If MAchinesOnly Then
                Dim Loadmach As New BLL.MachinesPrestatie
                Werknemers = Loadmach.GetMachines(Datum.Date, Shift, VoyageSelected)
            Else
                Dim loadwerknemers As New BLL.WerknemersPrestatie
                Werknemers = loadwerknemers.GetWerknemers(Datum.Date, Shift, VoyageSelected)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub loadWerknemersTo()

        Try
            If MAchinesOnly Then
                Dim Loadmach As New BLL.MachinesPrestatie
                WerknemersTO = Loadmach.GetMachines(Datum.Date, Shift, VoyageToSelected)
            Else
                Dim loadwerknemers As New BLL.WerknemersPrestatie
                WerknemersTO = loadwerknemers.GetWerknemers(Datum.Date, Shift, VoyageToSelected)
            End If
        Catch ex As Exception

        End Try

    End Sub


    Private _orgUren As Decimal
    Public Property OrgUren() As Decimal
        Get
            Return _orgUren
        End Get
        Set(ByVal value As Decimal)
            _orgUren = value
        End Set
    End Property

    Public Function validate() As Boolean

        If Uren = 0 Then
            MsgBox("Overgedragen uren moeten groter zijn dan 0", MsgBoxStyle.Information)
            Return False
        End If
        If OrgUren - Uren < 0 Then
            MsgBox("Over te dragen uren kunnen NOOIT meer zijn dan de beschikbare uren( " & OrgUren & " )", MsgBoxStyle.Information)
            Return False
        End If

        If VoyageToSelected = 0 Then
            MsgBox("Selecteer de 'over te boeken uren' voyage", MsgBoxStyle.Information)
            Return False
        End If
        Return True
    End Function

End Class
