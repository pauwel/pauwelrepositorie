<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.NavigationPane1 = New DevComponents.DotNetBar.NavigationPane
        Me.NavigationPanePanel2 = New DevComponents.DotNetBar.NavigationPanePanel
        Me.ExplorerBar1 = New DevComponents.DotNetBar.ExplorerBar
        Me.grpConfiguratie = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.cmdMail = New DevComponents.DotNetBar.ButtonItem
        Me.cmdMailActive = New DevComponents.DotNetBar.ButtonItem
        Me.grpKosten = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.ButtonItemRederijen = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItemTransportmiddel = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItemKostenplaats = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem
        Me.ExplorerBarGroupItem3 = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.btnItemCategories = New DevComponents.DotNetBar.ButtonItem
        Me.btnItemFuncties = New DevComponents.DotNetBar.ButtonItem
        Me.btnItemEmployeeTypes = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItemLoonkost = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItemFeestdagen = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem
        Me.btnItemShiften = New DevComponents.DotNetBar.ButtonItem
        Me.ExplorerBarGroupItem4 = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.btnItemMachines = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItemMachinesPerFuncties = New DevComponents.DotNetBar.ButtonItem
        Me.cmdConfig = New DevComponents.DotNetBar.ButtonItem
        Me.navPanel = New DevComponents.DotNetBar.NavigationPanePanel
        Me.cmdButtonLoonPerFunctie = New DevComponents.DotNetBar.ButtonItem
        Me.NavigationPanePanel3 = New DevComponents.DotNetBar.NavigationPanePanel
        Me.panelFuncties = New DevComponents.DotNetBar.ExpandablePanel
        Me.treeFuncties = New System.Windows.Forms.TreeView
        Me.panelKosten = New DevComponents.DotNetBar.ExpandablePanel
        Me.treeKosten = New System.Windows.Forms.TreeView
        Me.cmdButtonPlanning = New DevComponents.DotNetBar.ButtonItem
        Me.NavigationPanePanel4 = New DevComponents.DotNetBar.NavigationPanePanel
        Me.ExplorerBar2 = New DevComponents.DotNetBar.ExplorerBar
        Me.TimekeepingGroup = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.cmdDagOverzicht = New DevComponents.DotNetBar.ButtonItem
        Me.ExplorerBarGroupItem5 = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.cmdKostenAnalyse = New DevComponents.DotNetBar.ButtonItem
        Me.cmdKosten = New DevComponents.DotNetBar.ButtonItem
        Me.ExplorerBarGroupItem6 = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.cmdAfwezigheden = New DevComponents.DotNetBar.ButtonItem
        Me.ExplorerBarGroupItem7 = New DevComponents.DotNetBar.ExplorerBarGroupItem
        Me.cmdOverzicht = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem
        Me.Timekeeping = New DevComponents.DotNetBar.ButtonItem
        Me.NavigationPanePanel1 = New DevComponents.DotNetBar.NavigationPanePanel
        Me.cmdButtonWerknemers = New DevComponents.DotNetBar.ButtonItem
        Me.TreekostenImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.treefunctieImagelist = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.SelectedVoyage = New System.Windows.Forms.ToolStripStatusLabel
        Me.Selectedfunction = New System.Windows.Forms.ToolStripStatusLabel
        Me.cmdRederijKosten = New DevComponents.DotNetBar.ButtonItem
        Me.NavigationPane1.SuspendLayout()
        Me.NavigationPanePanel2.SuspendLayout()
        CType(Me.ExplorerBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPanePanel3.SuspendLayout()
        Me.panelFuncties.SuspendLayout()
        Me.panelKosten.SuspendLayout()
        Me.NavigationPanePanel4.SuspendLayout()
        CType(Me.ExplorerBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavigationPane1
        '
        Me.NavigationPane1.Controls.Add(Me.NavigationPanePanel4)
        Me.NavigationPane1.Controls.Add(Me.navPanel)
        Me.NavigationPane1.Controls.Add(Me.NavigationPanePanel3)
        Me.NavigationPane1.Controls.Add(Me.NavigationPanePanel1)
        Me.NavigationPane1.Controls.Add(Me.NavigationPanePanel2)
        Me.NavigationPane1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavigationPane1.ItemPaddingBottom = 2
        Me.NavigationPane1.ItemPaddingTop = 2
        Me.NavigationPane1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdButtonLoonPerFunctie, Me.cmdButtonWerknemers, Me.cmdButtonPlanning, Me.cmdConfig, Me.Timekeeping})
        Me.NavigationPane1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.NavigationBarHeight = 207
        Me.NavigationPane1.Padding = New System.Windows.Forms.Padding(1)
        Me.NavigationPane1.Size = New System.Drawing.Size(184, 764)
        Me.NavigationPane1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.NavigationPane1.TabIndex = 9
        '
        '
        '
        Me.NavigationPane1.TitlePanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005
        Me.NavigationPane1.TitlePanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.NavigationPane1.TitlePanel.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NavigationPane1.TitlePanel.Location = New System.Drawing.Point(1, 1)
        Me.NavigationPane1.TitlePanel.Name = "panelTitle"
        Me.NavigationPane1.TitlePanel.Size = New System.Drawing.Size(182, 24)
        Me.NavigationPane1.TitlePanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.NavigationPane1.TitlePanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.NavigationPane1.TitlePanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.NavigationPane1.TitlePanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.NavigationPane1.TitlePanel.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.NavigationPane1.TitlePanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.NavigationPane1.TitlePanel.Style.GradientAngle = 90
        Me.NavigationPane1.TitlePanel.Style.MarginLeft = 4
        Me.NavigationPane1.TitlePanel.TabIndex = 0
        Me.NavigationPane1.TitlePanel.Text = "Timekeeping"
        '
        'NavigationPanePanel2
        '
        Me.NavigationPanePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.NavigationPanePanel2.Controls.Add(Me.ExplorerBar1)
        Me.NavigationPanePanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPanePanel2.Location = New System.Drawing.Point(1, 1)
        Me.NavigationPanePanel2.Name = "NavigationPanePanel2"
        Me.NavigationPanePanel2.ParentItem = Me.cmdConfig
        Me.NavigationPanePanel2.Size = New System.Drawing.Size(182, 762)
        Me.NavigationPanePanel2.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.NavigationPanePanel2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.NavigationPanePanel2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.NavigationPanePanel2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.NavigationPanePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.NavigationPanePanel2.Style.GradientAngle = 90
        Me.NavigationPanePanel2.TabIndex = 6
        '
        'ExplorerBar1
        '
        Me.ExplorerBar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar
        Me.ExplorerBar1.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.ExplorerBar1.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2
        Me.ExplorerBar1.BackStyle.BackColorGradientAngle = 90
        Me.ExplorerBar1.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground
        Me.ExplorerBar1.Cursor = System.Windows.Forms.Cursors.Default
        Me.ExplorerBar1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExplorerBar1.GroupImages = Nothing
        Me.ExplorerBar1.Groups.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.grpConfiguratie, Me.grpKosten, Me.ExplorerBarGroupItem3, Me.ExplorerBarGroupItem4})
        Me.ExplorerBar1.Images = Nothing
        Me.ExplorerBar1.Location = New System.Drawing.Point(0, 0)
        Me.ExplorerBar1.Name = "ExplorerBar1"
        Me.ExplorerBar1.Size = New System.Drawing.Size(182, 762)
        Me.ExplorerBar1.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.ExplorerBar1.TabIndex = 2
        Me.ExplorerBar1.Text = "ExplorerBar1"
        Me.ExplorerBar1.ThemeAware = True
        '
        'grpConfiguratie
        '
        '
        '
        '
        Me.grpConfiguratie.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.grpConfiguratie.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.grpConfiguratie.BackStyle.BorderBottomWidth = 1
        Me.grpConfiguratie.BackStyle.BorderColor = System.Drawing.Color.White
        Me.grpConfiguratie.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.grpConfiguratie.BackStyle.BorderLeftWidth = 1
        Me.grpConfiguratie.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.grpConfiguratie.BackStyle.BorderRightWidth = 1
        Me.grpConfiguratie.Cursor = System.Windows.Forms.Cursors.Default
        Me.grpConfiguratie.Expanded = True
        Me.grpConfiguratie.Name = "grpConfiguratie"
        Me.grpConfiguratie.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.grpConfiguratie.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdMail, Me.cmdMailActive})
        Me.grpConfiguratie.Text = "Configuratie"
        '
        '
        '
        Me.grpConfiguratie.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.grpConfiguratie.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.grpConfiguratie.TitleHotStyle.CornerDiameter = 3
        Me.grpConfiguratie.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpConfiguratie.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpConfiguratie.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.grpConfiguratie.TitleStyle.BackColor = System.Drawing.Color.White
        Me.grpConfiguratie.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.grpConfiguratie.TitleStyle.CornerDiameter = 3
        Me.grpConfiguratie.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpConfiguratie.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpConfiguratie.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'cmdMail
        '
        Me.cmdMail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdMail.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdMail.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdMail.HotFontUnderline = True
        Me.cmdMail.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdMail.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdMail.ImagePaddingHorizontal = 8
        Me.cmdMail.Name = "cmdMail"
        Me.cmdMail.Tag = "frmMailadressen"
        Me.cmdMail.Text = "Email adressen"
        '
        'cmdMailActive
        '
        Me.cmdMailActive.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdMailActive.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdMailActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdMailActive.HotFontUnderline = True
        Me.cmdMailActive.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdMailActive.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdMailActive.ImagePaddingHorizontal = 8
        Me.cmdMailActive.Name = "cmdMailActive"
        Me.cmdMailActive.Tag = "frmMailRule"
        Me.cmdMailActive.Text = "Mail activatie"
        '
        'grpKosten
        '
        '
        '
        '
        Me.grpKosten.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.grpKosten.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.grpKosten.BackStyle.BorderBottomWidth = 1
        Me.grpKosten.BackStyle.BorderColor = System.Drawing.Color.White
        Me.grpKosten.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.grpKosten.BackStyle.BorderLeftWidth = 1
        Me.grpKosten.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.grpKosten.BackStyle.BorderRightWidth = 1
        Me.grpKosten.Cursor = System.Windows.Forms.Cursors.Default
        Me.grpKosten.Expanded = True
        Me.grpKosten.Name = "grpKosten"
        Me.grpKosten.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.grpKosten.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItemRederijen, Me.ButtonItemTransportmiddel, Me.ButtonItemKostenplaats, Me.ButtonItem1})
        Me.grpKosten.Text = "Kosten"
        '
        '
        '
        Me.grpKosten.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.grpKosten.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.grpKosten.TitleHotStyle.CornerDiameter = 3
        Me.grpKosten.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpKosten.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpKosten.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.grpKosten.TitleStyle.BackColor = System.Drawing.Color.White
        Me.grpKosten.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.grpKosten.TitleStyle.CornerDiameter = 3
        Me.grpKosten.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpKosten.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.grpKosten.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'ButtonItemRederijen
        '
        Me.ButtonItemRederijen.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItemRederijen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItemRederijen.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItemRederijen.HotFontUnderline = True
        Me.ButtonItemRederijen.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItemRederijen.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItemRederijen.ImagePaddingHorizontal = 8
        Me.ButtonItemRederijen.Name = "ButtonItemRederijen"
        Me.ButtonItemRederijen.Tag = "frmRederij"
        Me.ButtonItemRederijen.Text = "Rederijen"
        '
        'ButtonItemTransportmiddel
        '
        Me.ButtonItemTransportmiddel.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItemTransportmiddel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItemTransportmiddel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItemTransportmiddel.HotFontUnderline = True
        Me.ButtonItemTransportmiddel.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItemTransportmiddel.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItemTransportmiddel.ImagePaddingHorizontal = 8
        Me.ButtonItemTransportmiddel.Name = "ButtonItemTransportmiddel"
        Me.ButtonItemTransportmiddel.Tag = "frmTransportMiddel"
        Me.ButtonItemTransportmiddel.Text = "Transportmiddelen"
        '
        'ButtonItemKostenplaats
        '
        Me.ButtonItemKostenplaats.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItemKostenplaats.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItemKostenplaats.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItemKostenplaats.HotFontUnderline = True
        Me.ButtonItemKostenplaats.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItemKostenplaats.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItemKostenplaats.ImagePaddingHorizontal = 8
        Me.ButtonItemKostenplaats.Name = "ButtonItemKostenplaats"
        Me.ButtonItemKostenplaats.Tag = "frmKostenplaats"
        Me.ButtonItemKostenplaats.Text = "Kostenplaats"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItem1.HotFontUnderline = True
        Me.ButtonItem1.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItem1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItem1.ImagePaddingHorizontal = 8
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Tag = "frmVoyage"
        Me.ButtonItem1.Text = "Voyages"
        '
        'ExplorerBarGroupItem3
        '
        '
        '
        '
        Me.ExplorerBarGroupItem3.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem3.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem3.BackStyle.BorderBottomWidth = 1
        Me.ExplorerBarGroupItem3.BackStyle.BorderColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem3.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem3.BackStyle.BorderLeftWidth = 1
        Me.ExplorerBarGroupItem3.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem3.BackStyle.BorderRightWidth = 1
        Me.ExplorerBarGroupItem3.Cursor = System.Windows.Forms.Cursors.Default
        Me.ExplorerBarGroupItem3.Expanded = True
        Me.ExplorerBarGroupItem3.Name = "ExplorerBarGroupItem3"
        Me.ExplorerBarGroupItem3.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.ExplorerBarGroupItem3.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnItemCategories, Me.btnItemFuncties, Me.btnItemEmployeeTypes, Me.ButtonItemLoonkost, Me.ButtonItemFeestdagen, Me.ButtonItem2, Me.btnItemShiften})
        Me.ExplorerBarGroupItem3.Text = "Werknemers"
        '
        '
        '
        Me.ExplorerBarGroupItem3.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem3.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem3.TitleHotStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem3.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem3.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem3.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.ExplorerBarGroupItem3.TitleStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem3.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem3.TitleStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem3.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem3.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem3.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'btnItemCategories
        '
        Me.btnItemCategories.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnItemCategories.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnItemCategories.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.btnItemCategories.HotFontUnderline = True
        Me.btnItemCategories.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnItemCategories.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.btnItemCategories.ImagePaddingHorizontal = 8
        Me.btnItemCategories.Name = "btnItemCategories"
        Me.btnItemCategories.Tag = "frmCategory"
        Me.btnItemCategories.Text = "Categories"
        '
        'btnItemFuncties
        '
        Me.btnItemFuncties.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnItemFuncties.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnItemFuncties.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.btnItemFuncties.HotFontUnderline = True
        Me.btnItemFuncties.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnItemFuncties.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.btnItemFuncties.ImagePaddingHorizontal = 8
        Me.btnItemFuncties.Name = "btnItemFuncties"
        Me.btnItemFuncties.Tag = "frmFunctions"
        Me.btnItemFuncties.Text = "Functies"
        '
        'btnItemEmployeeTypes
        '
        Me.btnItemEmployeeTypes.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnItemEmployeeTypes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnItemEmployeeTypes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.btnItemEmployeeTypes.HotFontUnderline = True
        Me.btnItemEmployeeTypes.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnItemEmployeeTypes.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.btnItemEmployeeTypes.ImagePaddingHorizontal = 8
        Me.btnItemEmployeeTypes.Name = "btnItemEmployeeTypes"
        Me.btnItemEmployeeTypes.Tag = "frmEmployeeTypes"
        Me.btnItemEmployeeTypes.Text = "Type werknemers"
        '
        'ButtonItemLoonkost
        '
        Me.ButtonItemLoonkost.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItemLoonkost.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItemLoonkost.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItemLoonkost.HotFontUnderline = True
        Me.ButtonItemLoonkost.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItemLoonkost.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItemLoonkost.ImagePaddingHorizontal = 8
        Me.ButtonItemLoonkost.Name = "ButtonItemLoonkost"
        Me.ButtonItemLoonkost.Tag = "frmLoonkost"
        Me.ButtonItemLoonkost.Text = "Loonkost"
        '
        'ButtonItemFeestdagen
        '
        Me.ButtonItemFeestdagen.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItemFeestdagen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItemFeestdagen.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItemFeestdagen.HotFontUnderline = True
        Me.ButtonItemFeestdagen.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItemFeestdagen.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItemFeestdagen.ImagePaddingHorizontal = 8
        Me.ButtonItemFeestdagen.Name = "ButtonItemFeestdagen"
        Me.ButtonItemFeestdagen.Tag = "frmFeestdagen"
        Me.ButtonItemFeestdagen.Text = "Feestdagen"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItem2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItem2.HotFontUnderline = True
        Me.ButtonItem2.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItem2.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItem2.ImagePaddingHorizontal = 8
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.Tag = "frmLeeftijdcoefficient"
        Me.ButtonItem2.Text = "Leeftijdco�ffici�nt"
        '
        'btnItemShiften
        '
        Me.btnItemShiften.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnItemShiften.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnItemShiften.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.btnItemShiften.HotFontUnderline = True
        Me.btnItemShiften.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnItemShiften.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.btnItemShiften.ImagePaddingHorizontal = 8
        Me.btnItemShiften.Name = "btnItemShiften"
        Me.btnItemShiften.Tag = "frmShifts"
        Me.btnItemShiften.Text = "Shiften"
        '
        'ExplorerBarGroupItem4
        '
        '
        '
        '
        Me.ExplorerBarGroupItem4.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem4.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem4.BackStyle.BorderBottomWidth = 1
        Me.ExplorerBarGroupItem4.BackStyle.BorderColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem4.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem4.BackStyle.BorderLeftWidth = 1
        Me.ExplorerBarGroupItem4.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem4.BackStyle.BorderRightWidth = 1
        Me.ExplorerBarGroupItem4.Cursor = System.Windows.Forms.Cursors.Default
        Me.ExplorerBarGroupItem4.Expanded = True
        Me.ExplorerBarGroupItem4.Name = "ExplorerBarGroupItem4"
        Me.ExplorerBarGroupItem4.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.ExplorerBarGroupItem4.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnItemMachines, Me.ButtonItemMachinesPerFuncties})
        Me.ExplorerBarGroupItem4.Text = "Machines"
        '
        '
        '
        Me.ExplorerBarGroupItem4.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem4.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem4.TitleHotStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem4.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem4.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem4.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.ExplorerBarGroupItem4.TitleStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem4.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem4.TitleStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem4.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem4.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem4.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'btnItemMachines
        '
        Me.btnItemMachines.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnItemMachines.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnItemMachines.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.btnItemMachines.HotFontUnderline = True
        Me.btnItemMachines.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnItemMachines.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.btnItemMachines.ImagePaddingHorizontal = 8
        Me.btnItemMachines.Name = "btnItemMachines"
        Me.btnItemMachines.Tag = "frmMachines"
        Me.btnItemMachines.Text = "Machines"
        '
        'ButtonItemMachinesPerFuncties
        '
        Me.ButtonItemMachinesPerFuncties.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItemMachinesPerFuncties.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItemMachinesPerFuncties.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItemMachinesPerFuncties.HotFontUnderline = True
        Me.ButtonItemMachinesPerFuncties.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItemMachinesPerFuncties.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItemMachinesPerFuncties.ImagePaddingHorizontal = 8
        Me.ButtonItemMachinesPerFuncties.Name = "ButtonItemMachinesPerFuncties"
        Me.ButtonItemMachinesPerFuncties.Tag = "frmMachinesPerFunctie"
        Me.ButtonItemMachinesPerFuncties.Text = "Machines per functie"
        '
        'cmdConfig
        '
        Me.cmdConfig.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdConfig.Image = Global.IMT_Timekeeping.My.Resources.Resources.Gear
        Me.cmdConfig.ImagePaddingHorizontal = 8
        Me.cmdConfig.Name = "cmdConfig"
        Me.cmdConfig.OptionGroup = "navBar"
        Me.cmdConfig.Text = "Configuratie"
        '
        'navPanel
        '
        Me.navPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.navPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.navPanel.Location = New System.Drawing.Point(1, 1)
        Me.navPanel.Name = "navPanel"
        Me.navPanel.ParentItem = Me.cmdButtonLoonPerFunctie
        Me.navPanel.Size = New System.Drawing.Size(182, 555)
        Me.navPanel.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.navPanel.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.navPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.navPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.navPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.navPanel.Style.GradientAngle = 90
        Me.navPanel.TabIndex = 2
        '
        'cmdButtonLoonPerFunctie
        '
        Me.cmdButtonLoonPerFunctie.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdButtonLoonPerFunctie.Image = Global.IMT_Timekeeping.My.Resources.Resources.Categories
        Me.cmdButtonLoonPerFunctie.ImagePaddingHorizontal = 8
        Me.cmdButtonLoonPerFunctie.Name = "cmdButtonLoonPerFunctie"
        Me.cmdButtonLoonPerFunctie.OptionGroup = "navBar"
        Me.cmdButtonLoonPerFunctie.Text = "Loon per functie"
        '
        'NavigationPanePanel3
        '
        Me.NavigationPanePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.NavigationPanePanel3.Controls.Add(Me.panelFuncties)
        Me.NavigationPanePanel3.Controls.Add(Me.panelKosten)
        Me.NavigationPanePanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPanePanel3.Location = New System.Drawing.Point(1, 1)
        Me.NavigationPanePanel3.Name = "NavigationPanePanel3"
        Me.NavigationPanePanel3.ParentItem = Me.cmdButtonPlanning
        Me.NavigationPanePanel3.Size = New System.Drawing.Size(182, 555)
        Me.NavigationPanePanel3.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.NavigationPanePanel3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.NavigationPanePanel3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.NavigationPanePanel3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.NavigationPanePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.NavigationPanePanel3.Style.GradientAngle = 90
        Me.NavigationPanePanel3.TabIndex = 5
        '
        'panelFuncties
        '
        Me.panelFuncties.CanvasColor = System.Drawing.SystemColors.Control
        Me.panelFuncties.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.panelFuncties.Controls.Add(Me.treeFuncties)
        Me.panelFuncties.ExpandButtonVisible = False
        Me.panelFuncties.Location = New System.Drawing.Point(4, 220)
        Me.panelFuncties.Name = "panelFuncties"
        Me.panelFuncties.Size = New System.Drawing.Size(147, 100)
        Me.panelFuncties.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.panelFuncties.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.panelFuncties.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.panelFuncties.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.panelFuncties.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.panelFuncties.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.panelFuncties.Style.GradientAngle = 90
        Me.panelFuncties.TabIndex = 6
        Me.panelFuncties.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.panelFuncties.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.panelFuncties.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.panelFuncties.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.panelFuncties.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.panelFuncties.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.panelFuncties.TitleStyle.GradientAngle = 90
        Me.panelFuncties.TitleText = "Functies"
        '
        'treeFuncties
        '
        Me.treeFuncties.Dock = System.Windows.Forms.DockStyle.Fill
        Me.treeFuncties.Location = New System.Drawing.Point(0, 26)
        Me.treeFuncties.Name = "treeFuncties"
        Me.treeFuncties.Size = New System.Drawing.Size(147, 74)
        Me.treeFuncties.TabIndex = 2
        '
        'panelKosten
        '
        Me.panelKosten.CanvasColor = System.Drawing.SystemColors.Control
        Me.panelKosten.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.panelKosten.Controls.Add(Me.treeKosten)
        Me.panelKosten.ExpandButtonVisible = False
        Me.panelKosten.Location = New System.Drawing.Point(4, 8)
        Me.panelKosten.Name = "panelKosten"
        Me.panelKosten.Size = New System.Drawing.Size(147, 100)
        Me.panelKosten.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.panelKosten.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.panelKosten.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.panelKosten.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.panelKosten.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.panelKosten.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.panelKosten.Style.GradientAngle = 90
        Me.panelKosten.TabIndex = 5
        Me.panelKosten.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.panelKosten.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.panelKosten.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.panelKosten.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.panelKosten.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.panelKosten.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.panelKosten.TitleStyle.GradientAngle = 90
        Me.panelKosten.TitleText = "Kostenplaats"
        '
        'treeKosten
        '
        Me.treeKosten.Dock = System.Windows.Forms.DockStyle.Fill
        Me.treeKosten.Location = New System.Drawing.Point(0, 26)
        Me.treeKosten.Name = "treeKosten"
        Me.treeKosten.Size = New System.Drawing.Size(147, 74)
        Me.treeKosten.TabIndex = 1
        '
        'cmdButtonPlanning
        '
        Me.cmdButtonPlanning.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdButtonPlanning.Image = Global.IMT_Timekeeping.My.Resources.Resources.DateTime
        Me.cmdButtonPlanning.ImagePaddingHorizontal = 8
        Me.cmdButtonPlanning.Name = "cmdButtonPlanning"
        Me.cmdButtonPlanning.OptionGroup = "navBar"
        Me.cmdButtonPlanning.Text = "Planning"
        '
        'NavigationPanePanel4
        '
        Me.NavigationPanePanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.NavigationPanePanel4.Controls.Add(Me.ExplorerBar2)
        Me.NavigationPanePanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPanePanel4.Location = New System.Drawing.Point(1, 25)
        Me.NavigationPanePanel4.Name = "NavigationPanePanel4"
        Me.NavigationPanePanel4.ParentItem = Me.Timekeeping
        Me.NavigationPanePanel4.Size = New System.Drawing.Size(182, 531)
        Me.NavigationPanePanel4.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.NavigationPanePanel4.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.NavigationPanePanel4.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.NavigationPanePanel4.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.NavigationPanePanel4.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.NavigationPanePanel4.Style.GradientAngle = 90
        Me.NavigationPanePanel4.TabIndex = 7
        '
        'ExplorerBar2
        '
        Me.ExplorerBar2.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar
        Me.ExplorerBar2.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.ExplorerBar2.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2
        Me.ExplorerBar2.BackStyle.BackColorGradientAngle = 90
        Me.ExplorerBar2.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground
        Me.ExplorerBar2.Cursor = System.Windows.Forms.Cursors.Default
        Me.ExplorerBar2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExplorerBar2.GroupImages = Nothing
        Me.ExplorerBar2.Groups.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.TimekeepingGroup, Me.ExplorerBarGroupItem5, Me.ExplorerBarGroupItem6, Me.ExplorerBarGroupItem7})
        Me.ExplorerBar2.Images = Nothing
        Me.ExplorerBar2.Location = New System.Drawing.Point(0, 0)
        Me.ExplorerBar2.Name = "ExplorerBar2"
        Me.ExplorerBar2.Size = New System.Drawing.Size(182, 531)
        Me.ExplorerBar2.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.ExplorerBar2.TabIndex = 0
        Me.ExplorerBar2.Text = "Kosten"
        Me.ExplorerBar2.ThemeAware = True
        '
        'TimekeepingGroup
        '
        '
        '
        '
        Me.TimekeepingGroup.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TimekeepingGroup.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.TimekeepingGroup.BackStyle.BorderBottomWidth = 1
        Me.TimekeepingGroup.BackStyle.BorderColor = System.Drawing.Color.White
        Me.TimekeepingGroup.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.TimekeepingGroup.BackStyle.BorderLeftWidth = 1
        Me.TimekeepingGroup.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.TimekeepingGroup.BackStyle.BorderRightWidth = 1
        Me.TimekeepingGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.TimekeepingGroup.Expanded = True
        Me.TimekeepingGroup.Name = "TimekeepingGroup"
        Me.TimekeepingGroup.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.TimekeepingGroup.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdDagOverzicht})
        Me.TimekeepingGroup.Text = "Timekeeping"
        '
        '
        '
        Me.TimekeepingGroup.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.TimekeepingGroup.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TimekeepingGroup.TitleHotStyle.CornerDiameter = 3
        Me.TimekeepingGroup.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.TimekeepingGroup.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.TimekeepingGroup.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.TimekeepingGroup.TitleStyle.BackColor = System.Drawing.Color.White
        Me.TimekeepingGroup.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TimekeepingGroup.TitleStyle.CornerDiameter = 3
        Me.TimekeepingGroup.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.TimekeepingGroup.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.TimekeepingGroup.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'cmdDagOverzicht
        '
        Me.cmdDagOverzicht.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdDagOverzicht.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdDagOverzicht.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdDagOverzicht.HotFontUnderline = True
        Me.cmdDagOverzicht.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdDagOverzicht.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdDagOverzicht.ImagePaddingHorizontal = 8
        Me.cmdDagOverzicht.Name = "cmdDagOverzicht"
        Me.cmdDagOverzicht.Text = "Validatie"
        '
        'ExplorerBarGroupItem5
        '
        '
        '
        '
        Me.ExplorerBarGroupItem5.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem5.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem5.BackStyle.BorderBottomWidth = 1
        Me.ExplorerBarGroupItem5.BackStyle.BorderColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem5.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem5.BackStyle.BorderLeftWidth = 1
        Me.ExplorerBarGroupItem5.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem5.BackStyle.BorderRightWidth = 1
        Me.ExplorerBarGroupItem5.Cursor = System.Windows.Forms.Cursors.Default
        Me.ExplorerBarGroupItem5.Expanded = True
        Me.ExplorerBarGroupItem5.Name = "ExplorerBarGroupItem5"
        Me.ExplorerBarGroupItem5.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.ExplorerBarGroupItem5.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdKostenAnalyse, Me.cmdKosten, Me.cmdRederijKosten})
        Me.ExplorerBarGroupItem5.Text = "Kosten"
        '
        '
        '
        Me.ExplorerBarGroupItem5.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem5.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem5.TitleHotStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem5.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem5.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem5.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.ExplorerBarGroupItem5.TitleStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem5.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem5.TitleStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem5.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem5.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem5.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'cmdKostenAnalyse
        '
        Me.cmdKostenAnalyse.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdKostenAnalyse.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdKostenAnalyse.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdKostenAnalyse.HotFontUnderline = True
        Me.cmdKostenAnalyse.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdKostenAnalyse.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdKostenAnalyse.ImagePaddingHorizontal = 8
        Me.cmdKostenAnalyse.Name = "cmdKostenAnalyse"
        Me.cmdKostenAnalyse.Text = "Herverdeel prestaties"
        '
        'cmdKosten
        '
        Me.cmdKosten.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdKosten.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdKosten.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdKosten.HotFontUnderline = True
        Me.cmdKosten.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdKosten.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdKosten.ImagePaddingHorizontal = 8
        Me.cmdKosten.Name = "cmdKosten"
        Me.cmdKosten.Text = "Kostenanalyse"
        '
        'ExplorerBarGroupItem6
        '
        '
        '
        '
        Me.ExplorerBarGroupItem6.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem6.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem6.BackStyle.BorderBottomWidth = 1
        Me.ExplorerBarGroupItem6.BackStyle.BorderColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem6.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem6.BackStyle.BorderLeftWidth = 1
        Me.ExplorerBarGroupItem6.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem6.BackStyle.BorderRightWidth = 1
        Me.ExplorerBarGroupItem6.Cursor = System.Windows.Forms.Cursors.Default
        Me.ExplorerBarGroupItem6.Expanded = True
        Me.ExplorerBarGroupItem6.Name = "ExplorerBarGroupItem6"
        Me.ExplorerBarGroupItem6.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.ExplorerBarGroupItem6.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdAfwezigheden})
        Me.ExplorerBarGroupItem6.Text = "Overzichten"
        '
        '
        '
        Me.ExplorerBarGroupItem6.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem6.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem6.TitleHotStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem6.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem6.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem6.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.ExplorerBarGroupItem6.TitleStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem6.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem6.TitleStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem6.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem6.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem6.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'cmdAfwezigheden
        '
        Me.cmdAfwezigheden.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdAfwezigheden.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdAfwezigheden.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdAfwezigheden.HotFontUnderline = True
        Me.cmdAfwezigheden.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdAfwezigheden.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdAfwezigheden.ImagePaddingHorizontal = 8
        Me.cmdAfwezigheden.Name = "cmdAfwezigheden"
        Me.cmdAfwezigheden.Tag = "frmResetAfwezigheden"
        Me.cmdAfwezigheden.Text = "Afwezigheden"
        '
        'ExplorerBarGroupItem7
        '
        '
        '
        '
        Me.ExplorerBarGroupItem7.BackStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem7.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem7.BackStyle.BorderBottomWidth = 1
        Me.ExplorerBarGroupItem7.BackStyle.BorderColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem7.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem7.BackStyle.BorderLeftWidth = 1
        Me.ExplorerBarGroupItem7.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ExplorerBarGroupItem7.BackStyle.BorderRightWidth = 1
        Me.ExplorerBarGroupItem7.Cursor = System.Windows.Forms.Cursors.Default
        Me.ExplorerBarGroupItem7.Expanded = True
        Me.ExplorerBarGroupItem7.Name = "ExplorerBarGroupItem7"
        Me.ExplorerBarGroupItem7.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors
        Me.ExplorerBarGroupItem7.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOverzicht, Me.ButtonItem3})
        Me.ExplorerBarGroupItem7.Text = "CEPA"
        '
        '
        '
        Me.ExplorerBarGroupItem7.TitleHotStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem7.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem7.TitleHotStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem7.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem7.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem7.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.ExplorerBarGroupItem7.TitleStyle.BackColor = System.Drawing.Color.White
        Me.ExplorerBarGroupItem7.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ExplorerBarGroupItem7.TitleStyle.CornerDiameter = 3
        Me.ExplorerBarGroupItem7.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem7.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.ExplorerBarGroupItem7.TitleStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        '
        'cmdOverzicht
        '
        Me.cmdOverzicht.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdOverzicht.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOverzicht.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdOverzicht.HotFontUnderline = True
        Me.cmdOverzicht.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdOverzicht.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdOverzicht.ImagePaddingHorizontal = 8
        Me.cmdOverzicht.Name = "cmdOverzicht"
        Me.cmdOverzicht.Text = "Overzicht verloning"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonItem3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.ButtonItem3.HotFontUnderline = True
        Me.ButtonItem3.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItem3.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.ButtonItem3.ImagePaddingHorizontal = 8
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.Text = "Invoer loon spaarplanners"
        '
        'Timekeeping
        '
        Me.Timekeeping.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.Timekeeping.Checked = True
        Me.Timekeeping.Image = Global.IMT_Timekeeping.My.Resources.Resources.Clock
        Me.Timekeeping.ImagePaddingHorizontal = 8
        Me.Timekeeping.Name = "Timekeeping"
        Me.Timekeeping.OptionGroup = "navBar"
        Me.Timekeeping.Text = "Timekeeping"
        '
        'NavigationPanePanel1
        '
        Me.NavigationPanePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.NavigationPanePanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPanePanel1.Location = New System.Drawing.Point(1, 1)
        Me.NavigationPanePanel1.Name = "NavigationPanePanel1"
        Me.NavigationPanePanel1.ParentItem = Me.cmdButtonWerknemers
        Me.NavigationPanePanel1.Size = New System.Drawing.Size(182, 555)
        Me.NavigationPanePanel1.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.NavigationPanePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.NavigationPanePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.NavigationPanePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.NavigationPanePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.NavigationPanePanel1.Style.GradientAngle = 90
        Me.NavigationPanePanel1.TabIndex = 3
        '
        'cmdButtonWerknemers
        '
        Me.cmdButtonWerknemers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdButtonWerknemers.Image = Global.IMT_Timekeeping.My.Resources.Resources.User3
        Me.cmdButtonWerknemers.ImagePaddingHorizontal = 8
        Me.cmdButtonWerknemers.Name = "cmdButtonWerknemers"
        Me.cmdButtonWerknemers.OptionGroup = "navBar"
        Me.cmdButtonWerknemers.Text = "Werknemers"
        '
        'TreekostenImageList
        '
        Me.TreekostenImageList.ImageStream = CType(resources.GetObject("TreekostenImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.TreekostenImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.TreekostenImageList.Images.SetKeyName(0, "Account.png")
        Me.TreekostenImageList.Images.SetKeyName(1, "Boat.png")
        Me.TreekostenImageList.Images.SetKeyName(2, "CheckMark.png")
        '
        'treefunctieImagelist
        '
        Me.treefunctieImagelist.ImageStream = CType(resources.GetObject("treefunctieImagelist.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.treefunctieImagelist.TransparentColor = System.Drawing.Color.Transparent
        Me.treefunctieImagelist.Images.SetKeyName(0, "ProjectTracking.png")
        Me.treefunctieImagelist.Images.SetKeyName(1, "CheckMark.png")
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SelectedVoyage, Me.Selectedfunction})
        Me.StatusStrip1.Location = New System.Drawing.Point(184, 742)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(820, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'SelectedVoyage
        '
        Me.SelectedVoyage.Name = "SelectedVoyage"
        Me.SelectedVoyage.Padding = New System.Windows.Forms.Padding(100, 0, 0, 0)
        Me.SelectedVoyage.Size = New System.Drawing.Size(100, 17)
        '
        'Selectedfunction
        '
        Me.Selectedfunction.Name = "Selectedfunction"
        Me.Selectedfunction.Padding = New System.Windows.Forms.Padding(150, 0, 0, 0)
        Me.Selectedfunction.Size = New System.Drawing.Size(150, 17)
        '
        'cmdRederijKosten
        '
        Me.cmdRederijKosten.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdRederijKosten.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdRederijKosten.ForeColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(198, Byte), Integer))
        Me.cmdRederijKosten.HotFontUnderline = True
        Me.cmdRederijKosten.HotForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cmdRederijKosten.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None
        Me.cmdRederijKosten.ImagePaddingHorizontal = 8
        Me.cmdRederijKosten.Name = "cmdRederijKosten"
        Me.cmdRederijKosten.Text = "Kosten per rederij"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1004, 764)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.NavigationPane1)
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.IsMdiContainer = True
        Me.Name = "frmMain"
        Me.Text = "Hoofdscherm"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.NavigationPane1.ResumeLayout(False)
        Me.NavigationPanePanel2.ResumeLayout(False)
        CType(Me.ExplorerBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPanePanel3.ResumeLayout(False)
        Me.panelFuncties.ResumeLayout(False)
        Me.panelKosten.ResumeLayout(False)
        Me.NavigationPanePanel4.ResumeLayout(False)
        CType(Me.ExplorerBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents NavigationPane1 As DevComponents.DotNetBar.NavigationPane
    Friend WithEvents navPanel As DevComponents.DotNetBar.NavigationPanePanel
    Friend WithEvents cmdButtonLoonPerFunctie As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents NavigationPanePanel1 As DevComponents.DotNetBar.NavigationPanePanel
    Friend WithEvents cmdButtonWerknemers As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents NavigationPanePanel3 As DevComponents.DotNetBar.NavigationPanePanel
    Friend WithEvents cmdButtonPlanning As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents panelFuncties As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents panelKosten As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents treeKosten As System.Windows.Forms.TreeView
    Friend WithEvents treeFuncties As System.Windows.Forms.TreeView
    Friend WithEvents TreekostenImageList As System.Windows.Forms.ImageList
    Friend WithEvents treefunctieImagelist As System.Windows.Forms.ImageList
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents SelectedVoyage As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Selectedfunction As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents NavigationPanePanel2 As DevComponents.DotNetBar.NavigationPanePanel
    Friend WithEvents cmdConfig As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ExplorerBar1 As DevComponents.DotNetBar.ExplorerBar
    Friend WithEvents grpConfiguratie As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents btnItemCategories As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnItemFuncties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnItemShiften As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnItemEmployeeTypes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnItemMachines As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItemMachinesPerFuncties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItemFeestdagen As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItemLoonkost As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItemKostenplaats As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItemRederijen As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItemTransportmiddel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents NavigationPanePanel4 As DevComponents.DotNetBar.NavigationPanePanel
    Friend WithEvents Timekeeping As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ExplorerBar2 As DevComponents.DotNetBar.ExplorerBar
    Friend WithEvents TimekeepingGroup As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents cmdDagOverzicht As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdMail As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdMailActive As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdAfwezigheden As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdKostenAnalyse As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents grpKosten As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents ExplorerBarGroupItem3 As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents ExplorerBarGroupItem4 As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents ExplorerBarGroupItem5 As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents ExplorerBarGroupItem6 As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents cmdOverzicht As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ExplorerBarGroupItem7 As DevComponents.DotNetBar.ExplorerBarGroupItem
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdKosten As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdRederijKosten As DevComponents.DotNetBar.ButtonItem

End Class
