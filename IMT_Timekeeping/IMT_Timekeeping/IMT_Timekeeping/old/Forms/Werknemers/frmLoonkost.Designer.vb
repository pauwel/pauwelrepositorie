<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoonkost
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ToolStripTypeWerknemers = New System.Windows.Forms.ToolStrip
        Me.cmdAdd = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdEdit = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdUpdate = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtRSZ = New NumericBox.NumericBox
        Me.txtVakopl = New NumericBox.NumericBox
        Me.txtCEPA = New NumericBox.NumericBox
        Me.txtSIWHA = New NumericBox.NumericBox
        Me.txtPreventie = New NumericBox.NumericBox
        Me.txtSpecFonds = New NumericBox.NumericBox
        Me.txtStructuur = New NumericBox.NumericBox
        Me.txtHVD = New NumericBox.NumericBox
        Me.txtPvorming = New NumericBox.NumericBox
        Me.txtBasis = New NumericBox.NumericBox
        Me.txtSanering = New NumericBox.NumericBox
        Me.txtRSZjaar = New NumericBox.NumericBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.dgvLoonkost = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.txtWalking = New NumericBox.NumericBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtPremie = New NumericBox.NumericBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cmbWNTYPE = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.ToolStripTypeWerknemers.SuspendLayout()
        CType(Me.dgvLoonkost, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStripTypeWerknemers
        '
        Me.ToolStripTypeWerknemers.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAdd, Me.ToolStripSeparator1, Me.cmdEdit, Me.ToolStripSeparator2, Me.cmdUpdate, Me.ToolStripSeparator3, Me.cmdDelete})
        Me.ToolStripTypeWerknemers.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripTypeWerknemers.Name = "ToolStripTypeWerknemers"
        Me.ToolStripTypeWerknemers.Size = New System.Drawing.Size(976, 25)
        Me.ToolStripTypeWerknemers.TabIndex = 12
        Me.ToolStripTypeWerknemers.Text = "ToolStrip1"
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = Global.IMT_Timekeeping.My.Resources.Resources.AddTableHS
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(49, 22)
        Me.cmdAdd.Text = "Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'cmdEdit
        '
        Me.cmdEdit.Image = Global.IMT_Timekeeping.My.Resources.Resources.EditInformationHS
        Me.cmdEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(47, 22)
        Me.cmdEdit.Text = "Edit"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdUpdate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(65, 22)
        Me.cmdUpdate.Text = "Update"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        Me.cmdDelete.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Werknemer Type"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(259, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "RSZ"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(322, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "vakopl."
        '
        'txtRSZ
        '
        Me.txtRSZ.BackColor = System.Drawing.SystemColors.Window
        Me.txtRSZ.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtRSZ.Location = New System.Drawing.Point(262, 66)
        Me.txtRSZ.Name = "txtRSZ"
        Me.txtRSZ.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtRSZ.Size = New System.Drawing.Size(54, 20)
        Me.txtRSZ.TabIndex = 16
        Me.txtRSZ.Text = "0,00"
        '
        'txtVakopl
        '
        Me.txtVakopl.BackColor = System.Drawing.SystemColors.Window
        Me.txtVakopl.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtVakopl.Location = New System.Drawing.Point(322, 67)
        Me.txtVakopl.Name = "txtVakopl"
        Me.txtVakopl.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtVakopl.Size = New System.Drawing.Size(54, 20)
        Me.txtVakopl.TabIndex = 17
        Me.txtVakopl.Text = "0,00"
        '
        'txtCEPA
        '
        Me.txtCEPA.BackColor = System.Drawing.SystemColors.Window
        Me.txtCEPA.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtCEPA.Location = New System.Drawing.Point(382, 67)
        Me.txtCEPA.Name = "txtCEPA"
        Me.txtCEPA.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtCEPA.Size = New System.Drawing.Size(54, 20)
        Me.txtCEPA.TabIndex = 18
        Me.txtCEPA.Text = "0,00"
        '
        'txtSIWHA
        '
        Me.txtSIWHA.BackColor = System.Drawing.SystemColors.Window
        Me.txtSIWHA.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSIWHA.Location = New System.Drawing.Point(442, 67)
        Me.txtSIWHA.Name = "txtSIWHA"
        Me.txtSIWHA.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSIWHA.Size = New System.Drawing.Size(54, 20)
        Me.txtSIWHA.TabIndex = 19
        Me.txtSIWHA.Text = "0,00"
        '
        'txtPreventie
        '
        Me.txtPreventie.BackColor = System.Drawing.SystemColors.Window
        Me.txtPreventie.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtPreventie.Location = New System.Drawing.Point(16, 119)
        Me.txtPreventie.Name = "txtPreventie"
        Me.txtPreventie.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtPreventie.Size = New System.Drawing.Size(54, 20)
        Me.txtPreventie.TabIndex = 20
        Me.txtPreventie.Text = "0,00"
        '
        'txtSpecFonds
        '
        Me.txtSpecFonds.BackColor = System.Drawing.SystemColors.Window
        Me.txtSpecFonds.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSpecFonds.Location = New System.Drawing.Point(76, 119)
        Me.txtSpecFonds.Name = "txtSpecFonds"
        Me.txtSpecFonds.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSpecFonds.Size = New System.Drawing.Size(54, 20)
        Me.txtSpecFonds.TabIndex = 21
        Me.txtSpecFonds.Text = "0,00"
        '
        'txtStructuur
        '
        Me.txtStructuur.BackColor = System.Drawing.SystemColors.Window
        Me.txtStructuur.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtStructuur.Location = New System.Drawing.Point(140, 119)
        Me.txtStructuur.Name = "txtStructuur"
        Me.txtStructuur.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtStructuur.Size = New System.Drawing.Size(54, 20)
        Me.txtStructuur.TabIndex = 22
        Me.txtStructuur.Text = "0,00"
        '
        'txtHVD
        '
        Me.txtHVD.BackColor = System.Drawing.SystemColors.Window
        Me.txtHVD.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtHVD.Location = New System.Drawing.Point(202, 119)
        Me.txtHVD.Name = "txtHVD"
        Me.txtHVD.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtHVD.Size = New System.Drawing.Size(54, 20)
        Me.txtHVD.TabIndex = 23
        Me.txtHVD.Text = "0,00"
        '
        'txtPvorming
        '
        Me.txtPvorming.BackColor = System.Drawing.SystemColors.Window
        Me.txtPvorming.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtPvorming.Location = New System.Drawing.Point(262, 119)
        Me.txtPvorming.Name = "txtPvorming"
        Me.txtPvorming.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtPvorming.Size = New System.Drawing.Size(54, 20)
        Me.txtPvorming.TabIndex = 24
        Me.txtPvorming.Text = "0,00"
        '
        'txtBasis
        '
        Me.txtBasis.BackColor = System.Drawing.SystemColors.Window
        Me.txtBasis.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtBasis.Location = New System.Drawing.Point(322, 119)
        Me.txtBasis.Name = "txtBasis"
        Me.txtBasis.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtBasis.Size = New System.Drawing.Size(54, 20)
        Me.txtBasis.TabIndex = 25
        Me.txtBasis.Text = "0,00"
        '
        'txtSanering
        '
        Me.txtSanering.BackColor = System.Drawing.SystemColors.Window
        Me.txtSanering.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSanering.Location = New System.Drawing.Point(382, 119)
        Me.txtSanering.Name = "txtSanering"
        Me.txtSanering.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSanering.Size = New System.Drawing.Size(54, 20)
        Me.txtSanering.TabIndex = 26
        Me.txtSanering.Text = "0,00"
        '
        'txtRSZjaar
        '
        Me.txtRSZjaar.BackColor = System.Drawing.SystemColors.Window
        Me.txtRSZjaar.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtRSZjaar.Location = New System.Drawing.Point(442, 119)
        Me.txtRSZjaar.Name = "txtRSZjaar"
        Me.txtRSZjaar.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtRSZjaar.Size = New System.Drawing.Size(54, 20)
        Me.txtRSZjaar.TabIndex = 27
        Me.txtRSZjaar.Text = "0,00"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(379, 51)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "CEPA"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(439, 51)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "SIWHA"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(13, 103)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 13)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "preventie"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(73, 103)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Spec.fonds"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(137, 103)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 13)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "structuur"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(202, 103)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(30, 13)
        Me.Label9.TabIndex = 33
        Me.Label9.Text = "HVD"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(262, 103)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 13)
        Me.Label10.TabIndex = 34
        Me.Label10.Text = "P.vorming"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(319, 103)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "basis"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(382, 103)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 36
        Me.Label12.Text = "sanering"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(439, 103)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 13)
        Me.Label13.TabIndex = 37
        Me.Label13.Text = "RSZ/jaar"
        '
        'dgvLoonkost
        '
        Me.dgvLoonkost.AllowUserToAddRows = False
        Me.dgvLoonkost.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLoonkost.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        Me.dgvLoonkost.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLoonkost.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLoonkost.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvLoonkost.Location = New System.Drawing.Point(-1, 145)
        Me.dgvLoonkost.Name = "dgvLoonkost"
        Me.dgvLoonkost.ReadOnly = True
        Me.dgvLoonkost.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoonkost.Size = New System.Drawing.Size(977, 171)
        Me.dgvLoonkost.TabIndex = 38
        '
        'txtWalking
        '
        Me.txtWalking.BackColor = System.Drawing.SystemColors.Window
        Me.txtWalking.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtWalking.Location = New System.Drawing.Point(140, 66)
        Me.txtWalking.Name = "txtWalking"
        Me.txtWalking.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtWalking.Size = New System.Drawing.Size(54, 20)
        Me.txtWalking.TabIndex = 40
        Me.txtWalking.Text = "0,00"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(137, 51)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(46, 13)
        Me.Label14.TabIndex = 39
        Me.Label14.Text = "Walking"
        '
        'txtPremie
        '
        Me.txtPremie.BackColor = System.Drawing.SystemColors.Window
        Me.txtPremie.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtPremie.Location = New System.Drawing.Point(200, 67)
        Me.txtPremie.Name = "txtPremie"
        Me.txtPremie.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtPremie.Size = New System.Drawing.Size(54, 20)
        Me.txtPremie.TabIndex = 42
        Me.txtPremie.Text = "0,00"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(197, 52)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(39, 13)
        Me.Label15.TabIndex = 41
        Me.Label15.Text = "Premie"
        '
        'cmbWNTYPE
        '
        Me.cmbWNTYPE.DisplayMember = "Text"
        Me.cmbWNTYPE.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbWNTYPE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWNTYPE.FormattingEnabled = True
        Me.cmbWNTYPE.ItemHeight = 14
        Me.cmbWNTYPE.Location = New System.Drawing.Point(16, 67)
        Me.cmbWNTYPE.Name = "cmbWNTYPE"
        Me.cmbWNTYPE.Size = New System.Drawing.Size(118, 20)
        Me.cmbWNTYPE.TabIndex = 43
        '
        'frmLoonkost
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(976, 314)
        Me.Controls.Add(Me.cmbWNTYPE)
        Me.Controls.Add(Me.txtPremie)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtWalking)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.dgvLoonkost)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtRSZjaar)
        Me.Controls.Add(Me.txtSanering)
        Me.Controls.Add(Me.txtBasis)
        Me.Controls.Add(Me.txtPvorming)
        Me.Controls.Add(Me.txtHVD)
        Me.Controls.Add(Me.txtStructuur)
        Me.Controls.Add(Me.txtSpecFonds)
        Me.Controls.Add(Me.txtPreventie)
        Me.Controls.Add(Me.txtSIWHA)
        Me.Controls.Add(Me.txtCEPA)
        Me.Controls.Add(Me.txtVakopl)
        Me.Controls.Add(Me.txtRSZ)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStripTypeWerknemers)
        Me.Name = "frmLoonkost"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loonkost"
        Me.ToolStripTypeWerknemers.ResumeLayout(False)
        Me.ToolStripTypeWerknemers.PerformLayout()
        CType(Me.dgvLoonkost, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStripTypeWerknemers As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdUpdate As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtRSZ As NumericBox.NumericBox
    Friend WithEvents txtVakopl As NumericBox.NumericBox
    Friend WithEvents txtCEPA As NumericBox.NumericBox
    Friend WithEvents txtSIWHA As NumericBox.NumericBox
    Friend WithEvents txtPreventie As NumericBox.NumericBox
    Friend WithEvents txtSpecFonds As NumericBox.NumericBox
    Friend WithEvents txtStructuur As NumericBox.NumericBox
    Friend WithEvents txtHVD As NumericBox.NumericBox
    Friend WithEvents txtPvorming As NumericBox.NumericBox
    Friend WithEvents txtBasis As NumericBox.NumericBox
    Friend WithEvents txtSanering As NumericBox.NumericBox
    Friend WithEvents txtRSZjaar As NumericBox.NumericBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtWalking As NumericBox.NumericBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtPremie As NumericBox.NumericBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cmbWNTYPE As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents dgvLoonkost As DevComponents.DotNetBar.Controls.DataGridViewX
End Class
