Public Class frmLoonkost

    Private clsLoonkost As New BLL.BLLLoonkostSP

    Private id As Integer = 0
    Private menubar As New clsMenuBar

    Private Sub frmLoonkost_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        refreshCombo()
        clearfields()

        cmbWNTYPE.Select()

    End Sub

    Private Sub refreshCombo()

        cmbWNTYPE.DataSource = clBLL_TypeWerknemers.GetWerknemersType
        cmbWNTYPE.DisplayMember = clBLL_TypeWerknemers.GetWerknemersType.StelselColumn.ToString
        cmbWNTYPE.ValueMember = clBLL_TypeWerknemers.GetWerknemersType.TypeWerknemerIDColumn.ToString

    End Sub

    Private Sub clearfields()

        For Each c As Control In Me.Controls
            If c.GetType.ToString = "System.Windows.Forms.ComboBox" Then
                Dim cb As ComboBox = c
                cb.SelectedIndex = -1

            End If
            If c.GetType.ToString = "NumericBox.NumericBox" Then
                c.Text = 0
            End If
        Next

        refreshgrid()
    End Sub

    Private Sub refreshgrid()

        dgvLoonkost.DataSource = clsLoonkost.GetLoonkost
        dgvLoonkost.Columns(clBLL_loonkost.GetLoonkost.LoonKostIDColumn.ColumnName).Visible = False
        dgvLoonkost.Columns(clBLL_loonkost.GetLoonkost.TypeWerkNemerIDColumn.ColumnName).Visible = False


        menubar.Add()
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        menubar.Load()
        clBLL_loonkost.InsertLoonkost(cmbWNTYPE.SelectedValue, txtWalking.Text, txtPremie.Text, txtRSZ.Text, txtVakopl.Text, txtCEPA.Text, _
        txtSIWHA.Text, txtPreventie.Text, txtSpecFonds.Text, txtStructuur.Text, txtHVD.Text, txtPvorming.Text, _
        txtBasis.Text, txtSanering.Text, txtRSZjaar.Text)

        clearfields()

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Try
            id = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.LoonKostIDColumn.ColumnName, dgvLoonkost.CurrentCellAddress.Y).Value
            Try
                clBLL_loonkost.DeleteLoonkost(id)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            clearfields()
        Catch ex As ArgumentOutOfRangeException
            MsgBox("klik op een cel om deze te selecteren", MsgBoxStyle.Information)
        End Try


    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        menubar.Update()
        id = dgvLoonkost.CurrentCellAddress.Y
        With dgvLoonkost

            Try
                cmbWNTYPE.SelectedValue = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.TypeWerkNemerIDColumn.ColumnName, id).Value.ToString

                txtWalking.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.WalkingColumn.ColumnName, id).Value
                txtPremie.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.PremieColumn.ColumnName, id).Value


                txtRSZ.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.RSZColumn.ColumnName, id).Value
                txtVakopl.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.VakopleidingColumn.ColumnName, id).Value
                txtCEPA.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.CEPAColumn.ColumnName, id).Value
                txtSIWHA.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.SIWHAColumn.ColumnName, id).Value
                txtPreventie.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.PreventieColumn.ColumnName, id).Value
                txtSpecFonds.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.SpecFondsColumn.ColumnName, id).Value
                txtStructuur.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.StructuurColumn.ColumnName, id).Value
                txtHVD.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.HVDColumn.ColumnName, id).Value
                txtPvorming.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.PVormingColumn.ColumnName, id).Value
                txtBasis.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.BasisColumn.ColumnName, id).Value
                txtSanering.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.SaneringColumn.ColumnName, id).Value
                txtRSZjaar.Text = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.RSZJaarColumn.ColumnName, id).Value

            Catch ex As ArgumentOutOfRangeException
                MsgBox("klik op een cel om deze te selecteren", MsgBoxStyle.Information)
            End Try



        End With


    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        menubar.Load()
        id = dgvLoonkost.Item(clBLL_loonkost.GetLoonkost.LoonKostIDColumn.ColumnName, id).Value.ToString

        clBLL_loonkost.UpdateLoonkost(cmbWNTYPE.SelectedValue, txtWalking.Text, txtPremie.Text, txtRSZ.Text, txtVakopl.Text, txtCEPA.Text, _
        txtSIWHA.Text, txtPreventie.Text, txtSpecFonds.Text, txtStructuur.Text, txtHVD.Text, txtPvorming.Text, _
        txtBasis.Text, txtSanering.Text, txtRSZjaar.Text, id)



        clearfields()
    End Sub

    Private Sub dgvLoonkost_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLoonkost.SelectionChanged
        menubar.edit()
    End Sub

    Private Sub txtRSZjaar_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRSZjaar.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub
End Class