
Option Compare Text

Public Class frmMailRule

    Dim mr As New BLL.mailrule

    

    Private Sub frmMailRule_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        dgv.EndEdit()

        For Each r As DataGridViewRow In dgv.Rows
            Dim id As Integer = dgv.Item("ruleid", r.Index).Value
            Dim rule As String = dgv.Item("mailrule", r.Index).Value
            Dim actief As Boolean = dgv.Item("actief", r.Index).Value


            mr.UpdateRule(rule, actief, id)

        Next


    End Sub
    Private Sub frmMailRule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshgrid()

    End Sub

    Private Sub refreshgrid()
        dgv.DataSource = mr.GetMailRule
        dgv.Columns("ruleid").Visible = False

        dgv.Columns("MailRule").ReadOnly = True
    End Sub

    
    
End Class