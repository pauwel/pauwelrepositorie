Public Class frmMachinesPerFunctie

    Private id As Integer = 0
    Private rijen As New ArrayList




    Private Sub frmMachinesPerFunctie_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Loaddata()
        ClearLists()
        refreshgrid()

    End Sub

    Private Sub Loaddata()
        lstFuncties.DataSource = clsPreload.functies
        lstFuncties.DisplayMember = clsPreload.functiesdisplaymember
        lstFuncties.ValueMember = "functieid"


        lstMachines.DataSource = clsPreload.Machines
        lstMachines.DisplayMember = clsPreload.MachineDisplayMember
        lstMachines.ValueMember = clsPreload.MachineValueMember

    End Sub

    Private Sub lstFuncties_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstFuncties.Enter
        lstFuncties.DataSource = clsPreload.functies
        lstFuncties.DisplayMember = clsPreload.functiesdisplaymember
        lstFuncties.ValueMember = clsPreload.functiesvaluemember
    End Sub

    Private Sub lstMachines_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstMachines.Enter
        lstMachines.DataSource = clsPreload.Machines
        lstMachines.DisplayMember = clsPreload.MachineDisplayMember
        lstMachines.ValueMember = clsPreload.MachineValueMember

    End Sub
    Private Sub ClearLists()
        lstFuncties.SelectedIndex = -1
        lstMachines.SelectedIndex = -1
    End Sub
    Private Sub refreshgrid()
        dgview.DataSource = clBLL_GetFunctiePerMachine.GetFunctiesPerMachine
        dgview.Columns(clBLL_GetFunctiePerMachine.GetFunctiesPerMachine.MachFunctIDColumn.ColumnName).Visible = False
        dgview.Columns(clBLL_GetFunctiePerMachine.GetFunctiesPerMachine.MachinecodeColumn.ColumnName).Visible = False

    End Sub

 
    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        For Each f As DataRowView In lstFuncties.SelectedItems

            For Each m As DataRowView In lstMachines.SelectedItems
                'voor gebruik in de (bll)message indien de record reeds werd toegevoegd

                Dim functie As String = f.Row(1).ToString
                Dim machine As String = m.Row(1).ToString
                '**************
                clBLL_FunctiePerMachine.AddFunctiesPerMachine(f.Row(0).ToString, m.Row(0).ToString, functie, machine)


            Next

        Next
        refreshgrid()
    End Sub



    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click

        GetSelectedRows()
        verwijder()

    End Sub


    Private Sub verwijder()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                clBLL_FunctiePerMachine.DeleteFunctiesPerMachine(id)
            Next

            refreshgrid()
        Catch ex As Exception

        End Try
    End Sub
    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgview.Item(clBLL_FunctiePerMachine.GetFunctiesPerMachine.MachFunctIDColumn.ColumnName, dr.Index).Value)
        Next
    End Sub



    Private Sub dgview_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgview.KeyUp
        If e.KeyCode = Keys.Delete Then
            verwijder()
        End If
    End Sub

    Private Sub dgview_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgview.KeyDown
        GetSelectedRows()
    End Sub

    
    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    
        _frmReports.Reportnaam = "MachinesPerFunctie.rpt"
        _frmReports.ShowDialog()
    End Sub

End Class