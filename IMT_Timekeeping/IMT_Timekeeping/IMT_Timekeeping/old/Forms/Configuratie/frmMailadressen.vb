Public Class frmMailadressen
    Private menubar As clsMenuBar

    Private rijen As New ArrayList

    Private id As Int16

    Private blnEdit As Boolean = False
    Private Sub frmMailadressen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()

        refreshgrid()
    End Sub

    Public Overrides Sub initform()

    End Sub

    Private Sub clear()
        txtMail.Clear()

    End Sub

    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()

        With Me.dgView
            .DataSource = clBLL_Mail.GetMailadres
            .Columns("mailid").Visible = False
        End With
        clear()
        blnEdit = False
    End Sub

    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()

      
        Try
            If Not String.IsNullOrEmpty(Trim(txtMail.Text)) Then
                clBLL_Mail.InsertMailadres(Trim(txtMail.Text))


                refreshgrid()
            End If

        Catch ex As Exception

        End Try


    End Sub

    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()
        GetSelectedRows()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                clBLL_Mail.DeleteMailadre(id)
            Next
            refreshgrid()

        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub onCmdReportClick()
        MyBase.onCmdReportClick()
        _frmReports.Reportnaam = "mailadrressen.rpt"
        _frmReports.ShowDialog()
    End Sub


    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()



        id = dgView.Item("mailid", dgView.CurrentCellAddress.Y).Value

        txtMail.Text = dgView.Item("MailAdres", dgView.CurrentCellAddress.Y).Value

        menubar.Update()
        blnEdit = True
    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()
        menubar.Load()


        Try
            If clBLL_Mail.UpdateMailadres(Trim(txtMail.Text), id) Then
                clear()
            End If


        Catch ex As Exception

        End Try

        blnEdit = False
    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try

    End Sub



    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item("mailid", dr.Index).Value)
        Next
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    clBLL_Mail.DeleteMailadre(id)
                Next


            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub txtMail_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMail.TextChanged

        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtMail.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If

    End Sub

    Private Sub txtMail_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMail.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub
End Class
