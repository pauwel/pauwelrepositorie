Public Class frmMachines
    Private menubar As clsMenuBar

    Private rijen As New ArrayList
    Private MachineCode As String = Nothing, kost As Decimal = 0, omschrijving As String = Nothing
    Private id As Int16

    Private blnEdit As Boolean = False

   


    Private Sub frmMachines_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        clear()
        refreshgrid()

    End Sub
    Protected Overrides Sub onCmdReportClick()
        MyBase.onCmdReportClick()

        _frmReports.Reportnaam = "machines.rpt"
        _frmReports.ShowDialog()
    End Sub

    Public Overrides Sub initform()

    End Sub

    Private Sub clear()
        txtMachineCode.Clear()
        txtKost.Text = 0
        txtOmschrijving.Clear()
    End Sub

    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()

        With Me.dgView
            .DataSource = clsPreload.Machines
            .Columns(clsPreload.MachineValueMember).Visible = False
        End With
    End Sub

    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()

        MachineCode = txtMachineCode.Text
        omschrijving = txtOmschrijving.Text
        kost = txtKost.Text
        Try
            If Not String.IsNullOrEmpty(Trim(MachineCode)) Then
                clBLL_Machines.InsertMachine(MachineCode, omschrijving, kost)

                clsPreload.Machines = clBLL_Machines.GetMAchine
                refreshgrid()
            End If

        Catch ex As Exception

        End Try
        clear()

    End Sub

    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()
        GetSelectedRows()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                Try
                    clBLL_Machines.DeleteMachine(id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next

            clsPreload.Machines = clBLL_Machines.GetMAchine
        Catch ex As Exception

        End Try
        clear()
    End Sub

    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()



        id = dgView.Item(clBLL_Machines.GetMAchine.MachineIDColumn.ColumnName, dgView.CurrentCellAddress.Y).Value

        MachineCode = dgView.Item(clBLL_Machines.GetMAchine.MachinecodeColumn.ColumnName, dgView.CurrentCellAddress.Y).Value
        omschrijving = dgView.Item(clBLL_Machines.GetMAchine.MachineOmschrijvingColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()
        kost = dgView.Item(clBLL_Machines.GetMAchine.KostColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()
        

        txtMachineCode.Text = MachineCode
        txtOmschrijving.Text = omschrijving
        txtKost.Text = kost

        menubar.Update()
        blnEdit = True
    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()
        menubar.Load()

        MachineCode = txtMachineCode.Text
        omschrijving = txtOmschrijving.Text
        kost = txtKost.Text

        Try
            clBLL_Machines.UpdateMachine(MachineCode, omschrijving, kost, id)
            clsPreload.Machines = clBLL_Machines.GetMAchine
            clear()
        Catch ex As Exception

        End Try
    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try

    End Sub

    

    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item(clsPreload.MachineValueMember, dr.Index).Value)
        Next
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    Try
                        clBLL_Machines.DeleteMachine(id)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                Next

                clsPreload.Machines = clBLL_Machines.GetMAchine
            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub txtMachineCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMachineCode.TextChanged

        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtMachineCode.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If

    End Sub


    Private Sub txtOmschrijving_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOmschrijving.KeyUp
        If e.KeyCode = Keys.F1 Then
            cmdAdd.PerformClick()
        End If
    End Sub
End Class
