<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoonType
    Inherits IMT_Timekeeping.frmBaseConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtOmschrijving = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkActief = New System.Windows.Forms.CheckBox
        Me.chkCEPAverplicht = New System.Windows.Forms.CheckBox
        Me.chkUrenInvoer = New System.Windows.Forms.CheckBox
        Me.chkcode30Weergave = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'txtOmschrijving
        '
        Me.txtOmschrijving.Location = New System.Drawing.Point(12, 73)
        Me.txtOmschrijving.MaxLength = 50
        Me.txtOmschrijving.Multiline = True
        Me.txtOmschrijving.Name = "txtOmschrijving"
        Me.txtOmschrijving.Size = New System.Drawing.Size(229, 62)
        Me.txtOmschrijving.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Omschrijving"
        '
        'chkActief
        '
        Me.chkActief.AutoSize = True
        Me.chkActief.Checked = True
        Me.chkActief.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkActief.Location = New System.Drawing.Point(247, 73)
        Me.chkActief.Name = "chkActief"
        Me.chkActief.Size = New System.Drawing.Size(53, 17)
        Me.chkActief.TabIndex = 8
        Me.chkActief.Text = "Actief"
        Me.chkActief.UseVisualStyleBackColor = True
        '
        'chkCEPAverplicht
        '
        Me.chkCEPAverplicht.AutoSize = True
        Me.chkCEPAverplicht.Location = New System.Drawing.Point(306, 73)
        Me.chkCEPAverplicht.Name = "chkCEPAverplicht"
        Me.chkCEPAverplicht.Size = New System.Drawing.Size(124, 17)
        Me.chkCEPAverplicht.TabIndex = 9
        Me.chkCEPAverplicht.Text = "CEPA code verplicht"
        Me.chkCEPAverplicht.UseVisualStyleBackColor = True
        '
        'chkUrenInvoer
        '
        Me.chkUrenInvoer.AutoSize = True
        Me.chkUrenInvoer.Location = New System.Drawing.Point(436, 73)
        Me.chkUrenInvoer.Name = "chkUrenInvoer"
        Me.chkUrenInvoer.Size = New System.Drawing.Size(81, 17)
        Me.chkUrenInvoer.TabIndex = 10
        Me.chkUrenInvoer.Text = "Uren invoer"
        Me.chkUrenInvoer.UseVisualStyleBackColor = True
        '
        'chkcode30Weergave
        '
        Me.chkcode30Weergave.AutoSize = True
        Me.chkcode30Weergave.Location = New System.Drawing.Point(247, 107)
        Me.chkcode30Weergave.Name = "chkcode30Weergave"
        Me.chkcode30Weergave.Size = New System.Drawing.Size(129, 17)
        Me.chkcode30Weergave.TabIndex = 11
        Me.chkcode30Weergave.Text = "Weergave in code 30"
        Me.chkcode30Weergave.UseVisualStyleBackColor = True
        '
        'frmLoonType
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(584, 464)
        Me.Controls.Add(Me.chkcode30Weergave)
        Me.Controls.Add(Me.chkUrenInvoer)
        Me.Controls.Add(Me.chkCEPAverplicht)
        Me.Controls.Add(Me.chkActief)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtOmschrijving)
        Me.Name = "frmLoonType"
        Me.Text = "LoonType"
        Me.Controls.SetChildIndex(Me.txtOmschrijving, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.chkActief, 0)
        Me.Controls.SetChildIndex(Me.chkCEPAverplicht, 0)
        Me.Controls.SetChildIndex(Me.chkUrenInvoer, 0)
        Me.Controls.SetChildIndex(Me.chkcode30Weergave, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtOmschrijving As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkActief As System.Windows.Forms.CheckBox
    Friend WithEvents chkCEPAverplicht As System.Windows.Forms.CheckBox
    Friend WithEvents chkUrenInvoer As System.Windows.Forms.CheckBox
    Friend WithEvents chkcode30Weergave As System.Windows.Forms.CheckBox

End Class
