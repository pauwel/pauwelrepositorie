<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFeestdagen
    Inherits IMT_Timekeeping.frmBaseConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dteFeestdag = New System.Windows.Forms.DateTimePicker
        Me.txtFeestdag = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'dteFeestdag
        '
        Me.dteFeestdag.Location = New System.Drawing.Point(267, 76)
        Me.dteFeestdag.Name = "dteFeestdag"
        Me.dteFeestdag.Size = New System.Drawing.Size(200, 20)
        Me.dteFeestdag.TabIndex = 1
        '
        'txtFeestdag
        '
        Me.txtFeestdag.Location = New System.Drawing.Point(12, 76)
        Me.txtFeestdag.MaxLength = 50
        Me.txtFeestdag.Name = "txtFeestdag"
        Me.txtFeestdag.Size = New System.Drawing.Size(249, 20)
        Me.txtFeestdag.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(267, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Datum"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Omschrijving"
        '
        'frmFeestdagen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(584, 464)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dteFeestdag)
        Me.Controls.Add(Me.txtFeestdag)
        Me.Name = "frmFeestdagen"
        Me.Text = "Feestdagen"
        Me.Controls.SetChildIndex(Me.txtFeestdag, 0)
        Me.Controls.SetChildIndex(Me.dteFeestdag, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dteFeestdag As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFeestdag As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
