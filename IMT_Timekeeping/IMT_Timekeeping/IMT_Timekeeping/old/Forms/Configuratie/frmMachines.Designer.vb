<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMachines
    Inherits IMT_Timekeeping.frmBaseConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtMachineCode = New System.Windows.Forms.TextBox
        Me.txtOmschrijving = New System.Windows.Forms.TextBox
        Me.txtKost = New NumericBox.NumericBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtMachineCode
        '
        Me.txtMachineCode.Location = New System.Drawing.Point(12, 51)
        Me.txtMachineCode.MaxLength = 15
        Me.txtMachineCode.Name = "txtMachineCode"
        Me.txtMachineCode.Size = New System.Drawing.Size(100, 20)
        Me.txtMachineCode.TabIndex = 6
        '
        'txtOmschrijving
        '
        Me.txtOmschrijving.Location = New System.Drawing.Point(12, 96)
        Me.txtOmschrijving.MaxLength = 150
        Me.txtOmschrijving.Multiline = True
        Me.txtOmschrijving.Name = "txtOmschrijving"
        Me.txtOmschrijving.Size = New System.Drawing.Size(392, 39)
        Me.txtOmschrijving.TabIndex = 7
        '
        'txtKost
        '
        Me.txtKost.BackColor = System.Drawing.SystemColors.Window
        Me.txtKost.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtKost.Location = New System.Drawing.Point(118, 51)
        Me.txtKost.Name = "txtKost"
        Me.txtKost.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtKost.Size = New System.Drawing.Size(86, 20)
        Me.txtKost.TabIndex = 8
        Me.txtKost.Text = "0,00"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Machine code"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(118, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Kost"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Omschrijving"
        '
        'frmMachines
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(584, 540)
        Me.Controls.Add(Me.txtOmschrijving)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtMachineCode)
        Me.Controls.Add(Me.txtKost)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmMachines"
        Me.Text = "Configuratie - Machines -"
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtKost, 0)
        Me.Controls.SetChildIndex(Me.txtMachineCode, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.txtOmschrijving, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtMachineCode As System.Windows.Forms.TextBox
    Friend WithEvents txtOmschrijving As System.Windows.Forms.TextBox
    Friend WithEvents txtKost As NumericBox.NumericBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label

End Class
