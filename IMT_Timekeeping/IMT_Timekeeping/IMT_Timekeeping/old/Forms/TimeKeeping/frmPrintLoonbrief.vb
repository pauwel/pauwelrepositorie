Public Class frmPrintLoonbrief

    Private Sub frmPrintLoonbrief_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim TIP As New BLL.GetShiftenToonInPLanning
        Try
            cmbShiften.DataSource = TIP.GetShiften
            cmbShiften.DisplayMember = "Omschrijving"
            cmbShiften.ValueMember = "ShiftID"

            cmbShiften.SelectedIndex = -1
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        _frmReports.Shiften = cmbShiften.Text
        If cmbShiften.Text <> "" Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Else
            MsgBox("selecteer een shift alvorens OK te klikken")

        End If

    End Sub
End Class