Public Class frmGetOldCEPAdata

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        If Me.cmbDate.Text <> "" Then

            'genereer(bestand)

            Dim cf As New BLL.CEPAfile
            dlgSave.Title = "CEPA FILE"
            dlgSave.ShowDialog()
            cf.path = dlgSave.FileName
            cf.Datum = Me.cmbDate.Text
            cf.schrijver()

            dlgSave.Title = "BORDEREL"
            dlgSave.FileName = Nothing
            dlgSave.ShowDialog()
            cf.path = dlgSave.FileName
            cf.borderel()


            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Else
            MsgBox("Selecteer een datum uit de lijst en klik OK")
        End If


    End Sub

    Private Sub frmGetOldCEPAdata_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim d As New BLL.OLDCEPAData

        cmbDate.DataSource = d.GetBetalingdatums
        cmbDate.DisplayMember = "CEPAdatum"


    End Sub
End Class