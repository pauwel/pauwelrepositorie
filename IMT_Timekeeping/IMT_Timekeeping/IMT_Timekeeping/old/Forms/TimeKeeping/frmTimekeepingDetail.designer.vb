<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimekeepingDetail
    'Inherits DevComponents.DotNetBar.Office2007Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.PanelEx1 = New DevComponents.DotNetBar.PanelEx
        Me.lblNaam = New DevComponents.DotNetBar.PanelEx
        Me.lblWBnummer = New DevComponents.DotNetBar.LabelX
        Me.Bar1 = New DevComponents.DotNetBar.Bar
        Me.lblEdit = New DevComponents.DotNetBar.LabelItem
        Me.btnEdit = New DevComponents.DotNetBar.ButtonItem
        Me.btnSave = New DevComponents.DotNetBar.ButtonItem
        Me.btnRefresh = New DevComponents.DotNetBar.ButtonItem
        Me.btnValideer = New DevComponents.DotNetBar.ButtonItem
        Me.btnValideerSluit = New DevComponents.DotNetBar.ButtonItem
        Me.cmdReport = New DevComponents.DotNetBar.ButtonItem
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonItem
        Me.btnOverboeken = New DevComponents.DotNetBar.ButtonItem
        Me.lblSaldo = New DevComponents.DotNetBar.Controls.ReflectionLabel
        Me.GroupBoxLoonDetails = New System.Windows.Forms.GroupBox
        Me.Bar2 = New DevComponents.DotNetBar.Bar
        Me.cmdBewaarDetail = New DevComponents.DotNetBar.ButtonItem
        Me.cmbSupplement = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.dgvSupplement = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtCommentaar = New System.Windows.Forms.TextBox
        Me.lblUren = New System.Windows.Forms.Label
        Me.txtSupplementUren = New NumericBox.NumericBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtCEPACode = New System.Windows.Forms.TextBox
        Me.txtSupplementBedrag = New NumericBox.NumericBox
        Me.GroupBoxInfo = New System.Windows.Forms.GroupBox
        Me.chkIsWeekend = New System.Windows.Forms.CheckBox
        Me.cmbVoyages = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbFunctie = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbShiften = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.txtAfwezig = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.chkSpaarplanner = New System.Windows.Forms.CheckBox
        Me.chkAfwezig = New System.Windows.Forms.CheckBox
        Me.chkIsFeestdag = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtUren = New NumericBox.NumericBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.dteDetail = New System.Windows.Forms.DateTimePicker
        Me.chkWordtUitbetaald = New System.Windows.Forms.CheckBox
        Me.ItemPanel1 = New DevComponents.DotNetBar.ItemPanel
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem
        Me.TextBoxItem1 = New DevComponents.DotNetBar.TextBoxItem
        Me.PanelEx1.SuspendLayout()
        Me.lblNaam.SuspendLayout()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxLoonDetails.SuspendLayout()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSupplement, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelEx1
        '
        Me.PanelEx1.CanvasColor = System.Drawing.SystemColors.Control
        Me.PanelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.PanelEx1.Controls.Add(Me.lblNaam)
        Me.PanelEx1.Controls.Add(Me.Bar1)
        Me.PanelEx1.Controls.Add(Me.lblSaldo)
        Me.PanelEx1.Controls.Add(Me.GroupBoxLoonDetails)
        Me.PanelEx1.Controls.Add(Me.GroupBoxInfo)
        Me.PanelEx1.Controls.Add(Me.chkWordtUitbetaald)
        Me.PanelEx1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelEx1.Location = New System.Drawing.Point(0, 0)
        Me.PanelEx1.Name = "PanelEx1"
        Me.PanelEx1.Size = New System.Drawing.Size(835, 701)
        Me.PanelEx1.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.PanelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.PanelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.PanelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.PanelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.PanelEx1.Style.GradientAngle = 90
        Me.PanelEx1.TabIndex = 0
        '
        'lblNaam
        '
        Me.lblNaam.CanvasColor = System.Drawing.SystemColors.Control
        Me.lblNaam.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005
        Me.lblNaam.Controls.Add(Me.lblWBnummer)
        Me.lblNaam.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblNaam.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNaam.Location = New System.Drawing.Point(0, 0)
        Me.lblNaam.Name = "lblNaam"
        Me.lblNaam.Size = New System.Drawing.Size(835, 31)
        Me.lblNaam.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.lblNaam.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.lblNaam.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.lblNaam.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.lblNaam.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.lblNaam.Style.GradientAngle = 90
        Me.lblNaam.TabIndex = 6
        Me.lblNaam.Text = "Naam onbekend"
        '
        'lblWBnummer
        '
        Me.lblWBnummer.Location = New System.Drawing.Point(12, 3)
        Me.lblWBnummer.Name = "lblWBnummer"
        Me.lblWBnummer.Size = New System.Drawing.Size(244, 23)
        Me.lblWBnummer.TabIndex = 7
        Me.lblWBnummer.Text = "LabelX1"
        '
        'Bar1
        '
        Me.Bar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.lblEdit, Me.btnEdit, Me.btnSave, Me.btnRefresh, Me.btnValideer, Me.cmdReport, Me.cmdDelete, Me.btnOverboeken})
        Me.Bar1.Location = New System.Drawing.Point(0, 668)
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(835, 33)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.Bar1.TabIndex = 5
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'lblEdit
        '
        Me.lblEdit.BeginGroup = True
        Me.lblEdit.BorderSide = DevComponents.DotNetBar.eBorderSide.None
        Me.lblEdit.DividerStyle = True
        Me.lblEdit.Name = "lblEdit"
        Me.lblEdit.Text = "Edit mode off"
        '
        'btnEdit
        '
        Me.btnEdit.AutoCheckOnClick = True
        Me.btnEdit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnEdit.Image = Global.IMT_Timekeeping.My.Resources.Resources.EditInformationHS
        Me.btnEdit.ImagePaddingHorizontal = 8
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE)
        Me.btnEdit.Text = "Bewerken"
        '
        'btnSave
        '
        Me.btnSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSave.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.btnSave.ImagePaddingHorizontal = 8
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS)
        Me.btnSave.Text = "Opslaan"
        '
        'btnRefresh
        '
        Me.btnRefresh.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnRefresh.Image = Global.IMT_Timekeeping.My.Resources.Resources.Refresh
        Me.btnRefresh.ImagePaddingHorizontal = 8
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5)
        Me.btnRefresh.Text = "Vernieuwen"
        Me.btnRefresh.Tooltip = "De gegevens vernieuwen"
        '
        'btnValideer
        '
        Me.btnValideer.BeginGroup = True
        Me.btnValideer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnValideer.Image = Global.IMT_Timekeeping.My.Resources.Resources.DatabaseEdit
        Me.btnValideer.ImagePaddingHorizontal = 8
        Me.btnValideer.Name = "btnValideer"
        Me.btnValideer.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F12)
        Me.btnValideer.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnValideerSluit})
        Me.btnValideer.Text = "Valideer"
        '
        'btnValideerSluit
        '
        Me.btnValideerSluit.ImagePaddingHorizontal = 8
        Me.btnValideerSluit.Name = "btnValideerSluit"
        Me.btnValideerSluit.Text = "Valideren en sluiten "
        '
        'cmdReport
        '
        Me.cmdReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdReport.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdReport.ImagePaddingHorizontal = 8
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP)
        Me.cmdReport.Text = "LoonBrief"
        '
        'cmdDelete
        '
        Me.cmdDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImagePaddingHorizontal = 8
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Text = "Verwijder"
        '
        'btnOverboeken
        '
        Me.btnOverboeken.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnOverboeken.Image = Global.IMT_Timekeeping.My.Resources.Resources.MoveRight
        Me.btnOverboeken.ImagePaddingHorizontal = 8
        Me.btnOverboeken.Name = "btnOverboeken"
        Me.btnOverboeken.Text = "Overboeken"
        '
        'lblSaldo
        '
        Me.lblSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldo.ForeColor = System.Drawing.Color.Green
        Me.lblSaldo.Location = New System.Drawing.Point(612, 41)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(175, 70)
        Me.lblSaldo.TabIndex = 3
        Me.lblSaldo.Text = "Saldo onbekend"
        '
        'GroupBoxLoonDetails
        '
        Me.GroupBoxLoonDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxLoonDetails.Controls.Add(Me.Bar2)
        Me.GroupBoxLoonDetails.Controls.Add(Me.cmbSupplement)
        Me.GroupBoxLoonDetails.Controls.Add(Me.dgvSupplement)
        Me.GroupBoxLoonDetails.Controls.Add(Me.Label7)
        Me.GroupBoxLoonDetails.Controls.Add(Me.txtCommentaar)
        Me.GroupBoxLoonDetails.Controls.Add(Me.lblUren)
        Me.GroupBoxLoonDetails.Controls.Add(Me.txtSupplementUren)
        Me.GroupBoxLoonDetails.Controls.Add(Me.Label13)
        Me.GroupBoxLoonDetails.Controls.Add(Me.Label12)
        Me.GroupBoxLoonDetails.Controls.Add(Me.Label11)
        Me.GroupBoxLoonDetails.Controls.Add(Me.txtCEPACode)
        Me.GroupBoxLoonDetails.Controls.Add(Me.txtSupplementBedrag)
        Me.GroupBoxLoonDetails.Location = New System.Drawing.Point(12, 303)
        Me.GroupBoxLoonDetails.Name = "GroupBoxLoonDetails"
        Me.GroupBoxLoonDetails.Size = New System.Drawing.Size(811, 371)
        Me.GroupBoxLoonDetails.TabIndex = 1
        Me.GroupBoxLoonDetails.TabStop = False
        Me.GroupBoxLoonDetails.Text = "Loondetails / Supplement"
        '
        'Bar2
        '
        Me.Bar2.BarType = DevComponents.DotNetBar.eBarType.MenuBar
        Me.Bar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdBewaarDetail})
        Me.Bar2.Location = New System.Drawing.Point(692, 9)
        Me.Bar2.Name = "Bar2"
        Me.Bar2.Size = New System.Drawing.Size(113, 25)
        Me.Bar2.Stretch = True
        Me.Bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.Bar2.TabIndex = 87
        Me.Bar2.TabStop = False
        Me.Bar2.Text = "Bar2"
        '
        'cmdBewaarDetail
        '
        Me.cmdBewaarDetail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdBewaarDetail.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdBewaarDetail.ImagePaddingHorizontal = 8
        Me.cmdBewaarDetail.Name = "cmdBewaarDetail"
        Me.cmdBewaarDetail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS)
        Me.cmdBewaarDetail.Text = "Opslaan"
        '
        'cmbSupplement
        '
        Me.cmbSupplement.DisplayMember = "Text"
        Me.cmbSupplement.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbSupplement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSupplement.Enabled = False
        Me.cmbSupplement.FormattingEnabled = True
        Me.cmbSupplement.ItemHeight = 14
        Me.cmbSupplement.Location = New System.Drawing.Point(13, 50)
        Me.cmbSupplement.Name = "cmbSupplement"
        Me.cmbSupplement.Size = New System.Drawing.Size(153, 20)
        Me.cmbSupplement.TabIndex = 0
        '
        'dgvSupplement
        '
        Me.dgvSupplement.AllowUserToAddRows = False
        Me.dgvSupplement.AllowUserToDeleteRows = False
        Me.dgvSupplement.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSupplement.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvSupplement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSupplement.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSupplement.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvSupplement.Location = New System.Drawing.Point(10, 77)
        Me.dgvSupplement.Name = "dgvSupplement"
        Me.dgvSupplement.ReadOnly = True
        Me.dgvSupplement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSupplement.Size = New System.Drawing.Size(765, 288)
        Me.dgvSupplement.TabIndex = 85
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(426, 31)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 13)
        Me.Label7.TabIndex = 84
        Me.Label7.Text = "Commentaar"
        '
        'txtCommentaar
        '
        Me.txtCommentaar.Enabled = False
        Me.txtCommentaar.Location = New System.Drawing.Point(428, 50)
        Me.txtCommentaar.MaxLength = 50
        Me.txtCommentaar.Name = "txtCommentaar"
        Me.txtCommentaar.Size = New System.Drawing.Size(347, 20)
        Me.txtCommentaar.TabIndex = 4
        '
        'lblUren
        '
        Me.lblUren.AutoSize = True
        Me.lblUren.Location = New System.Drawing.Point(169, 31)
        Me.lblUren.Name = "lblUren"
        Me.lblUren.Size = New System.Drawing.Size(28, 13)
        Me.lblUren.TabIndex = 83
        Me.lblUren.Text = "uren"
        '
        'txtSupplementUren
        '
        Me.txtSupplementUren.BackColor = System.Drawing.SystemColors.Window
        Me.txtSupplementUren.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSupplementUren.Enabled = False
        Me.txtSupplementUren.Location = New System.Drawing.Point(172, 50)
        Me.txtSupplementUren.Name = "txtSupplementUren"
        Me.txtSupplementUren.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSupplementUren.Size = New System.Drawing.Size(52, 20)
        Me.txtSupplementUren.TabIndex = 1
        Me.txtSupplementUren.Text = "0,00"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(289, 31)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(62, 13)
        Me.Label13.TabIndex = 82
        Me.Label13.Text = "CEPA code"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(227, 31)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 13)
        Me.Label12.TabIndex = 81
        Me.Label12.Text = "bedrag"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(10, 31)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 13)
        Me.Label11.TabIndex = 80
        Me.Label11.Text = "Supplement"
        '
        'txtCEPACode
        '
        Me.txtCEPACode.Enabled = False
        Me.txtCEPACode.Location = New System.Drawing.Point(292, 50)
        Me.txtCEPACode.MaxLength = 30
        Me.txtCEPACode.Name = "txtCEPACode"
        Me.txtCEPACode.Size = New System.Drawing.Size(131, 20)
        Me.txtCEPACode.TabIndex = 3
        '
        'txtSupplementBedrag
        '
        Me.txtSupplementBedrag.BackColor = System.Drawing.SystemColors.Window
        Me.txtSupplementBedrag.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSupplementBedrag.Enabled = False
        Me.txtSupplementBedrag.Location = New System.Drawing.Point(230, 50)
        Me.txtSupplementBedrag.Name = "txtSupplementBedrag"
        Me.txtSupplementBedrag.NegativeNumber = True
        Me.txtSupplementBedrag.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSupplementBedrag.Size = New System.Drawing.Size(55, 20)
        Me.txtSupplementBedrag.TabIndex = 2
        Me.txtSupplementBedrag.Text = "0,00"
        '
        'GroupBoxInfo
        '
        Me.GroupBoxInfo.Controls.Add(Me.chkIsWeekend)
        Me.GroupBoxInfo.Controls.Add(Me.cmbVoyages)
        Me.GroupBoxInfo.Controls.Add(Me.cmbFunctie)
        Me.GroupBoxInfo.Controls.Add(Me.cmbShiften)
        Me.GroupBoxInfo.Controls.Add(Me.txtAfwezig)
        Me.GroupBoxInfo.Controls.Add(Me.chkSpaarplanner)
        Me.GroupBoxInfo.Controls.Add(Me.chkAfwezig)
        Me.GroupBoxInfo.Controls.Add(Me.chkIsFeestdag)
        Me.GroupBoxInfo.Controls.Add(Me.Label5)
        Me.GroupBoxInfo.Controls.Add(Me.txtUren)
        Me.GroupBoxInfo.Controls.Add(Me.Label4)
        Me.GroupBoxInfo.Controls.Add(Me.Label3)
        Me.GroupBoxInfo.Controls.Add(Me.Label2)
        Me.GroupBoxInfo.Controls.Add(Me.Label1)
        Me.GroupBoxInfo.Controls.Add(Me.dteDetail)
        Me.GroupBoxInfo.Location = New System.Drawing.Point(12, 117)
        Me.GroupBoxInfo.Name = "GroupBoxInfo"
        Me.GroupBoxInfo.Size = New System.Drawing.Size(811, 168)
        Me.GroupBoxInfo.TabIndex = 0
        Me.GroupBoxInfo.TabStop = False
        Me.GroupBoxInfo.Text = "Info"
        '
        'chkIsWeekend
        '
        Me.chkIsWeekend.AutoSize = True
        Me.chkIsWeekend.Enabled = False
        Me.chkIsWeekend.Location = New System.Drawing.Point(154, 128)
        Me.chkIsWeekend.Name = "chkIsWeekend"
        Me.chkIsWeekend.Size = New System.Drawing.Size(96, 17)
        Me.chkIsWeekend.TabIndex = 89
        Me.chkIsWeekend.Text = "Weekendwerk"
        Me.chkIsWeekend.UseVisualStyleBackColor = True
        '
        'cmbVoyages
        '
        Me.cmbVoyages.DisplayMember = "Text"
        Me.cmbVoyages.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbVoyages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVoyages.FormattingEnabled = True
        Me.cmbVoyages.ItemHeight = 14
        Me.cmbVoyages.Location = New System.Drawing.Point(292, 93)
        Me.cmbVoyages.Name = "cmbVoyages"
        Me.cmbVoyages.Size = New System.Drawing.Size(121, 20)
        Me.cmbVoyages.TabIndex = 88
        '
        'cmbFunctie
        '
        Me.cmbFunctie.DisplayMember = "Text"
        Me.cmbFunctie.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbFunctie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFunctie.FormattingEnabled = True
        Me.cmbFunctie.ItemHeight = 14
        Me.cmbFunctie.Location = New System.Drawing.Point(154, 93)
        Me.cmbFunctie.Name = "cmbFunctie"
        Me.cmbFunctie.Size = New System.Drawing.Size(121, 20)
        Me.cmbFunctie.TabIndex = 87
        '
        'cmbShiften
        '
        Me.cmbShiften.DisplayMember = "Text"
        Me.cmbShiften.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbShiften.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShiften.FormattingEnabled = True
        Me.cmbShiften.ItemHeight = 14
        Me.cmbShiften.Location = New System.Drawing.Point(27, 93)
        Me.cmbShiften.Name = "cmbShiften"
        Me.cmbShiften.Size = New System.Drawing.Size(121, 20)
        Me.cmbShiften.TabIndex = 86
        '
        'txtAfwezig
        '
        '
        '
        '
        Me.txtAfwezig.Border.Class = "TextBoxBorder"
        Me.txtAfwezig.Enabled = False
        Me.txtAfwezig.Location = New System.Drawing.Point(523, 40)
        Me.txtAfwezig.Multiline = True
        Me.txtAfwezig.Name = "txtAfwezig"
        Me.txtAfwezig.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAfwezig.Size = New System.Drawing.Size(252, 79)
        Me.txtAfwezig.TabIndex = 85
        '
        'chkSpaarplanner
        '
        Me.chkSpaarplanner.AutoSize = True
        Me.chkSpaarplanner.Enabled = False
        Me.chkSpaarplanner.Location = New System.Drawing.Point(292, 128)
        Me.chkSpaarplanner.Name = "chkSpaarplanner"
        Me.chkSpaarplanner.Size = New System.Drawing.Size(89, 17)
        Me.chkSpaarplanner.TabIndex = 83
        Me.chkSpaarplanner.Text = "Spaarplanner"
        Me.chkSpaarplanner.UseVisualStyleBackColor = True
        '
        'chkAfwezig
        '
        Me.chkAfwezig.AutoSize = True
        Me.chkAfwezig.Location = New System.Drawing.Point(523, 24)
        Me.chkAfwezig.Name = "chkAfwezig"
        Me.chkAfwezig.Size = New System.Drawing.Size(63, 17)
        Me.chkAfwezig.TabIndex = 81
        Me.chkAfwezig.Text = "Afwezig"
        Me.chkAfwezig.UseVisualStyleBackColor = True
        '
        'chkIsFeestdag
        '
        Me.chkIsFeestdag.AutoSize = True
        Me.chkIsFeestdag.Location = New System.Drawing.Point(27, 128)
        Me.chkIsFeestdag.Name = "chkIsFeestdag"
        Me.chkIsFeestdag.Size = New System.Drawing.Size(78, 17)
        Me.chkIsFeestdag.TabIndex = 70
        Me.chkIsFeestdag.Text = "Is feestdag"
        Me.chkIsFeestdag.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(231, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 13)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "Uren gewerkt"
        '
        'txtUren
        '
        Me.txtUren.BackColor = System.Drawing.SystemColors.Window
        Me.txtUren.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtUren.Enabled = False
        Me.txtUren.Location = New System.Drawing.Point(234, 40)
        Me.txtUren.Name = "txtUren"
        Me.txtUren.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtUren.Size = New System.Drawing.Size(55, 20)
        Me.txtUren.TabIndex = 67
        Me.txtUren.Text = "0,00"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(289, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "Voyage"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(151, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 13)
        Me.Label3.TabIndex = 64
        Me.Label3.Text = "Functie"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "Datum"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 61
        Me.Label1.Text = "Shift"
        '
        'dteDetail
        '
        Me.dteDetail.Enabled = False
        Me.dteDetail.Location = New System.Drawing.Point(27, 40)
        Me.dteDetail.Name = "dteDetail"
        Me.dteDetail.Size = New System.Drawing.Size(201, 20)
        Me.dteDetail.TabIndex = 59
        '
        'chkWordtUitbetaald
        '
        Me.chkWordtUitbetaald.AutoSize = True
        Me.chkWordtUitbetaald.Location = New System.Drawing.Point(25, 62)
        Me.chkWordtUitbetaald.Name = "chkWordtUitbetaald"
        Me.chkWordtUitbetaald.Size = New System.Drawing.Size(104, 17)
        Me.chkWordtUitbetaald.TabIndex = 84
        Me.chkWordtUitbetaald.Text = "Wordt uitbetaald"
        Me.chkWordtUitbetaald.UseVisualStyleBackColor = True
        '
        'ItemPanel1
        '
        '
        '
        '
        Me.ItemPanel1.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel1.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel1.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel1.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel1.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel1.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem1, Me.ButtonItem3, Me.ButtonItem4, Me.TextBoxItem1})
        Me.ItemPanel1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.ItemPanel1.Location = New System.Drawing.Point(470, 53)
        Me.ItemPanel1.Name = "ItemPanel1"
        Me.ItemPanel1.Size = New System.Drawing.Size(178, 55)
        Me.ItemPanel1.TabIndex = 6
        Me.ItemPanel1.Text = "ItemPanel1"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.ImagePaddingHorizontal = 8
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Text = "ButtonItem1"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.ImagePaddingHorizontal = 8
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.Text = "ButtonItem3"
        '
        'ButtonItem4
        '
        Me.ButtonItem4.ImagePaddingHorizontal = 8
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.Text = "ButtonItem4"
        '
        'TextBoxItem1
        '
        Me.TextBoxItem1.Name = "TextBoxItem1"
        '
        'frmTimekeepingDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(835, 701)
        Me.Controls.Add(Me.PanelEx1)
        Me.MaximizeBox = False
        Me.Name = "frmTimekeepingDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Details prestatie / loon"
        Me.PanelEx1.ResumeLayout(False)
        Me.PanelEx1.PerformLayout()
        Me.lblNaam.ResumeLayout(False)
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxLoonDetails.ResumeLayout(False)
        Me.GroupBoxLoonDetails.PerformLayout()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSupplement, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxInfo.ResumeLayout(False)
        Me.GroupBoxInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelEx1 As DevComponents.DotNetBar.PanelEx
    Friend WithEvents GroupBoxInfo As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBoxLoonDetails As System.Windows.Forms.GroupBox
    Friend WithEvents chkWordtUitbetaald As System.Windows.Forms.CheckBox
    Friend WithEvents chkSpaarplanner As System.Windows.Forms.CheckBox
    Friend WithEvents chkAfwezig As System.Windows.Forms.CheckBox
    Friend WithEvents chkIsFeestdag As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtUren As NumericBox.NumericBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dteDetail As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCommentaar As System.Windows.Forms.TextBox
    Friend WithEvents lblUren As System.Windows.Forms.Label
    Friend WithEvents txtSupplementUren As NumericBox.NumericBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCEPACode As System.Windows.Forms.TextBox
    Friend WithEvents txtSupplementBedrag As NumericBox.NumericBox
    Friend WithEvents lblSaldo As DevComponents.DotNetBar.Controls.ReflectionLabel
    Friend WithEvents txtAfwezig As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents dgvSupplement As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents cmbVoyages As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbFunctie As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbShiften As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbSupplement As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ItemPanel1 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents TextBoxItem1 As DevComponents.DotNetBar.TextBoxItem
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents btnEdit As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSave As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnValideer As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnValideerSluit As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents lblNaam As DevComponents.DotNetBar.PanelEx
    Friend WithEvents chkIsWeekend As System.Windows.Forms.CheckBox
    Friend WithEvents lblEdit As DevComponents.DotNetBar.LabelItem
    Friend WithEvents lblWBnummer As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdReport As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Bar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdBewaarDetail As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnOverboeken As DevComponents.DotNetBar.ButtonItem
End Class
