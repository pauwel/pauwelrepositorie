<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoonPerFunctie
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.cmdCategory = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdJob = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdShiften = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdEmployees = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdReport = New System.Windows.Forms.ToolStripButton
        Me.menuke = New System.Windows.Forms.ToolStrip
        Me.cmdAdd = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdUpdate = New System.Windows.Forms.ToolStripButton
        Me.tabdatagrid = New System.Windows.Forms.TabControl
        Me.fraLPF = New System.Windows.Forms.GroupBox
        Me.lblDubbeleShift = New System.Windows.Forms.Label
        Me.txtSupplDubbeleShift = New NumericBox.NumericBox
        Me.cmbFunctie = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbCat = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtEindejaarspremie = New NumericBox.NumericBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtTotaalOverUren = New NumericBox.NumericBox
        Me.txtTotBedragSupp = New NumericBox.NumericBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtFormule = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtStdLoon = New NumericBox.NumericBox
        Me.ToolStrip1.SuspendLayout()
        Me.menuke.SuspendLayout()
        Me.fraLPF.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdCategory, Me.ToolStripSeparator1, Me.cmdJob, Me.ToolStripSeparator4, Me.cmdShiften, Me.ToolStripSeparator5, Me.cmdEmployees, Me.ToolStripSeparator6, Me.cmdReport})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(858, 25)
        Me.ToolStrip1.TabIndex = 2
        Me.ToolStrip1.Text = "Category"
        '
        'cmdCategory
        '
        Me.cmdCategory.Image = Global.IMT_Timekeeping.My.Resources.Resources.CascadeWindowsHS
        Me.cmdCategory.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdCategory.Name = "cmdCategory"
        Me.cmdCategory.Size = New System.Drawing.Size(83, 22)
        Me.cmdCategory.Text = "Categories"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'cmdJob
        '
        Me.cmdJob.Image = Global.IMT_Timekeeping.My.Resources.Resources.CascadeWindowsHS
        Me.cmdJob.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdJob.Name = "cmdJob"
        Me.cmdJob.Size = New System.Drawing.Size(71, 22)
        Me.cmdJob.Text = "Functies"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'cmdShiften
        '
        Me.cmdShiften.Image = Global.IMT_Timekeeping.My.Resources.Resources.CascadeWindowsHS
        Me.cmdShiften.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdShiften.Name = "cmdShiften"
        Me.cmdShiften.Size = New System.Drawing.Size(64, 22)
        Me.cmdShiften.Text = "Shiften"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'cmdEmployees
        '
        Me.cmdEmployees.Image = Global.IMT_Timekeeping.My.Resources.Resources.CascadeWindowsHS
        Me.cmdEmployees.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEmployees.Name = "cmdEmployees"
        Me.cmdEmployees.Size = New System.Drawing.Size(113, 22)
        Me.cmdEmployees.Text = "Employee Types"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 25)
        '
        'cmdReport
        '
        Me.cmdReport.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdReport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(62, 22)
        Me.cmdReport.Text = "Report"
        '
        'menuke
        '
        Me.menuke.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAdd, Me.ToolStripSeparator3, Me.cmdDelete, Me.ToolStripSeparator2, Me.cmdUpdate})
        Me.menuke.Location = New System.Drawing.Point(0, 25)
        Me.menuke.Name = "menuke"
        Me.menuke.Size = New System.Drawing.Size(858, 25)
        Me.menuke.TabIndex = 3
        Me.menuke.Text = "Add"
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = Global.IMT_Timekeeping.My.Resources.Resources.AddTableHS
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(49, 22)
        Me.cmdAdd.Text = "Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        Me.cmdDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdUpdate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(65, 22)
        Me.cmdUpdate.Text = "Update"
        Me.cmdUpdate.Visible = False
        '
        'tabdatagrid
        '
        Me.tabdatagrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabdatagrid.Location = New System.Drawing.Point(0, 165)
        Me.tabdatagrid.Name = "tabdatagrid"
        Me.tabdatagrid.SelectedIndex = 0
        Me.tabdatagrid.Size = New System.Drawing.Size(857, 501)
        Me.tabdatagrid.TabIndex = 22
        Me.tabdatagrid.TabStop = False
        '
        'fraLPF
        '
        Me.fraLPF.Controls.Add(Me.lblDubbeleShift)
        Me.fraLPF.Controls.Add(Me.txtSupplDubbeleShift)
        Me.fraLPF.Controls.Add(Me.cmbFunctie)
        Me.fraLPF.Controls.Add(Me.cmbCat)
        Me.fraLPF.Controls.Add(Me.Label6)
        Me.fraLPF.Controls.Add(Me.txtEindejaarspremie)
        Me.fraLPF.Controls.Add(Me.Label11)
        Me.fraLPF.Controls.Add(Me.txtTotaalOverUren)
        Me.fraLPF.Controls.Add(Me.txtTotBedragSupp)
        Me.fraLPF.Controls.Add(Me.Label10)
        Me.fraLPF.Controls.Add(Me.Label12)
        Me.fraLPF.Controls.Add(Me.txtFormule)
        Me.fraLPF.Controls.Add(Me.Label3)
        Me.fraLPF.Controls.Add(Me.Label2)
        Me.fraLPF.Controls.Add(Me.Label1)
        Me.fraLPF.Controls.Add(Me.txtStdLoon)
        Me.fraLPF.Location = New System.Drawing.Point(12, 52)
        Me.fraLPF.Name = "fraLPF"
        Me.fraLPF.Size = New System.Drawing.Size(627, 112)
        Me.fraLPF.TabIndex = 31
        Me.fraLPF.TabStop = False
        '
        'lblDubbeleShift
        '
        Me.lblDubbeleShift.AutoSize = True
        Me.lblDubbeleShift.Location = New System.Drawing.Point(443, 72)
        Me.lblDubbeleShift.Name = "lblDubbeleShift"
        Me.lblDubbeleShift.Size = New System.Drawing.Size(139, 13)
        Me.lblDubbeleShift.TabIndex = 46
        Me.lblDubbeleShift.Text = "Supplement bij dubbele shift"
        '
        'txtSupplDubbeleShift
        '
        Me.txtSupplDubbeleShift.BackColor = System.Drawing.SystemColors.Window
        Me.txtSupplDubbeleShift.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSupplDubbeleShift.Location = New System.Drawing.Point(474, 86)
        Me.txtSupplDubbeleShift.Name = "txtSupplDubbeleShift"
        Me.txtSupplDubbeleShift.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSupplDubbeleShift.Size = New System.Drawing.Size(71, 20)
        Me.txtSupplDubbeleShift.TabIndex = 36
        Me.txtSupplDubbeleShift.Text = "0,00"
        '
        'cmbFunctie
        '
        Me.cmbFunctie.DisplayMember = "Text"
        Me.cmbFunctie.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbFunctie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFunctie.FormattingEnabled = True
        Me.cmbFunctie.ItemHeight = 14
        Me.cmbFunctie.Location = New System.Drawing.Point(166, 32)
        Me.cmbFunctie.Name = "cmbFunctie"
        Me.cmbFunctie.Size = New System.Drawing.Size(126, 20)
        Me.cmbFunctie.TabIndex = 45
        '
        'cmbCat
        '
        Me.cmbCat.DisplayMember = "Text"
        Me.cmbCat.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbCat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCat.FormattingEnabled = True
        Me.cmbCat.ItemHeight = 14
        Me.cmbCat.Location = New System.Drawing.Point(19, 31)
        Me.cmbCat.Name = "cmbCat"
        Me.cmbCat.Size = New System.Drawing.Size(126, 20)
        Me.cmbCat.TabIndex = 44
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(310, 70)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(109, 13)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "Eindejaarsvergoeding"
        '
        'txtEindejaarspremie
        '
        Me.txtEindejaarspremie.BackColor = System.Drawing.SystemColors.Window
        Me.txtEindejaarspremie.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtEindejaarspremie.Location = New System.Drawing.Point(333, 86)
        Me.txtEindejaarspremie.Name = "txtEindejaarspremie"
        Me.txtEindejaarspremie.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtEindejaarspremie.Size = New System.Drawing.Size(68, 20)
        Me.txtEindejaarspremie.TabIndex = 35
        Me.txtEindejaarspremie.Text = "0,00"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(115, 72)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(23, 13)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "OU"
        '
        'txtTotaalOverUren
        '
        Me.txtTotaalOverUren.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotaalOverUren.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtTotaalOverUren.Location = New System.Drawing.Point(115, 86)
        Me.txtTotaalOverUren.Name = "txtTotaalOverUren"
        Me.txtTotaalOverUren.NegativeNumber = True
        Me.txtTotaalOverUren.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtTotaalOverUren.Size = New System.Drawing.Size(75, 20)
        Me.txtTotaalOverUren.TabIndex = 33
        Me.txtTotaalOverUren.Text = "0,00"
        '
        'txtTotBedragSupp
        '
        Me.txtTotBedragSupp.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotBedragSupp.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtTotBedragSupp.Location = New System.Drawing.Point(214, 86)
        Me.txtTotBedragSupp.Name = "txtTotBedragSupp"
        Me.txtTotBedragSupp.NegativeNumber = True
        Me.txtTotBedragSupp.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtTotBedragSupp.Size = New System.Drawing.Size(75, 20)
        Me.txtTotBedragSupp.TabIndex = 34
        Me.txtTotBedragSupp.Text = "0,00"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(211, 70)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 13)
        Me.Label10.TabIndex = 41
        Me.Label10.Text = "Supplement"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(321, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(44, 13)
        Me.Label12.TabIndex = 40
        Me.Label12.Text = "Formule"
        '
        'txtFormule
        '
        Me.txtFormule.Location = New System.Drawing.Point(324, 32)
        Me.txtFormule.Name = "txtFormule"
        Me.txtFormule.Size = New System.Drawing.Size(288, 20)
        Me.txtFormule.TabIndex = 31
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 39
        Me.Label3.Text = "StdLoon"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(163, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Functie"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Category"
        '
        'txtStdLoon
        '
        Me.txtStdLoon.BackColor = System.Drawing.SystemColors.Window
        Me.txtStdLoon.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtStdLoon.Location = New System.Drawing.Point(19, 86)
        Me.txtStdLoon.Name = "txtStdLoon"
        Me.txtStdLoon.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtStdLoon.Size = New System.Drawing.Size(75, 20)
        Me.txtStdLoon.TabIndex = 32
        Me.txtStdLoon.Text = "0,00"
        '
        'frmLoonPerFunctie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(858, 664)
        Me.Controls.Add(Me.fraLPF)
        Me.Controls.Add(Me.tabdatagrid)
        Me.Controls.Add(Me.menuke)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmLoonPerFunctie"
        Me.Text = "Loon Per Functie"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.menuke.ResumeLayout(False)
        Me.menuke.PerformLayout()
        Me.fraLPF.ResumeLayout(False)
        Me.fraLPF.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdCategory As System.Windows.Forms.ToolStripButton
    Friend WithEvents menuke As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdJob As System.Windows.Forms.ToolStripButton
    Friend WithEvents cmdEmployees As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdShiften As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabdatagrid As System.Windows.Forms.TabControl
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdReport As System.Windows.Forms.ToolStripButton
    Friend WithEvents cmdUpdate As System.Windows.Forms.ToolStripButton
    Friend WithEvents fraLPF As System.Windows.Forms.GroupBox
    Friend WithEvents lblDubbeleShift As System.Windows.Forms.Label
    Friend WithEvents txtSupplDubbeleShift As NumericBox.NumericBox
    Friend WithEvents cmbFunctie As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbCat As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtEindejaarspremie As NumericBox.NumericBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtTotaalOverUren As NumericBox.NumericBox
    Friend WithEvents txtTotBedragSupp As NumericBox.NumericBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtFormule As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtStdLoon As NumericBox.NumericBox

End Class
