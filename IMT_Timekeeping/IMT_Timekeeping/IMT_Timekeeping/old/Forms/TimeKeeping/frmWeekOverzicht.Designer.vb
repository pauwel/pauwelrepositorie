<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWeekOverzicht
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dteBegin = New System.Windows.Forms.DateTimePicker
        Me.dteEind = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblNaam = New System.Windows.Forms.Label
        Me.chkPensioenplanner = New System.Windows.Forms.CheckBox
        Me.dgvData = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.cmdRefresh = New System.Windows.Forms.ToolStripButton
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdGenereer = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdReportOpen = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdGetOldData = New System.Windows.Forms.ToolStripButton
        Me.chkAll = New System.Windows.Forms.CheckBox
        Me.cmbLoonboek = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.ChkAlleGegevens = New System.Windows.Forms.CheckBox
        Me.dlgSave = New System.Windows.Forms.SaveFileDialog
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip
        Me.cmdOverzichtWachtbestand = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dteBegin
        '
        Me.dteBegin.Location = New System.Drawing.Point(10, 65)
        Me.dteBegin.Name = "dteBegin"
        Me.dteBegin.Size = New System.Drawing.Size(200, 20)
        Me.dteBegin.TabIndex = 0
        '
        'dteEind
        '
        Me.dteEind.Location = New System.Drawing.Point(217, 65)
        Me.dteEind.Name = "dteEind"
        Me.dteEind.Size = New System.Drawing.Size(200, 20)
        Me.dteEind.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Begindatum"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(214, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Einddatum"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Loonboek"
        '
        'lblNaam
        '
        Me.lblNaam.AutoSize = True
        Me.lblNaam.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNaam.Location = New System.Drawing.Point(190, 118)
        Me.lblNaam.Name = "lblNaam"
        Me.lblNaam.Size = New System.Drawing.Size(10, 15)
        Me.lblNaam.TabIndex = 6
        Me.lblNaam.Text = "."
        '
        'chkPensioenplanner
        '
        Me.chkPensioenplanner.AutoSize = True
        Me.chkPensioenplanner.Enabled = False
        Me.chkPensioenplanner.Location = New System.Drawing.Point(451, 119)
        Me.chkPensioenplanner.Name = "chkPensioenplanner"
        Me.chkPensioenplanner.Size = New System.Drawing.Size(105, 17)
        Me.chkPensioenplanner.TabIndex = 7
        Me.chkPensioenplanner.Text = "Pensioenplanner"
        Me.chkPensioenplanner.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvData.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvData.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvData.Location = New System.Drawing.Point(2, 142)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(849, 403)
        Me.dgvData.TabIndex = 8
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdRefresh, Me.cmdDelete, Me.ToolStripSeparator1, Me.cmdGenereer, Me.ToolStripSeparator2, Me.cmdReportOpen, Me.ToolStripSeparator3, Me.cmdGetOldData})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(850, 25)
        Me.ToolStrip1.TabIndex = 9
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Image = Global.IMT_Timekeeping.My.Resources.Resources.Refresh
        Me.cmdRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(76, 22)
        Me.cmdRefresh.Text = "Vernieuw"
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'cmdGenereer
        '
        Me.cmdGenereer.Image = Global.IMT_Timekeeping.My.Resources.Resources.euro
        Me.cmdGenereer.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdGenereer.Name = "cmdGenereer"
        Me.cmdGenereer.Size = New System.Drawing.Size(159, 22)
        Me.cmdGenereer.Text = "Genereer CEPA gegevens"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'cmdReportOpen
        '
        Me.cmdReportOpen.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdReportOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdReportOpen.Name = "cmdReportOpen"
        Me.cmdReportOpen.Size = New System.Drawing.Size(136, 22)
        Me.cmdReportOpen.Text = "Report alle gegevens"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'cmdGetOldData
        '
        Me.cmdGetOldData.Image = Global.IMT_Timekeeping.My.Resources.Resources.euro
        Me.cmdGetOldData.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdGetOldData.Name = "cmdGetOldData"
        Me.cmdGetOldData.Size = New System.Drawing.Size(365, 22)
        Me.cmdGetOldData.Text = "Cre�er CEPA bestand op basis van eerder gevalideerde gegevens"
        '
        'chkAll
        '
        Me.chkAll.AutoSize = True
        Me.chkAll.Checked = True
        Me.chkAll.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAll.Location = New System.Drawing.Point(451, 65)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(158, 17)
        Me.chkAll.TabIndex = 10
        Me.chkAll.Text = "Toon alle periode gegevens"
        Me.chkAll.UseVisualStyleBackColor = True
        '
        'cmbLoonboek
        '
        Me.cmbLoonboek.DisplayMember = "Text"
        Me.cmbLoonboek.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbLoonboek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLoonboek.FormattingEnabled = True
        Me.cmbLoonboek.ItemHeight = 14
        Me.cmbLoonboek.Location = New System.Drawing.Point(12, 113)
        Me.cmbLoonboek.Name = "cmbLoonboek"
        Me.cmbLoonboek.Size = New System.Drawing.Size(164, 20)
        Me.cmbLoonboek.TabIndex = 20
        '
        'ChkAlleGegevens
        '
        Me.ChkAlleGegevens.AutoSize = True
        Me.ChkAlleGegevens.Checked = True
        Me.ChkAlleGegevens.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkAlleGegevens.Location = New System.Drawing.Point(451, 89)
        Me.ChkAlleGegevens.Name = "ChkAlleGegevens"
        Me.ChkAlleGegevens.Size = New System.Drawing.Size(120, 17)
        Me.ChkAlleGegevens.TabIndex = 21
        Me.ChkAlleGegevens.Text = "Toon alle gegevens"
        Me.ChkAlleGegevens.UseVisualStyleBackColor = True
        '
        'dlgSave
        '
        Me.dlgSave.DefaultExt = "mht"
        Me.dlgSave.Filter = "CEPA bestand| *.txt"
        '
        'ToolStrip2
        '
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdOverzichtWachtbestand, Me.ToolStripButton1})
        Me.ToolStrip2.Location = New System.Drawing.Point(0, 25)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(850, 25)
        Me.ToolStrip2.TabIndex = 22
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'cmdOverzichtWachtbestand
        '
        Me.cmdOverzichtWachtbestand.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdOverzichtWachtbestand.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdOverzichtWachtbestand.Name = "cmdOverzichtWachtbestand"
        Me.cmdOverzichtWachtbestand.Size = New System.Drawing.Size(154, 22)
        Me.cmdOverzichtWachtbestand.Text = "Overzicht wachtbestand"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(68, 22)
        Me.ToolStripButton1.Text = "Provisie"
        '
        'frmWeekOverzicht
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(850, 544)
        Me.Controls.Add(Me.ToolStrip2)
        Me.Controls.Add(Me.ChkAlleGegevens)
        Me.Controls.Add(Me.cmbLoonboek)
        Me.Controls.Add(Me.chkAll)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.chkPensioenplanner)
        Me.Controls.Add(Me.lblNaam)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dteEind)
        Me.Controls.Add(Me.dteBegin)
        Me.Name = "frmWeekOverzicht"
        Me.Text = "werknemer overzicht"
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dteBegin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteEind As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblNaam As System.Windows.Forms.Label
    Friend WithEvents chkPensioenplanner As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdRefresh As System.Windows.Forms.ToolStripButton
    Friend WithEvents chkAll As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdGenereer As System.Windows.Forms.ToolStripButton
    Friend WithEvents cmbLoonboek As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ChkAlleGegevens As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdReportOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents dlgSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdGetOldData As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdOverzichtWachtbestand As System.Windows.Forms.ToolStripButton
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvData As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
End Class
