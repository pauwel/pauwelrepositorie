Option Compare Text

Imports System.Threading
Imports System.Windows.Forms



Public Class frmPlanning


    Dim menubar As New clsMenuBar

    Private planninggegevens As New clsPlanning
    Private dgv As DataGridView
    Private tabVoyage As TabControl, tagVoyage As Integer = 0

    Private shift As Integer, VOYAGE As Integer

    Private rijen As New ArrayList
    Private id As Integer

    Private SelectedTabIndex As Integer = 100

    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean


    Private Sub frmPlanning_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            LockWindowUpdate(Me.Handle.ToInt64)
            Me.WindowState = FormWindowState.Maximized
            Dim TIP As New BLL.GetShiftenToonInPLanning
            Try
                cmbShift.DataSource = TIP.GetShiften
                cmbShift.DisplayMember = "Omschrijving"
                cmbShift.ValueMember = "ShiftID"

                cmbShift.SelectedIndex = -1
            Catch ex As Exception

            End Try


        Catch ex As Exception
        Finally

            Dim mb As New clsMenuBar(cmdAdd, cmbEdit, cmdUpdate, cmdDelete)
            menubar = mb
            LOADSHIFTEN()

            menubar.Add()
            LockWindowUpdate(0)

        End Try


    End Sub


    Private Sub LOADSHIFTEN()

        LockWindowUpdate(Me.Handle.ToInt64)

        Try
            tabPrestaties.TabPages.Clear()
        Catch ex As Exception

        End Try


        Dim datum As DateTime = dtePrestatie.Value.Date

        Dim shiftid As Integer = 0
        Dim t As Integer = 0

        Dim TIP As New BLL.GetShiftenToonInPLanning
        For Each r As DataRow In TIP.GetShiften

            tabPrestaties.TabPages.Add(r("omschrijving"))
            shiftid = r("shiftid")
            tabPrestaties.TabPages(t).Tag = shiftid
            AddHandler tabPrestaties.SelectedIndexChanged, AddressOf tabPrestaties_SelectedIndexChanged

            
            Dim raster As New DataGridView

            tabPrestaties.TabPages(t).Controls.Add(raster)

            raster.Dock = DockStyle.Fill
            raster.AllowUserToAddRows = False
            raster.AllowUserToResizeRows = False
            raster.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            raster.ReadOnly = True

            AddHandler raster.KeyDown, AddressOf dgv_KeyDown
            AddHandler raster.KeyUp, AddressOf dgv_KeyUp
            AddHandler raster.CellEnter, AddressOf dgv_cellChanged
            AddHandler raster.CellClick, AddressOf dgv_cellClick

            t = t + 1

        Next
        Try
            tabPrestaties.SelectedIndex = 0
            LoadComboVoyages()

            GetTabSelection()
            refreshgrid()
        Catch ex As Exception

        End Try


        LockWindowUpdate(0)
    End Sub

    Private Sub dgv_cellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Dim dgview As DataGridView = sender
        dgview.Rows(dgview.CurrentCellAddress.Y).Selected = True
        menubar.edit()

    End Sub

    Private Sub dgv_cellChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Dim dgview As DataGridView = sender
        dgview.Rows(dgview.CurrentCellAddress.Y).Selected = True

    End Sub




    Private Sub GetTabSelection()
        shift = tabPrestaties.SelectedTab.Tag.ToString
        VOYAGE = cmbVoyage.SelectedValue
    End Sub


#Region "GetGridData"
    Dim thExecuteTaskAsync As Thread = Nothing
    Private Sub StartExecuteTaskAsync()
        'clear existing thread
        If Not thExecuteTaskAsync Is Nothing Then
            thExecuteTaskAsync.Abort()
            thExecuteTaskAsync.Join()
            thExecuteTaskAsync = Nothing
        End If
        'start a new thread to execute the task asynchronously
        thExecuteTaskAsync = New Thread(AddressOf ExecuteTaskAsync)
        thExecuteTaskAsync.Start()
    End Sub

    Private Sub ExecuteTaskAsync()
        planninggegevens.GridData = clBLL_getPrestatieBydate.GetPrestatieByDate(dtePrestatie.Value.Date, VOYAGE, shift)
    End Sub


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'this is necessary if the form is trying to close, even before the completion of task
        If Not thExecuteTaskAsync Is Nothing Then thExecuteTaskAsync.Abort()
    End Sub

#End Region


    Private Sub refreshgrid()

        LockWindowUpdate(Me.Handle.ToInt64)


        Try
            StartExecuteTaskAsync()
            For Each c As Control In tabPrestaties.SelectedTab.Controls


                If c.GetType.ToString = "System.Windows.Forms.DataGridView" Then

                    Dim dg As DataGridView = c
                    dgv = c

                    While thExecuteTaskAsync.ThreadState = ThreadState.Running
                        Thread.Sleep(0)
                    End While

                    dg.DataSource = planninggegevens.GridData
                    dg.Columns("PrestatieID").Visible = False
                    dg.Columns("gewerkteshiftid").Visible = False
                    dg.Columns("validated").Visible = False
                    dg.Columns("machineid").Visible = False
                    dg.Columns("werknemerid").Visible = False
                    dg.Columns("functieid").Visible = False
                    dg.Columns("wachtid").Visible = False
                    dg.Columns("voyageid").Visible = False

                End If
                
            Next
        Catch ex As NullReferenceException
            'ingeval er nog geen voyages geselecteerd werden
        End Try
        clear()

        cmbWerknemers.Enabled = True
        cmbShift.Enabled = True
        txtPloeg.Enabled = True
        chkWordtUitbetaald.Enabled = True
        menubar.Add()
        LockWindowUpdate(0)


    End Sub



    Private Sub tabPrestaties_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabPrestaties.SelectedIndexChanged
        If SelectedTabIndex <> tabPrestaties.SelectedIndex Then
            SelectedTabIndex = tabPrestaties.SelectedIndex
            LoadComboVoyages()
            GetTabSelection()
            refreshgrid()
        End If


    End Sub

    Sub LoadComboVoyages()
        cmbVoyage.DataSource = cllBLL_getVoyageByDate.GetVoyagesByDate(dtePrestatie.Value.Date, tabPrestaties.SelectedTab.Tag)
        cmbVoyage.DisplayMember = "voyagenbr"
        cmbVoyage.ValueMember = "VoyageID"
    End Sub

    Private Sub cmbWerknemers_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWerknemers.Enter
        LockWindowUpdate(Me.Handle.ToInt64)
        Dim FID As Integer = clsPlanner.SelectedFunctie


        With cmbWerknemers

            .DataSource = clBLL_GetWerknemersByFunctie.WerknemersBySelectedFunction(FID)
            .DisplayMember = "Naam"

            .ValueMember = "WerknemerID"
            .SelectedIndex = -1
        End With

        LockWindowUpdate(0)
    End Sub

    Private Sub cmbMachine_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Enter, cmbMachine.Enter
        LockWindowUpdate(Me.Handle.ToInt64)

        If chkAllMachines.Checked Then

            With cmbMachine
                .DataSource = clBLL_Machines.GetMAchine
                .DisplayMember = clBLL_Machines.GetMAchine.CodeWithOmschrijvingColumn.ToString
                .ValueMember = clBLL_Machines.GetMAchine.MachineIDColumn.ToString

            End With

        Else
            Dim FID As Integer = clsPlanner.SelectedFunctie
            With cmbMachine

                .DataSource = clBLL_GetMachinesPerSelectedFunctie.GetMachines(FID)
                .DisplayMember = clBLL_GetMachinesPerSelectedFunctie.GetMachines(FID).CodeWithOmschrijvingColumn.ToString
                .ValueMember = clBLL_GetMachinesPerSelectedFunctie.GetMachines(FID).MachineIDColumn.ToString
                .SelectedIndex = -1
            End With

        End If

        LockWindowUpdate(0)
    End Sub


    Private Sub chkAllMachines_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllMachines.CheckedChanged

        If chkAllMachines.Checked Then
            cmbWerknemers.DataSource = Nothing
            cmbWerknemers.Enabled = False
        Else
            cmbWerknemers.Enabled = True
            cmbMachine.SelectedIndex = -1
            cmbMachine.Text = Nothing
        End If


    End Sub


#Region "toevoeg acties"
    Private Sub clear()
        'cmbMachine.DataSource = Nothing
        cmbMachine.Text = ""
        cmbMachine.SelectedIndex = -1

        'cmbWerknemers.DataSource = Nothing
        cmbWerknemers.Text = ""
        cmbWerknemers.SelectedIndex = -1

        'txtPloeg.Clear()

    End Sub
    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        LockWindowUpdate(Me.Handle.ToInt64)

        'controle dat een functie gekoppeld is aan het besturen van een machine

        Dim cm As New BLL.UpdateLoonperfunctie
        Dim machineBesturen As Integer = cm.CheckMachineFunctie(clsPlanner.SelectedFunctie)

        If machineBesturen > 0 Then
            If String.IsNullOrEmpty(cmbMachine.Text) Then
                Dim answer As Integer = MsgBox("Deze functie welke normaliter een machine / voertuig bestuurd wordt gepland zonder machine!!" & vbNewLine & "Doorgaan?", MsgBoxStyle.YesNo)

                If answer = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If


        End If
        '''''''''''''''

        If (Not String.IsNullOrEmpty(cmbWerknemers.Text)) Or _
        (chkAllMachines.Checked = True And String.IsNullOrEmpty(cmbWerknemers.Text)) Then

            Dim schrijver As New BLL.WachtRegelWriter
            With schrijver

                .datum = dtePrestatie.Value.Date
                .functieid = clsPlanner.SelectedFunctie
                .machineid = cmbMachine.SelectedValue
                .ploeg = txtPloeg.Text
                .shiftid = cmbShift.SelectedValue

                .gewerkteshiftid = cmbShift.SelectedValue ' wordt gebruikt om prestaties weer te geven volgens shift
                .voyageid = clsPlanner.SelectedVoyage
                .werknemerid = cmbWerknemers.SelectedValue
                .wordtuitbetaald = chkWordtUitbetaald.Checked

                .weekend(.shiftid, .datum)
                .feestdag(.datum, .shiftid)
                .wachtypeid = 1 'Prestatie
                .Validated = False
                .store()
            End With

            LoadComboVoyages()

            cmbVoyage.SelectedValue = clsPlanner.SelectedVoyage
            GetTabSelection()
            refreshgrid()
        Else
            MsgBox(" selecteer een werknemer uit de lijst", MsgBoxStyle.Information)
        End If



        LockWindowUpdate(0)
    End Sub


    Private Sub dtePrestatie_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtePrestatie.ValueChanged

        LoadComboVoyages()
        GetTabSelection()
        refreshgrid()

    End Sub

    Private Sub cmbVoyage_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVoyage.SelectedValueChanged
        Try
            GetTabSelection()
            refreshgrid()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CorrectTabSelection()

        Dim tel As Integer = 0
        For Each t As TabPage In tabPrestaties.TabPages
            If t.Text = cmbShift.Text Then
                tabPrestaties.SelectedIndex = tel
                Exit For
            End If

            tel += 1
        Next
        tel = 0
    End Sub

    Private Sub cmbShift_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbShift.SelectedValueChanged
        Try

            'selecteren van de juiste tab
            CorrectTabSelection()
        Catch ex As Exception
            clsPlanner.Shift = 0
        End Try

    End Sub

    Private Sub cmbWerknemers_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWerknemers.SelectedValueChanged
        Try
            clsPlanner.Werknemer = cmbWerknemers.SelectedValue
            chkSpaarplanner.Checked = clsPlanner.IsSpaarplanner

           
            If chkSpaarplanner.Checked Then
                chkWordtUitbetaald.Checked = False
                chkWordtUitbetaald.Enabled = False
            Else
                chkWordtUitbetaald.Checked = True
                chkWordtUitbetaald.Enabled = True
            End If

        Catch ex As Exception
            clsPlanner.Werknemer = 0
        End Try

    End Sub

    Private Sub cmbMachine_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMachine.SelectedValueChanged
        Try
            clsPlanner.Machine = cmbMachine.SelectedValue
        Catch ex As Exception
            clsPlanner.Machine = 0
        End Try

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        GetSelectedRows()
        Verwijder()
    End Sub

    Private Sub dgv_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        GetSelectedRows()
    End Sub
    Private Sub dgv_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            Verwijder()
        End If
    End Sub

    Sub Verwijder()

        Dim d As New BLL.InsertWachtregels

        For dr As Integer = 0 To rijen.Count - 1
            id = rijen(dr)
            Try
                d.DeletePrestatie(id)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try


        Next
        GetTabSelection()
        refreshgrid()
    End Sub
    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgv.SelectedRows
            rijen.Add(dgv.Item("prestatieid", dr.Index).Value)
        Next


    End Sub
#End Region

    'Private Sub cmbWerknemers_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbWerknemers.KeyUp
    '    'If e.KeyCode = Keys.Enter Then
    '    '    cmdAdd.PerformClick()
    '    'End If
    'End Sub

   
    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        _frmReports.Datum = Me.dtePrestatie.Value.Date
        _frmReports.Reportnaam = "prestaties.rpt"
        _frmReports.ShowDialog()
    End Sub

    Private Sub cmbEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEdit.Click


        cmbWerknemers.Enabled = False
        cmbShift.Enabled = False
        txtPloeg.Enabled = False
        'veld disabelen
        If dgv.Item("validated", dgv.CurrentCellAddress.Y).Value Then
            chkWordtUitbetaald.Enabled = False
        Else
            chkWordtUitbetaald.Enabled = True
        End If

        Dim machine As Integer = dgv.Item("machineid", dgv.CurrentCellAddress.Y).Value
        Dim gSh As Integer = dgv.Item("gewerkteshiftid", dgv.CurrentCellAddress.Y).Value
        Dim pl As String = dgv.Item("ploeg", dgv.CurrentCellAddress.Y).Value
        Dim w As Integer = dgv.Item("werknemerid", dgv.CurrentCellAddress.Y).Value

        Dim f As Integer = dgv.Item("functieid", dgv.CurrentCellAddress.Y).Value
        Dim v As Integer = dgv.Item("voyageid", dgv.CurrentCellAddress.Y).Value



        clsPlanner.SelectedVoyage = v
        clsPlanner.SelectedFunctie = f

        'opladen van de juiste gegevens in combo werknemers & machines

        With cmbMachine

            .DataSource = clBLL_GetMachinesPerSelectedFunctie.GetMachines(f)
            .DisplayMember = clBLL_GetMachinesPerSelectedFunctie.GetMachines(f).CodeWithOmschrijvingColumn.ToString
            .ValueMember = clBLL_GetMachinesPerSelectedFunctie.GetMachines(f).MachineIDColumn.ToString

            .SelectedValue = machine
        End With

        With cmbWerknemers

            .DataSource = clBLL_GetWerknemersByFunctie.WerknemersBySelectedFunction(f)
            .DisplayMember = "Naam"

            .ValueMember = "WerknemerID"
            .SelectedValue = w
        End With

        cmbShift.SelectedValue = gSh
        txtPloeg.Text = pl

        menubar.Update()
    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click

        Dim u As New BLL.UpdateAfwezig

        Dim prestatieid As Integer = dgv.Item("prestatieid", dgv.CurrentCellAddress.Y).Value
        Dim machineid = cmbMachine.SelectedValue
        Dim wordtuitbetaald As Boolean = chkWordtUitbetaald.Checked
        Dim wachtid As Integer = dgv.Item("wachtid", dgv.CurrentCellAddress.Y).Value

        Dim controle As New BLL.InsertWachtregels
        If Not controle.CheckExisting(dtePrestatie.Value.Date, 0, _
            cmbShift.SelectedValue, machineid) Then

            u.UpdateprestatieNaInput(prestatieid, machineid, wordtuitbetaald, wachtid)

        Else
            clear()
        End If
        refreshgrid()

    End Sub
End Class