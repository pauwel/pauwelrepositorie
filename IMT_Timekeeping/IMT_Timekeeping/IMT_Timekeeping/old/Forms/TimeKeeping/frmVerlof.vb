
Option Compare Text


Imports BLL.Timekeeping
Public Class frmVerlof

    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean
    Private s As Decimal = 0

    Public Sub New(ByVal list As DataTable, ByVal naam As String)

        ' This call is required by the Windows Form Designer.
        LockWindowUpdate(Me.Handle.ToInt64)

        InitializeComponent()

        lblNaam.Text = naam
        dgvVerlof.DataSource = list

        dgvVerlof.SelectAll()
        berekensaldo()
        Me.txtBedrag.Text = Me.getMinimumLoon

        LockWindowUpdate(0)

    End Sub


    Private Sub berekensaldo()

        Dim waarde As New BLL.Wachtregel
        s = 0
        For Each r As DataGridViewRow In dgvVerlof.SelectedRows

            waarde.WachtID = dgvVerlof.Item("wachtid", r.Index).Value.ToString
            s += waarde.Saldo

        Next
        lblSaldo.Text = "Saldo: � " + Math.Round(s, 2).ToString
    End Sub



    Private Sub dgvVerlof_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvVerlof.SelectionChanged
        LockWindowUpdate(Me.Handle.ToInt64)

        Try
            dgvVerlof.Rows(dgvVerlof.CurrentCellAddress.Y).Selected = True
        Catch ex As Exception
        Finally
            berekensaldo()
        End Try
        
        LockWindowUpdate(0)
    End Sub


    Private Sub registreerVerlof()
        If validator() Then
            Dim verlofreg As New BLL.Verlofregistratie(dgvVerlof.Item("werknemerid", dgvVerlof.CurrentCellAddress.Y).Value.ToString, _
                                                        dteVerlof.Value.Date, txtBedrag.Text)


            For Each r As DataGridViewRow In Me.dgvVerlof.SelectedRows
                verlofreg.addSourceWachtID(r.Cells("WachtID").Value)

            Next

            Try
                Dim d As Integer = 36
                Select Case dteVerlof.Value.DayOfWeek
                    Case DayOfWeek.Saturday
                        d = Register.Shiften.zaterdag
                    Case DayOfWeek.Sunday
                        d = Register.Shiften.zondag
                    Case Else
                        d = Register.Shiften.dag
                End Select
                verlofreg.verlofdag = d

                verlofreg.boeken()

                Me.loadDetailForm(verlofreg.NewWachtregel.WachtID)
            Catch ex As Exception
                MsgBox("Het verlof kon niet worden geregistreerd", MsgBoxStyle.Information, "Fout verlofregistratie")
            End Try


          

            berekensaldo()
        End If
    End Sub

    Private Sub txtBedrag_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBedrag.KeyUp

        If e.KeyCode = Keys.Enter Then
            registreerVerlof()
        End If

    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        registreerVerlof()


    End Sub

    Function validator() As Boolean
        Dim ok As Boolean = True

        Dim gevraagd As Decimal = txtBedrag.Text
        If gevraagd > s Then
            MsgBox("Onvoldoende saldo, verminder het gevraagde bedrag of maak een bredere selectie", MsgBoxStyle.Information)
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub dgvVerlof_CellMouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvVerlof.CellMouseDoubleClick
        Dim id As Integer
        id = Me.dgvVerlof.Item("WachtID", Me.dgvVerlof.SelectedRows(0).Index).Value
        Me.loadDetailForm(id)

    End Sub

    Private Sub loadDetailForm(ByVal id As Integer)
        Try
            Dim form As Form = New frmTimekeepingDetail(id)
            form.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Opzoeken wat het minimum bedrag is dat moet aangevraagd worden 
    ''' </summary>
    ''' <returns>minimum loon </returns>
    ''' <remarks></remarks>
    Public Function getMinimumLoon() As Decimal
        Dim sl As New BLL.InsertBetalingenSpaarplanner

        Dim d As Integer = 36
        Select Case dteVerlof.Value.DayOfWeek
            Case DayOfWeek.Saturday
                d = Register.Shiften.zaterdag
            Case DayOfWeek.Sunday
                d = Register.Shiften.zondag
            Case Else
                d = Register.Shiften.dag
        End Select

        Dim StdLoon As Decimal = sl.getstdloon(dgvVerlof.Item("werknemerid", 0).Value.ToString, d)
        Return StdLoon
    End Function

    Private Sub dteVerlof_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteVerlof.ValueChanged
        txtBedrag.Text = getMinimumLoon()
    End Sub
End Class