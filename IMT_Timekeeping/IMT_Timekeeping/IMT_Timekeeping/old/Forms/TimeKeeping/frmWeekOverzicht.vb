
Option Compare Text

Imports System.Threading
Public Class frmWeekOverzicht

    Dim loading As Boolean = True
    Dim dt As DataTable

#Region "Asynchronous handling"


    Dim thExecuteTaskAsync As Thread = Nothing
    Private Sub StartExecuteTaskAsync()
        'clear existing thread
        If Not thExecuteTaskAsync Is Nothing Then
            thExecuteTaskAsync.Abort()
            thExecuteTaskAsync.Join()
            thExecuteTaskAsync = Nothing
        End If
        'start a new thread to execute the task asynchronously
        thExecuteTaskAsync = New Thread(AddressOf ExecuteTaskAsync)
        thExecuteTaskAsync.Start()
    End Sub

    Private Sub ExecuteTaskAsync()
        Dim lb As New BLL.GetAllLoonBoeknbrs
        dt = lb.GetAllLoonboeken
    End Sub


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'this is necessary if the form is trying to close, even before the completion of task
        If Not thExecuteTaskAsync Is Nothing Then thExecuteTaskAsync.Abort()
    End Sub

#End Region

    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean

    Private Sub frmWeekOverzicht_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LockWindowUpdate(Me.Handle.ToInt64)
        StartExecuteTaskAsync()
        Me.WindowState = FormWindowState.Maximized
        dteBegin.Value = DateAdd(DateInterval.Month, -1, dteEind.Value)
        LoadData()
        LoadGrid()
        loading = False
        LockWindowUpdate(0)
    End Sub

    Sub LoadData()
        While thExecuteTaskAsync.ThreadState = ThreadState.Running
            'effe wachten tot de data geladden is
            Thread.Sleep(0)
        End While

        cmbLoonboek.DataSource = dt
        cmbLoonboek.DisplayMember = "Loonboek"
        cmbLoonboek.ValueMember = "WerknemerID"
        cmbLoonboek.SelectedIndex = -1
    End Sub


    Private Sub cmbLoonboek_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLoonboek.Enter


    End Sub


    Private Sub cmbLoonboek_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLoonboek.SelectedValueChanged

        If Not loading Then
            Dim wn As New BLL.clsWerknemersnaam
            Try
                Dim id As Nullable(Of Integer) = cmbLoonboek.SelectedValue
                If id.HasValue Then

                    For Each r As DataRow In wn.GetAllLoonboeken(id).Rows
                        lblNaam.Text = r("Naam")
                        Try
                            chkPensioenplanner.Checked = r("Spaarplan")
                        Catch ex As InvalidCastException
                            chkPensioenplanner.Checked = False
                        End Try

                    Next
                End If

            Catch ex As InvalidCastException

            End Try
            LoadGrid()
        End If
    End Sub

    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        If chkAll.Checked Then
            cmbLoonboek.Enabled = False
            cmbLoonboek.SelectedValue = 0
            chkAll.Text = "Toon alle periode gegevens"
        Else
            cmbLoonboek.Enabled = True
            chkAll.Text = "Toon gegevens per loonboek"

        End If
        LoadGrid()
    End Sub
    Sub LoadGrid()


        Try
            Dim begin As DateTime = dteBegin.Value.Date
            Dim eind As DateTime = dteEind.Value.Date


            Dim bc As New BindingSource


            If Not chkAll.Checked Then
                Dim data As New BLL.GetBestandDataByWerknemersID
                Dim id As Integer = cmbLoonboek.SelectedValue

                bc.DataSource = data.Getdata(begin, eind, id)
                ' dgvData.DataSource = bc
            Else
                'alle gegevens ophalen
                Dim data As New BLL.GetAllBestanddata
                bc.DataSource = data.Getdata(begin, eind)
                'dgvData.DataSource = data.Getdata(begin, eind)
            End If
            If ChkAlleGegevens.Checked Then
                bc.Filter = ""
            Else
                bc.Filter = "cepa is null"
            End If
            dgvData.DataSource = bc



            For Each c As DataGridViewColumn In dgvData.Columns
                Select Case c.Name
                    Case "wbbrief", "datum", "shift", "datumuit", "status", "naam", "loonboek", "voyagenbr"
                        c.Visible = True
                        c.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                    Case "stdloon", "walking", "premie", "code30", "kledijvergoeding", "verzekeringsbedrag", "cepabedrag"
                        c.Visible = True
                        c.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                        c.DefaultCellStyle.Format = "f2"
                        c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    Case Else
                        c.Visible = False
                End Select
            Next
            Try
                If chkPensioenplanner.Checked Then
                    dgvData.Columns("groepsverzekering").Visible = True
                End If
            Catch ex As NullReferenceException

            End Try

            dgvData.Update()

        Catch ex As InvalidCastException

        End Try

    End Sub

    Private Sub dteBegin_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteBegin.ValueChanged

        If Not loading Then LoadGrid()
    End Sub

    Private Sub dteEind_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteEind.ValueChanged
        If Not loading Then LoadGrid()
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        Me.LoadGrid()
    End Sub




    Private Sub cmdGenereer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenereer.Click
        frmSetCEPA.ShowDialog()

        If frmSetCEPA.DialogResult = Windows.Forms.DialogResult.OK Then
            Me.LoadGrid()
            'genereer(bestand)

            Dim cf As New BLL.CEPAfile
            dlgSave.Title = "CEPA FILE"
            dlgSave.ShowDialog()
            cf.path = dlgSave.FileName
            If String.IsNullOrEmpty(dlgSave.FileName) Then
                MsgBox("U selecteerde annuleren of selecteerde een ongeldige locatie " & vbNewLine & "het CEPA bestand & borderel wordt niet gecre�erd", MsgBoxStyle.Information)

            Else
                cf.Datum = CEPAdatum
                cf.Presatieperiode = prestatieperiode
                cf.schrijver()

                dlgSave.Title = "BORDEREL"
                dlgSave.FileName = Nothing
                dlgSave.ShowDialog()
                If String.IsNullOrEmpty(dlgSave.FileName) Then
                    MsgBox("U selecteerde annuleren of selecteerde een ongeldige locatie " & vbNewLine & "de borderel wordt niet gecre�erd", MsgBoxStyle.Information)

                Else
                    cf.path = dlgSave.FileName
                    cf.borderel()
                End If


            End If
        End If


    End Sub

    Private Sub ChkAlleGegevens_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkAlleGegevens.CheckedChanged
        If ChkAlleGegevens.Checked Then
            ChkAlleGegevens.Text = "Toon alle gegevens"
            LoadGrid()
        Else
            ChkAlleGegevens.Text = "Toon openstaande gegevens"
            LoadGrid()
        End If
    End Sub

    Private Sub cmdReportOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportOpen.Click
        _frmReports.bDatum = dteBegin.Value.Date
        _frmReports.Edatum = dteEind.Value.Date
        Dim wn As Integer = cmbLoonboek.SelectedValue
        _frmReports.loonboek = wn
        _frmReports.Reportnaam = "betalingsoverzicht.rpt"
        _frmReports.ShowDialog()
    End Sub



    Private Sub cmdGetOldData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGetOldData.Click


        frmGetOldCEPAdata.ShowDialog()

        



    End Sub

    Private Sub cmdOverzichtWachtbestand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOverzichtWachtbestand.Click
        If cmbLoonboek.SelectedValue > 0 Then

            Dim wn As New BLL.Werknemers
            If wn.CheckSpaarplan(cmbLoonboek.SelectedValue) Then
                _frmReports.Werknemerid = cmbLoonboek.SelectedValue
                _frmReports.bDatum = dteBegin.Value.Date
                _frmReports.Edatum = dteEind.Value.Date
                _frmReports.Reportnaam = "Spaarplanner.rpt"

                _frmReports.ShowDialog()
            Else
                _frmReports.Werknemerid = cmbLoonboek.SelectedValue
                _frmReports.bDatum = dteBegin.Value.Date
                _frmReports.Edatum = dteEind.Value.Date
                _frmReports.Reportnaam = "NIETSpaarplanner.rpt"

                _frmReports.ShowDialog()
            End If




        Else
            MsgBox("Selecteer een loonboeknummer")
        End If


    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click

        Dim betaalid As Integer = dgvData.Item("betaalid", dgvData.CurrentCellAddress.Y).Value.ToString
        Dim d As New BLL.InsertBetalingenSpaarplanner

        Dim i As Integer = d.DeleteBetalingsregel(betaalid)
        If i = 0 Then
            MsgBox("betaling werd reeds uitgevoerd, record kon niet verwijdert worden", MsgBoxStyle.Information)
        Else
            LoadGrid()
        End If



    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        _frmReports.Reportnaam = "rptProvisie.rpt"
        _frmReports.ShowDialog()
    End Sub
End Class