Public Class frmReportKosten


    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean

    Private Sub frmReportKosten_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        LockWindowUpdate(Me.Handle.ToInt64)

        Dim v As New BLL.Voyagedetail
        dgvVoyages.DataSource = v.GetVoyageDeatil(3)
        dgvVoyages.Columns("voyageid").Visible = False
        LockWindowUpdate(0)

    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click

        Dim rij As Integer = 0
        For Each r As DataGridViewRow In dgvVoyages.SelectedRows
            rij = r.Index
        Next

        _frmReports.bDatum = Bdate.Value.Date
        _frmReports.Edatum = Edate.Value.Date
        _frmReports.VoyageID = dgvVoyages.Item("voyageid", rij).Value

        _frmReports.Reportnaam = "KostenAnalyse.rpt"
        _frmReports.ShowDialog()
    End Sub
End Class