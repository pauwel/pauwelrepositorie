﻿Public Class FrmKostenPerRederij

    Private Sub dteBegin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteBegin.ValueChanged
        dteEind.MinDate = dteBegin.Value
    End Sub

    Private Sub FrmKostenPerRederij_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbRederij.DataSource = clBLL_Rederijen.GetRederijen
        cmbRederij.DisplayMember = "rederijnaam"
        cmbRederij.ValueMember = "rederijid"
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        _frmReports.Reportnaam = "KostenAnalyseALL.rpt"
        _frmReports.RederijID = cmbRederij.SelectedValue
        _frmReports.bDatum = dteBegin.Value
        _frmReports.Edatum = dteEind.Value
        _frmReports.ShowDialog()
    End Sub
End Class