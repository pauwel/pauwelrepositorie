<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVoyage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.lblVoyageNbr = New System.Windows.Forms.Label
        Me.txtVoyageNbr = New System.Windows.Forms.TextBox
        Me.txtKade = New System.Windows.Forms.TextBox
        Me.lblKade = New System.Windows.Forms.Label
        Me.txtOmschrijving = New System.Windows.Forms.TextBox
        Me.lblOmschrijving = New System.Windows.Forms.Label
        Me.dgvVoyages = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.lblKostID = New System.Windows.Forms.Label
        Me.chkAfgewerkt = New System.Windows.Forms.CheckBox
        Me.cmdShowRecords = New System.Windows.Forms.ToolStrip
        Me.cmdAdd = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdEdit = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdUpdate = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdReport = New System.Windows.Forms.ToolStripButton
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.ShowRecordsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.cmdall = New System.Windows.Forms.ToolStripMenuItem
        Me.cmdActive = New System.Windows.Forms.ToolStripMenuItem
        Me.cmdNotActive = New System.Windows.Forms.ToolStripMenuItem
        Me.cmbKostID = New DevComponents.DotNetBar.Controls.ComboBoxEx
        CType(Me.dgvVoyages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmdShowRecords.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblVoyageNbr
        '
        Me.lblVoyageNbr.AutoSize = True
        Me.lblVoyageNbr.Location = New System.Drawing.Point(23, 79)
        Me.lblVoyageNbr.Name = "lblVoyageNbr"
        Me.lblVoyageNbr.Size = New System.Drawing.Size(83, 13)
        Me.lblVoyageNbr.TabIndex = 11
        Me.lblVoyageNbr.Text = "Voyage nummer"
        '
        'txtVoyageNbr
        '
        Me.txtVoyageNbr.Location = New System.Drawing.Point(112, 76)
        Me.txtVoyageNbr.MaxLength = 15
        Me.txtVoyageNbr.Name = "txtVoyageNbr"
        Me.txtVoyageNbr.Size = New System.Drawing.Size(135, 20)
        Me.txtVoyageNbr.TabIndex = 0
        '
        'txtKade
        '
        Me.txtKade.Location = New System.Drawing.Point(112, 102)
        Me.txtKade.MaxLength = 20
        Me.txtKade.Name = "txtKade"
        Me.txtKade.Size = New System.Drawing.Size(135, 20)
        Me.txtKade.TabIndex = 1
        '
        'lblKade
        '
        Me.lblKade.AutoSize = True
        Me.lblKade.Location = New System.Drawing.Point(23, 105)
        Me.lblKade.Name = "lblKade"
        Me.lblKade.Size = New System.Drawing.Size(32, 13)
        Me.lblKade.TabIndex = 13
        Me.lblKade.Text = "Kade"
        '
        'txtOmschrijving
        '
        Me.txtOmschrijving.Location = New System.Drawing.Point(112, 128)
        Me.txtOmschrijving.MaxLength = 50
        Me.txtOmschrijving.Name = "txtOmschrijving"
        Me.txtOmschrijving.Size = New System.Drawing.Size(335, 20)
        Me.txtOmschrijving.TabIndex = 2
        '
        'lblOmschrijving
        '
        Me.lblOmschrijving.AutoSize = True
        Me.lblOmschrijving.Location = New System.Drawing.Point(23, 131)
        Me.lblOmschrijving.Name = "lblOmschrijving"
        Me.lblOmschrijving.Size = New System.Drawing.Size(67, 13)
        Me.lblOmschrijving.TabIndex = 15
        Me.lblOmschrijving.Text = "Omschrijving"
        '
        'dgvVoyages
        '
        Me.dgvVoyages.AllowUserToAddRows = False
        Me.dgvVoyages.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvVoyages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvVoyages.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvVoyages.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvVoyages.Location = New System.Drawing.Point(0, 203)
        Me.dgvVoyages.Name = "dgvVoyages"
        Me.dgvVoyages.ReadOnly = True
        Me.dgvVoyages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVoyages.Size = New System.Drawing.Size(734, 361)
        Me.dgvVoyages.TabIndex = 19
        '
        'lblKostID
        '
        Me.lblKostID.AutoSize = True
        Me.lblKostID.Location = New System.Drawing.Point(23, 157)
        Me.lblKostID.Name = "lblKostID"
        Me.lblKostID.Size = New System.Drawing.Size(68, 13)
        Me.lblKostID.TabIndex = 20
        Me.lblKostID.Text = "Kostenplaats"
        '
        'chkAfgewerkt
        '
        Me.chkAfgewerkt.AutoSize = True
        Me.chkAfgewerkt.Location = New System.Drawing.Point(377, 104)
        Me.chkAfgewerkt.Name = "chkAfgewerkt"
        Me.chkAfgewerkt.Size = New System.Drawing.Size(80, 17)
        Me.chkAfgewerkt.TabIndex = 4
        Me.chkAfgewerkt.Text = "Afgewerkt?"
        Me.chkAfgewerkt.UseVisualStyleBackColor = True
        '
        'cmdShowRecords
        '
        Me.cmdShowRecords.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAdd, Me.ToolStripSeparator1, Me.cmdEdit, Me.ToolStripSeparator2, Me.cmdUpdate, Me.ToolStripSeparator3, Me.cmdDelete, Me.ToolStripSeparator4, Me.cmdReport})
        Me.cmdShowRecords.Location = New System.Drawing.Point(0, 24)
        Me.cmdShowRecords.Name = "cmdShowRecords"
        Me.cmdShowRecords.Size = New System.Drawing.Size(734, 25)
        Me.cmdShowRecords.TabIndex = 10
        Me.cmdShowRecords.Text = "Show records"
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = Global.IMT_Timekeeping.My.Resources.Resources.AddTableHS
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(49, 22)
        Me.cmdAdd.Text = "Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'cmdEdit
        '
        Me.cmdEdit.Image = Global.IMT_Timekeeping.My.Resources.Resources.EditInformationHS
        Me.cmdEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(47, 22)
        Me.cmdEdit.Text = "Edit"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdUpdate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(65, 22)
        Me.cmdUpdate.Text = "Update"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'cmdReport
        '
        Me.cmdReport.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdReport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(62, 22)
        Me.cmdReport.Text = "Report"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ShowRecordsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(734, 24)
        Me.MenuStrip1.TabIndex = 21
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ShowRecordsToolStripMenuItem
        '
        Me.ShowRecordsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdall, Me.cmdActive, Me.cmdNotActive})
        Me.ShowRecordsToolStripMenuItem.Image = Global.IMT_Timekeeping.My.Resources.Resources.DatabaseTable
        Me.ShowRecordsToolStripMenuItem.Name = "ShowRecordsToolStripMenuItem"
        Me.ShowRecordsToolStripMenuItem.Size = New System.Drawing.Size(109, 20)
        Me.ShowRecordsToolStripMenuItem.Text = "Show Records"
        '
        'cmdall
        '
        Me.cmdall.Name = "cmdall"
        Me.cmdall.Size = New System.Drawing.Size(128, 22)
        Me.cmdall.Text = "Alles"
        '
        'cmdActive
        '
        Me.cmdActive.Name = "cmdActive"
        Me.cmdActive.Size = New System.Drawing.Size(128, 22)
        Me.cmdActive.Text = "Actieve"
        '
        'cmdNotActive
        '
        Me.cmdNotActive.Name = "cmdNotActive"
        Me.cmdNotActive.Size = New System.Drawing.Size(128, 22)
        Me.cmdNotActive.Text = "Afgewerkt"
        '
        'cmbKostID
        '
        Me.cmbKostID.DisplayMember = "Text"
        Me.cmbKostID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbKostID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbKostID.FormattingEnabled = True
        Me.cmbKostID.ItemHeight = 14
        Me.cmbKostID.Location = New System.Drawing.Point(112, 154)
        Me.cmbKostID.Name = "cmbKostID"
        Me.cmbKostID.Size = New System.Drawing.Size(192, 20)
        Me.cmbKostID.TabIndex = 23
        '
        'frmVoyage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(734, 564)
        Me.Controls.Add(Me.cmbKostID)
        Me.Controls.Add(Me.chkAfgewerkt)
        Me.Controls.Add(Me.lblKostID)
        Me.Controls.Add(Me.dgvVoyages)
        Me.Controls.Add(Me.txtOmschrijving)
        Me.Controls.Add(Me.lblOmschrijving)
        Me.Controls.Add(Me.txtKade)
        Me.Controls.Add(Me.lblKade)
        Me.Controls.Add(Me.txtVoyageNbr)
        Me.Controls.Add(Me.lblVoyageNbr)
        Me.Controls.Add(Me.cmdShowRecords)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmVoyage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Voyages"
        CType(Me.dgvVoyages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmdShowRecords.ResumeLayout(False)
        Me.cmdShowRecords.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdShowRecords As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdUpdate As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblVoyageNbr As System.Windows.Forms.Label
    Friend WithEvents txtVoyageNbr As System.Windows.Forms.TextBox
    Friend WithEvents txtKade As System.Windows.Forms.TextBox
    Friend WithEvents lblKade As System.Windows.Forms.Label
    Friend WithEvents txtOmschrijving As System.Windows.Forms.TextBox
    Friend WithEvents lblOmschrijving As System.Windows.Forms.Label
    Friend WithEvents lblKostID As System.Windows.Forms.Label
    Friend WithEvents chkAfgewerkt As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ShowRecordsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdall As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdActive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdNotActive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmbKostID As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmdReport As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvVoyages As DevComponents.DotNetBar.Controls.DataGridViewX
End Class
