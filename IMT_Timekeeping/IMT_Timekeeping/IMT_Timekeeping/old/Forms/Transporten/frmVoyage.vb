

Option Compare Text

Public Class frmVoyage
    Dim voyage As BLL.CVoyage = New BLL.CVoyage
    Dim menubar As New clsMenuBar
    Dim blnEdit As Boolean = False
    Dim rijen As New ArrayList, id As Int32

    Dim all As Boolean

    Dim status As New ShowRecordsVoyage


    Private Sub frmVoyage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        Dim k As New BLL.Kostenplaats
        cmbKostID.DataSource = k.GetActiveKostenPlaats
        cmbKostID.DisplayMember = k.GetActiveKostenPlaats.KostenplaatsColumn.ToString
        cmbKostID.ValueMember = k.GetActiveKostenPlaats.KostIDColumn.ToString

        cmdall.Checked = False
        cmdActive.Checked = True
        cmdNotActive.Checked = False

        status.Status = False
        status.Vlag = 1
        Me.refreshgrid()
    End Sub

    Private Sub refreshgrid()

        Try
            With Me.dgvVoyages
                Dim d As New BLL.Voyagedetail
                If status.Vlag = 0 Then
                    .DataSource = d.GetVoyageDeatil(1)
                ElseIf status.Vlag = 1 Then
                    .DataSource = d.GetVoyageDeatil(0)
                Else
                    .DataSource = d.GetVoyageDeatil(2)
                End If
                .Columns("kostid").Visible = False
                .Columns("voyageid").Visible = False
                If .Rows.Count > 0 Then
                    .Rows(0).Selected = True
                End If
            End With
            Me.clearform()
        Catch ex As Exception

        End Try


    End Sub

    Private Sub readInput()
        'Me.voyage = New BLL.CVoyage
        Me.voyage.VoyageNbr = Me.txtVoyageNbr.Text
        Me.voyage.Omschrijving = Me.txtOmschrijving.Text
        Me.voyage.KostID = Me.cmbKostID.SelectedValue
        Me.voyage.Kade = Me.txtKade.Text
        Me.voyage.IsAfgewerkt = Me.chkAfgewerkt.Checked
    End Sub

    Private Sub clearform()
        Me.txtKade.Clear()
        Me.txtOmschrijving.Clear()
        Me.txtVoyageNbr.Clear()
        Me.chkAfgewerkt.Checked = False
    End Sub
    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Me.readInput() 'inputvelden in CVoyage object inladen 
        menubar.Load()
        Try
            clBLL_Voyages.InsertVoyage(Me.voyage)
        Catch ex As Exception
            MsgBox(e.ToString, MsgBoxStyle.Critical, "Fout")
        End Try

        Me.refreshgrid()

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Try
            GetSelectedRows()
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                Try
                    clBLL_Voyages.DeleteVoyage(id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next

        Catch ex As Exception

        End Try
        Me.refreshgrid()

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click



        Me.voyage = New BLL.CVoyage
        Me.voyage.Id = Me.dgvVoyages.Item(clBLL_Voyages.GetVoyages.VoyageIDColumn.ColumnName, Me.dgvVoyages.CurrentCellAddress.Y).Value
        Me.voyage.Kade = Me.dgvVoyages.Item(clBLL_Voyages.GetVoyages.KadeColumn.ColumnName, Me.dgvVoyages.CurrentCellAddress.Y).Value.ToString
        Me.voyage.IsAfgewerkt = Me.dgvVoyages.Item(clBLL_Voyages.GetVoyages.is_afgewerktColumn.ColumnName, Me.dgvVoyages.CurrentCellAddress.Y).Value
        Me.voyage.KostID = Me.dgvVoyages.Item(clBLL_Voyages.GetVoyages.KostIDColumn.ColumnName, Me.dgvVoyages.CurrentCellAddress.Y).Value
        Me.voyage.Omschrijving = Me.dgvVoyages.Item(clBLL_Voyages.GetVoyages.OmschrijvingColumn.ColumnName, Me.dgvVoyages.CurrentCellAddress.Y).Value
        Me.voyage.VoyageNbr = Me.dgvVoyages.Item(clBLL_Voyages.GetVoyages.VoyageNbrColumn.ColumnName, Me.dgvVoyages.CurrentCellAddress.Y).Value

        Me.txtKade.Text = Me.voyage.Kade
        Me.txtOmschrijving.Text = Me.voyage.Omschrijving
        Me.txtVoyageNbr.Text = Me.voyage.VoyageNbr
        Me.cmbKostID.SelectedValue = Me.voyage.KostID
        Me.chkAfgewerkt.Checked = Me.voyage.IsAfgewerkt

        menubar.Update()
    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Me.readInput()
        menubar.Load()
        clBLL_Voyages.UpdateVoyage(Me.voyage)
        Me.refreshgrid()
    End Sub


    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgvVoyages.SelectedRows
            rijen.Add(dgvVoyages.Item("voyageid", dr.Index).Value)
        Next
        'TODO boodschap weergeven, indien er geen rijen geselecteerd werden
    End Sub


    Private Sub txtVoyageNbr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVoyageNbr.TextChanged

        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtVoyageNbr.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If


    End Sub

    Private Sub dgvVoyages_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvVoyages.Click
        Try
            menubar.edit()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvVoyages_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvVoyages.KeyUp
        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    Try
                        clBLL_Voyages.DeleteVoyage(id)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                Next

            End If
        Catch ex As Exception

        End Try
        refreshgrid()
    End Sub

    Private Sub dgvVoyages_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvVoyages.KeyDown
        GetSelectedRows()
    End Sub

    Private Sub cmdall_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdall.Click
        cmdall.Checked = True
        cmdActive.Checked = False
        cmdNotActive.Checked = False

        status.Status = False
        status.Vlag = 2
        all = False
        refreshgrid()
    End Sub

    Private Sub cmdActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActive.Click
        cmdall.Checked = False
        cmdActive.Checked = True
        cmdNotActive.Checked = False

        status.Status = False
        status.Vlag = 1
        all = True
        refreshgrid()
    End Sub

    Private Sub cmdNotActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNotActive.Click
        cmdall.Checked = False
        cmdActive.Checked = False
        cmdNotActive.Checked = True

        status.Status = True
        status.Vlag = 0
        all = True
        refreshgrid()
    End Sub

    Private Sub cmbKostID_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbKostID.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    

        _frmReports.Reportnaam = "voyages.rpt"
        _frmReports.ShowDialog()
    End Sub

End Class