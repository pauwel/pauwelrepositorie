<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransportMiddel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ToolStripCategory = New System.Windows.Forms.ToolStrip
        Me.cmdAdd = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdEdit = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdUpdate = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdReport = New System.Windows.Forms.ToolStripButton
        Me.lblTransCode = New System.Windows.Forms.Label
        Me.txtTransCode = New System.Windows.Forms.TextBox
        Me.lblTransportOmschrijving = New System.Windows.Forms.Label
        Me.txtTransportOmschijving = New System.Windows.Forms.TextBox
        Me.lblRederij = New System.Windows.Forms.Label
        Me.dgvTransportMiddelen = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.cmbRederij = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.ToolStripCategory.SuspendLayout()
        CType(Me.dgvTransportMiddelen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStripCategory
        '
        Me.ToolStripCategory.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAdd, Me.ToolStripSeparator1, Me.cmdEdit, Me.ToolStripSeparator2, Me.cmdUpdate, Me.ToolStripSeparator3, Me.cmdDelete, Me.ToolStripSeparator4, Me.cmdReport})
        Me.ToolStripCategory.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripCategory.Name = "ToolStripCategory"
        Me.ToolStripCategory.Size = New System.Drawing.Size(624, 25)
        Me.ToolStripCategory.TabIndex = 10
        Me.ToolStripCategory.Text = "ToolStrip1"
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = Global.IMT_Timekeeping.My.Resources.Resources.AddTableHS
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(49, 22)
        Me.cmdAdd.Text = "Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'cmdEdit
        '
        Me.cmdEdit.Image = Global.IMT_Timekeeping.My.Resources.Resources.EditInformationHS
        Me.cmdEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(47, 22)
        Me.cmdEdit.Text = "Edit"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdUpdate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(65, 22)
        Me.cmdUpdate.Text = "Update"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'cmdReport
        '
        Me.cmdReport.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdReport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(62, 22)
        Me.cmdReport.Text = "Report"
        '
        'lblTransCode
        '
        Me.lblTransCode.AutoSize = True
        Me.lblTransCode.Location = New System.Drawing.Point(13, 41)
        Me.lblTransCode.Name = "lblTransCode"
        Me.lblTransCode.Size = New System.Drawing.Size(79, 13)
        Me.lblTransCode.TabIndex = 11
        Me.lblTransCode.Text = "Transport code"
        '
        'txtTransCode
        '
        Me.txtTransCode.Location = New System.Drawing.Point(101, 41)
        Me.txtTransCode.MaxLength = 10
        Me.txtTransCode.Name = "txtTransCode"
        Me.txtTransCode.Size = New System.Drawing.Size(158, 20)
        Me.txtTransCode.TabIndex = 0
        '
        'lblTransportOmschrijving
        '
        Me.lblTransportOmschrijving.AutoSize = True
        Me.lblTransportOmschrijving.Location = New System.Drawing.Point(16, 78)
        Me.lblTransportOmschrijving.Name = "lblTransportOmschrijving"
        Me.lblTransportOmschrijving.Size = New System.Drawing.Size(67, 13)
        Me.lblTransportOmschrijving.TabIndex = 13
        Me.lblTransportOmschrijving.Text = "Omschrijving"
        '
        'txtTransportOmschijving
        '
        Me.txtTransportOmschijving.Location = New System.Drawing.Point(101, 75)
        Me.txtTransportOmschijving.MaxLength = 100
        Me.txtTransportOmschijving.Multiline = True
        Me.txtTransportOmschijving.Name = "txtTransportOmschijving"
        Me.txtTransportOmschijving.Size = New System.Drawing.Size(442, 49)
        Me.txtTransportOmschijving.TabIndex = 1
        '
        'lblRederij
        '
        Me.lblRederij.AutoSize = True
        Me.lblRederij.Location = New System.Drawing.Point(16, 130)
        Me.lblRederij.Name = "lblRederij"
        Me.lblRederij.Size = New System.Drawing.Size(40, 13)
        Me.lblRederij.TabIndex = 15
        Me.lblRederij.Text = "Rederij"
        '
        'dgvTransportMiddelen
        '
        Me.dgvTransportMiddelen.AllowUserToAddRows = False
        Me.dgvTransportMiddelen.AllowUserToResizeColumns = False
        Me.dgvTransportMiddelen.AllowUserToResizeRows = False
        Me.dgvTransportMiddelen.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTransportMiddelen.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTransportMiddelen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTransportMiddelen.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTransportMiddelen.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvTransportMiddelen.Location = New System.Drawing.Point(0, 156)
        Me.dgvTransportMiddelen.Name = "dgvTransportMiddelen"
        Me.dgvTransportMiddelen.ReadOnly = True
        Me.dgvTransportMiddelen.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTransportMiddelen.Size = New System.Drawing.Size(624, 308)
        Me.dgvTransportMiddelen.TabIndex = 17
        Me.dgvTransportMiddelen.TabStop = False
        '
        'cmbRederij
        '
        Me.cmbRederij.DisplayMember = "Text"
        Me.cmbRederij.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbRederij.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRederij.FormattingEnabled = True
        Me.cmbRederij.ItemHeight = 14
        Me.cmbRederij.Location = New System.Drawing.Point(101, 130)
        Me.cmbRederij.Name = "cmbRederij"
        Me.cmbRederij.Size = New System.Drawing.Size(192, 20)
        Me.cmbRederij.TabIndex = 2
        '
        'frmTransportMiddel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(624, 464)
        Me.Controls.Add(Me.cmbRederij)
        Me.Controls.Add(Me.dgvTransportMiddelen)
        Me.Controls.Add(Me.lblRederij)
        Me.Controls.Add(Me.txtTransportOmschijving)
        Me.Controls.Add(Me.lblTransportOmschrijving)
        Me.Controls.Add(Me.txtTransCode)
        Me.Controls.Add(Me.lblTransCode)
        Me.Controls.Add(Me.ToolStripCategory)
        Me.Name = "frmTransportMiddel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TransportMiddel"
        Me.ToolStripCategory.ResumeLayout(False)
        Me.ToolStripCategory.PerformLayout()
        CType(Me.dgvTransportMiddelen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStripCategory As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdUpdate As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTransCode As System.Windows.Forms.Label
    Friend WithEvents txtTransCode As System.Windows.Forms.TextBox
    Friend WithEvents lblTransportOmschrijving As System.Windows.Forms.Label
    Friend WithEvents txtTransportOmschijving As System.Windows.Forms.TextBox
    Friend WithEvents lblRederij As System.Windows.Forms.Label
    Friend WithEvents cmbRederij As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdReport As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvTransportMiddelen As DevComponents.DotNetBar.Controls.DataGridViewX
End Class
