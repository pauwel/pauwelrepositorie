<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRederij
    Inherits IMT_Timekeeping.frmBaseConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtRederij = New System.Windows.Forms.TextBox
        Me.lblRederij = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtRederij
        '
        Me.txtRederij.Location = New System.Drawing.Point(58, 91)
        Me.txtRederij.Name = "txtRederij"
        Me.txtRederij.Size = New System.Drawing.Size(251, 20)
        Me.txtRederij.TabIndex = 6
        '
        'lblRederij
        '
        Me.lblRederij.AutoSize = True
        Me.lblRederij.Location = New System.Drawing.Point(12, 94)
        Me.lblRederij.Name = "lblRederij"
        Me.lblRederij.Size = New System.Drawing.Size(40, 13)
        Me.lblRederij.TabIndex = 7
        Me.lblRederij.Text = "Rederij"
        '
        'frmRederij
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(584, 464)
        Me.Controls.Add(Me.lblRederij)
        Me.Controls.Add(Me.txtRederij)
        Me.Name = "frmRederij"
        Me.Text = "Configuratie - Rederijen"
        Me.Controls.SetChildIndex(Me.txtRederij, 0)
        Me.Controls.SetChildIndex(Me.lblRederij, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtRederij As System.Windows.Forms.TextBox
    Friend WithEvents lblRederij As System.Windows.Forms.Label

End Class
