Public Class frmTransportMiddel

    Private menubar As New clsMenuBar

    Private id As Nullable(Of Int32)

    Private Sub refreshGrid()
        With Me.dgvTransportMiddelen
            .DataSource = clBLL_getTransportmiddelen.GetTransportMiddel
            .Columns("transid").Visible = False
            .Columns("rederijid").Visible = False
            If .Rows.Count > 0 Then
                .Rows(0).Selected = True
            End If
        End With
        'Velden terug leegmaken 
        Me.txtTransCode.Clear()
        Me.txtTransportOmschijving.Clear()
        Me.txtTransCode.Select()
        Me.cmbRederij.SelectedIndex = -1

    End Sub
    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        clBLL_TransportMiddel.InsertTransportMiddel(txtTransCode.Text, txtTransportOmschijving.Text, Me.cmbRederij.SelectedValue)
        Me.refreshGrid()
    End Sub

    Private Sub frmTransportMiddel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
       
        Me.loadRederijen()
        Me.refreshGrid()


    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        id = Me.dgvTransportMiddelen.Item(clBLL_TransportMiddel.GetTransportMiddel.TransIDColumn.ColumnName, Me.dgvTransportMiddelen.CurrentCellAddress.Y).Value
        Try
            clBLL_TransportMiddel.DeleteTransportMiddel(id)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.refreshGrid()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click


        Dim transcode As String = Me.dgvTransportMiddelen.Item(clBLL_TransportMiddel.GetTransportMiddel.TransCodeColumn.ColumnName, Me.dgvTransportMiddelen.CurrentCellAddress.Y).Value
        Dim transportOmschrijving As String = Me.dgvTransportMiddelen.Item(clBLL_TransportMiddel.GetTransportMiddel.TransportOmschrijvingColumn.ColumnName, Me.dgvTransportMiddelen.CurrentCellAddress.Y).Value

        Dim rederij As Integer = Me.dgvTransportMiddelen.Item(clBLL_TransportMiddel.GetTransportMiddel.RederijIDColumn.ColumnName, Me.dgvTransportMiddelen.CurrentCellAddress.Y).Value

        Me.txtTransCode.Text = transcode
        Me.txtTransportOmschijving.Text = transportOmschrijving
        'Me.txtRederij.Text = rederij
        Me.cmbRederij.SelectedValue = rederij
        Me.txtTransCode.Select()
        menubar.Update()
    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click


        id = Me.dgvTransportMiddelen.Item(clBLL_TransportMiddel.GetTransportMiddel.TransIDColumn.ColumnName, Me.dgvTransportMiddelen.CurrentCellAddress.Y).Value
        Dim transcode As String = Me.txtTransCode.Text
        Dim omschrijving As String = Me.txtTransportOmschijving.Text
        Dim rederij As String = Me.cmbRederij.SelectedValue

        clBLL_TransportMiddel.UpdateTransportMiddel(id, transcode, omschrijving, rederij)
        Me.refreshGrid()
        'nadat update gebeurt is cmd button terug disable 
        Me.cmdUpdate.Enabled = False
        menubar.Load()

    End Sub

    Private Function formValuesCheck() As Boolean
        Dim check As Boolean = True

        If Me.txtTransCode.Text = "" Then
            check = False
        ElseIf Me.cmbRederij.SelectedValue = "" Then
            check = False
        ElseIf Me.txtTransportOmschijving.Text = "" Then
            check = False
        End If

        Return check
    End Function

   

    ''' <summary>
    ''' Laden van rederijen in combo box 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadRederijen()
        With Me.cmbRederij
            .DataSource = clsPreload.rederij
            .DisplayMember = clsPreload.rederijDisplayMember
            .ValueMember = clsPreload.rederijValueMember
        End With
    End Sub

    Private Sub dgvTransportMiddelen_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvTransportMiddelen.SelectionChanged

        If dgvTransportMiddelen.CurrentCellAddress.Y >= 0 Then
            menubar.edit()
        Else
            menubar.Load()
        End If

    End Sub

    Private Sub txtTransCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTransCode.TextChanged
        If String.IsNullOrEmpty(Trim(txtTransCode.Text)) Then
            menubar.Load()
        Else
            menubar.Add()
        End If
    End Sub

    Private Sub cmbRederij_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbRederij.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    
        _frmReports.Reportnaam = "transportmiddelen.rpt"
        _frmReports.ShowDialog()
    End Sub

End Class