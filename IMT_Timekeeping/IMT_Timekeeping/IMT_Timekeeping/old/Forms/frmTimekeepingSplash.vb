Public NotInheritable Class frmTimekeepingSplash


    Private Sub frmTimekeepingSplash_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        With clsPreload
            .category = clBLL_Category.GetCategories
            .categorydisplaymember = clBLL_Category.GetCategories.CatCodeColumn.ToString
            .categoryvaluemember = clBLL_Category.GetCategories.CatIDColumn.ToString


            .employeetypes = clBLL_TypeWerknemers.GetWerknemersType
            .employeetypesdisplaymember = clBLL_TypeWerknemers.GetWerknemersType.StelselColumn.ToString()
            .employeetypesvaluemember = clBLL_TypeWerknemers.GetWerknemersType.TypeWerknemerIDColumn.ToString()

            .Shift = clBLL_Shiften.GetShift
            .shiftdisplaymember = clBLL_Shiften.GetShift.OmschrijvingColumn.ToString
            .shiftvaluemember = clBLL_Shiften.GetShift.ShiftIDColumn.ToString

            .functies = clBLL_Functies.GetFuncties
            .functiesdisplaymember = clBLL_Functies.GetFuncties.FunctieCodeColumn.ToString
            .functiesvaluemember = clBLL_Functies.GetFuncties.FunctieIDColumn.ToString

            .employees = clBLL_werknemers.GetWerknemers
            '.employeesdisplaymember
            '.employeesvaluemember

            .land = clBLL_landen.GetLanden
            .landdisplaymember = clBLL_landen.GetLanden.LandColumn.ToString
            .landvaluemember = clBLL_landen.GetLanden.LandIDColumn.ToString

        End With


    End Sub



End Class
