

Imports DAL
Public Class WerknemersPrestatie

    Private _Adapter As KostenTableAdapters.GetPrestatieWerknemersByShiftByDateTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.GetPrestatieWerknemersByShiftByDateTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.GetPrestatieWerknemersByShiftByDateTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetWerknemers(ByVal datum As DateTime, ByVal shiftid As Integer, ByVal voyageid As Integer) As DAL.Kosten.GetPrestatieWerknemersByShiftByDateDataTable
        Return Me.Adapter.GetData(datum, shiftid, voyageid)
    End Function


End Class
