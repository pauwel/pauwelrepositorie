Imports DAL

Public Class SplitPrestatie


    Private _Adapter As KostenTableAdapters.SplitPrestatie = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.SplitPrestatie
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.SplitPrestatie
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Split(ByVal machineid As Integer, ByVal datum As DateTime, ByVal resturen As Decimal, _
    ByVal ploeg As String, ByVal voyageid As Integer, ByVal wachtid As Integer, ByVal prestatieid As Integer, _
    ByVal gewerkteshiftid As Integer, ByVal newUren As Decimal, ByVal restmachinekost As Decimal, ByVal newmachinekost As Decimal) As Integer

        Return Me.Adapter.SplitPrestatie(machineid, datum, resturen, newUren, restmachinekost, newmachinekost, ploeg, voyageid, wachtid, prestatieid, gewerkteshiftid)
    End Function

End Class
