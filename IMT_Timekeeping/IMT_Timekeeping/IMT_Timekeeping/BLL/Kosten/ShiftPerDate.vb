
Imports DAL
Public Class ShiftPerDate

    Private _Adapter As KostenTableAdapters.GetPrestatieShiftenPerDatumTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.GetPrestatieShiftenPerDatumTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.GetPrestatieShiftenPerDatumTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetShiften(ByVal datum As DateTime) As DAL.Kosten.GetPrestatieShiftenPerDatumDataTable
        Return Me.Adapter.GetData(datum)
    End Function

End Class
