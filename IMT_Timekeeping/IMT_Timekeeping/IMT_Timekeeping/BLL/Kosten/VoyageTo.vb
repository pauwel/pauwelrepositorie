Imports DAL


Public Class VoyageTo

    Private _Adapter As KostenTableAdapters.GetPrestatieNotSelectedVoyageTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.GetPrestatieNotSelectedVoyageTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.GetPrestatieNotSelectedVoyageTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetVoyagesTo(ByVal voyageid As Integer) As DAL.Kosten.GetPrestatieNotSelectedVoyageDataTable
        Return Me.Adapter.GetData(voyageid)
    End Function

End Class


