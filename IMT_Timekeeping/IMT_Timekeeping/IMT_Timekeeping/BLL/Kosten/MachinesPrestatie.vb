
Imports DAL

Public Class MachinesPrestatie



    Private _Adapter As KostenTableAdapters.GetPrestatieMachinesByShiftByDateTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.GetPrestatieMachinesByShiftByDateTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.GetPrestatieMachinesByShiftByDateTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetMachines(ByVal datum As DateTime, ByVal shiftid As Integer, ByVal voyageid As Integer) As DAL.Kosten.GetPrestatieMachinesByShiftByDateDataTable
        Return Me.Adapter.GetData(datum, shiftid, voyageid)
    End Function

End Class
