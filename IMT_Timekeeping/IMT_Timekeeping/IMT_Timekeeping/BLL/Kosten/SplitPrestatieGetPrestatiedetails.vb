
Imports DAL

Public Class SplitPrestatieGetPrestatiedetails


    Private _Adapter As KostenTableAdapters.SplitPrestatieGetPrestatieregelTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As KostenTableAdapters.SplitPrestatieGetPrestatieregelTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New KostenTableAdapters.SplitPrestatieGetPrestatieregelTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetPresatieregel(ByVal prestatieid As Integer) As DAL.Kosten.SplitPrestatieGetPrestatieregelRow

        Return Me.Adapter.GetData(prestatieid).Rows(0)

    End Function

End Class
