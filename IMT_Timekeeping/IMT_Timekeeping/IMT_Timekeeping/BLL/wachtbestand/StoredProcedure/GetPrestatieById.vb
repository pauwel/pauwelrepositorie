
Imports DAL
Public Class GetPrestatieById


    Private _adapter As SPWachtbestandTableAdapters.GetPrestatieByIdTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetPrestatieByIdTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SPWachtbestandTableAdapters.GetPrestatieByIdTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Getdata(ByVal id As Integer) As SPWachtbestand.GetPrestatieByIdDataTable
        Return Me.Adapter.GetData(id)
    End Function
End Class

