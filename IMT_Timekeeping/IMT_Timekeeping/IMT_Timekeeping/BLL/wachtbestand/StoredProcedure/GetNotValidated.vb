
Imports DAL

Imports System.Text
Imports System.Configuration
Imports System.Data.SqlClient


Public Class GetNotValidated

    Dim sb As New StringBuilder

    Private _adapter As SPWachtbestandTableAdapters.GetNotValidatedPrestatiesTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetNotValidatedPrestatiesTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SPWachtbestandTableAdapters.GetNotValidatedPrestatiesTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Getdata(ByVal loonboek As Integer, ByVal bdate As DateTime, ByVal edate As DateTime) As SPWachtbestand.GetNotValidatedPrestatiesDataTable
        Return Me.Adapter.GetData(loonboek, bdate, edate)
    End Function

End Class
