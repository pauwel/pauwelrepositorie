
Imports DAL

Public Class GetBestandDataByWerknemersID

    Private _Adapter As SPWachtbestandTableAdapters.GetBestandDataByWerknemersIDTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As SPWachtbestandTableAdapters.GetBestandDataByWerknemersIDTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New SPWachtbestandTableAdapters.GetBestandDataByWerknemersIDTableAdapter
            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function Getdata(ByVal begin As DateTime, ByVal eind As DateTime, ByVal id As Integer) As SPWachtbestand.GetBestandDataByWerknemersIDDataTable

        Return Adapter.GetData(begin, eind, id)

    End Function


End Class
