Option Explicit On
Imports DAL

Public Class GetLoondetailsByWachtid
    Private _adapter As DAL.wachtbestandTableAdapters.GetLoondetailsByWachtidTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL.wachtbestandTableAdapters.GetLoondetailsByWachtidTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New DAL.wachtbestandTableAdapters.GetLoondetailsByWachtidTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Getdata(ByVal wachtid As Integer) As DAL.wachtbestand.GetLoondetailsByWachtidDataTable
        Return Me.Adapter.GetData(wachtid)
    End Function
End Class
