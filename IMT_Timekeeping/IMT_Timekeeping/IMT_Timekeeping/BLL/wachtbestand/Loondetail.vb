Option Compare Text

Imports DAL

Public Class Loondetail
    Private _adapter As DAL.wachtbestandTableAdapters.tblLoonDetailTableAdapter

    Protected ReadOnly Property Adapter() As wachtbestandTableAdapters.tblLoonDetailTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New wachtbestandTableAdapters.tblLoonDetailTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Sub insertLoonDetail(ByVal wachtID As Integer, ByVal bedrag As Decimal, ByVal redenID As Integer, ByVal datum As DateTime, ByVal commentaar As String, ByVal loontypeID As Integer, ByVal cepacode As String, ByVal uren As Decimal, ByVal relatedWachtID As Integer)
        Me.Adapter.Insert(wachtID, uren, bedrag, redenID, datum, commentaar, loontypeID, cepacode, relatedWachtID)
    End Sub

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function getLoonDetailByWachtid(ByVal wachtID As Integer)
        Return Me.Adapter.GetDataByWachtID(wachtID)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function getLoonDetailByWachtidGrouped(ByVal wachtID As Integer)
        Return Me.Adapter.GetDataByWachtIdGrouped(wachtID)
    End Function
End Class
