Imports DAL

Public Class LoonType

    Private _adapter As DAL.wachtbestandTableAdapters.tblLoontypeTableAdapter = Nothing

    Protected ReadOnly Property Adapter() As wachtbestandTableAdapters.tblLoontypeTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New wachtbestandTableAdapters.tblLoontypeTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Sub insertloonType(ByVal omschrijving As String, ByVal special As Boolean, ByVal actief As Boolean, ByVal codeverplicht As Boolean, ByVal ureninvoer As Boolean)
        Me.Adapter.Insert(omschrijving, special, actief, codeverplicht, ureninvoer)
    End Sub
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
        Public Sub UpdateloonType(ByVal omschrijving As String, ByVal special As Boolean, ByVal actief As Boolean, ByVal codeverplicht As Boolean, ByVal ureninvoer As Boolean, ByVal id As Integer)
        Me.Adapter.Update(omschrijving, special, actief, codeverplicht, ureninvoer, id)
    End Sub
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
            Public Sub DeleteloonType(ByVal id As Integer)
        Me.Adapter.Delete(id)
    End Sub
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function getLoontype()
        Return Me.Adapter.GetData
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
       Public Function CheckCepaVerplicht(ByVal id As Int32) As Boolean
        Return Me.Adapter.CEPAVerplicht(id)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
           Public Function CheckUrenInvoer(ByVal id As Int32) As Boolean
        Return Me.Adapter.CheckUrenInvoer(id)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
       Public Function getAllLoontypes()
        Return Me.Adapter.GetDataByAllLoontypes
    End Function
    ''' <summary>
    ''' Alle Loontypes waar user input toegestaan is
    ''' </summary>
    ''' <remarks></remarks>
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function getAllowedUserInput()
        Return Me.Adapter.GetDataByAllowedUserInput
    End Function
End Class

