
Imports DAL


Public Class CheckAantalGevalideerdeGewerkteShiften

    Private _adapter As wachtbestandTableAdapters.aantalGewerkteShiften = Nothing
    Protected ReadOnly Property Adapter() As wachtbestandTableAdapters.aantalGewerkteShiften
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New wachtbestandTableAdapters.aantalGewerkteShiften
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function Getdata(ByVal werknemerid As Integer, ByVal datum As DateTime) As Integer

        Return Me.Adapter.CheckAantalGewerkteShiften(werknemerid, datum)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
   Public Function CheckReedsUitbetaald(ByVal wachtid As Integer) As Integer

        Return Me.Adapter.Check_BeforeValidateddelete(wachtid)
    End Function
   

End Class