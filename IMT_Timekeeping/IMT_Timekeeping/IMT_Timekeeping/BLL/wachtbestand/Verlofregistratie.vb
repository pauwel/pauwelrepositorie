Public Class Verlofregistratie

    Private loontypes As DataTable

    Public Sub New()
        Dim LT As BLL.LoonType = New BLL.LoonType
        Me.loontypes = LT.getAllLoontypes
        Me.sourceWachtIds = New List(Of Integer)
    End Sub

    ''' <summary>
    ''' Constructor voor nieuwe verlof registratie
    ''' </summary>
    ''' <param name="werknemerID"></param>
    ''' <param name="datum">Datum waarop verlof wordt genomen</param>
    ''' <param name="bedrag">Bedrag dat werknemer wil uitbetaald krijgen</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal werknemerID As Integer, ByVal datum As Date, ByVal bedrag As Decimal)
        Me.New()
        Me.WerknemerID = werknemerID
        Me.Datum = datum
        Me.Bedrag = bedrag
    End Sub

    ''' <summary>
    ''' Boekt alle nodige records voor een verlofregistratie
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub boeken()
        'nakijken of er wel wachtID's werden meegegeven
        If Me.sourceWachtIds.Count = 0 Then
            Throw New Exception("Er werden geen loonbriefjes aangegeven waarop het verlof kan worden afgeboekt")
        End If
        'is het gevraagde loon voldoende? 
        If Not Me.checkStdLoon Then
            Throw New Exception("Gevraagde bedrag is kleiner dan minimum bedrag ( " & Me.getMinimumLoon.ToString & " )")
        End If


        ' eerst een nieuwe wachtregel aanmaken test
        ' eerst een nieuwe wachtregel aanmaken 'testje
        Me.createWachtregel()

        Dim resterend As Decimal = Me.Bedrag 'resterend initieel met gevraagd bedrag gelijkstellen

        Dim teBoeken As Decimal
        Dim wr As Wachtregel
        Dim details As DataTable
        Dim i As Integer = 0 'teller


        While resterend > 0

            wr = New BLL.Wachtregel(Me.sourceWachtIds.Item(i))

            details = wr.DetailregelsGrouped

            For Each r As DataRow In Me.loontypes.Rows
                Dim loontype As Integer = r("loontypeid")

                For Each det As DataRow In details.Rows
                    If det("loontypeid") = loontype Then
                        Dim saldo As Decimal = det("bedrag") 'saldo op bron briefje

                        If saldo > resterend Then
                            teBoeken = resterend 'enkel nog resterend afboeken, rest laten staan 
                        Else
                            teBoeken = saldo 'volledige detaillijn afboeken
                        End If
                        If teBoeken > 0 Then 'geen boeking doen als het bedrag toch 0 is 
                            wr.addDetail((teBoeken * -1), Timekeeping.Register.LoonDetailReden.Verlof, "Overboeking voor verlof naar: " & Me.NewWachtregel.Wbbrief, loontype, Nothing, Nothing, Me.NewWachtregel.WachtID)
                            resterend = resterend - teBoeken
                        End If
                        Exit For
                    End If
                Next

            Next
            i += 1
        End While
      


        Dim validatie As New BLL.WachtregelValidatie(Me.NewWachtregel)
        validatie.valideerWachtRegel() 'bij validatie worden de standaard bedragen (stdLoon, walking time en premie) al toegevoegd
        ' het verschil tussen gevraagd en standaard waardes bij validatie nog toevoegen als code30 :
        Dim code30bedrag As Decimal = Me.Bedrag - Me.NewWachtregel.Saldo
        If Not code30bedrag = 0 Then
            Me.NewWachtregel.addDetail(code30bedrag, Timekeeping.Register.LoonDetailReden.System, "", Timekeeping.Register.Loontypes.Code30, Nothing, Nothing)
        End If

    End Sub

    ''' <summary>
    ''' Een nieuwe prestatie/wachtregel (loonbriefje) wordt aangemaakt om het verlof op te boeken 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub createWachtregel()
        Dim schrijver As New BLL.WachtRegelWriter
        With schrijver
            .datum = Me.Datum
            .functieid = 0  ' geen bepaalde functie meegeven, de laagste functie wordt dan geselecteerd in stored procedure 
            .machineid = 0
            .ploeg = Nothing
            .shiftid = Timekeeping.Register.Shiften.dag

            .gewerkteshiftid = Timekeeping.Register.Shiften.dag ' wordt gebruikt om prestaties weer te geven volgens shift
            .voyageid = Timekeeping.Register.Voyages.Verlof
            .werknemerid = Me.WerknemerID
            .wordtuitbetaald = True

            .weekend(.shiftid, .datum)
            .feestdag(.datum, .shiftid)
            .wachtypeid = Timekeeping.Register.WachtTypes.Verlof  'verlof
            .Validated = False

        End With
        schrijver.store()
        Dim nieuw As Integer = schrijver.wachtid
        Me.NewWachtID = nieuw



    End Sub


    ''' <summary>
    ''' Een wachtID toevoegen aan de lijst met wachtID's waarvan het verlof wordt afgeboekt
    ''' </summary>
    ''' <param name="wachtid"></param>
    ''' <remarks></remarks>
    Public Sub addSourceWachtID(ByVal wachtid As Integer)
        Me.sourceWachtIds.Add(wachtid)
    End Sub
    ''' <summary>
    ''' Controle of het gevraagde loon boven het minimum ligt 
    ''' </summary>
    ''' <returns>true indien ok, false indien niet ok</returns>
    ''' <remarks></remarks>
    Public Function checkStdLoon() As Boolean
        Dim stdloon As Decimal = Me.getMinimumLoon
        If Me.Bedrag < stdloon Then
            Return False
        End If
        Return True

    End Function


    ''' <summary>
    ''' bepaald de dag dat de werknemer verlof neemt
    ''' </summary>
    ''' <remarks></remarks>
    Private _verlofdag As Integer
    Public Property verlofdag() As Integer
        Get
            Return _verlofdag
        End Get
        Set(ByVal value As Integer)
            _verlofdag = value
        End Set
    End Property


    ''' <summary>
    ''' Opzoeken wat het minimum bedrag is dat moet aangevraagd worden 
    ''' </summary>
    ''' <returns>minimum loon </returns>
    ''' <remarks></remarks>
    Public Function getMinimumLoon() As Decimal
        Dim sl As New BLL.InsertBetalingenSpaarplanner
        Dim StdLoon As Decimal = sl.getstdloon(Me.WerknemerID, verlofdag)
        Return StdLoon
    End Function

    Private _newWachtID As Integer
    ''' <summary>
    ''' ID van de nieuwe wachtregel waarop het verlof geboekt wordt.  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property NewWachtID() As Integer
        Get
            Return _newWachtID
        End Get
        Set(ByVal value As Integer)
            _newWachtID = value
            Me.NewWachtregel = New Wachtregel(value)
        End Set
    End Property



    Private _newWachtregel As Wachtregel
    ''' <summary>
    ''' De nieuwe wachtregel die werd aangemaakt voor de verlofdag, en waarop de overboekingen moeten gebeuren
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property NewWachtregel() As Wachtregel

        Get
            If _newWachtregel Is Nothing Then
                Dim nw As New BLL.Wachtregel
                _newWachtregel = nw

            End If

            Return _newWachtregel
        End Get
        Set(ByVal value As Wachtregel)
            _newWachtregel = value
        End Set
    End Property

    Private _sourceWachtids As List(Of Integer)
    ''' <summary>
    ''' Wacht ID's waarvan verlof moet worden afgeboekt.  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property sourceWachtIds() As List(Of Integer)
        Get
            Return _sourceWachtids
        End Get
        Set(ByVal value As List(Of Integer))
            _sourceWachtids = value
        End Set
    End Property


    Private _bedrag As Decimal
    ''' <summary>
    ''' Gevraagd bedrag voor uitbetaling
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Bedrag() As Decimal
        Get
            Return _bedrag
        End Get
        Set(ByVal value As Decimal)
            _bedrag = value
        End Set
    End Property

    Private _datum As DateTime
    ''' <summary>
    ''' De datum waarop verlof wordt genomen
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)
            _datum = value
        End Set
    End Property


    Private _werknemerID As Integer
    ''' <summary>
    ''' ID van werknemer waarvoor verlof moet geregistreerd worden
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property WerknemerID() As Integer
        Get
            Return _werknemerID
        End Get
        Set(ByVal value As Integer)
            _werknemerID = value
        End Set
    End Property

End Class
