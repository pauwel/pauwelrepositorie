Public Class WachtregelValidatie

    Private wachtregel As Wachtregel
    Private Loonkosten As BLL.LoonkostByWerknemer = New BLL.LoonkostByWerknemer
    Private LPF As BLL.LoonPerFuncties = New BLL.LoonPerFuncties
    Private ca As New BLL.CheckAantalGevalideerdeGewerkteShiften

    Dim loongegevens As DataRow
    Dim loonkost As DataRow
    Private isDubbeleShift As Boolean = False

    ''' <summary>
    ''' Nieuw validatie object aanmaken om te valideren
    ''' </summary>
    ''' <param name="wachtregel">wachtregel object</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal wachtregel As Wachtregel)
        Me.wachtregel = wachtregel
        Me.loongegevens = LPF.GetByID(wachtregel.loonID)(0)
        Me.loonkost = Loonkosten.GetLoonkost(wachtregel.WerknemerID)(0)

        If Me.ca.Getdata(wachtregel.WerknemerID, wachtregel.Datum) > 0 Then
            Me.isDubbeleShift = True
        End If
    End Sub
    ''' <summary>
    ''' Nieuw validatie object op basis van wachtID 
    ''' </summary>
    ''' <param name="wachtID"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal wachtID As Integer)
        Me.New(New BLL.Wachtregel(wachtID))
    End Sub

    ''' <summary>
    ''' Valideren van wachtregel
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub valideerWachtRegel()
        ' eerst nakijken of prestatie al gevalideerd is? 
        Dim checker As BLL.PrestatieValidatedCheck = New BLL.PrestatieValidatedCheck
        If checker.isValidated(Me.wachtregel.WachtID) Then
            Throw New Timekeeping.Exceptions.AlreadyValidatedException()
        End If

        ' waarde validated op true zetten 
        wachtregel.Validated = True

        Dim WN As BLL.Werknemers = New BLL.Werknemers

        'nakijken of werknemer van deze prestatie een spaarplanner is
        Dim spaarplanner As Boolean = WN.CheckSpaarplan(Me.wachtregel.WerknemerID)

        

        'enkel als werknemer niet afwezig was mogen er detaillijnen voor lonen geboekt worden.  
        If wachtregel.isAfwezig = False Then

            'Dim loongegevens As DataRow = LPF.GetByID(wachtregel.loonID)(0)
            'Dim loonkost As DataRow = Loonkosten.GetLoonkost(wachtregel.WerknemerID)(0)

            'Standaard detaillijnen toevoegen
            wachtregel.addDetail(loongegevens("stdLoon"), Timekeeping.Register.LoonDetailReden.System, Nothing, Timekeeping.Register.Loontypes.StdLoon, Nothing, Nothing)
            wachtregel.addDetail(loonkost("Walking"), Timekeeping.Register.LoonDetailReden.System, Nothing, Timekeeping.Register.Loontypes.WalkingTime, Nothing, Nothing)
            wachtregel.addDetail(loonkost("Premie"), Timekeeping.Register.LoonDetailReden.System, Nothing, Timekeeping.Register.Loontypes.Premie, Nothing, Nothing)

            If (wachtregel.Is_weekend Or wachtregel.Is_feestdag) And spaarplanner = True Then
                wachtregel.addDetail(Me.berekenCodexWE, Timekeeping.Register.LoonDetailReden.System, Nothing, Timekeeping.Register.Loontypes.CodexWE, Nothing, Nothing)
            Else
                wachtregel.addDetail(loongegevens("TotBedragOveruren"), Timekeeping.Register.LoonDetailReden.System, Nothing, Timekeeping.Register.Loontypes.OverurenFunctie, Nothing, Nothing)
                wachtregel.addDetail(loongegevens("TotSupplement"), Timekeeping.Register.LoonDetailReden.System, Nothing, Timekeeping.Register.Loontypes.SupplementFunctie, Nothing, Nothing)
                If Me.isDubbeleShift Then
                    wachtregel.addDetail(loongegevens("SupplementDubbeleShift"), Timekeeping.Register.LoonDetailReden.System, Nothing, Timekeeping.Register.Loontypes.SupplementDubbeleShift, Nothing, Nothing)
                End If
            End If

            
        End If

    End Sub
    ''' <summary>
    ''' Berekenen van de Codex Weekend op basis van wachtregel object 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function berekenCodexWE()
        Dim codexwe As Decimal
        codexwe = loongegevens("TotBedragOveruren") + loongegevens("TotSupplement")
        If Me.isDubbeleShift Then
            codexwe += loongegevens("SupplementDubbeleShift")
        End If
        Return codexwe
    End Function




End Class
