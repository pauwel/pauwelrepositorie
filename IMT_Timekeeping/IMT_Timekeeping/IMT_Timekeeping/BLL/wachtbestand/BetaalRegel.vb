


Public Class BetaalRegel




    Private _werkenemerid As Integer
    Public Property Werknemerid() As Integer
        Get
            Return _werkenemerid
        End Get
        Set(ByVal value As Integer)
            _werkenemerid = value
        End Set
    End Property


    Private _saldo As Decimal
    Public Property Saldo() As Decimal
        Get
            Return _saldo
        End Get
        Set(ByVal value As Decimal)
            _saldo = value
        End Set
    End Property


    Private _shift As Integer
    Public Property Shift() As Integer
        Get
            Return _shift
        End Get
        Set(ByVal value As Integer)
            _shift = value
        End Set
    End Property


    Private _stdloon As Decimal
    Public Property StdLoon() As Decimal
        Get
            Return _stdloon
        End Get
        Set(ByVal value As Decimal)
            _stdloon = value
        End Set
    End Property


    Private _datum As DateTime
    Public Property datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)
            _datum = value
        End Set
    End Property


    Private _gevraagdloon As Decimal
    Public Property gevraagdloon() As Decimal
        Get
            Return _gevraagdloon
        End Get
        Set(ByVal value As Decimal)
            _gevraagdloon = value
        End Set
    End Property

    Public Enum weekend As Integer
        zaterdag = 40
        zondag = 41
    End Enum
    Function validate() As Boolean

        Dim sl As New BLL.InsertBetalingenSpaarplanner

        'indien loon gevraagd op zaterdag of zondag


        If datum.DayOfWeek = DayOfWeek.Sunday Then
            StdLoon = sl.getstdloon(Werknemerid, weekend.zondag)
        ElseIf datum.DayOfWeek = DayOfWeek.Saturday Then
            StdLoon = sl.getstdloon(Werknemerid, weekend.zaterdag)
        Else
            StdLoon = sl.getstdloon(Werknemerid, Shift)
        End If


        Dim ok As Boolean = True

        If (Saldo >= gevraagdloon) Then
        Else
            MsgBox("Het saldo is niet toereikend", MsgBoxStyle.Information)
            Return False

        End If

        If (gevraagdloon >= StdLoon) Then
        Else

            MsgBox("Het gevraagd loon is kleiner dan het standaardloon + WT + Premie" & vbNewLine & "voor de opgegeven shift(� " & StdLoon & ")", MsgBoxStyle.Information)
            Return False
        End If

        If Not (sl.CheckReedsBetaald(Werknemerid, datum)) Then
        Else
            MsgBox("Er werd reeds een betaling genoteerd op deze datum", MsgBoxStyle.Information)
            ok = False
        End If
        Return ok
    End Function





End Class
