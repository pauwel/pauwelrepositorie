Public Class WachtRegelWriter

    Sub New()

    End Sub


    Private _wachtid As Integer
    Public Property wachtid() As Integer
        Get
            Return _wachtid
        End Get
        Set(ByVal value As Integer)
            _wachtid = value
        End Set
    End Property

    Public Sub store()
        Me.store(False)
    End Sub


    Dim _bewaarGegevens As Boolean
    Property BewaarGegevens() As Boolean
        Get
            Return _bewaarGegevens
        End Get
        Set(ByVal value As Boolean)
            _bewaarGegevens = value
        End Set
    End Property

    Dim _generatemail As Boolean
    Property generatemail() As Boolean
        Get
            Return _generatemail
        End Get
        Set(ByVal value As Boolean)
            _generatemail = value
        End Set
    End Property

    Dim _orgafwezig As String
    Property orgAfwezig() As String
        Get
            Return _orgafwezig
        End Get
        Set(ByVal value As String)
            _orgafwezig = value

        End Set
    End Property

    Dim _orgReden As String
    Property orgReden() As String
        Get
            Return _orgReden
        End Get
        Set(ByVal value As String)
            _orgReden = value
        End Set
    End Property
    Dim _orgFeestdag As String
    Property orgFeestdag() As String
        Get
            Return _orgFeestdag
        End Get
        Set(ByVal value As String)
            _orgFeestdag = value
        End Set
    End Property

    Dim _orgFunctie As String
    Property orgFunctie() As String
        Get
            Return _orgFunctie
        End Get
        Set(ByVal value As String)
            _orgFunctie = value
        End Set
    End Property

    Dim _orgShift As String
    Property orgShift() As String
        Get
            Return _orgShift
        End Get
        Set(ByVal value As String)
            _orgShift = value
        End Set
    End Property
    Dim _orgVoyage As String
    Property orgVoyage() As String
        Get
            Return _orgVoyage
        End Get
        Set(ByVal value As String)
            _orgVoyage = value
        End Set
    End Property

    ''' <summary>
    ''' Nieuwe wachtregel opslaan
    ''' </summary>
    ''' <param name="forceInsert">Als deze true is, wordt er geen controle gedaan of er reeds een prestatie bestaat op die shift voor die werknemer 
    ''' Standaard: false
    ''' </param>
    ''' <remarks></remarks>
    Public Sub store(ByVal forceInsert As Boolean)
        Dim s As New BLL.InsertWachtregels
        If voyageid > 0 And shiftid > 0 Then
            wachtid = s.InsertData(datum, voyageid, gewerktetijd, isweekend, isfeestdag, wachtypeid, _
                     werknemerid, functieid, shiftid, gewerkteshiftid, wordtuitbetaald, machineid, ploeg, Validated, forceInsert)
        Else
            MsgBox("Niet alle verplichte (datum, voyage, shift) velden werden ingevuld, controleer!", MsgBoxStyle.Information)

        End If
    End Sub






    Public Sub update(ByVal wachtregel As BLL.Wachtregel)
        Me.weekend(Me.gewerkteshiftid, wachtregel.Datum)
        Me.feestdag(wachtregel.Datum, Me.gewerkteshiftid)
        Dim updater As BLL.UpdateWachtregels = New BLL.UpdateWachtregels





        Dim mail As New BLL.MailMessage
        If Not generatemail Then
            updater.UpdateWachtregel(wachtregel.WachtID, Me.voyageid, Me.isweekend, Me.isfeestdag, wachtregel.WerknemerID, _
            Me.functieid, Me.shiftid, Me.gewerkteshiftid, Me.wordtuitbetaald, Me.isAfwezig, Me.redenAfwezigheid)
            Dim f As New BLL.Functions
            Dim s As New BLL.Shiften
            Dim v As New BLL.Voyages

            orgAfwezig = wachtregel.isAfwezig
            orgReden = wachtregel.redenAfwezigheid
            orgFeestdag = wachtregel.Is_feestdag
            orgFunctie = f.GetFunctienaam(wachtregel.FunctieID)
            orgShift = s.GetShiftnaam(wachtregel.ShiftID)
            orgVoyage = v.GETVoyagenaam(wachtregel.VoyageID)


            BewaarGegevens = True

        Else
            If BewaarGegevens And generatemail Then
                With mail
                    .newFeestdag = Me.isfeestdag

                    .NewAfwezig = Me.isAfwezig
                    .newReden = Me.redenAfwezigheid

                    Dim f As New BLL.Functions

                    .newFunctie = f.GetFunctienaam(Me.functieid)

                    Dim s As New BLL.Shiften
                    .newShift = s.GetShiftnaam(Me.shiftid)

                    Dim v As New BLL.Voyages

                    .newVoyage = v.GETVoyagenaam(voyageid) ' Me.voyageid


                    If String.IsNullOrEmpty(orgAfwezig) Then orgAfwezig = .NewAfwezig
                    .orgAfwezig = orgAfwezig
                    If String.IsNullOrEmpty(orgReden) Then orgReden = .newReden
                    .orgReden = orgReden
                    If String.IsNullOrEmpty(orgFeestdag) Then orgFeestdag = .newFeestdag
                    .OrgFeestdag = orgFeestdag
                    If String.IsNullOrEmpty(orgFunctie) Then orgFunctie = .newFunctie
                    .orgFunctie = orgFunctie
                    If String.IsNullOrEmpty(orgShift) Then orgShift = .newShift
                    .orgShift = orgShift
                    If String.IsNullOrEmpty(orgVoyage) Then orgVoyage = .newVoyage
                    .orgVoyage = orgVoyage

                    .Werknemer = wachtregel.Voornaam & " " _
                                                   & wachtregel.Naam & " " _
                                                   & "( " & wachtregel.Loonboek & " )"

                    .Datum = wachtregel.Datum
                    .Wachtid = wachtid

                    .sendmail()
                End With
            End If
            BewaarGegevens = False


        End If



    End Sub






#Region "Properties"


    Private _datum As DateTime
    Public Property datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)
            _datum = value

        End Set
    End Property


    Private _voyageid As Integer
    Public Property voyageid() As Integer
        Get
            Return _voyageid
        End Get
        Set(ByVal value As Integer)
            _voyageid = value
        End Set
    End Property


    Private _gewerktetijd As Decimal
    Public ReadOnly Property gewerktetijd() As Decimal
        Get
            If _gewerktetijd = 0 Then
                _gewerktetijd = 7.25
            End If
            Return _gewerktetijd
        End Get
    End Property

    Private _isweekend As Boolean = False
    Public Property isweekend() As Boolean
        Get
            Return _isweekend
        End Get
        Set(ByVal value As Boolean)
            _isweekend = value
        End Set
    End Property


    Private _isfeestdag As Boolean = False
    Public Property isfeestdag() As Boolean
        Get
            Return _isfeestdag
        End Get
        Set(ByVal value As Boolean)
            _isfeestdag = value
        End Set
    End Property


    Private _wachtypeid As Integer
    Public Property wachtypeid() As Integer
        Get
            Return _wachtypeid
        End Get
        Set(ByVal value As Integer)
            _wachtypeid = value
        End Set
    End Property


    Private _werknemerid As Integer
    Public Property werknemerid() As Integer
        Get
            Return _werknemerid
        End Get
        Set(ByVal value As Integer)
            _werknemerid = value
        End Set
    End Property


    Private _functieid As Integer
    Public Property functieid() As Integer
        Get
            Return _functieid
        End Get
        Set(ByVal value As Integer)
            _functieid = value
        End Set
    End Property


    Private _shiftid As Integer
    Public Property shiftid() As Integer
        Get
            Return _shiftid
        End Get
        Set(ByVal value As Integer)
            _shiftid = value
        End Set
    End Property


    Private _gewerkteshiftid As Integer
    Public Property gewerkteshiftid() As Integer
        Get
            Return _gewerkteshiftid
        End Get
        Set(ByVal value As Integer)
            _gewerkteshiftid = value
        End Set
    End Property


    Private _wordtuitbetaald As Boolean
    Public Property wordtuitbetaald() As Boolean
        Get
            Return _wordtuitbetaald
        End Get
        Set(ByVal value As Boolean)
            _wordtuitbetaald = value
        End Set
    End Property


    Private _machineid As Integer
    Public Property machineid() As Integer
        Get
            Return _machineid
        End Get
        Set(ByVal value As Integer)
            _machineid = value
        End Set
    End Property


    Private _ploeg As String
    Public Property ploeg() As String
        Get
            Return _ploeg
        End Get
        Set(ByVal value As String)
            _ploeg = value
        End Set
    End Property


    Private _validated As Boolean
    Public Property Validated() As Boolean
        Get
            Return _validated
        End Get
        Set(ByVal value As Boolean)
            _validated = value
        End Set
    End Property


    Private _isAfwezig As Boolean
    Public Property isAfwezig() As Boolean
        Get
            Return _isAfwezig
        End Get
        Set(ByVal value As Boolean)
            _isAfwezig = value
        End Set
    End Property


    Private _redenAfwezigheid As String
    Public Property redenAfwezigheid() As String
        Get
            Return _redenAfwezigheid
        End Get
        Set(ByVal value As String)
            _redenAfwezigheid = value
        End Set
    End Property

#End Region





    Function weekend(ByVal shift As Integer, ByVal datum As DateTime) As Boolean




        If datum.DayOfWeek = DayOfWeek.Saturday Then
            If shift = Timekeeping.Register.Shiften.nacht Then
                shiftid = Timekeeping.Register.Shiften.zondag
            Else
                shiftid = Timekeeping.Register.Shiften.zaterdag

            End If
            isweekend = True
        ElseIf datum.DayOfWeek = DayOfWeek.Sunday Then
            isweekend = True
            shiftid = Timekeeping.Register.Shiften.zondag

            'ElseIf datum.DayOfWeek = DayOfWeek.Friday And shift = Timekeeping.Register.Shiften.nacht Then
            '    'nacht op vrijdag
            '    isweekend = True
            '    shiftid = Timekeeping.Register.Shiften.zaterdag
        Else
            isweekend = False
        End If
    End Function

    Function feestdag(ByVal datum As DateTime, ByVal shift As Integer) As Boolean

        Dim volgendedag As DateTime = DateAdd(DateInterval.Day, 1, datum)
        Dim f As New Feestdagen
        If (f.CheckFeestdag(volgendedag) And shift = Timekeeping.Register.Shiften.nacht) Or _
                 f.CheckFeestdag(datum) Then
            isfeestdag = True
            shiftid = Timekeeping.Register.Shiften.feestdag
        Else
            isfeestdag = False
        End If
    End Function
End Class
