

Option Compare Text

Imports System.Security.Principal
Imports System.Text
Imports System.Configuration

Imports System.DirectoryServices


Public Class isInRole

    Private _IsPlanner As Boolean
    Public Property IsPlanner() As Boolean
        Get
            GetProfile()
            Return _IsPlanner
        End Get
        Set(ByVal value As Boolean)
            _IsPlanner = value
        End Set
    End Property



    Private _isTimekeeper As Boolean
    Public Property IsTimekeeper() As Boolean
        Get
            GetProfile()
            Return _isTimekeeper
        End Get
        Set(ByVal value As Boolean)
            _isTimekeeper = value
        End Set
    End Property


    Private _isadmin As Boolean
    Public Property IsAdmin() As Boolean
        Get
            GetProfile()
            Return _isadmin
        End Get
        Set(ByVal value As Boolean)
            _isadmin = value
        End Set
    End Property


    Private Sub GetProfile()

        'IsAdmin = True
        'IsTimekeeper = True
        Dim ADUser As Boolean = False 'maakt de gebruiker gebruik van AD? indien niet, beperkte weergave


        Try
            ADUser = ConfigurationManager.AppSettings("UseActiveDirectory")
        Catch ex As Exception
            ADUser = False
        End Try


        If ADUser Then
            Dim ADString As String
            ADString = ConfigurationManager.AppSettings("ActiveDirectoryPath")

            Dim ADPlannergroup As String
            ADPlannergroup = ConfigurationManager.AppSettings("Timekeeping_P")


            Dim ADTimekeepergroup As String
            ADTimekeepergroup = ConfigurationManager.AppSettings("Timekeeping_TK")


            Dim ADAdmingroup As String
            ADAdmingroup = ConfigurationManager.AppSettings("Timekeeping_All")


            Dim ci As WindowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent
            Dim strArry() = ci.Name.Split("\")

            Dim domein As String = strArry(0)
            Dim gebruiker As String = strArry(1)
            Dim groups As StringBuilder = Nothing

            Dim obEntry As DirectoryServices.DirectoryEntry = New DirectoryServices.DirectoryEntry(ADString) '("LDAP://" & domein)
            Dim srch As DirectoryServices.DirectorySearcher = New DirectoryServices.DirectorySearcher(obEntry, "(sAMAccountName=" & gebruiker & ")")

            Dim res As DirectoryServices.SearchResult = srch.FindOne()


            Dim obUser As New DirectoryServices.DirectoryEntry(res.Path)
            Dim obGroups As Object = obUser.Invoke("Groups")



            IsPlanner = False
            IsTimekeeper = False
            IsAdmin = False

            For Each ob As Object In obGroups

                Dim obGpEntry As DirectoryServices.DirectoryEntry = New DirectoryServices.DirectoryEntry(ob)

                If obGpEntry.Name.Replace("CN=", "").ToString = ADPlannergroup Then

                    IsPlanner = True
                ElseIf obGpEntry.Name.Replace("CN=", "").ToString = ADTimekeepergroup Then

                    IsTimekeeper = True
                ElseIf obGpEntry.Name.Replace("CN=", "").ToString = ADAdmingroup Then

                    IsAdmin = True
                End If

            Next
        Else



        End If


    End Sub



End Class
