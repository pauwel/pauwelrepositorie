


Imports DAL

Public Class Admin
    Private _adapter As SecurityTableAdapters.Security_AdminTableAdapter = Nothing

    Protected ReadOnly Property Adapter() As SecurityTableAdapters.Security_AdminTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New SecurityTableAdapters.Security_AdminTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function getAdminControls() As Security.Security_AdminDataTable
        Return Adapter.GetData()
    End Function

End Class
