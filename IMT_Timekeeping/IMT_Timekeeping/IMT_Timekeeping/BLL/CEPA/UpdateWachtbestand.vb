
Imports DAL

Public Class UpdateWachtbestand

    Private _adapter As CEPA_configTableAdapters.UpdateWachtbestand = Nothing

    Protected ReadOnly Property Adapter() As CEPA_configTableAdapters.UpdateWachtbestand
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New CEPA_configTableAdapters.UpdateWachtbestand
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function UpdateGegevens(ByVal datum As DateTime)
        Me.Adapter.UpdateWachtBestandVoorCEPA(datum)
        Return True
    End Function
End Class
