﻿Public Class BLLogging
    Public Enum logActieid As Integer
        OvergangVanSpaarplannerNaarNietSpaarplanner = 1
        OvergangVanNIETSpaarplannerNaarSpaarplanner = 2
        OpheffenBlokeringVanWachtbriefNaDoorsturingNaarCepa = 3

    End Enum

    Public Function InsertLogging(ByVal betaalid As Integer, ByVal werknemerid As Integer, ByVal actieid As Integer, ByVal memo As String) As Boolean
        Dim log As New DAL.Logging

        log.Log(betaalid, werknemerid, actieid, memo)

    End Function

End Class
