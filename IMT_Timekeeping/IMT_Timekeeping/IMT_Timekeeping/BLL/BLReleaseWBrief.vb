﻿
Imports DAL.DLReleaseWBrief

Public Class BLReleaseWBrief
    Public Function GetNietSpaarplanners() As DataTable
        Try
            Dim release As New DAL.DLReleaseWBrief
            Return release.DLGetNietSpaarplanner()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return Nothing
    End Function

    Public Function GetLockedPrestaties(ByVal werknemerid As Integer) As DataTable
        Try
            Dim release As New DAL.DLReleaseWBrief
            Return release.DLGetPrestatiesSendedToCEPA(werknemerid)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return Nothing
    End Function

    Public Function ReleaseWBBRIEF(ByVal betaalid As Integer, ByVal wbbrief As Integer) As Boolean
        Try
            Dim release As New DAL.DLReleaseWBrief
            Return release.DLReleaseWachtid(betaalid, wbbrief)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return False

    End Function

End Class
