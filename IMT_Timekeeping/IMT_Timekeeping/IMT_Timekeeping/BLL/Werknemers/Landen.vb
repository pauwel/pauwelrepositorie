
Option Compare Text

Imports DAL
Public Class Landen
    Private _Adapter As WerknemersTableAdapters.tblLandenTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As WerknemersTableAdapters.tblLandenTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New WerknemersTableAdapters.tblLandenTableAdapter
            End If

            Return _Adapter
        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetLanden() As DAL.Werknemers.tblLandenDataTable
        Return Adapter.GetData
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function UpdateLand(ByVal land As String, ByVal id As Int32) As Boolean
        Try

            Adapter.Update(land, id)
            
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Insert, True)> _
            Public Function InsertLand(ByVal land As String) As Boolean
        Try
            If CheckLand(land) Then
                MsgBox("Land is in gebruik")
            Else
                Adapter.Insert(land)
                Return True
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteLand(ByVal id As Integer) As Boolean
        Try
            Return Adapter.Delete(id)
        Catch ex As Exception
            Return False
        End Try

    End Function


    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function CheckLand(ByVal land As String) As Boolean

        Dim Exists As Boolean = False
        If Adapter.CheckLand(land) > 0 Then
            Exists = True
        End If

        Return Exists
    End Function

End Class
