
Imports DAL

Public Class GetSpaarplanners
    Private _Adapter As _SPWerknemersTableAdapters.GetSpaarplannersTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As _SPWerknemersTableAdapters.GetSpaarplannersTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New _SPWerknemersTableAdapters.GetSpaarplannersTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetSpaarplanners() As DAL._SPWerknemers.GetSpaarplannersDataTable
        Return Adapter.GetData()
    End Function
End Class
