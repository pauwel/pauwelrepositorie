
Imports DAL
Public Class clsWerknemersnaam



    Private _Adapter As DAL._SPWerknemersTableAdapters.GetWerknemersNaamTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL._SPWerknemersTableAdapters.GetWerknemersNaamTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL._SPWerknemersTableAdapters.GetWerknemersNaamTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetAllLoonboeken(ByVal id As Integer) As DAL._SPWerknemers.GetWerknemersNaamDataTable
        Return Adapter.GetData(id)
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetNaam(ByVal id As Integer) As String
        Try
            Dim r As DataRow = Adapter.GetData(id).Rows(0)
            Return r("naam")
        Catch ex As IndexOutOfRangeException
            Return Nothing
        End Try

    End Function
End Class
