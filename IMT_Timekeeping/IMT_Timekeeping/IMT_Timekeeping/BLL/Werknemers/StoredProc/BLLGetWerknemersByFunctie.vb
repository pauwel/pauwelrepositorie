
Option Compare Text
Imports DAL

Public Class BLLGetWerknemersByFunctie
    Private _Adapter As DAL._SPWerknemersTableAdapters.GetWerknemersBySelectedFunctieTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL._SPWerknemersTableAdapters.GetWerknemersBySelectedFunctieTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL._SPWerknemersTableAdapters.GetWerknemersBySelectedFunctieTableAdapter

            End If
            Return _Adapter

        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
            Public Function WerknemersBySelectedFunction(ByVal functieID As Integer) As _SPWerknemers.GetWerknemersBySelectedFunctieDataTable

        Return Adapter.GetData(functieID)
    End Function



End Class
