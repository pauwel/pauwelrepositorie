
Imports DAL

Public Class GetAllLoonBoeknbrs


    Private _Adapter As DAL._SPWerknemersTableAdapters.GetAllLoonboeknbrsTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As DAL._SPWerknemersTableAdapters.GetAllLoonboeknbrsTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New DAL._SPWerknemersTableAdapters.GetAllLoonboeknbrsTableAdapter

            End If
            Return _Adapter

        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetAllLoonboeken() As DAL._SPWerknemers.GetAllLoonboeknbrsDataTable
        Return Adapter.GetData
    End Function
End Class
