
Imports DAL

Imports system.Text.RegularExpressions

Public Class Mail

    Private _adapter As MailTableAdapters.tblMailAdressenTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As MailTableAdapters.tblMailAdressenTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New MailTableAdapters.tblMailAdressenTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetMailadres() As DAL.Mail.tblMailAdressenDataTable
        Return Me.Adapter.GetData
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function DeleteMailadre(ByVal id As Integer) As Boolean
        Return Me.Adapter.Delete(id)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function UpdateMailadres(ByVal mailadres As String, ByVal id As Integer) As Boolean

        Dim r As New Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")

        If IsUniek(Trim(mailadres)) Then
            If r.IsMatch(Trim(mailadres)) Then
                Return Me.Adapter.Update(mailadres, id)
            Else
                MsgBox("Ongeldig emailadres")
            End If
        Else
            MsgBox("Emailadres reeds ingevoerd")
        End If



    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function InsertMailadres(ByVal mailadres As String) As Boolean
        Dim r As New Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")
        If IsUniek(Trim(mailadres)) Then

            If r.IsMatch(Trim(mailadres)) Then
                Return Me.Adapter.Insert(Trim(mailadres))
            Else
                MsgBox("Ongeldig emailadres")
            End If

        Else
            MsgBox("Emailadres reeds ingevoerd")
        End If

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
   Public Function IsUniek(ByVal mailadres As String) As Boolean

        If Adapter.IsUniek(mailadres) = 0 Then
            Return True
        Else
            Return False
        End If


    End Function

End Class
