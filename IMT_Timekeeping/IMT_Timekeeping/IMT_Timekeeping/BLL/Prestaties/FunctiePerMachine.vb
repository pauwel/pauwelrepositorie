
Option Compare Text
Imports DAL

Public Class FunctiePerMachine

    Private _Adapter As prestatiesTableAdapters.tblFunctieMachinesTableAdapter
    Protected ReadOnly Property Adapter() As prestatiesTableAdapters.tblFunctieMachinesTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New prestatiesTableAdapters.tblFunctieMachinesTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetFunctiesPerMachine() As prestaties.tblFunctieMachinesDataTable
        Return Me.Adapter.GetData()
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteFunctiesPerMachine(ByVal id As Integer) As Boolean
        Return Me.Adapter.Delete(id)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function AddFunctiesPerMachine(ByVal functieid As Integer, ByVal machineid As Integer, ByVal functie As String, ByVal machine As String) As Boolean
        If isUnique(functieid, machineid) Then
            Return Me.Adapter.Insert(functieid, machineid)
        Else
            MsgBox("de functie " & functie & " is reeds gekoppeld met machine " & machine)
            Return False
        End If

    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
            Public Function isUnique(ByVal functieid As Integer, ByVal machineid As Integer) As Boolean

        If Adapter.Is_Unique(functieid, machineid) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
