

Option Compare Text
Imports DAL

Public Class Voyages

    Private _Adapter As TransportTableAdapters.tblVoyageTableAdapter
    Protected ReadOnly Property Adapter() As TransportTableAdapters.tblVoyageTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New TransportTableAdapters.tblVoyageTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetVoyages() As Transport.tblVoyageDataTable
        Return Me.Adapter.GetData()
    End Function


    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
  Public Function GetVoyagesByKostID(ByVal KostID As Integer) As Transport.tblVoyageDataTable
        Return Me.Adapter.GetDataByKostID(KostID)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function InsertVoyage(ByVal v As CVoyage)
        Try
            Me.Adapter.Insert(v.VoyageNbr, v.Kade, v.Omschrijving, v.IsAfgewerkt, v.KostID, True)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    ''' <summary>
    ''' Voyage uit database verwijderen
    ''' </summary>
    ''' <param name="id">Voyage id to delete</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function DeleteVoyage(ByVal id As Integer)
        Try
            Return Me.Adapter.Delete(id)

        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function UpdateVoyage(ByVal voyage As CVoyage)
        Me.Adapter.Update(voyage.VoyageNbr, voyage.Kade, voyage.Omschrijving, voyage.IsAfgewerkt, voyage.KostID, True, voyage.Id)
        Return True
    End Function


    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function SetAfgewerkt(ByVal afgewerkt As Boolean, ByVal kostid As Int32)

        If kostid > 0 Then
            Try
                Me.Adapter.SETAfgewerkt(afgewerkt, kostid)
                Return True
            Catch ex As Exception
                Throw
            End Try

        End If
        Return False


    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function GETVoyagenaam(ByVal voyageid As Integer) As String
        Return Me.Adapter.GetVoyageNaam(voyageid)

    End Function
End Class

