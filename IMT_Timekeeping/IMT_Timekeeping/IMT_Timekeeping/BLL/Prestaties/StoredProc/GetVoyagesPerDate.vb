Option Compare Text
Imports DAL


Public Class GetVoyagesPerDate

    Private _Adapter As PLanningSPTableAdapters.GetVoyagesPerDateTableAdapter
    Protected ReadOnly Property Adapter() As PLanningSPTableAdapters.GetVoyagesPerDateTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New PLanningSPTableAdapters.GetVoyagesPerDateTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetVoyagesByDate(ByVal bdatum As DateTime, ByVal shift As Integer) As PLanningSP.GetVoyagesPerDateDataTable
        Return Me.Adapter.GetData(bdatum, shift)
    End Function

End Class
