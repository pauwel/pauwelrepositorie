
Option Compare Text
Imports DAL



Public Class TreeKostenPlaats
    Private _Adapter As PLanningSPTableAdapters.GetAvailableKostenplaatsenTableAdapter
    Protected ReadOnly Property Adapter() As PLanningSPTableAdapters.GetAvailableKostenplaatsenTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New PLanningSPTableAdapters.GetAvailableKostenplaatsenTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetTreeKosten() As PLanningSP.GetAvailableKostenplaatsenDataTable

        Return Me.Adapter.GetData()
    End Function
End Class
