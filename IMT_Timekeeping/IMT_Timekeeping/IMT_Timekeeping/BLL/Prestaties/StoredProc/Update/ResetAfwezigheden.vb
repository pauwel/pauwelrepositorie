
Imports DAL
Public Class ResetAfwezigheden

    Private _Adapter As UpdatePrestatiesTableAdapters.ResetAfwezighedenTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As UpdatePrestatiesTableAdapters.ResetAfwezighedenTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New UpdatePrestatiesTableAdapters.ResetAfwezighedenTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function GetAfwezigheden() As UpdatePrestaties.ResetAfwezighedenDataTable
        Return Me.Adapter.GetData
    End Function
End Class


