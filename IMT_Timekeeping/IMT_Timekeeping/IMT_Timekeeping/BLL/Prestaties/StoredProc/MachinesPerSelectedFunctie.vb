Option Compare Text
Imports DAL

Public Class MachinesPerSelectedFunctie


    Private _Adapter As PLanningSPTableAdapters.GetMachinesPerFunctieSelectionTableAdapter
    Protected ReadOnly Property Adapter() As PLanningSPTableAdapters.GetMachinesPerFunctieSelectionTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New PLanningSPTableAdapters.GetMachinesPerFunctieSelectionTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetMachines(ByVal functieid As Integer) As PLanningSP.GetMachinesPerFunctieSelectionDataTable
        Return Me.Adapter.GetData(functieid)
    End Function

End Class
