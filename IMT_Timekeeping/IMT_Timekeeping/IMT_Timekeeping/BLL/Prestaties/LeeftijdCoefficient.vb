

Option Compare Text
Imports DAL
Public Class LeeftijdCoefficient

    Private _Adapter As CEPA_configTableAdapters.tblLeeftijdCoefficientTableAdapter
    Protected ReadOnly Property Adapter() As CEPA_configTableAdapters.tblLeeftijdCoefficientTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New CEPA_configTableAdapters.tblLeeftijdCoefficientTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetLeeftijd() As CEPA_config.tblLeeftijdCoefficientDataTable
        Return Me.Adapter.GetData()
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function DeleteLeeftijd(ByVal id As Integer) As Boolean

        Adapter.Delete(id)

    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function InsertLeeftijd(ByVal leeftijd As Integer, ByVal coefficient As Decimal) As Boolean
        If leeftijd >= 18 And coefficient > 0 Then
            If IsUniek(leeftijd) Then
                Adapter.Insert(leeftijd, coefficient)
            Else
                MsgBox("Deze leeftijd werd reeds ingevoerd", MsgBoxStyle.Information)
            End If


        Else
            If leeftijd <= 18 Then
                MsgBox("leeftijd < 18 is niet toegelaten", MsgBoxStyle.Information)
            End If
            If coefficient <= 0 Then
                MsgBox("leeftijdco�ffici�nt < = 0 is niet toegelaten", MsgBoxStyle.Information)
            End If
        End If


    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
       Public Function UpdateLeeftijd(ByVal leeftijd As Integer, ByVal coefficient As Decimal, ByVal id As Integer) As Boolean

        If leeftijd >= 18 And coefficient > 0 Then
            Adapter.Update(leeftijd, coefficient, id)
        Else
            If leeftijd <= 18 Then
                MsgBox("leeftijd < 18 is niet toegelaten", MsgBoxStyle.Information)
            End If
            If coefficient <= 0 Then
                MsgBox("leeftijdco�ffici�nt < = 0 is niet toegelaten", MsgBoxStyle.Information)
            End If
        End If


    End Function


    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Function IsUniek(ByVal leeftijd As Integer) As Boolean

        If Adapter.IsUnique(leeftijd) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function



End Class

