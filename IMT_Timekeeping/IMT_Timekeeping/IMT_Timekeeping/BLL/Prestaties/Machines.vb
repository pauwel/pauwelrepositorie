
Option Compare Text
Imports DAL

Public Class Machines

    Private _Adapter As prestatiesTableAdapters.tblMachinesTableAdapter

    Protected ReadOnly Property Adapter() As prestatiesTableAdapters.tblMachinesTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New prestatiesTableAdapters.tblMachinesTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetMAchine() As prestaties.tblMachinesDataTable
        Return Me.Adapter.GetData()
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function InsertMachine(ByVal machinecode As String, ByVal omschrijving As String, ByVal kost As Decimal)
        Try

            If isUnique(machinecode) Then
                Me.Adapter.Insert(machinecode, omschrijving, kost)
                Return True
            Else
                MsgBox("Deze machine bestaat reeds")
                Return False
            End If
           
        Catch ex As Exception
            Return False
        End Try
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function DeleteMachine(ByVal ID As Integer)
        Try
            Return Me.Adapter.Delete(ID)

        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function UpdateMachine(ByVal machinecode As String, ByVal omschrijving As String, ByVal kost As Decimal, ByVal id As Integer) As Boolean
        Try

            Me.Adapter.Update(machinecode, omschrijving, kost, id)
            Return True
           

        Catch ex As Exception
            Return False
        End Try
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function GetMachineKost(ByVal id As Integer) As Decimal

        Dim i As Decimal
        Try
            i = Adapter.GetKostByMachineID(id)
            Return i
        Catch ex As Exception
            Return 0
        End Try
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function isUnique(ByVal machinecode As String) As Boolean

        If Adapter.Is_Unique(machinecode) = 0 Then
            Return True
        Else
            Return False
        End If

        
    End Function

End Class
