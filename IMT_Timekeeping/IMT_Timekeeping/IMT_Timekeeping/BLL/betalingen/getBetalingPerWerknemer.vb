

Imports DAL
Public Class getBetalingPerWerknemer

    Private _adapter As betalingenTableAdapters.GetBetalingenPerWerkNemerTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As betalingenTableAdapters.GetBetalingenPerWerkNemerTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New betalingenTableAdapters.GetBetalingenPerWerkNemerTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function GetBetalingGegevens(ByVal werknemerid As Integer) As betalingen.GetBetalingenPerWerkNemerDataTable

        Return Adapter.GetData(werknemerid)

    End Function
End Class
