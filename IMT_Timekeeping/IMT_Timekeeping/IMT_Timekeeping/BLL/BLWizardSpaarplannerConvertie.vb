﻿
Imports DAL.wizardSpaarplannerAccessor


Public Class BLWizardSpaarplannerConvertie

    Public Function BLGetSpaarplanners() As DataTable
        Try
            Dim wizard As New DAL.wizardSpaarplannerAccessor

            Dim dt As New DataTable
            dt = wizard.GetSpaarplanners.Clone
            For Each r As DataRow In wizard.GetSpaarplanners.Rows
                Dim rij As DataRow = dt.NewRow

                If Not r("naam").ToString.ToLower.Contains("chauffeur cuypers") Then
                    rij("naam") = r("naam")
                    rij("WerknemerID") = r("WerknemerID")
                    dt.Rows.Add(rij)
                End If

            Next
            Return dt
     
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function
    Public Function BLOvergangNaarNietSpaarplanner(ByVal werknemerid As Integer, ByVal saldo As Decimal) As Boolean

        Dim errMsg As String = String.Empty
        Try
            Dim wizard As New DAL.wizardSpaarplannerAccessor
            Return wizard.OvergangNietSpaarplanner(werknemerid, saldo)

        Catch ex As Exception
            errMsg = ex.Message
            MsgBox(ex.Message)
            Return False
        Finally
            Dim Log As New BLLogging
            Log.InsertLogging(0, werknemerid, BLLogging.logActieid.OvergangVanSpaarplannerNaarNietSpaarplanner, errMsg)
        End Try

    End Function

    Public Function BLUpdateStatuut(ByVal werknemerid As Integer) As Boolean

        Dim errMsg As String = "een extra lijn werd in de tabel werknemerstatuut toegevoegd"
        Try
            Dim wizard As New DAL.wizardSpaarplannerAccessor
            Return wizard.UpdateStatuut(werknemerid)

        Catch ex As Exception
            errMsg = ex.Message
            MsgBox(ex.Message)
            Return False
        Finally
            Dim Log As New BLLogging
            Log.InsertLogging(0, werknemerid, BLLogging.logActieid.OvergangVanSpaarplannerNaarNietSpaarplanner, errMsg)
        End Try

    End Function

End Class
