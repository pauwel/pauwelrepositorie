Imports DAL


Public Class vwLoonPerFunctie

    Private _Adapter As vwLoonPFTableAdapters.vwLoonPerFunctieTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As vwLoonPFTableAdapters.vwLoonPerFunctieTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New vwLoonPFTableAdapters.vwLoonPerFunctieTableAdapter
            End If

            Return _Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetvwLoonPerFunctie(ByVal Shiftid As Integer, ByVal WNtype As Integer) As vwLoonPF.vwLoonPerFunctieDataTable
        Return Adapter.GetData(Shiftid, WNtype)
    End Function
End Class
