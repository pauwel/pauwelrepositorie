Option Compare Text

Imports DAL
Public Class LoonPerFuncties
    Private _Adapter As LoonPerFunctieTableAdapters.tblLoonPerFunctieTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As LoonPerFunctieTableAdapters.tblLoonPerFunctieTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New LoonPerFunctieTableAdapters.tblLoonPerFunctieTableAdapter
            End If

            Return _Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetLoonIDPerFunctie(ByVal werknemerid As Integer, ByVal functieid As Integer, ByVal shiftid As Integer) As Integer
        Return Adapter.GetLoonIDPerPrestatie(functieid, shiftid, werknemerid)
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
   Public Function GetByID(ByVal loonID As Integer) As LoonPerFunctie.tblLoonPerFunctieDataTable
        Return Me.Adapter.GetDataByLoonID(loonID)
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Insert, True)> _
            Public Function InsertLoonPerFunctie(ByVal catid As Integer, ByVal functieid As Integer, ByVal stdloon As Decimal, ByVal totbedragoveruren As Decimal, ByVal totbedragsupplement As Decimal, _
     ByVal formule As String, ByVal eindejaarspremie As Decimal, ByVal werknemerstype As Integer, ByVal shiftid As Integer, ByVal SupplementDubbeleShift As Decimal) As Boolean

        Try

            Adapter.Insert(catid, functieid, formule, stdloon, totbedragoveruren, totbedragsupplement, eindejaarspremie, werknemerstype, Now.Date, shiftid, SupplementDubbeleShift)
            Return True


        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteLoonPerFunctie(ByVal catid As Integer, ByVal functieid As Integer) As Boolean
        Try
            Return Adapter.Delete(catid, functieid)

        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try

    End Function

    '<System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
    '   Public Function UpdateLoonPerFunctie(ByVal stdloon As Decimal, ByVal overuren As Decimal, ByVal supplement As Decimal, ByVal eindejaarspremie As Decimal, ByVal wnid As Integer, ByVal shiftid As Integer, ByVal catid As Integer, ByVal functieid As Integer) As Boolean
    '    Try
    '        Return Adapter.UpdateLPF(stdloon, overuren, supplement, eindejaarspremie, Now.Date, wnid, shiftid, catid, functieid)
    '        Return True
    '    Catch ex As Exception
    '        Return False
    '    End Try

    'End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function CheckBestaande(ByVal catid As Integer, ByVal functieid As Integer) As Boolean

        If Adapter.CheckExisting(catid, functieid) = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class