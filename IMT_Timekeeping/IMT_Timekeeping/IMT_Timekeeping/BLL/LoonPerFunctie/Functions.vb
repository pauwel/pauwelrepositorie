
Option Compare Text

Imports dal
Public Class Functions

    Private _Adapter As LoonPerFunctieTableAdapters.tblFunctiesTableAdapter = Nothing
    Protected ReadOnly Property Adapter() As LoonPerFunctieTableAdapters.tblFunctiesTableAdapter
        Get
            If _Adapter Is Nothing Then
                _Adapter = New LoonPerFunctieTableAdapters.tblFunctiesTableAdapter
            End If

            Return _Adapter
        End Get
    End Property


    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
        Public Function GetFuncties() As LoonPerFunctie.tblFunctiesDataTable
        Return Adapter.GetData
    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Update, True)> _
        Public Function UpdateFuncties(ByVal job As String, ByVal omschrijving As String, ByVal id As Int32) As Boolean
        Try

            Adapter.Update(job, omschrijving, id)
            Return True


        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Insert, True)> _
            Public Function InsertFuncties(ByVal job As String, ByVal omschrijving As String) As Boolean
        Try
            If IsUnique(job) Then
                Adapter.Insert(job, omschrijving)
                Return True
            Else
                MsgBox("Deze functie bestaat reeds")
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Delete, True)> _
        Public Function DeleteFuncties(ByVal id As Integer) As Boolean
        Try
            Return Adapter.Delete(id)

        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try

    End Function
    '<System.ComponentModel.DataObjectMethod(System.ComponentModel.DataObjectMethodType.Select, True)> _
    '        Public Function GetAfzetByFunctieID(ByVal id As Integer) As Boolean
    '    Try
    '        Return Adapter.GETAfzetByFunction(id)

    '    Catch ex As Exception
    '        Return False
    '    End Try

    'End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function IsUnique(ByVal functie As String) As Boolean

        If Adapter.IS_Unique(functie) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetFunctienaam(ByVal functieid As Integer) As String
        Return Adapter.GETFUNCTIENAAM(functieid)
    End Function
End Class
