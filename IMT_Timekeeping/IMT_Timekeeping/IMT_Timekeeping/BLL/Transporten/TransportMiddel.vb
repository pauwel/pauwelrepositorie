Option Compare Text

Imports DAL
' Business Logic voor transportmiddelen
' Hier worden de respectievelijke Data-access methoden aangesproken voor nodige opslag
Public Class TransportMiddel
 
    Private _Adapter As TransportTableAdapters.tblTransportMiddelTableAdapter

    Protected ReadOnly Property Adapter() As TransportTableAdapters.tblTransportMiddelTableAdapter
        Get
            If Me._Adapter Is Nothing Then
                Me._Adapter = New TransportTableAdapters.tblTransportMiddelTableAdapter
            End If
            Return Me._Adapter
        End Get
    End Property

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
    Public Function GetTransportMiddel() As DAL.Transport.tblTransportMiddelDataTable
        Return Me.Adapter.GetData()
    End Function

    

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function InsertTransportMiddel(ByVal TransCode As String, ByVal Transportomschrijving As String, ByVal RederijID As Integer)
        '  Try
        Me.Adapter.Insert(TransCode, Transportomschrijving, RederijID, True)
        Return True
        'Catch ex As Exception
        Return False
        'End Try
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function DeleteTransportMiddel(ByVal TransID As Integer)
        Try
            Return Me.Adapter.Delete(TransID)

        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try
    End Function

    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function UpdateTransportMiddel(ByVal id As Int32, ByVal TransCode As String, ByVal TransportOmschrijving As String, ByVal Rederij As Integer) As Boolean
        Try
            Me.Adapter.Update(TransCode, TransportOmschrijving, Rederij, True, id)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
