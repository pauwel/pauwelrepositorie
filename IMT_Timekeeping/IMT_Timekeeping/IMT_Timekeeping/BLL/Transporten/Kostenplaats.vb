Public Class Kostenplaats
    Private _adapter As DAL.TransportTableAdapters.tblKostenPlaatsTableAdapter

    Protected ReadOnly Property Adapter() As DAL.TransportTableAdapters.tblKostenPlaatsTableAdapter
        Get
            If Me._adapter Is Nothing Then
                Me._adapter = New DAL.TransportTableAdapters.tblKostenPlaatsTableAdapter
            End If
            Return Me._adapter
        End Get
    End Property
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Insert, True)> _
    Public Function InsertKostenplaats(ByVal kostenplaats As String, ByVal omschrijving As String, ByVal is_actief As Boolean, ByVal transID As Integer)
        Me.Adapter.Insert(kostenplaats, omschrijving, is_actief, transID, True)
        Return True
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Delete, True)> _
    Public Function DeleteKostenplaats(ByVal kostenplaatsID As Integer)
        Try
            Return Me.Adapter.Delete(kostenplaatsID)

        Catch ex As Exception
            Throw New Exception("De gegevens zijn in gebruik in verschillende tabellen, hierdoor kunnen deze gegevens NIET verwijdert worden", ex)
        End Try

    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Update, True)> _
    Public Function UpdateKostenplaats(ByVal kostID As Integer, ByVal kostenplaats As String, ByVal omschrijving As String, ByVal is_actief As Boolean, ByVal transID As Integer)
        Me.Adapter.Update(kostenplaats, omschrijving, is_actief, transID, True, kostID)
        Return True
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
   Public Function GetKosten() As DAL.Transport.tblKostenPlaatsDataTable
        Return Me.Adapter.GetData()
    End Function
    <System.ComponentModel.DataObjectMethod(ComponentModel.DataObjectMethodType.Select, True)> _
       Public Function GetActiveKostenPlaats() As DAL.Transport.tblKostenPlaatsDataTable
        Return Me.Adapter.GetDataByActiveKostenplaats
    End Function

End Class
