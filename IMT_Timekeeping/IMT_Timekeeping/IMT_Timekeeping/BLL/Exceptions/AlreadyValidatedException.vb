Imports System
Namespace Timekeeping.Exceptions

    Public Class AlreadyValidatedException
        Inherits Exception

        Public Sub New()
            MyBase.New("Already validated")
        End Sub
    End Class

End Namespace
