Public Class frmRederij

    ''' <summary>
    ''' Wordt gebruikt om de id tijdelijk op te slaan bij edit/update van een rederij 
    ''' </summary>
    ''' <remarks></remarks>
    Private rederijID As Integer
    Private rijen As New ArrayList
    Private id As Integer

    Private menubar As clsMenuBar

    Dim blnEdit As Boolean = False

    Protected Overrides Sub onCmdReportClick()
        MyBase.onCmdReportClick()

        _frmReports.Reportnaam = "rederijen.rpt"
        _frmReports.ShowDialog()
    End Sub


    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()
        With Me.dgView
            .DataSource = clBLL_Rederijen.GetRederijen
            .Columns("rederijID").Visible = False
            .Columns("tonen").Visible = False
        End With
    End Sub
    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()
        Dim rederijNaam As String
        rederijNaam = Me.txtRederij.Text
        clBLL_Rederijen.InsertRederij(rederijNaam)

        clsPreload.rederij = clBLL_Rederijen.GetRederijen
        txtRederij.Clear()
    End Sub

    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()
        Try
            GetSelectedRows()
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                Try
                    clBLL_Rederijen.DeleteRederij(id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try


            Next

            clsPreload.rederij = clBLL_Rederijen.GetRederijen

        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()

        blnEdit = True
        'id opslaan, zo weten we bij het updaten nog welke de gebruikt wilde aanpassen.  

        Try
            Me.rederijID = Me.dgView.Item(clBLL_Rederijen.GetRederijen.rederijIDColumn.ColumnName, Me.dgView.CurrentCellAddress.Y).Value
            Dim rederijNaam As String = Me.dgView.Item(ClassVAR.clBLL_Rederijen.GetRederijen.RederijnaamColumn.ColumnName, Me.dgView.CurrentCellAddress.Y).Value
            Me.txtRederij.Text = rederijNaam
        Catch ex As ArgumentOutOfRangeException
            MsgBox("Gelieve eerst uw keuze te selecteren", MsgBoxStyle.Exclamation)
        End Try

        menubar.Update()

    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()
        menubar.Load()
        clBLL_Rederijen.UpdateRederij(Me.rederijID, Me.txtRederij.Text)

        clsPreload.rederij = clBLL_Rederijen.GetRederijen
        txtRederij.Clear()
        blnEdit = False
    End Sub

   
    Private Sub frmRederij_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
    End Sub

  

    Private Sub txtRederij_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRederij.TextChanged
        If Not blnEdit Then
            If String.IsNullOrEmpty(Trim(txtRederij.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If


    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try


    End Sub

    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item(clsPreload.rederijValueMember, dr.Index).Value)
        Next
        'TODO boodschap weergeven, indien er geen rijen geselecteerd werden
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    Try
                        clBLL_Rederijen.DeleteRederij(id)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try


                Next

                clsPreload.rederij = clBLL_Rederijen.GetRederijen
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtRederij_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRederij.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub

    Private Sub mnuRederijKosten_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

