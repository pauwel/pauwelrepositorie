Public Class frmKostenplaats
    Private menubar As New clsMenuBar

    Private rijen As New ArrayList, id As Integer, blnEdit As Boolean = False



    Private Sub frmKostenplaatsN_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
    End Sub
    Public Overrides Sub initform()
        Me.loadRederijen()

    End Sub

    Protected Overrides Sub onCmdReportClick()
        MyBase.onCmdReportClick()

        _frmReports.Reportnaam = "kostenplaats.rpt"
        _frmReports.ShowDialog()
    End Sub
    Private Sub loadRederijen()
        With Me.cmbRederij
            .DataSource = clBLL_Rederijen.GetRederijen
            .DisplayMember = clBLL_Rederijen.GetRederijen.RederijnaamColumn.ToString
            .ValueMember = clBLL_Rederijen.GetRederijen.rederijIDColumn.ToString
        End With
    End Sub
    ''' <summary>
    ''' Combo box met transportmiddelen laden 
    ''' De inhoud is afhankelijk van de geselecteerde rederij 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadTransportmiddelen()

        Dim r As New BLL.GetRederij

        Me.clearCmbTransport()
        Try
            Dim rederij As Nullable(Of Integer) = Me.cmbRederij.SelectedValue

            If rederij.HasValue Then
                With Me.cmbTransid
                    .DataSource = r.GetTransportMiddelByRederij(rederij)
                    .DisplayMember = r.GetTransportMiddelByRederij(rederij).TransCodeColumn.ToString
                    .ValueMember = r.GetTransportMiddelByRederij(rederij).TransIDColumn.ToString
                End With
            End If
        Catch ex As Exception
            'niets doen... 
            'TODO verschillende exceptions onderscheiden (bij laden/ sqlclient)
        End Try


    End Sub

    Private Sub clearCmbTransport()
        Me.cmbTransID.DataSource = Nothing
        Me.cmbTransID.Text = ""
        Me.cmbTransID.SelectedIndex = -1
    End Sub

    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()

        Dim kost As New BLL.KostenplaatsDetail
        With Me.dgView
            .DataSource = kost.GetKostenplatsDetail
            .Columns("kostid").Visible = False
            .Columns("rederijid").Visible = False
            .Columns("transid").Visible = False

        End With
    End Sub


    Private Sub cmbRederij_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRederij.SelectedValueChanged
        'De lijst met mogelijke transportmiddelen moet opnieuw geladen worden
        Me.loadTransportmiddelen()

    End Sub

   

    Private Sub clear()
        txtKostenplaats.Clear()
        txtOmschrijving.Clear()
        chkActief.Checked = False

    End Sub

   
    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()

        Dim kostenplaats As String = txtKostenplaats.Text
        Dim omschrijving As String = txtOmschrijving.Text
        Dim is_actief As Boolean = chkActief.CheckState
        'Dim rederijid As Nullable(Of Integer) = cmbRederij.SelectedValue
        Dim transportid As Nullable(Of Integer) = cmbTransid.SelectedValue
        
        Try
            If Not String.IsNullOrEmpty(Trim(txtKostenplaats.Text)) Then
                clBLL_Kostenplaats.InsertKostenplaats(kostenplaats, omschrijving, is_actief, transportid)
                refreshgrid()
            End If

        Catch ex As Exception

        End Try
        clear()

    End Sub

    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()

        GetSelectedRows()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                Try
                    clBLL_Kostenplaats.DeleteKostenplaats(id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next

            refreshgrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()
        menubar.Update()
        blnEdit = True

        
        With dgView
            id = .Item(clBLL_Kostenplaats.GetKosten.KostIDColumn.ColumnName, .CurrentCellAddress.Y).Value
            Dim kostenplaats As String = .Item(clBLL_Kostenplaats.GetKosten.KostenplaatsColumn.ColumnName, .CurrentCellAddress.Y).Value
            Dim omschrijving As String = .Item(clBLL_Kostenplaats.GetKosten.KostOmschrijvingColumn.ColumnName, .CurrentCellAddress.Y).Value
            Dim is_actief As Boolean = .Item(clBLL_Kostenplaats.GetKosten.is_actiefColumn.ColumnName, .CurrentCellAddress.Y).Value
            Dim rederijid As Nullable(Of Integer) = .Item("rederijid", .CurrentCellAddress.Y).Value
            Dim transportid As Nullable(Of Integer) = .Item(clBLL_Kostenplaats.GetKosten.TransIDColumn.ColumnName, .CurrentCellAddress.Y).Value



            txtKostenplaats.Text = kostenplaats
            txtOmschrijving.Text = omschrijving
            chkActief.Checked = is_actief
            cmbRederij.SelectedValue = rederijid
            cmbTransid.SelectedValue = transportid

        End With
        

        

    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()


        Dim id As Integer = dgView.Item("kostid", dgView.CurrentCellAddress.Y).Value
        Dim kostenplaats As String = txtKostenplaats.Text
        Dim omschrijving As String = txtOmschrijving.Text
        Dim is_actief As Boolean = chkActief.CheckState
        Dim rederijid As Nullable(Of Integer) = cmbRederij.SelectedValue
        Dim transportid As Nullable(Of Integer) = cmbTransid.SelectedValue

        Try
            If Not String.IsNullOrEmpty(Trim(txtKostenplaats.Text)) Then
                clBLL_Kostenplaats.UpdateKostenplaats(id, kostenplaats, omschrijving, is_actief, transportid)
                refreshgrid()
            End If

            If Not is_actief Then
                clBLL_Voyages.SetAfgewerkt(True, id)
            End If
        Catch ex As Exception

        End Try
        clear()
        menubar.Load()
    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try

    End Sub

    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item("kostid", dr.Index).Value)
        Next
        'TODO boodschap weergeven, indien er geen rijen geselecteerd werden
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    Try
                        clBLL_Kostenplaats.DeleteKostenplaats(id)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                Next

            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub txtKostenplaats_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKostenplaats.TextChanged


        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtKostenplaats.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If


    End Sub
    
    
    Private Sub cmbTransid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbTransid.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub
End Class
