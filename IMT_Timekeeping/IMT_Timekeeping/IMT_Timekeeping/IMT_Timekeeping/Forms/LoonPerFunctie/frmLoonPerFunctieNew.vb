﻿Public Class frmLoonPerFunctieNew


    Public updated As Boolean = False

    Sub New()


        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            Dim b As New BLL.BLLoonPerFunctie
            LoonPerFunctieEntityBindingSource.DataSource = b.getLoonPerFunctie
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try



    End Sub



    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Try
            Dim b As New BLL.BLLoonPerFunctie
            For Each r As DataGridViewRow In dgvFormule.SelectedRows
                Dim data As New DAL.entities.LoonPerFunctie_Entity
                data = r.DataBoundItem
                'validatie
                If String.IsNullOrEmpty(TextBox1.Text.Trim()) Then
                    MsgBox("Formule moet een waarde bevatten, het is niet mogelijk een leeg veld toe te voegen", MsgBoxStyle.Information)
                    Exit For
                End If
                data.Formule = TextBox1.Text.Trim()
                b.UpdateLoonPerFunctie(data)
                updated = True
            Next
        
            LoonPerFunctieEntityBindingSource.DataSource = b.getLoonPerFunctie

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try


    End Sub

    Private Sub ButtonClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonClose.Click
        If updated Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If
    End Sub
End Class