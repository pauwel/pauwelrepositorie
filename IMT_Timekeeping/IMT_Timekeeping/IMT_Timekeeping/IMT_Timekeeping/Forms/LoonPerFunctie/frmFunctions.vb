
Imports BLL

Public Class frmFunctions
    Dim id As Nullable(Of Int32)
    Private rijen As New ArrayList
    Dim menubar As New clsMenuBar

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        menubar.Load()
        clBLL_Functies.InsertFuncties(txtJob.Text, txtDescription.Text)
        clsPreload.functies = clBLL_feestdagen.getdata
        Refreshgrid()
    End Sub

    

    

    Private Sub frmFunctions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        Refreshgrid()
    End Sub

    Private Sub Refreshgrid()
        With dgvFunctions
            .DataSource = clBLL_Functies.GetFuncties
            .Columns(clBLL_Functies.GetFuncties.FunctieIDColumn.ColumnName).Visible = False
        End With
        txtDescription.Clear()
        txtJob.Clear()
        txtJob.Select()


        menubar.Load()
    End Sub


    Private Sub dgvFunctions_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvFunctions.KeyUp

        If e.KeyCode = Keys.Delete Then
            'For dr As Integer = 0 To rijen.Count - 1
            id = dgvFunctions.Item("functieid", dgvFunctions.CurrentCellAddress.Y).Value
            clBLL_Functies.DeleteFuncties(id)
            'Next
            clsPreload.functies = clBLL_feestdagen.getdata
        End If

    End Sub


    Private Sub dgvFunctions_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvFunctions.KeyDown

        GetSelectedRows()

    End Sub
    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgvFunctions.SelectedRows
            rijen.Add(dgvFunctions.Item("FunctieID", dr.Index).Value)
        Next
    End Sub


    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Dim job As String = dgvFunctions.Item(clBLL_Functies.GetFuncties.FunctieCodeColumn.ColumnName, dgvFunctions.CurrentCellAddress.Y).Value
        Dim omschrijving As String = dgvFunctions.Item(clBLL_Functies.GetFuncties.FunctieOmschrijvingColumn.ColumnName, dgvFunctions.CurrentCellAddress.Y).Value
       
        txtJob.Text = job
        txtDescription.Text = omschrijving

        menubar.Update()

    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Dim job As String = txtJob.Text
        Dim omschrijving As String = txtDescription.Text

        id = dgvFunctions.Item("FunctieID", dgvFunctions.CurrentCellAddress.Y).Value

        clBLL_Functies.UpdateFuncties(job, omschrijving, id)


        menubar.Load()
        clsPreload.functies = clBLL_feestdagen.getdata
        Refreshgrid()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click

        '    For dr As Integer = 0 To rijen.Count - 1
        Try
            id = dgvFunctions.Item("functieid", dgvFunctions.CurrentCellAddress.Y).Value
            Try
                clBLL_Functies.DeleteFuncties(id)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            'Next
            clsPreload.functies = clBLL_feestdagen.getdata
            Refreshgrid()
        Catch ex As ArgumentOutOfRangeException
            MsgBox("klik op een cel om deze te selecteren", MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub txtJob_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJob.TextChanged
        If String.IsNullOrEmpty(Trim(txtJob.Text)) Then
            menubar.Load()
        Else
            menubar.Add()
        End If
    End Sub



    Private Sub dgvFunctions_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvFunctions.SelectionChanged

        Try
            If dgvFunctions.SelectedRows.Count = 1 Then
                menubar.edit()
            Else
                menubar.Load()
            End If
            dgvFunctions.Rows(dgvFunctions.CurrentCellAddress.Y).Selected = True
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtDescription_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescription.KeyUp
        If e.KeyCode = Keys.F1 Then
            cmdAdd.PerformClick()
        End If
    End Sub


    
    
    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        _frmReports.Reportnaam = "functies.rpt"
        _frmReports.ShowDialog()
    End Sub
End Class