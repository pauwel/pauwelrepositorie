Public Class frmCategory

    Private id As Nullable(Of Int32)
    Private menubar As New clsMenuBar

    Private Sub refreshgrid()
        With dgvCategory
            .DataSource = clBLL_Category.GetCategories
            .Columns(clBLL_Category.GetCategories.CatIDColumn.ColumnName).Visible = False
            If .Rows.Count > 0 Then
                .Rows(0).Selected = True
            End If
        End With
        txtDescription.Clear()
        txtCatCode.Clear()
        txtCEPACODE.Clear()

        txtCepaFunctieCode.Clear()
        txtCatCode.Select()
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        If Not String.IsNullOrEmpty(Trim(txtCEPACODE.Text)) And Not String.IsNullOrEmpty(Trim(txtCatCode.Text)) Then
            clBLL_Category.InsertCategory(txtCatCode.Text, txtDescription.Text, txtCEPACODE.Text, txtCepaFunctieCode.Text)
            clsPreload.category = clBLL_Category.GetCategories
            refreshgrid()
        Else
            MsgBox("Category code & CEPA Code zijn verplichte velden", MsgBoxStyle.Information)
        End If



    End Sub

    Private Sub frmCategory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        refreshgrid()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Try
            Dim code As String = dgvCategory.Item(clBLL_Category.GetCategories.CatCodeColumn.ColumnName, dgvCategory.CurrentCellAddress.Y).Value
            Dim omschrijving As String = dgvCategory.Item(clBLL_Category.GetCategories.CatOmschrijvingColumn.ColumnName, dgvCategory.CurrentCellAddress.Y).Value
            Dim CEPAcode As String = dgvCategory.Item(clBLL_Category.GetCategories.CEPACodeColumn.ColumnName, dgvCategory.CurrentCellAddress.Y).Value
            Dim cepafunctiecode As String = dgvCategory.Item(clBLL_Category.GetCategories.CEPAFunctiecodeColumn.ColumnName, dgvCategory.CurrentCellAddress.Y).Value.ToString

            txtCatCode.Text = code
            txtDescription.Text = omschrijving
            txtCEPACODE.Text = CEPAcode
            txtCepaFunctieCode.Text = cepafunctiecode
            txtCatCode.Select()

            menubar.Update()
        Catch arex As ArgumentOutOfRangeException
            MsgBox("klik op een cel om deze te selecteren", MsgBoxStyle.Information)
        Catch ex As Exception

        End Try


    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Dim code As String = txtCatCode.Text
        Dim omschrijving As String = txtDescription.Text
        Dim CEPACODE As String = txtCEPACODE.Text
        Dim cepafunctiecode As String = txtCepaFunctieCode.Text

        id = dgvCategory.Item(clBLL_Category.GetCategories.CatIDColumn.ColumnName, dgvCategory.CurrentCellAddress.Y).Value
        If Not String.IsNullOrEmpty(Trim(txtCEPACODE.Text)) And Not String.IsNullOrEmpty(Trim(txtCatCode.Text)) Then
            clBLL_Category.UpdateCategory(code, omschrijving, CEPACODE, cepafunctiecode, id)
            clsPreload.category = clBLL_Category.GetCategories
            menubar.Load()
            refreshgrid()
        Else
            MsgBox("Category code & CEPA Code zijn verplichte velden", MsgBoxStyle.Information)
        End If


    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Try
            id = dgvCategory.Item(clBLL_Category.GetCategories.CatIDColumn.ColumnName, dgvCategory.CurrentCellAddress.Y).Value
            Try
                clBLL_Category.DeleteCategory(id)
                clsPreload.category = clBLL_Category.GetCategories
                refreshgrid()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Catch ex As ArgumentOutOfRangeException
            MsgBox("klik op een cel om deze te selecteren", MsgBoxStyle.Information)
        End Try




    End Sub

    Private Sub dgvCategory_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvCategory.KeyDown
        Try
            id = dgvCategory.Item(clBLL_Category.GetCategories.CatIDColumn.ColumnName, dgvCategory.CurrentCellAddress.Y).Value
        Catch ex As Exception
            id = Nothing
        End Try
    End Sub

    Private Sub dgvCategory_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvCategory.KeyUp
        If e.KeyCode = Keys.Delete And id.HasValue Then
            Try
                clBLL_Category.DeleteCategory(id)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If
    End Sub

  
    Private Sub txtCatCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCatCode.TextChanged
        If String.IsNullOrEmpty(Trim(txtCatCode.Text)) Then
            menubar.Load()
        Else
            menubar.Add()
        End If
    End Sub

    Private Sub dgvCategory_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvCategory.SelectionChanged
        Try
            menubar.edit()
            dgvCategory.Rows(dgvCategory.CurrentCellAddress.Y).Selected = True
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtDescription_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescription.KeyUp
        If e.KeyCode = Keys.F1 Then
            cmdAdd.PerformClick()
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        _frmReports.Reportnaam = "category.rpt"
        _frmReports.ShowDialog()
    End Sub

   
End Class