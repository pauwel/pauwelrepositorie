﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoonPerFunctieNew
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.dgvFormule = New System.Windows.Forms.DataGridView
        Me.ButtonSave = New System.Windows.Forms.Button
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.CatCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FunctieCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StelselDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OmschrijvingDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LoonIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CatIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FunctieIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FormuleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StdLoonDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TotBedragOverurenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TotSupplementDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EindejaarspremieDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TypeWerknemerIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ShiftIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SupplementDubbeleShiftDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IsValidDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.IsDirtyDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.LoonPerFunctieEntityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ButtonClose = New System.Windows.Forms.Button
        CType(Me.dgvFormule, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoonPerFunctieEntityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvFormule
        '
        Me.dgvFormule.AllowUserToAddRows = False
        Me.dgvFormule.AllowUserToDeleteRows = False
        Me.dgvFormule.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvFormule.AutoGenerateColumns = False
        Me.dgvFormule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFormule.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CatCodeDataGridViewTextBoxColumn, Me.FunctieCodeDataGridViewTextBoxColumn, Me.StelselDataGridViewTextBoxColumn, Me.OmschrijvingDataGridViewTextBoxColumn, Me.LoonIDDataGridViewTextBoxColumn, Me.CatIDDataGridViewTextBoxColumn, Me.FunctieIDDataGridViewTextBoxColumn, Me.FormuleDataGridViewTextBoxColumn, Me.StdLoonDataGridViewTextBoxColumn, Me.TotBedragOverurenDataGridViewTextBoxColumn, Me.TotSupplementDataGridViewTextBoxColumn, Me.EindejaarspremieDataGridViewTextBoxColumn, Me.TypeWerknemerIDDataGridViewTextBoxColumn, Me.DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn, Me.ShiftIDDataGridViewTextBoxColumn, Me.SupplementDubbeleShiftDataGridViewTextBoxColumn, Me.IsValidDataGridViewCheckBoxColumn, Me.IsDirtyDataGridViewCheckBoxColumn})
        Me.dgvFormule.DataSource = Me.LoonPerFunctieEntityBindingSource
        Me.dgvFormule.Location = New System.Drawing.Point(0, 0)
        Me.dgvFormule.Name = "dgvFormule"
        Me.dgvFormule.ReadOnly = True
        Me.dgvFormule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFormule.Size = New System.Drawing.Size(650, 374)
        Me.dgvFormule.TabIndex = 0
        '
        'ButtonSave
        '
        Me.ButtonSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonSave.Location = New System.Drawing.Point(481, 390)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.ButtonSave.TabIndex = 1
        Me.ButtonSave.Text = "Bewaren"
        Me.ButtonSave.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(107, 392)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(368, 20)
        Me.TextBox1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(57, 395)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Formule"
        '
        'CatCodeDataGridViewTextBoxColumn
        '
        Me.CatCodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.CatCodeDataGridViewTextBoxColumn.DataPropertyName = "CatCode"
        Me.CatCodeDataGridViewTextBoxColumn.HeaderText = "Category"
        Me.CatCodeDataGridViewTextBoxColumn.Name = "CatCodeDataGridViewTextBoxColumn"
        Me.CatCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.CatCodeDataGridViewTextBoxColumn.Width = 74
        '
        'FunctieCodeDataGridViewTextBoxColumn
        '
        Me.FunctieCodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.FunctieCodeDataGridViewTextBoxColumn.DataPropertyName = "FunctieCode"
        Me.FunctieCodeDataGridViewTextBoxColumn.HeaderText = "FunctieCode"
        Me.FunctieCodeDataGridViewTextBoxColumn.Name = "FunctieCodeDataGridViewTextBoxColumn"
        Me.FunctieCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.FunctieCodeDataGridViewTextBoxColumn.Width = 92
        '
        'StelselDataGridViewTextBoxColumn
        '
        Me.StelselDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.StelselDataGridViewTextBoxColumn.DataPropertyName = "Stelsel"
        Me.StelselDataGridViewTextBoxColumn.HeaderText = "Stelsel"
        Me.StelselDataGridViewTextBoxColumn.Name = "StelselDataGridViewTextBoxColumn"
        Me.StelselDataGridViewTextBoxColumn.ReadOnly = True
        Me.StelselDataGridViewTextBoxColumn.Width = 63
        '
        'OmschrijvingDataGridViewTextBoxColumn
        '
        Me.OmschrijvingDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.OmschrijvingDataGridViewTextBoxColumn.DataPropertyName = "Omschrijving"
        Me.OmschrijvingDataGridViewTextBoxColumn.HeaderText = "Shift"
        Me.OmschrijvingDataGridViewTextBoxColumn.Name = "OmschrijvingDataGridViewTextBoxColumn"
        Me.OmschrijvingDataGridViewTextBoxColumn.ReadOnly = True
        Me.OmschrijvingDataGridViewTextBoxColumn.Width = 53
        '
        'LoonIDDataGridViewTextBoxColumn
        '
        Me.LoonIDDataGridViewTextBoxColumn.DataPropertyName = "LoonID"
        Me.LoonIDDataGridViewTextBoxColumn.HeaderText = "LoonID"
        Me.LoonIDDataGridViewTextBoxColumn.Name = "LoonIDDataGridViewTextBoxColumn"
        Me.LoonIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.LoonIDDataGridViewTextBoxColumn.Visible = False
        '
        'CatIDDataGridViewTextBoxColumn
        '
        Me.CatIDDataGridViewTextBoxColumn.DataPropertyName = "CatID"
        Me.CatIDDataGridViewTextBoxColumn.HeaderText = "CatID"
        Me.CatIDDataGridViewTextBoxColumn.Name = "CatIDDataGridViewTextBoxColumn"
        Me.CatIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.CatIDDataGridViewTextBoxColumn.Visible = False
        '
        'FunctieIDDataGridViewTextBoxColumn
        '
        Me.FunctieIDDataGridViewTextBoxColumn.DataPropertyName = "FunctieID"
        Me.FunctieIDDataGridViewTextBoxColumn.HeaderText = "FunctieID"
        Me.FunctieIDDataGridViewTextBoxColumn.Name = "FunctieIDDataGridViewTextBoxColumn"
        Me.FunctieIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.FunctieIDDataGridViewTextBoxColumn.Visible = False
        '
        'FormuleDataGridViewTextBoxColumn
        '
        Me.FormuleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.FormuleDataGridViewTextBoxColumn.DataPropertyName = "Formule"
        Me.FormuleDataGridViewTextBoxColumn.HeaderText = "Formule"
        Me.FormuleDataGridViewTextBoxColumn.MaxInputLength = 150
        Me.FormuleDataGridViewTextBoxColumn.Name = "FormuleDataGridViewTextBoxColumn"
        Me.FormuleDataGridViewTextBoxColumn.ReadOnly = True
        '
        'StdLoonDataGridViewTextBoxColumn
        '
        Me.StdLoonDataGridViewTextBoxColumn.DataPropertyName = "stdLoon"
        Me.StdLoonDataGridViewTextBoxColumn.HeaderText = "stdLoon"
        Me.StdLoonDataGridViewTextBoxColumn.Name = "StdLoonDataGridViewTextBoxColumn"
        Me.StdLoonDataGridViewTextBoxColumn.ReadOnly = True
        Me.StdLoonDataGridViewTextBoxColumn.Visible = False
        '
        'TotBedragOverurenDataGridViewTextBoxColumn
        '
        Me.TotBedragOverurenDataGridViewTextBoxColumn.DataPropertyName = "TotBedragOveruren"
        Me.TotBedragOverurenDataGridViewTextBoxColumn.HeaderText = "TotBedragOveruren"
        Me.TotBedragOverurenDataGridViewTextBoxColumn.Name = "TotBedragOverurenDataGridViewTextBoxColumn"
        Me.TotBedragOverurenDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotBedragOverurenDataGridViewTextBoxColumn.Visible = False
        '
        'TotSupplementDataGridViewTextBoxColumn
        '
        Me.TotSupplementDataGridViewTextBoxColumn.DataPropertyName = "TotSupplement"
        Me.TotSupplementDataGridViewTextBoxColumn.HeaderText = "TotSupplement"
        Me.TotSupplementDataGridViewTextBoxColumn.Name = "TotSupplementDataGridViewTextBoxColumn"
        Me.TotSupplementDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotSupplementDataGridViewTextBoxColumn.Visible = False
        '
        'EindejaarspremieDataGridViewTextBoxColumn
        '
        Me.EindejaarspremieDataGridViewTextBoxColumn.DataPropertyName = "eindejaarspremie"
        Me.EindejaarspremieDataGridViewTextBoxColumn.HeaderText = "eindejaarspremie"
        Me.EindejaarspremieDataGridViewTextBoxColumn.Name = "EindejaarspremieDataGridViewTextBoxColumn"
        Me.EindejaarspremieDataGridViewTextBoxColumn.ReadOnly = True
        Me.EindejaarspremieDataGridViewTextBoxColumn.Visible = False
        '
        'TypeWerknemerIDDataGridViewTextBoxColumn
        '
        Me.TypeWerknemerIDDataGridViewTextBoxColumn.DataPropertyName = "TypeWerknemerID"
        Me.TypeWerknemerIDDataGridViewTextBoxColumn.HeaderText = "TypeWerknemerID"
        Me.TypeWerknemerIDDataGridViewTextBoxColumn.Name = "TypeWerknemerIDDataGridViewTextBoxColumn"
        Me.TypeWerknemerIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.TypeWerknemerIDDataGridViewTextBoxColumn.Visible = False
        '
        'DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn
        '
        Me.DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn.DataPropertyName = "DatumBijdrageBerekeningCEPA"
        Me.DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn.HeaderText = "DatumBijdrageBerekeningCEPA"
        Me.DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn.Name = "DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn"
        Me.DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn.ReadOnly = True
        Me.DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn.Visible = False
        '
        'ShiftIDDataGridViewTextBoxColumn
        '
        Me.ShiftIDDataGridViewTextBoxColumn.DataPropertyName = "ShiftID"
        Me.ShiftIDDataGridViewTextBoxColumn.HeaderText = "ShiftID"
        Me.ShiftIDDataGridViewTextBoxColumn.Name = "ShiftIDDataGridViewTextBoxColumn"
        Me.ShiftIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.ShiftIDDataGridViewTextBoxColumn.Visible = False
        '
        'SupplementDubbeleShiftDataGridViewTextBoxColumn
        '
        Me.SupplementDubbeleShiftDataGridViewTextBoxColumn.DataPropertyName = "SupplementDubbeleShift"
        Me.SupplementDubbeleShiftDataGridViewTextBoxColumn.HeaderText = "SupplementDubbeleShift"
        Me.SupplementDubbeleShiftDataGridViewTextBoxColumn.Name = "SupplementDubbeleShiftDataGridViewTextBoxColumn"
        Me.SupplementDubbeleShiftDataGridViewTextBoxColumn.ReadOnly = True
        Me.SupplementDubbeleShiftDataGridViewTextBoxColumn.Visible = False
        '
        'IsValidDataGridViewCheckBoxColumn
        '
        Me.IsValidDataGridViewCheckBoxColumn.DataPropertyName = "IsValid"
        Me.IsValidDataGridViewCheckBoxColumn.HeaderText = "IsValid"
        Me.IsValidDataGridViewCheckBoxColumn.Name = "IsValidDataGridViewCheckBoxColumn"
        Me.IsValidDataGridViewCheckBoxColumn.ReadOnly = True
        Me.IsValidDataGridViewCheckBoxColumn.Visible = False
        '
        'IsDirtyDataGridViewCheckBoxColumn
        '
        Me.IsDirtyDataGridViewCheckBoxColumn.DataPropertyName = "IsDirty"
        Me.IsDirtyDataGridViewCheckBoxColumn.HeaderText = "IsDirty"
        Me.IsDirtyDataGridViewCheckBoxColumn.Name = "IsDirtyDataGridViewCheckBoxColumn"
        Me.IsDirtyDataGridViewCheckBoxColumn.ReadOnly = True
        Me.IsDirtyDataGridViewCheckBoxColumn.Visible = False
        '
        'LoonPerFunctieEntityBindingSource
        '
        Me.LoonPerFunctieEntityBindingSource.DataSource = GetType(DAL.entities.LoonPerFunctie_Entity)
        '
        'ButtonClose
        '
        Me.ButtonClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonClose.Location = New System.Drawing.Point(563, 389)
        Me.ButtonClose.Name = "ButtonClose"
        Me.ButtonClose.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClose.TabIndex = 4
        Me.ButtonClose.Text = "Sluiten"
        Me.ButtonClose.UseVisualStyleBackColor = True
        '
        'frmLoonPerFunctieNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(650, 424)
        Me.ControlBox = False
        Me.Controls.Add(Me.ButtonClose)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.dgvFormule)
        Me.Name = "frmLoonPerFunctieNew"
        Me.Text = "Formule-omschrijving aanpassen"
        CType(Me.dgvFormule, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoonPerFunctieEntityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvFormule As System.Windows.Forms.DataGridView
    Friend WithEvents LoonPerFunctieEntityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CatCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FunctieCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StelselDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OmschrijvingDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LoonIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CatIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FunctieIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FormuleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StdLoonDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotBedragOverurenDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotSupplementDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EindejaarspremieDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypeWerknemerIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DatumBijdrageBerekeningCEPADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ShiftIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplementDubbeleShiftDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsValidDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDirtyDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ButtonSave As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ButtonClose As System.Windows.Forms.Button
End Class
