﻿
Imports DAL.entities

Public Class FormRapportSelector

    Private begin As DateTime
    Private eind As DateTime

    Dim rapportkeuze As Integer = -1
    Dim wn As WerknemerStatuut_entity = New WerknemerStatuut_entity

    Sub New(ByVal _rapport As Integer, ByVal bdatum As DateTime, ByVal edatum As DateTime, ByVal _data As WerknemerStatuut_entity)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        begin = bdatum
        eind = edatum
        bDate.Value = bdatum
        eDate.Value = edatum

        rapportkeuze = _rapport
        wn = _data
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Select Case rapportkeuze

            Case 0 'niet spaarplanner
                _frmReports.Werknemerid = wn.werknemerid
                _frmReports.bDatum = bDate.Value
                _frmReports.Edatum = eDate.Value
                _frmReports.Reportnaam = "NIETSpaarplanner.rpt"


                _frmReports.ShowDialog()
            Case 1 'spaarplanner
                _frmReports.Werknemerid = wn.werknemerid
                _frmReports.bDatum = bDate.Value
                _frmReports.Edatum = eDate.Value
                _frmReports.Reportnaam = "Spaarplanner.rpt"

                _frmReports.ShowDialog()
        End Select
    End Sub

    Private Sub eDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bDate.ValueChanged
        bDate.MinDate = begin
        bDate.MaxDate = eDate.Value

    End Sub

    Private Sub eDate_ValueChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles eDate.ValueChanged
        eDate.MinDate = bDate.Value
        eDate.MaxDate = eind
    End Sub
End Class