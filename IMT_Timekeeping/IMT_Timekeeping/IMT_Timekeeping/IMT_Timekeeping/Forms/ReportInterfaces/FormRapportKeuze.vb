﻿Public Class FormRapportKeuze

    Sub New(ByVal dt As List(Of DAL.entities.WerknemerStatuut_entity))

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DataGridView1.DataSource = dt
 


    End Sub

    Private Sub ButtonUitvoeren_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUitvoeren.Click
        'keuze tss de verschillende rapporten
        Dim dr As DAL.entities.WerknemerStatuut_entity = DataGridView1.CurrentRow.DataBoundItem

        Dim rapportkeuze As Integer = dr.statuutid
        Dim werknemerid As Integer = dr.werknemerid
        Dim bdatum As DateTime = dr.begindatum
        Dim edatum As DateTime = dr.einddatum


        Dim f As New FormRapportSelector(rapportkeuze, dr.begindatum, dr.einddatum, dr)
        f.StartPosition = FormStartPosition.CenterParent
        f.ShowDialog()

    End Sub

 
End Class