
Option Compare Text
Imports System.Data.SqlClient
Imports System.Configuration

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class _frmReports

    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean


    Private _Reportnaam As String
    Public Property Reportnaam() As String
        Get
            Return _Reportnaam
        End Get
        Set(ByVal value As String)
            _Reportnaam = value
        End Set
    End Property


    Private _wachtid As Integer
    Public Property Wachtid() As Integer
        Get
            Return _wachtid
        End Get
        Set(ByVal value As Integer)
            _wachtid = value
        End Set
    End Property


    Private _datum As DateTime
    Public Property Datum() As DateTime
        Get
            Return _datum
        End Get
        Set(ByVal value As DateTime)
            _datum = value
        End Set
    End Property


    Private _werknemerid As Integer
    Public Property Werknemerid() As Integer
        Get
            Return _werknemerid
        End Get
        Set(ByVal value As Integer)
            _werknemerid = value
        End Set
    End Property


    Private _edatum As DateTime
    Public Property Edatum() As DateTime
        Get
            Return _edatum
        End Get
        Set(ByVal value As DateTime)
            _edatum = value
        End Set
    End Property

    Private _bdatum As DateTime
    Public Property bDatum() As DateTime
        Get
            Return _bdatum
        End Get
        Set(ByVal value As DateTime)
            _bdatum = value
        End Set
    End Property


    Private _voayageid As Integer
    Public Property VoyageID() As Integer
        Get
            Return _voayageid
        End Get
        Set(ByVal value As Integer)
            _voayageid = value
        End Set
    End Property


    Private _loonboek As Integer
    Public Property loonboek() As Integer
        Get
            Return _loonboek
        End Get
        Set(ByVal value As Integer)
            _loonboek = value
        End Set
    End Property


    Private _shiften As String
    Public Property Shiften() As String
        Get
            Return _shiften
        End Get
        Set(ByVal value As String)
            _shiften = value
        End Set
    End Property

    Private _rederijID As Integer
    Public Property RederijID() As Integer
        Get
            Return _rederijID
        End Get
        Set(ByVal value As Integer)
            _rederijID = value
        End Set
    End Property


    Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo

    Private Sub frmReports_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LockWindowUpdate(Me.Handle.ToInt64)

        Dim strconn As String = ConfigurationManager.ConnectionStrings(0).ConnectionString

        Dim server As String = Mid(strconn, InStr(strconn, "Data source=", CompareMethod.Text) + 12)
        server = Mid(server, 1, InStr(server, ";", CompareMethod.Text) - 1)
        myConnectionInfo.ServerName = server

        Dim Catalog As String = Mid(strconn, InStr(strconn, "Initial catalog=", CompareMethod.Text) + 16)
        Catalog = Mid(Catalog, 1, InStr(Catalog, ";", CompareMethod.Text) - 1)
        myConnectionInfo.DatabaseName = Catalog

        Dim y As Integer = InStr(strconn, "User id=", CompareMethod.Text)

        Dim userid As String = Nothing
        Dim password As String = Nothing

        If y > 0 Then
            userid = Mid(strconn, InStr(strconn, "User id=", CompareMethod.Text) + 8)
            userid = Mid(userid, 1, InStr(userid, ";", CompareMethod.Text) - 1)
            myConnectionInfo.UserID = userid

            Dim x As Integer = InStr(strconn, "password=", CompareMethod.Text)

            password = Mid(strconn, x + 9)

            Dim pw As Integer = InStr(password, ";", CompareMethod.Text)
            If pw = 0 Then
                password = Mid(password, 1)
            Else
                password = Mid(password, 1, InStr(password, ";", CompareMethod.Text) - 1)
            End If

            myConnectionInfo.Password = password
            myConnectionInfo.IntegratedSecurity = False

        Else
            myConnectionInfo.IntegratedSecurity = True

        End If


        Dim report As New CrystalDecisions.CrystalReports.Engine.ReportDocument()
 
        report.Load(Application.StartupPath & "\reports\" & Reportnaam)

        Dim vernieuwen As Boolean = True
        'rapport zonder parameters best refreshen om gegevens welke tijdens 
        'de ontwikkeling aanwezig waren zeker niet te tonen
        Select Case Reportnaam
            Case "WBBrief.rpt"
                report.SetParameterValue(0, Wachtid)
                vernieuwen = False
            Case "WBALLNEW.rpt"
                report.SetParameterValue(0, Datum)
                report.SetParameterValue(1, Shiften)
                vernieuwen = False
            Case "prestaties.rpt"
                report.SetParameterValue(0, Datum)
                vernieuwen = False
            Case "betalingsoverzicht.rpt"
                'report.SetParameterValue(0, loonboek)
                report.SetParameterValue(0, bDatum)
                report.SetParameterValue(1, Edatum)


                vernieuwen = False
            Case "KostenAnalyseALL.rpt"

                report.SetParameterValue(0, bDatum)
                report.SetParameterValue(1, Edatum)
                report.SetParameterValue(2, RederijID)
                vernieuwen = False

            Case "KostenAnalyse.rpt"
                report.SetParameterValue(0, bDatum)
                report.SetParameterValue(1, Edatum)
                report.SetParameterValue(2, VoyageID)
                vernieuwen = False
            Case "Spaarplanner.rpt"
                report.SetParameterValue(0, bDatum)
                report.SetParameterValue(1, Edatum)
                report.SetParameterValue(2, Werknemerid)

             
                vernieuwen = False

            Case "NIETSpaarplanner.rpt"
                report.SetParameterValue("begindatum", bDatum)
                report.SetParameterValue("einddatum", Edatum)
                report.SetParameterValue("werknemerid", Werknemerid)
                vernieuwen = False
            Case "rptProvisie"
                vernieuwen = False
        End Select

        SetDBLogonForReport(myConnectionInfo, report)

        CrystalReportViewer.ReportSource = report
        If vernieuwen Then CrystalReportViewer.RefreshReport()
        LockWindowUpdate(0)


    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables



        Try
            For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)

            Next


        Catch ex As Exception

        End Try
    End Sub

    


  
End Class