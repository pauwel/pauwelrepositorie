﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormRapportKeuze
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ButtonUitvoeren = New System.Windows.Forms.Button
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.WerknemerStatuutentityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BLRapportenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WerknemerStatuutentityBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.WerknemeridDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StatuutidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BegindatumDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EinddatumDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StatuutDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WerknemerStatuutentityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BLRapportenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WerknemerStatuutentityBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ButtonUitvoeren)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 322)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(496, 76)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(377, 50)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "de geselecteerde werknemer is tijdens zijn loopbaan van statuut gewijzigd, select" & _
            "eer een periode en klik op uitvoeren om het respectievelijke rapport te bekomen"
        '
        'ButtonUitvoeren
        '
        Me.ButtonUitvoeren.Location = New System.Drawing.Point(409, 29)
        Me.ButtonUitvoeren.Name = "ButtonUitvoeren"
        Me.ButtonUitvoeren.Size = New System.Drawing.Size(75, 23)
        Me.ButtonUitvoeren.TabIndex = 0
        Me.ButtonUitvoeren.Text = "Uitvoeren"
        Me.ButtonUitvoeren.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WerknemeridDataGridViewTextBoxColumn, Me.StatuutidDataGridViewTextBoxColumn, Me.BegindatumDataGridViewTextBoxColumn, Me.EinddatumDataGridViewTextBoxColumn, Me.StatuutDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.WerknemerStatuutentityBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(496, 326)
        Me.DataGridView1.TabIndex = 1
        '
        'WerknemerStatuutentityBindingSource
        '
        Me.WerknemerStatuutentityBindingSource.DataSource = GetType(DAL.entities.WerknemerStatuut_entity)
        '
        'BLRapportenBindingSource
        '
        Me.BLRapportenBindingSource.DataSource = GetType(BLL.BLRapporten)
        '
        'WerknemerStatuutentityBindingSource1
        '
        Me.WerknemerStatuutentityBindingSource1.DataSource = GetType(DAL.entities.WerknemerStatuut_entity)
        '
        'WerknemeridDataGridViewTextBoxColumn
        '
        Me.WerknemeridDataGridViewTextBoxColumn.DataPropertyName = "werknemerid"
        Me.WerknemeridDataGridViewTextBoxColumn.HeaderText = "werknemerid"
        Me.WerknemeridDataGridViewTextBoxColumn.Name = "WerknemeridDataGridViewTextBoxColumn"
        Me.WerknemeridDataGridViewTextBoxColumn.ReadOnly = True
        Me.WerknemeridDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.WerknemeridDataGridViewTextBoxColumn.Visible = False
        '
        'StatuutidDataGridViewTextBoxColumn
        '
        Me.StatuutidDataGridViewTextBoxColumn.DataPropertyName = "statuutid"
        Me.StatuutidDataGridViewTextBoxColumn.HeaderText = "statuutid"
        Me.StatuutidDataGridViewTextBoxColumn.Name = "StatuutidDataGridViewTextBoxColumn"
        Me.StatuutidDataGridViewTextBoxColumn.ReadOnly = True
        Me.StatuutidDataGridViewTextBoxColumn.Visible = False
        '
        'BegindatumDataGridViewTextBoxColumn
        '
        Me.BegindatumDataGridViewTextBoxColumn.DataPropertyName = "begindatum"
        Me.BegindatumDataGridViewTextBoxColumn.HeaderText = "begindatum"
        Me.BegindatumDataGridViewTextBoxColumn.Name = "BegindatumDataGridViewTextBoxColumn"
        Me.BegindatumDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EinddatumDataGridViewTextBoxColumn
        '
        Me.EinddatumDataGridViewTextBoxColumn.DataPropertyName = "einddatum"
        Me.EinddatumDataGridViewTextBoxColumn.HeaderText = "einddatum"
        Me.EinddatumDataGridViewTextBoxColumn.Name = "EinddatumDataGridViewTextBoxColumn"
        Me.EinddatumDataGridViewTextBoxColumn.ReadOnly = True
        '
        'StatuutDataGridViewTextBoxColumn
        '
        Me.StatuutDataGridViewTextBoxColumn.DataPropertyName = "statuut"
        Me.StatuutDataGridViewTextBoxColumn.HeaderText = "statuut"
        Me.StatuutDataGridViewTextBoxColumn.Name = "StatuutDataGridViewTextBoxColumn"
        Me.StatuutDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FormRapportKeuze
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(496, 397)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FormRapportKeuze"
        Me.Text = "RapportKeuze"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WerknemerStatuutentityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BLRapportenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WerknemerStatuutentityBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ButtonUitvoeren As System.Windows.Forms.Button
    Friend WithEvents BLRapportenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WerknemerStatuutentityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WerknemerStatuutentityBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents WerknemeridDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatuutidDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BegindatumDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EinddatumDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatuutDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
