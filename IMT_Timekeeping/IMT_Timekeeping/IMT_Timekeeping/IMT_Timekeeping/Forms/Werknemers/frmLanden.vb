Public Class frmLanden


    Private id As Integer
    Private rijen As New ArrayList

    Dim menubar As New clsMenuBar


    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        Try
            Dim landen As String = txtLanden.Text
            If Not String.IsNullOrEmpty(landen) Then
                clBLL_landen.InsertLand(landen)
            End If
        Catch ex As Exception

        End Try
        txtLanden.Clear()
        refreshlanden()

    End Sub

    Private Sub frmLanden_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed


        With clsPreload
            .land = clBLL_landen.GetLanden
            .landdisplaymember = clBLL_landen.GetLanden.LandColumn.ToString
            .landvaluemember = clBLL_landen.GetLanden.LandIDColumn.ToString

        End With

    End Sub

    Private Sub frmLanden_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        refreshlanden()
    End Sub

    Private Sub refreshlanden()
        dgvLanden.DataSource = clBLL_landen.GetLanden
        dgvLanden.Columns(clBLL_landen.GetLanden.LandIDColumn.ColumnName).Visible = False
    End Sub

    Private Sub txtLanden_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLanden.TextChanged
        If String.IsNullOrEmpty(Trim(txtLanden.Text)) Then
            menubar.Load()
        Else
            menubar.Add()
        End If
    End Sub

    Private Sub dgvLanden_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLanden.KeyDown

        GetSelectedRows()

    End Sub

    Private Sub dgvLanden_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLanden.KeyUp
        For dr As Integer = 0 To rijen.Count - 1
            id = rijen(dr)
            clBLL_landen.DeleteLand(id)
        Next
    End Sub
    Sub GetSelectedRows()
        menubar.edit()
        rijen.Clear()
        For Each dr As DataGridViewRow In dgvLanden.SelectedRows
            rijen.Add(dgvLanden.Item("landid", dr.Index).Value)
        Next
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        For dr As Integer = 0 To rijen.Count - 1
            id = rijen(dr)
            clBLL_landen.DeleteLand(id)
        Next
        refreshlanden()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEdit.Click

        menubar.Update()
        id = dgvLanden.Item("landid", dgvLanden.CurrentCellAddress.Y).Value
        txtLanden.Text = dgvLanden.Item("land", dgvLanden.CurrentCellAddress.Y).Value

    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        menubar.Load()
        Try
            Dim landen As String = txtLanden.Text
            If Not String.IsNullOrEmpty(landen) Then
                clBLL_landen.UpdateLand(landen, id)
            End If
        Catch ex As Exception

        End Try
        txtLanden.Clear()
        refreshlanden()

    End Sub

    Private Sub dgvLanden_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLanden.SelectionChanged
        menubar.edit()
        GetSelectedRows()
    End Sub
End Class