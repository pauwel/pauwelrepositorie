

Option Compare Text
Imports System.Threading


Public Class frmWerkNemers
    Private clsWerknemers As New clsWerknemers
    Private clswerknemersSP As New BLL.BLLWerknemersSP
    Private LoonboekCheck As Boolean = True, WNTYPE As Integer
    Dim rijen As New ArrayList

    Dim menubar As New clsMenuBar

    Dim edit As Boolean = False
    Private dgv As DataGridView

    Dim closeform As Boolean = False
    Dim err As New ErrorProvider




    Private Sub txtLoonboek_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoonboek.Leave


        If closeform Then Exit Sub


        If LoonboekCheck Then
            Dim LBnr As String = Nothing, Controle As String = Nothing
            Try
                Dim loonboek As String = txtLoonboek.Text
                err.Clear()
                err.BlinkStyle = ErrorBlinkStyle.NeverBlink

                LBnr = loonboek.Substring(0, 5)
                Controle = loonboek.Substring(6, 2)
                If String.IsNullOrEmpty(Trim(LBnr)) Then
                    err.SetError(txtLoonboek, "Gelieve een loonboeknummer in te vullen aub ")

                    txtLoonboek.Select()
                Else
                    If Not LBnr Mod 97 = Controle Then
                        err.SetError(txtLoonboek, "Controle getal " & Controle & " voor loonboek " & LBnr & " incorrect")
                        ' MsgBox("Controle getal " & Controle & " voor loonboek " & LBnr & " incorrect" & vbCrLf & "Controleer", MsgBoxStyle.Critical, APPLICATIENAAM)
                        txtLoonboek.Select()
                    Else
                        'controle op bestaan van de nummer
                        If Not clBLL_werknemers.LoonboekIsUnique(txtLoonboek.Text) Then
                            err.SetError(txtLoonboek, "Loonboeknummer reeds in gebruik " & vbCrLf & "Controleer")
                            ' MsgBox("Loonboeknummer reeds in gebruik " & vbCrLf & "Controleer", MsgBoxStyle.Critical, APPLICATIENAAM)
                            txtLoonboek.Select()
                        End If
                    End If
                End If

            Catch a As ArgumentOutOfRangeException
                err.SetError(txtLoonboek, "Gelieve een loonboeknummer in te vullen aub ")
                'MsgBox("Gelieve een loonboeknummer in te vullen aub ", MsgBoxStyle.Critical, APPLICATIENAAM)
                txtLoonboek.Select()
            Catch ex As Exception


            End Try
        End If
        LoonboekCheck = True

    End Sub

    Private Sub frmWerkNemers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb

        menubar.Load()

        Me.WindowState = FormWindowState.Maximized
        dteGeboorte.Value = DateAdd(DateInterval.Year, -25, Now)
        LoadTypeEmployeesCombobox()
        LoadCatCombobox()
        LoadLandenCombobox()

        CreateTab()

        WNTYPE = tabEmployees.SelectedTab.Tag
        RefreshGrid()
        clear()


        Dim s As New BLL.isInRole
        If s.IsAdmin Then
            ButtonConverteerNaarNietSpaarplanner.Enabled = True
        Else
            ButtonConverteerNaarNietSpaarplanner.Enabled = False
        End If
    End Sub


    Private Sub clear()


        txtAddress.Clear()
        txtEMAIL.Clear()
        txtGemeente.Clear()
        txtLoonboek.Clear()
        txtMobiel.Clear()
        txtNaam.Clear()
        txtPostcode.Clear()
        txtTelefoon.Clear()
        txtVoornaam.Clear()

        cmbCategory.Text = ""
        cmbCategory.SelectedIndex = -1

        cmbLand.Text = ""
        cmbLand.SelectedIndex = -1

        cmbWNType.Text = ""
        cmbWNType.SelectedIndex = -1


    End Sub


    Private Sub CreateTab()


        Dim t As Integer = 0
        For Each dr As DataRowView In cmbWNType.Items

            tabEmployees.TabPages.Add(dr.Row(1).ToString)
            tabEmployees.TabPages(t).Tag = dr.Row(0).ToString
            Dim dg As New DataGridView

            tabEmployees.TabPages(t).Controls.Add(dg)

            AddHandler tabEmployees.SelectedIndexChanged, AddressOf tabEmployees_SelectedIndexChanged

            AddHandler dg.SelectionChanged, AddressOf dg_SelectionChanged
            AddHandler dg.CellEnter, AddressOf dg_cellChanged
            dg.Dock = DockStyle.Fill
            dg.ReadOnly = True

            t += 1

        Next

    End Sub

    Private Sub dg_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        menubar.edit()
    End Sub

    Private Sub dg_cellChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Dim dgview As DataGridView = sender
        dgview.Rows(dgview.CurrentCellAddress.Y).Selected = True
    End Sub

    Private Sub dg_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Delete Then
            Dim id As Integer
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                Try
                    clBLL_werknemers.deleteWerknemers(id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next
            clear()
        End If

    End Sub


    Private Sub dg_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        GetSelectedRows()

    End Sub
    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgv.SelectedRows
            rijen.Add(dgv.Item("WerknemerID", dr.Index).Value)
        Next
    End Sub




#Region "Asynchronous handling"


    Dim thExecuteTaskAsync As Thread = Nothing
    Private Sub StartExecuteTaskAsync()
        'clear existing thread
        If Not thExecuteTaskAsync Is Nothing Then
            thExecuteTaskAsync.Abort()
            thExecuteTaskAsync.Join()
            thExecuteTaskAsync = Nothing
        End If
        'start a new thread to execute the task asynchronously
        thExecuteTaskAsync = New Thread(AddressOf ExecuteTaskAsync)
        thExecuteTaskAsync.Start()
    End Sub

    Private Sub ExecuteTaskAsync()
        clsWerknemers.WerknemersData = clswerknemersSP.GetWerknemers(WNTYPE)
    End Sub


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'this is necessary if the form is trying to close, even before the completion of task
        closeform = True
        err.Clear()

        If Not thExecuteTaskAsync Is Nothing Then thExecuteTaskAsync.Abort()

        clear()
    End Sub

#End Region

    Private Sub RefreshGrid()


        StartExecuteTaskAsync()

        For Each c As Control In tabEmployees.SelectedTab.Controls
            If c.GetType.ToString = "System.Windows.Forms.DataGridView" Then
                dgv = c
                Exit For
            End If
        Next

        While thExecuteTaskAsync.ThreadState = ThreadState.Running
            Thread.Sleep(0)
        End While

        dgv.DataSource = clsWerknemers.WerknemersData 'clswerknemersSP.GetWerknemers(WNTYPE)
        dgv.AllowUserToAddRows = False
        dgv.AllowUserToDeleteRows = False
        dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        For Each c As DataGridViewColumn In dgv.Columns
            Select Case c.Name
                Case "loonboek", "naam", "voornaam", "telefoon", "Mobiel"
                Case Else
                    c.Visible = False
            End Select
        Next

    End Sub

    Private Sub cmbCategory_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCategory.Enter
        LoadCatCombobox()
    End Sub
    Private Sub cmbWNType_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWNType.Enter
        LoadTypeEmployeesCombobox()
    End Sub

    Private Sub cmbland_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLand.Enter
        LoadLandenCombobox()
    End Sub

    Private Sub LoadTypeEmployeesCombobox()
        With clsPreload
            cmbWNType.DataSource = .employeetypes
            cmbWNType.DisplayMember = .employeetypesdisplaymember
            cmbWNType.ValueMember = .employeetypesvaluemember
            cmbWNType.SelectedIndex = -1
        End With

    End Sub

    Private Sub LoadCatCombobox()
        With clsPreload
            cmbCategory.DataSource = .category
            cmbCategory.DisplayMember = .categorydisplaymember
            cmbCategory.ValueMember = .categoryvaluemember
            cmbCategory.SelectedIndex = -1
        End With

    End Sub


    Private Sub LoadLandenCombobox()
        With clsPreload
            cmbLand.DataSource = .land
            cmbLand.DisplayMember = .landdisplaymember
            cmbLand.ValueMember = .landvaluemember
            cmbLand.SelectedIndex = -1
        End With

    End Sub

    Private Sub cmdEmployees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEmployees.Click
        frmEmployeeTypes.ShowDialog()

    End Sub

    Private Sub cmdCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCategory.Click
        frmCategory.ShowDialog()

    End Sub

    Private Sub cmbWNType_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWNType.SelectedValueChanged
        Try
            clsWerknemers.IsVast = clBLL_TypeWerknemers.IsVast(cmbWNType.SelectedValue)
        Catch ex As Exception
            clsWerknemers.IsVast = False
        End Try


        If clsWerknemers.IsVast Then
            fraVastIndienst.Enabled = True
        Else
            fraVastIndienst.Enabled = False
        End If

        ValidateFields()
    End Sub

    Private Sub cmdLanden_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLanden.Click
        frmLanden.ShowDialog()

        LoadLandenCombobox()

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        menubar.Load()
        ReadInput()
        If clsWerknemers.VALIDATE Then
            With clsWerknemers
                clBLL_werknemers.InsertWerknemerStoredProc(.loonboek, .naam, .voornaam, .DatumGeboorte.Date, .adres, _
                .postcode, .gemeente, .land, .telefoon, .mobiel, .email, .DatumVastInDienst.Date, .spaarplan, _
                .DatumSpaarplan.Date, .category.Value, .Werknemerstype.Value)

                'vlag op - 1 
                'dit doet bij gebruik van de werknemers in het planningformulier
                'de combobox refreshen
                clsPlanner.SelectedFunctie = -1

            End With
        End If

        WNTYPE = tabEmployees.SelectedTab.Tag
        RefreshGrid()
        clear()
    End Sub

    Private Sub ReadInput()
        With clsWerknemers
            .loonboek = txtLoonboek.Text
            .naam = txtNaam.Text
            .voornaam = txtVoornaam.Text
            .Werknemerstype = cmbWNType.SelectedValue
            .category = cmbCategory.SelectedValue
            .adres = txtAddress.Text
            .postcode = txtPostcode.Text
            .gemeente = txtGemeente.Text
            .land = cmbLand.SelectedValue
            .email = txtEMAIL.Text
            .telefoon = txtTelefoon.Text
            .mobiel = txtMobiel.Text

            .spaarplan = chkSpaarplan.Checked
            If .IsVast Then
                .DatumVastInDienst = dteVastInDienst.Value

            Else
                .DatumVastInDienst = Nothing
            End If
            If .spaarplan Then
                .DatumGeboorte = dteGeboorte.Value
                .DatumSpaarplan = dteInstapSpaarplan.Value
            Else
                .DatumGeboorte = Nothing
                .DatumSpaarplan = Nothing
            End If

        End With


    End Sub


    Private Sub chkSpaarplan_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSpaarplan.CheckStateChanged
        If chkSpaarplan.CheckState Then
            dteGeboorte.Enabled = True
            dteInstapSpaarplan.Enabled = True
        Else
            dteGeboorte.Enabled = False
            dteInstapSpaarplan.Enabled = False
        End If
    End Sub

    Private Sub tabEmployees_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabEmployees.SelectedIndexChanged

        WNTYPE = tabEmployees.TabPages(tabEmployees.SelectedIndex).Tag
        RefreshGrid()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        edit = True
        With dgv
            clsWerknemers.Id = .Item(clBLL_werknemers.GetWerknemers.WerknemerIDColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString

            txtLoonboek.Text = .Item(clBLL_werknemers.GetWerknemers.LoonboekColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtNaam.Text = .Item(clBLL_werknemers.GetWerknemers.NaamColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtVoornaam.Text = .Item(clBLL_werknemers.GetWerknemers.VoornaamColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            cmbWNType.SelectedValue = .Item(clBLL_werknemers.GetWerknemers.TypeWerknemerIDColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            cmbCategory.SelectedValue = .Item(clBLL_werknemers.GetWerknemers.CatIDColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtAddress.Text = .Item(clBLL_werknemers.GetWerknemers.AdresColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtPostcode.Text = .Item(clBLL_werknemers.GetWerknemers.PostcodeColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtGemeente.Text = .Item(clBLL_werknemers.GetWerknemers.GemeenteColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            cmbLand.SelectedValue = .Item(clBLL_werknemers.GetWerknemers.LandColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtEMAIL.Text = .Item(clBLL_werknemers.GetWerknemers.EmailColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtTelefoon.Text = .Item(clBLL_werknemers.GetWerknemers.TelefoonColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString
            txtMobiel.Text = .Item(clBLL_werknemers.GetWerknemers.MobielColumn.ColumnName, .CurrentCellAddress.Y).Value.ToString

            Dim vastindienst As DateTime
            Try
                'indien 1900 -> betekend 0 waarde
                '0 waarde in datumveld = 1/1/1900
                vastindienst = .Item(clBLL_werknemers.GetWerknemers.DatumVastIndienstColumn.ColumnName, .CurrentCellAddress.Y).Value

            Catch ex As Exception
                vastindienst = "01/01/1900"
            End Try

            If Year(vastindienst) = "1900" Then
                fraVastIndienst.Enabled = False
                chkSpaarplan.Checked = False
            Else
                fraVastIndienst.Enabled = True
                dteVastInDienst.Value = vastindienst

                Dim spaarplan As Boolean = False
                Try
                    spaarplan = .Item(clBLL_werknemers.GetWerknemers.SpaarplanColumn.ColumnName, .CurrentCellAddress.Y).Value
                Catch ex As Exception

                End Try


                If spaarplan Then
                    fraSpaarplan.Enabled = False
                    chkSpaarplan.Checked = True
                    dteGeboorte.Value = .Item(clBLL_werknemers.GetWerknemers.GeboortedatumColumn.ColumnName, .CurrentCellAddress.Y).Value
                    dteInstapSpaarplan.Value = .Item(clBLL_werknemers.GetWerknemers.DatumSpaarplanColumn.ColumnName, .CurrentCellAddress.Y).Value
                Else
                    fraSpaarplan.Enabled = True
                    chkSpaarplan.Checked = False
                End If
            End If
        End With
        ReadInput()
        LoonboekCheck = False

        txtLoonboek.Enabled = False
        err.Clear()
        menubar.Update()


    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        edit = False
        menubar.Load()
        ReadInput()

        If clsWerknemers.VALIDATE Then

            With clsWerknemers
                clBLL_werknemers.UpdateWerknemerStoredProc(.loonboek, .naam, .voornaam, .DatumGeboorte, .adres, _
                .postcode, .gemeente, .land, .telefoon, .mobiel, .email, .DatumVastInDienst, .spaarplan, _
                .DatumSpaarplan, .category.Value, .Werknemerstype.Value, .Id)
            End With
        End If

        WNTYPE = tabEmployees.SelectedTab.Tag
        RefreshGrid()
        clear()
        txtLoonboek.Enabled = True
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click

        GetSelectedRows()

        Dim id As Integer
        For dr As Integer = 0 To rijen.Count - 1
            id = rijen(dr)
            Try
                clBLL_werknemers.deleteWerknemers(id)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        Next

        WNTYPE = tabEmployees.SelectedTab.Tag
        RefreshGrid()
        clear()
    End Sub


    Private Sub txtNaam_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNaam.TextChanged
        ValidateFields()
    End Sub

    Private Sub txtVoornaam_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVoornaam.TextChanged
        ValidateFields()
    End Sub

    Private Sub cmbCategory_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCategory.SelectedValueChanged
        ValidateFields()
    End Sub

    Function ValidateFields() As Boolean

        Dim naam As String = Trim(txtNaam.Text)
        Dim vnaam As String = Trim(txtVoornaam.Text)

        Try
            If edit Then
                menubar.Update()
            Else
                If Not String.IsNullOrEmpty(naam) And Not String.IsNullOrEmpty(vnaam) And cmbCategory.SelectedValue > 0 And cmbWNType.SelectedValue > 0 Then
                    menubar.Add()
                Else
                    menubar.Load()
                End If
            End If


        Catch ex As InvalidCastException
            menubar.Load()
        End Try


    End Function


    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        _frmReports.Reportnaam = "werknemers.rpt"
        _frmReports.ShowDialog()
    End Sub


    Private Sub ButtonConverteerNaarNietSpaarplanner_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConverteerNaarNietSpaarplanner.Click
        Dim f As New frmNietSpaarplannerOvergang
        f.StartPosition = FormStartPosition.CenterScreen
        f.ShowDialog()

    End Sub
End Class