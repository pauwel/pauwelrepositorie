<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWerkNemers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWerkNemers))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtNaam = New System.Windows.Forms.TextBox
        Me.txtVoornaam = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteGeboorte = New System.Windows.Forms.DateTimePicker
        Me.Label4 = New System.Windows.Forms.Label
        Me.fraAddress = New System.Windows.Forms.GroupBox
        Me.cmbLand = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.txtMobiel = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtEMAIL = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtTelefoon = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtGemeente = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtPostcode = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtAddress = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmbCategory = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbWNType = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.fraVastIndienst = New System.Windows.Forms.GroupBox
        Me.dteVastInDienst = New System.Windows.Forms.DateTimePicker
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtLoonboek = New System.Windows.Forms.MaskedTextBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripWerknemers = New System.Windows.Forms.ToolStrip
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.tabEmployees = New System.Windows.Forms.TabControl
        Me.cmdAdd = New System.Windows.Forms.ToolStripButton
        Me.cmdEdit = New System.Windows.Forms.ToolStripButton
        Me.cmdUpdate = New System.Windows.Forms.ToolStripButton
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.cmdEmployees = New System.Windows.Forms.ToolStripButton
        Me.cmdCategory = New System.Windows.Forms.ToolStripButton
        Me.cmdLanden = New System.Windows.Forms.ToolStripButton
        Me.cmdReport = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.ButtonConverteerNaarNietSpaarplanner = New System.Windows.Forms.ToolStripButton
        Me.fraSpaarplan = New System.Windows.Forms.GroupBox
        Me.dteInstapSpaarplan = New System.Windows.Forms.DateTimePicker
        Me.Label14 = New System.Windows.Forms.Label
        Me.chkSpaarplan = New System.Windows.Forms.CheckBox
        Me.fraAddress.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.fraVastIndienst.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.ToolStripWerknemers.SuspendLayout()
        Me.fraSpaarplan.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Loonboek"
        '
        'txtNaam
        '
        Me.txtNaam.Location = New System.Drawing.Point(6, 26)
        Me.txtNaam.MaxLength = 25
        Me.txtNaam.Name = "txtNaam"
        Me.txtNaam.Size = New System.Drawing.Size(159, 20)
        Me.txtNaam.TabIndex = 0
        '
        'txtVoornaam
        '
        Me.txtVoornaam.Location = New System.Drawing.Point(171, 26)
        Me.txtVoornaam.MaxLength = 20
        Me.txtVoornaam.Name = "txtVoornaam"
        Me.txtVoornaam.Size = New System.Drawing.Size(159, 20)
        Me.txtVoornaam.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Naam"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(168, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Voornaam"
        '
        'dteGeboorte
        '
        Me.dteGeboorte.Enabled = False
        Me.dteGeboorte.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteGeboorte.Location = New System.Drawing.Point(10, 79)
        Me.dteGeboorte.Name = "dteGeboorte"
        Me.dteGeboorte.Size = New System.Drawing.Size(99, 20)
        Me.dteGeboorte.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Geboortedatum"
        '
        'fraAddress
        '
        Me.fraAddress.Controls.Add(Me.cmbLand)
        Me.fraAddress.Controls.Add(Me.txtMobiel)
        Me.fraAddress.Controls.Add(Me.Label10)
        Me.fraAddress.Controls.Add(Me.txtEMAIL)
        Me.fraAddress.Controls.Add(Me.Label12)
        Me.fraAddress.Controls.Add(Me.txtTelefoon)
        Me.fraAddress.Controls.Add(Me.Label9)
        Me.fraAddress.Controls.Add(Me.Label8)
        Me.fraAddress.Controls.Add(Me.txtGemeente)
        Me.fraAddress.Controls.Add(Me.Label7)
        Me.fraAddress.Controls.Add(Me.txtPostcode)
        Me.fraAddress.Controls.Add(Me.Label6)
        Me.fraAddress.Controls.Add(Me.txtAddress)
        Me.fraAddress.Controls.Add(Me.Label5)
        Me.fraAddress.Location = New System.Drawing.Point(12, 110)
        Me.fraAddress.Name = "fraAddress"
        Me.fraAddress.Size = New System.Drawing.Size(644, 139)
        Me.fraAddress.TabIndex = 2
        Me.fraAddress.TabStop = False
        '
        'cmbLand
        '
        Me.cmbLand.DisplayMember = "Text"
        Me.cmbLand.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbLand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLand.FormattingEnabled = True
        Me.cmbLand.ItemHeight = 14
        Me.cmbLand.Location = New System.Drawing.Point(337, 71)
        Me.cmbLand.Name = "cmbLand"
        Me.cmbLand.Size = New System.Drawing.Size(129, 20)
        Me.cmbLand.TabIndex = 26
        '
        'txtMobiel
        '
        Me.txtMobiel.Location = New System.Drawing.Point(406, 113)
        Me.txtMobiel.MaxLength = 20
        Me.txtMobiel.Name = "txtMobiel"
        Me.txtMobiel.Size = New System.Drawing.Size(159, 20)
        Me.txtMobiel.TabIndex = 6
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(403, 96)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Mobiel"
        '
        'txtEMAIL
        '
        Me.txtEMAIL.Location = New System.Drawing.Point(7, 113)
        Me.txtEMAIL.MaxLength = 25
        Me.txtEMAIL.Name = "txtEMAIL"
        Me.txtEMAIL.Size = New System.Drawing.Size(228, 20)
        Me.txtEMAIL.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(4, 96)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(32, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Email"
        '
        'txtTelefoon
        '
        Me.txtTelefoon.Location = New System.Drawing.Point(241, 113)
        Me.txtTelefoon.MaxLength = 20
        Me.txtTelefoon.Name = "txtTelefoon"
        Me.txtTelefoon.Size = New System.Drawing.Size(159, 20)
        Me.txtTelefoon.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(238, 96)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Telefoon"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(332, 55)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(31, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Land"
        '
        'txtGemeente
        '
        Me.txtGemeente.Location = New System.Drawing.Point(101, 72)
        Me.txtGemeente.MaxLength = 25
        Me.txtGemeente.Name = "txtGemeente"
        Me.txtGemeente.Size = New System.Drawing.Size(228, 20)
        Me.txtGemeente.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(98, 55)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Gemeente"
        '
        'txtPostcode
        '
        Me.txtPostcode.Location = New System.Drawing.Point(7, 72)
        Me.txtPostcode.MaxLength = 10
        Me.txtPostcode.Name = "txtPostcode"
        Me.txtPostcode.Size = New System.Drawing.Size(90, 20)
        Me.txtPostcode.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(4, 55)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Postcode"
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(6, 28)
        Me.txtAddress.MaxLength = 50
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(460, 20)
        Me.txtAddress.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Adres"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbCategory)
        Me.GroupBox1.Controls.Add(Me.cmbWNType)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtNaam)
        Me.GroupBox1.Controls.Add(Me.txtVoornaam)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(87, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(569, 54)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'cmbCategory
        '
        Me.cmbCategory.DisplayMember = "Text"
        Me.cmbCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCategory.FormattingEnabled = True
        Me.cmbCategory.ItemHeight = 14
        Me.cmbCategory.Location = New System.Drawing.Point(447, 25)
        Me.cmbCategory.Name = "cmbCategory"
        Me.cmbCategory.Size = New System.Drawing.Size(104, 20)
        Me.cmbCategory.TabIndex = 25
        '
        'cmbWNType
        '
        Me.cmbWNType.DisplayMember = "Text"
        Me.cmbWNType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbWNType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWNType.FormattingEnabled = True
        Me.cmbWNType.ItemHeight = 14
        Me.cmbWNType.Location = New System.Drawing.Point(337, 25)
        Me.cmbWNType.Name = "cmbWNType"
        Me.cmbWNType.Size = New System.Drawing.Size(104, 20)
        Me.cmbWNType.TabIndex = 24
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(444, 10)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(49, 13)
        Me.Label15.TabIndex = 24
        Me.Label15.Text = "Category"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(334, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(86, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "WerknemerType"
        '
        'fraVastIndienst
        '
        Me.fraVastIndienst.Controls.Add(Me.fraSpaarplan)
        Me.fraVastIndienst.Controls.Add(Me.dteVastInDienst)
        Me.fraVastIndienst.Controls.Add(Me.dteGeboorte)
        Me.fraVastIndienst.Controls.Add(Me.Label13)
        Me.fraVastIndienst.Controls.Add(Me.Label4)
        Me.fraVastIndienst.Enabled = False
        Me.fraVastIndienst.Location = New System.Drawing.Point(666, 56)
        Me.fraVastIndienst.Name = "fraVastIndienst"
        Me.fraVastIndienst.Size = New System.Drawing.Size(137, 193)
        Me.fraVastIndienst.TabIndex = 3
        Me.fraVastIndienst.TabStop = False
        '
        'dteVastInDienst
        '
        Me.dteVastInDienst.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteVastInDienst.Location = New System.Drawing.Point(6, 28)
        Me.dteVastInDienst.Name = "dteVastInDienst"
        Me.dteVastInDienst.Size = New System.Drawing.Size(99, 20)
        Me.dteVastInDienst.TabIndex = 0
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 13)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 13)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Vast in dienst"
        '
        'txtLoonboek
        '
        Me.txtLoonboek.Location = New System.Drawing.Point(12, 82)
        Me.txtLoonboek.Mask = "00000/00"
        Me.txtLoonboek.Name = "txtLoonboek"
        Me.txtLoonboek.Size = New System.Drawing.Size(66, 20)
        Me.txtLoonboek.TabIndex = 0
        Me.txtLoonboek.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdEmployees, Me.ToolStripSeparator4, Me.cmdCategory, Me.ToolStripSeparator5, Me.cmdLanden, Me.ToolStripSeparator6, Me.cmdReport, Me.ToolStripSeparator7, Me.ButtonConverteerNaarNietSpaarplanner})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(846, 25)
        Me.ToolStrip1.TabIndex = 14
        Me.ToolStrip1.Text = "Category"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripWerknemers
        '
        Me.ToolStripWerknemers.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAdd, Me.ToolStripSeparator1, Me.cmdEdit, Me.ToolStripSeparator2, Me.cmdUpdate, Me.ToolStripSeparator3, Me.cmdDelete})
        Me.ToolStripWerknemers.Location = New System.Drawing.Point(0, 25)
        Me.ToolStripWerknemers.Name = "ToolStripWerknemers"
        Me.ToolStripWerknemers.Size = New System.Drawing.Size(846, 25)
        Me.ToolStripWerknemers.TabIndex = 15
        Me.ToolStripWerknemers.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'tabEmployees
        '
        Me.tabEmployees.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabEmployees.Location = New System.Drawing.Point(0, 249)
        Me.tabEmployees.Name = "tabEmployees"
        Me.tabEmployees.SelectedIndex = 0
        Me.tabEmployees.Size = New System.Drawing.Size(846, 392)
        Me.tabEmployees.TabIndex = 16
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = Global.IMT_Timekeeping.My.Resources.Resources.AddTableHS
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(49, 22)
        Me.cmdAdd.Text = "Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdEdit
        '
        Me.cmdEdit.Image = Global.IMT_Timekeeping.My.Resources.Resources.EditInformationHS
        Me.cmdEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(47, 22)
        Me.cmdEdit.Text = "Edit"
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdUpdate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(65, 22)
        Me.cmdUpdate.Text = "Update"
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        '
        'cmdEmployees
        '
        Me.cmdEmployees.Image = Global.IMT_Timekeeping.My.Resources.Resources.CascadeWindowsHS
        Me.cmdEmployees.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdEmployees.Name = "cmdEmployees"
        Me.cmdEmployees.Size = New System.Drawing.Size(113, 22)
        Me.cmdEmployees.Text = "Employee Types"
        '
        'cmdCategory
        '
        Me.cmdCategory.Image = Global.IMT_Timekeeping.My.Resources.Resources.CascadeWindowsHS
        Me.cmdCategory.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdCategory.Name = "cmdCategory"
        Me.cmdCategory.Size = New System.Drawing.Size(83, 22)
        Me.cmdCategory.Text = "Categories"
        '
        'cmdLanden
        '
        Me.cmdLanden.Image = Global.IMT_Timekeeping.My.Resources.Resources.CascadeWindowsHS
        Me.cmdLanden.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdLanden.Name = "cmdLanden"
        Me.cmdLanden.Size = New System.Drawing.Size(66, 22)
        Me.cmdLanden.Text = "Landen"
        '
        'cmdReport
        '
        Me.cmdReport.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdReport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(62, 22)
        Me.cmdReport.Text = "Report"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'ButtonConverteerNaarNietSpaarplanner
        '
        Me.ButtonConverteerNaarNietSpaarplanner.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ButtonConverteerNaarNietSpaarplanner.Image = CType(resources.GetObject("ButtonConverteerNaarNietSpaarplanner.Image"), System.Drawing.Image)
        Me.ButtonConverteerNaarNietSpaarplanner.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ButtonConverteerNaarNietSpaarplanner.Name = "ButtonConverteerNaarNietSpaarplanner"
        Me.ButtonConverteerNaarNietSpaarplanner.Size = New System.Drawing.Size(194, 22)
        Me.ButtonConverteerNaarNietSpaarplanner.Text = "Converteer naar Niet-Spaarplanner"
        '
        'fraSpaarplan
        '
        Me.fraSpaarplan.Controls.Add(Me.dteInstapSpaarplan)
        Me.fraSpaarplan.Controls.Add(Me.Label14)
        Me.fraSpaarplan.Controls.Add(Me.chkSpaarplan)
        Me.fraSpaarplan.Location = New System.Drawing.Point(10, 106)
        Me.fraSpaarplan.Name = "fraSpaarplan"
        Me.fraSpaarplan.Size = New System.Drawing.Size(121, 81)
        Me.fraSpaarplan.TabIndex = 26
        Me.fraSpaarplan.TabStop = False
        '
        'dteInstapSpaarplan
        '
        Me.dteInstapSpaarplan.Enabled = False
        Me.dteInstapSpaarplan.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteInstapSpaarplan.Location = New System.Drawing.Point(4, 50)
        Me.dteInstapSpaarplan.Name = "dteInstapSpaarplan"
        Me.dteInstapSpaarplan.Size = New System.Drawing.Size(99, 20)
        Me.dteInstapSpaarplan.TabIndex = 27
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(1, 34)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(87, 13)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "Instap Spaarplan"
        '
        'chkSpaarplan
        '
        Me.chkSpaarplan.AutoSize = True
        Me.chkSpaarplan.Location = New System.Drawing.Point(4, 9)
        Me.chkSpaarplan.Name = "chkSpaarplan"
        Me.chkSpaarplan.Size = New System.Drawing.Size(74, 17)
        Me.chkSpaarplan.TabIndex = 26
        Me.chkSpaarplan.Text = "Spaarplan"
        Me.chkSpaarplan.UseVisualStyleBackColor = True
        '
        'frmWerkNemers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 640)
        Me.Controls.Add(Me.tabEmployees)
        Me.Controls.Add(Me.ToolStripWerknemers)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.txtLoonboek)
        Me.Controls.Add(Me.fraVastIndienst)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.fraAddress)
        Me.Controls.Add(Me.Label1)
        Me.MinimumSize = New System.Drawing.Size(809, 676)
        Me.Name = "frmWerkNemers"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "WerkNemers"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.fraAddress.ResumeLayout(False)
        Me.fraAddress.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.fraVastIndienst.ResumeLayout(False)
        Me.fraVastIndienst.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ToolStripWerknemers.ResumeLayout(False)
        Me.ToolStripWerknemers.PerformLayout()
        Me.fraSpaarplan.ResumeLayout(False)
        Me.fraSpaarplan.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNaam As System.Windows.Forms.TextBox
    Friend WithEvents txtVoornaam As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteGeboorte As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents fraAddress As System.Windows.Forms.GroupBox
    Friend WithEvents txtGemeente As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPostcode As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMobiel As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtTelefoon As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtEMAIL As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents fraVastIndienst As System.Windows.Forms.GroupBox
    Friend WithEvents dteVastInDienst As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtLoonboek As System.Windows.Forms.MaskedTextBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdEmployees As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripWerknemers As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdUpdate As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdCategory As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdLanden As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabEmployees As System.Windows.Forms.TabControl
    Friend WithEvents cmbLand As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbCategory As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbWNType As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdReport As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ButtonConverteerNaarNietSpaarplanner As System.Windows.Forms.ToolStripButton
    Friend WithEvents fraSpaarplan As System.Windows.Forms.GroupBox
    Friend WithEvents dteInstapSpaarplan As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents chkSpaarplan As System.Windows.Forms.CheckBox
End Class
