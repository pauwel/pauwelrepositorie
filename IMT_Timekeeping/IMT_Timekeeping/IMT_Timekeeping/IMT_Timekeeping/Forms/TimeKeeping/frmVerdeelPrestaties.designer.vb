<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVerdeelPrestaties
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtePrestatieDatum = New System.Windows.Forms.DateTimePicker
        Me.txtUren = New NumericBox.NumericBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.dgvOrg = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.dgvTo = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.cmdForward = New System.Windows.Forms.Button
        Me.chkMachines = New System.Windows.Forms.CheckBox
        Me.cmbVoyage = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbVoyageTo = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbShift = New DevComponents.DotNetBar.Controls.ComboBoxEx
        CType(Me.dgvOrg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(224, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Shift"
        '
        'dtePrestatieDatum
        '
        Me.dtePrestatieDatum.Location = New System.Drawing.Point(22, 42)
        Me.dtePrestatieDatum.Name = "dtePrestatieDatum"
        Me.dtePrestatieDatum.Size = New System.Drawing.Size(200, 20)
        Me.dtePrestatieDatum.TabIndex = 0
        '
        'txtUren
        '
        Me.txtUren.BackColor = System.Drawing.SystemColors.Window
        Me.txtUren.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtUren.Location = New System.Drawing.Point(383, 85)
        Me.txtUren.Name = "txtUren"
        Me.txtUren.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtUren.Size = New System.Drawing.Size(60, 20)
        Me.txtUren.TabIndex = 5
        Me.txtUren.Text = "0,00"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(320, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Voyage"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(380, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Uren"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(445, 67)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Voyage"
        '
        'dgvOrg
        '
        Me.dgvOrg.AllowUserToAddRows = False
        Me.dgvOrg.AllowUserToDeleteRows = False
        Me.dgvOrg.AllowUserToResizeRows = False
        Me.dgvOrg.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvOrg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvOrg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOrg.Location = New System.Drawing.Point(22, 110)
        Me.dgvOrg.Name = "dgvOrg"
        Me.dgvOrg.ReadOnly = True
        Me.dgvOrg.Size = New System.Drawing.Size(420, 538)
        Me.dgvOrg.TabIndex = 12
        '
        'dgvTo
        '
        Me.dgvTo.AllowUserToAddRows = False
        Me.dgvTo.AllowUserToDeleteRows = False
        Me.dgvTo.AllowUserToResizeRows = False
        Me.dgvTo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvTo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvTo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTo.Location = New System.Drawing.Point(447, 110)
        Me.dgvTo.Name = "dgvTo"
        Me.dgvTo.ReadOnly = True
        Me.dgvTo.Size = New System.Drawing.Size(420, 538)
        Me.dgvTo.TabIndex = 13
        '
        'cmdForward
        '
        Me.cmdForward.Location = New System.Drawing.Point(367, 42)
        Me.cmdForward.Name = "cmdForward"
        Me.cmdForward.Size = New System.Drawing.Size(75, 23)
        Me.cmdForward.TabIndex = 14
        Me.cmdForward.Text = ">"
        Me.cmdForward.UseVisualStyleBackColor = True
        '
        'chkMachines
        '
        Me.chkMachines.AutoSize = True
        Me.chkMachines.Location = New System.Drawing.Point(22, 83)
        Me.chkMachines.Name = "chkMachines"
        Me.chkMachines.Size = New System.Drawing.Size(103, 17)
        Me.chkMachines.TabIndex = 17
        Me.chkMachines.Text = "Alleen machines"
        Me.chkMachines.UseVisualStyleBackColor = True
        '
        'cmbVoyage
        '
        Me.cmbVoyage.DisplayMember = "Text"
        Me.cmbVoyage.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbVoyage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVoyage.FormattingEnabled = True
        Me.cmbVoyage.ItemHeight = 14
        Me.cmbVoyage.Location = New System.Drawing.Point(240, 85)
        Me.cmbVoyage.Name = "cmbVoyage"
        Me.cmbVoyage.Size = New System.Drawing.Size(121, 20)
        Me.cmbVoyage.TabIndex = 18
        '
        'cmbVoyageTo
        '
        Me.cmbVoyageTo.DisplayMember = "Text"
        Me.cmbVoyageTo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbVoyageTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVoyageTo.FormattingEnabled = True
        Me.cmbVoyageTo.ItemHeight = 14
        Me.cmbVoyageTo.Location = New System.Drawing.Point(448, 85)
        Me.cmbVoyageTo.Name = "cmbVoyageTo"
        Me.cmbVoyageTo.Size = New System.Drawing.Size(121, 20)
        Me.cmbVoyageTo.TabIndex = 19
        '
        'cmbShift
        '
        Me.cmbShift.DisplayMember = "Text"
        Me.cmbShift.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShift.FormattingEnabled = True
        Me.cmbShift.ItemHeight = 14
        Me.cmbShift.Location = New System.Drawing.Point(227, 42)
        Me.cmbShift.Name = "cmbShift"
        Me.cmbShift.Size = New System.Drawing.Size(121, 20)
        Me.cmbShift.TabIndex = 20
        '
        'frmVerdeelPrestaties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(917, 660)
        Me.Controls.Add(Me.cmbShift)
        Me.Controls.Add(Me.cmbVoyageTo)
        Me.Controls.Add(Me.cmbVoyage)
        Me.Controls.Add(Me.chkMachines)
        Me.Controls.Add(Me.dtePrestatieDatum)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdForward)
        Me.Controls.Add(Me.dgvTo)
        Me.Controls.Add(Me.dgvOrg)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtUren)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmVerdeelPrestaties"
        Me.Text = "Kostenanalyse"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvOrg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtePrestatieDatum As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtUren As NumericBox.NumericBox
    Friend WithEvents dgvOrg As System.Windows.Forms.DataGridView
    Friend WithEvents dgvTo As System.Windows.Forms.DataGridView
    Friend WithEvents cmdForward As System.Windows.Forms.Button
    Friend WithEvents chkMachines As System.Windows.Forms.CheckBox
    Friend WithEvents cmbVoyage As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbVoyageTo As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbShift As DevComponents.DotNetBar.Controls.ComboBoxEx
End Class
