Option Compare Text
Imports BLL.Timekeeping


Public Class frmTimekeepingDetail
    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean

    ''' <summary>
    ''' Wachtregel object
    ''' </summary>
    ''' <remarks></remarks>
    Private WR As BLL.Wachtregel

    Private supplement As BLL.Supplement

    Private prestatie As DataRow

    Dim mail As New BLL.MailMessage
    Dim afwezig As New BLL.UpdateAfwezig
    Dim LPF As BLL.LoonPerFuncties = New BLL.LoonPerFuncties


    Private status As Boolean = False

    Public Sub New(ByVal id As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.initFormFields()

        Me.loadFromWachtbestand(id)

    End Sub

    

    Private Sub frmTimekeepingDetail_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If status = True Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If


        With updater
            .wachtid = WR.WachtID
            .generatemail = True
            .update(Me.WR)
        End With



    End Sub
    Private Sub frmTimekeepingDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' supplementgrid hier refreshen bij laden, anders worden de kleuren niet aangepast.  
        Me.refreshSupplementenGrid()
        status = False

        'ingeval van verlof, niet toelaten dat er supplementen worden toegevoegd
        If cmbVoyages.Text = "VERLOF" Then
            GroupBoxLoonDetails.Enabled = False
            btnEdit.Enabled = False
            btnRefresh.Enabled = False
            btnSave.Enabled = False
            btnOverboeken.Enabled = False
            
        End If

        updater.generatemail = False
    End Sub
    ''' <summary>
    ''' Hier worden de dropdowns en eventuele andere velden opgevuld van initi�le waarden
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub initFormFields()

        Try
            'Alle voyages ophalen
            Dim v As New BLL.GetVoyages
            cmbVoyages.DataSource = v.Getdata
            cmbVoyages.DisplayMember = "voyagenbr"
            cmbVoyages.ValueMember = "voyageid"
        Catch ex As Exception

        End Try

        Try
            'Shiften ophalen
            Dim TIP As New BLL.GetShiftenToonInPLanning

            cmbShiften.DataSource = TIP.GetShiften
            cmbShiften.DisplayMember = "omschrijving"
            cmbShiften.ValueMember = "shiftid"
        Catch ex As Exception
        End Try

        Dim LT As New BLL.LoonType
        With Me.cmbSupplement
            .DataSource = LT.getAllowedUserInput
            .DisplayMember = "Omschrijving"
            .ValueMember = "loontypeid"
            .SelectedIndex = -1
            .Text = Nothing
        End With
    End Sub

    ''' <summary>
    ''' Combobox met functies opvullen.  De mogelijke functies zijn afhankelijk van de werknemer
    ''' </summary>
    ''' <param name="werknemerID">WerknemerID - ID van werknemer voor wie de functies worden opgehaald</param>
    ''' <remarks></remarks>
    Private Sub loadCmbFuncties(ByVal werknemerID As Integer)
        Try
            Dim f As New BLL.GetFunctieByWerknemer
            'alle functies ophalen waarin de werknemer mag werken
            'deze combobox mag enkel opgevuld worden met prestatie af

            cmbFunctie.DataSource = f.Getdata(werknemerID)
            cmbFunctie.DisplayMember = "Functiecode"
            cmbFunctie.ValueMember = "functieid"
        Catch ex As SqlClient.SqlException
            Throw New Exception("Er heeft zich een database probleem voorgedaan.  De functies kunnen niet worden geladen", ex)
        End Try
    End Sub

    ''' <summary>
    ''' Gegevens vanuit wachtbestand inladen 
    ''' </summary>
    ''' <param name="id">WachtID</param>
    ''' <remarks></remarks>
    Public Sub loadFromWachtbestand(ByVal id As Integer)
        LockWindowUpdate(Me.Handle.ToInt64)
        Try
            Me.WR = New BLL.Wachtregel(id)
            Me.lblSaldo.Text = "saldo: � " & Math.Round(Me.WR.Saldo, 2)
            Me.refreshSupplementenGrid()
            Me.EditMode = False


            Me.loadCmbFuncties(Me.WR.WerknemerID)
            If Me.WR.Validated = True Then
                Me.btnValideer.Enabled = False
                Me.cmdReport.Enabled = True
                'Me.GroupBoxLoonDetails.Enabled = True
                'Me.GroupBoxInfo.Enabled = False
                'Me.btnEdit.Enabled = False
                Me.lblWBnummer.ForeColor = Nothing
                If Me.WR.isAfwezig Then
                    Me.GroupBoxLoonDetails.Enabled = False
                End If

                'controle of al uitbetaald naar cepa (datumUit is not null) 
                If Me.WR.DatumUit.HasValue Then
                    Me.GroupBoxInfo.Enabled = False
                    Me.GroupBoxLoonDetails.Enabled = False
                    Me.btnEdit.Enabled = False
                    Me.btnOverboeken.Enabled = False
                Else
                    Me.GroupBoxLoonDetails.Enabled = True
                    Me.btnEdit.Enabled = True
                    Me.btnOverboeken.Enabled = True
                End If
            Else
                Me.btnValideer.Enabled = True
                Me.GroupBoxLoonDetails.Enabled = False
                Me.GroupBoxInfo.Enabled = True
                Me.btnEdit.Enabled = True
                Me.lblWBnummer.ForeColor = Color.Red
                Me.cmdReport.Enabled = False
                Me.btnOverboeken.Enabled = False
            End If
            Me.cmbVoyages.SelectedValue = Me.WR.VoyageID
            Me.cmbFunctie.SelectedValue = Me.WR.FunctieID
            Me.cmbShiften.SelectedValue = Me.WR.gewerkteShift

            Me.txtUren.Text = Me.WR.GewerkteTijd
            Me.dteDetail.Value = Me.WR.Datum

            Me.chkIsWeekend.Checked = Me.WR.Is_weekend
            Me.chkIsFeestdag.Checked = Me.WR.Is_feestdag


            'nakijken of werknemer van deze prestatie een spaarplanner is
            Dim spaarplanner As Boolean = Me.checkSpaarplanner(Me.WR.WerknemerID)
            Me.chkSpaarplanner.Checked = spaarplanner

            If spaarplanner = True Then
                Me.chkWordtUitbetaald.Enabled = False
                Me.btnOverboeken.Enabled = False
            End If
            Me.chkWordtUitbetaald.Checked = Me.WR.WordtUitbetaald
            Me.chkWordtUitbetaald.Enabled = False
            'supplement object aanmaken 
            Me.supplement = New BLL.Supplement(Me.WR)
            'supplement velden enablen
            Me.cmbSupplement.Enabled = True
            Me.txtCommentaar.Enabled = True

            Me.GroupBoxInfo.Enabled = False
            'Me.GroupBoxLoonDetails.Enabled = True

            Me.lblNaam.Text = Me.WR.Naam & " " & Me.WR.Voornaam

            Me.chkAfwezig.Checked = Me.WR.isAfwezig
            Me.txtAfwezig.Text = Me.WR.redenAfwezigheid

            Me.lblWBnummer.Text = "Loonbriefje: " & Me.WR.Wbbrief

            Me.refreshSupplementenGrid()
        Catch ex As Exception
            MsgBox("De gegevens kunnen niet worden opgevraagd.  Gelieve uw systeembeheerder te contacteren" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation, "Fout bij laden - " & APPLICATIENAAM)

            Me.Close()
        End Try
        LockWindowUpdate(0)
    End Sub
    ''' <summary>
    ''' Controleren of een werknemer deelneemt aan het spaarplan
    ''' </summary>
    ''' <param name="werknemerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function checkSpaarplanner(ByVal werknemerID As Integer)
        Dim WN As BLL.Werknemers = New BLL.Werknemers
        'nakijken of werknemer van deze prestatie een spaarplanner is
        Dim spaarplanner As Boolean = WN.CheckSpaarplan(Me.WR.WerknemerID)
        Return spaarplanner
    End Function

    ''' <summary>
    ''' De prestatiegegevens valideren
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub valideer()
        Me.doorboeken()
        status = True
    End Sub

    ''' <summary>
    ''' De prestatiegegevens valideren en doorboeken naar het wachtbestand.  
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub doorboeken()

        Me.Cursor = Cursors.WaitCursor

        Try
            Dim validatie As BLL.WachtregelValidatie = New BLL.WachtregelValidatie(Me.WR)
            validatie.valideerWachtRegel()


        Catch avex As Exceptions.AlreadyValidatedException
            Dim response = MsgBox("Deze prestatie werd reeds gevalideerd" & ControlChars.NewLine & ControlChars.NewLine & "Wenst u de gevalideerde gegevens te bekijken?", MsgBoxStyle.YesNo, APPLICATIENAAM)
            If response = MsgBoxResult.Yes Then
                Me.loadFromWachtbestand(Me.WR.WachtID)
            Else
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("Er heeft zich een onbekende fout voorgedaan." & ControlChars.NewLine & ex.Message, MsgBoxStyle.Critical, APPLICATIENAAM & " - Onbekende fout")
        Finally
            Me.loadFromWachtbestand(Me.WR.WachtID)
        End Try


        Me.Cursor = Cursors.Default


    End Sub

    Private Function setAfwezig()

        If String.IsNullOrEmpty(Trim(Me.txtAfwezig.Text)) Then
            MsgBox("Reden van afwezigheid invullen aub!!", MsgBoxStyle.Exclamation)
            Return False
        Else
            afwezig.SetAfwezigheid(True, Me.txtAfwezig.Text, True, False, Me.prestatie("PrestatieID"))
            'mail versturen om afwezigheid te melden  
            With mail
                .newFeestdag = chkIsFeestdag.Checked
                '.newWeekdag = chkIsWeekdag.Checked

                .NewAfwezig = chkAfwezig.Checked
                .newReden = txtAfwezig.Text

                .newFunctie = cmbFunctie.Text
                .newShift = cmbShiften.Text
                .newVoyage = cmbVoyages.Text

                .Werknemer = Me.prestatie("voornaam") & " " _
                    & Me.prestatie("naam") & " " _
                    & "( " & Me.prestatie("loonboek") & " )"

                .Datum = Me.prestatie("datum")
            End With
            mail.sendmail()
        End If
        Return True
    End Function

    ''' <summary>
    ''' De datagridview met supplementen refreshen 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub refreshSupplementenGrid()
        If Me.WR.WachtID.HasValue Then
            Me.dgvSupplement.DataSource = Me.WR.Detailregels
        End If

        For Each c As DataGridViewColumn In dgvSupplement.Columns


            Select Case c.Name
                Case "Omschrijving"
                    dgvSupplement.Columns("omschrijving").DisplayIndex = 0
                    dgvSupplement.Columns("omschrijving").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                Case "Uren"
                    dgvSupplement.Columns("Uren").DisplayIndex = 1
                    With dgvSupplement.Columns("Uren")
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        .DefaultCellStyle.Format = "f2"
                        .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                    End With

                Case "bedrag"
                    dgvSupplement.Columns("Bedrag").DisplayIndex = 2
                    With dgvSupplement.Columns("Bedrag")
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        .DefaultCellStyle.Format = "f2"
                        .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                    End With

                Case "CepaCode"
                    dgvSupplement.Columns("CepaCode").DisplayIndex = 3
                    dgvSupplement.Columns("CepaCode").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

                Case "Commentaar" : dgvSupplement.Columns("commentaar").DisplayIndex = 4
                Case "Datum" : dgvSupplement.Columns("Datum").DisplayIndex = 5
                Case Else
                    c.Visible = False
            End Select

        Next

        Try
            For Each r As DataGridViewRow In dgvSupplement.Rows

                Dim loonT As Int32 = dgvSupplement.Item("loontypeid", r.Index).Value.ToString
                Dim comment As String = dgvSupplement.Item("commentaar", r.Index).Value.ToString
                Dim bedragske As String = dgvSupplement.Item("Bedrag", r.Index).Value.ToString
                Dim reden As Integer = dgvSupplement.Item("RedenID", r.Index).Value.ToString
                ' If loonT < 30 And Not String.IsNullOrEmpty(comment) Then

                Select Case reden
                    Case Register.LoonDetailReden.User
                        dgvSupplement.Rows(r.Index).DefaultCellStyle.ForeColor = Color.Blue
                        If loonT < 30 And Not String.IsNullOrEmpty(comment) Then
                            dgvSupplement.Rows(r.Index).DefaultCellStyle.BackColor = Color.LightGray
                        End If
                    Case Register.LoonDetailReden.Verlof
                        dgvSupplement.Rows(r.Index).DefaultCellStyle.ForeColor = Color.Green
                    Case Else
                        'niets wijzigen
                End Select

            Next
        Catch ex As ArgumentException
            'kolom loontype werd niet gevonden
        End Try
        Me.lblSaldo.Text = "saldo: � " & Math.Round(Me.WR.Saldo, 2)
    End Sub
    Private Sub cmbSupplement_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSupplement.SelectedValueChanged
        Try

       
            Me.supplement.Loontype = Me.cmbSupplement.SelectedValue

            If Me.supplement.CepaVerplicht Then
                txtCEPACode.Enabled = True
            Else
                txtCEPACode.Enabled = False
                txtCEPACode.Clear()
            End If

            If Me.supplement.UrenInvoer Then
                txtSupplementUren.Enabled = True
                txtSupplementBedrag.Enabled = True
            Else
                txtSupplementUren.Enabled = False
                txtSupplementUren.Text = 0
                txtSupplementBedrag.Enabled = True
            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub chkAfwezig_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAfwezig.CheckedChanged
        If chkAfwezig.Checked = True Then
            'NV.Afwezigheid = True
            txtAfwezig.Enabled = True
            txtAfwezig.Select()
        Else
            ' NV.Afwezigheid = False
            txtAfwezig.Clear()
            txtAfwezig.Enabled = False
        End If
    End Sub

    Private Sub btnValideer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValideer.Click
        Me.valideer()
    End Sub

    Private Sub btnValideerSluit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValideerSluit.Click
        Me.valideer()
        Me.Close()

    End Sub

    Private Sub addSupplement()
        Try
            Me.supplement.Uren = Me.txtSupplementUren.Text
            Me.supplement.Bedrag = Me.txtSupplementBedrag.Text
            Me.supplement.Comment = Me.txtCommentaar.Text
            Me.supplement.CepaCode = Me.txtCEPACode.Text

            Me.supplement.save()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
        Me.refreshSupplementenGrid()
        With Me.cmbSupplement
            .SelectedIndex = -1
            .Text = Nothing
        End With
        Me.txtCommentaar.Clear()
        Me.txtCEPACode.Clear()
        Me.txtSupplementBedrag.Text = 0
        Me.txtSupplementUren.Text = 0
        Me.supplement = New BLL.Supplement(Me.WR)
    End Sub


    Private Sub txtSupplementUren_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSupplementUren.Leave
        Try
            Me.supplement.Uren = Me.txtSupplementUren.Text
            Dim bedrag As Decimal = Me.supplement.berekenBedrag(Me.supplement.Uren)
            Me.txtSupplementBedrag.Text = bedrag
        Catch ex As Exception
            MsgBox("Het bedrag kon niet berekend worden ", MsgBoxStyle.Exclamation, APPLICATIENAAM & " - Berekeningsfout")
            Me.txtSupplementBedrag.Enabled = True 'gebruiker kan zelf bedrag invullen 
        End Try

    End Sub

    Dim updater As BLL.WachtRegelWriter = New BLL.WachtRegelWriter

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click


        'As BLL.UpdateWachtregels = New BLL.UpdateWachtregels
        '       updater.UpdateWachtregel(me.WR.WachtID,me.cmbVoyages.SelectedValue,
        With updater
            .generatemail = False
            .wordtuitbetaald = Me.chkWordtUitbetaald.Checked
            .voyageid = Me.cmbVoyages.SelectedValue
            .functieid = Me.cmbFunctie.SelectedValue
            .gewerkteshiftid = Me.cmbShiften.SelectedValue
            .shiftid = .gewerkteshiftid 'gelijkstellen, kan door de updater overschreven worden 
            .isAfwezig = Me.chkAfwezig.Checked
            .redenAfwezigheid = Me.txtAfwezig.Text
            .wordtuitbetaald = Me.chkWordtUitbetaald.Checked
            .isfeestdag = Me.chkIsFeestdag.Checked
            .update(Me.WR)

        End With
        status = True
        Me.loadFromWachtbestand(Me.WR.WachtID)

        updater.generatemail = True
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.loadFromWachtbestand(Me.WR.WachtID)
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Me.EditMode = Not Me.EditMode
    End Sub


    Private _editMode As Boolean
    Public Property EditMode() As Boolean
        Get
            Return _editMode
        End Get
        Set(ByVal value As Boolean)
            _editMode = value

            Me.btnSave.Enabled = value
            If Not Me.WR.Validated Then
                Me.btnValideer.Enabled = Not value
            End If

            Me.btnEdit.Checked = value
            'Me.cmdDelete.Enabled = value

            If Not Me.WR.Validated Then
                Me.GroupBoxInfo.Enabled = value
            End If

            If Me.checkSpaarplanner(Me.WR.WerknemerID) = False Then
                Me.chkWordtUitbetaald.Enabled = value
            End If
            If value = True Then
                Me.lblEdit.Text = "Edit mode ON"
                Me.lblEdit.ForeColor = Color.DarkGreen
            Else
                Me.lblEdit.Text = "Edit mode OFF"
                Me.lblEdit.ForeColor = Color.Black
            End If

        End Set
    End Property

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click

        _frmReports.Reportnaam = "WBBrief.rpt"
        _frmReports.Wachtid = Me.WR.WachtID.Value
        _frmReports.ShowDialog()
    End Sub

    Private Sub cmdBewaarDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBewaarDetail.Click
        Dim totaalBestaandSupplementBedrag As Decimal = 0

        Dim lijstje(Me.dgvSupplement.RowCount + 1) As String
        Dim nieuwSupplementNaam As String
        nieuwSupplementNaam = Me.cmbSupplement.Text

        For index As Integer = 0 To Me.dgvSupplement.RowCount - 1
            If (Me.cmbSupplement.Text.Equals(Me.dgvSupplement.Rows(index).Cells(0).Value.ToString)) Then
                totaalBestaandSupplementBedrag += Decimal.Parse(Me.dgvSupplement.Rows(index).Cells(4).Value)
            End If
        Next

        totaalBestaandSupplementBedrag = totaalBestaandSupplementBedrag + Decimal.Parse(txtSupplementBedrag.Text)

        If totaalBestaandSupplementBedrag > 0 Then

            updater.generatemail = False
            updater.BewaarGegevens = True

            BewaarSupplement()
        Else
            MsgBox("Er is niet voldoende krediet aanwezig voor dit supplement.", MsgBoxStyle.Information, "Fout Supplement")
        End If
        


    End Sub
    Private Sub txtCommentaar_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCommentaar.KeyUp
        If e.KeyCode = Keys.Enter Then
            updater.BewaarGegevens = True
            BewaarSupplement()
        End If

    End Sub

    Private Sub BewaarSupplement()

        Me.addSupplement()


    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click

        Dim answer As Integer = MsgBox("deze actie verwijdert de gegevens uit het wachtbestand en de planning!!" & vbNewLine & "Wens je door te gaan?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation + MsgBoxStyle.DefaultButton2)

        If answer = vbYes Then

            If MsgBoxResult.Yes Then

                'controle op reeds uitbetaald
                Dim v As New BLL.CheckAantalGevalideerdeGewerkteShiften
                If v.CheckReedsUitbetaald(WR.WachtID.Value) = 0 Then
                    Dim d As New BLL.DeleteValidated
                    Dim ID As Integer = WR.WachtID.Value
                    d.DeleteValidated(ID)
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                    Me.Close()
                Else
                    MsgBox("Deze prestatie werd reeds uitbetaald en kan NIET verwijdert worden!", MsgBoxStyle.Critical)


                End If


            End If
        End If

    End Sub

    Private Sub btnOverboeken_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOverboeken.Click
        Dim overboek As New BLL.Overboeken(Me.WR)
        Try
            overboek.boeken()
            If Not overboek.NewWachtRegel Is Nothing Then
                Me.loadDetailForm(overboek.NewWachtRegel.WachtID)
                Me.refreshSupplementenGrid()
            Else
                MsgBox("Er werd geen nieuw loonbriefje aangemaakt", MsgBoxStyle.Information, APPLICATIENAAM)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Fout bij overboeken")
        End Try


    End Sub

    Private Sub loadDetailForm(ByVal id As Integer)
        Try
            Dim form As Form = New frmTimekeepingDetail(id)
            form.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


End Class