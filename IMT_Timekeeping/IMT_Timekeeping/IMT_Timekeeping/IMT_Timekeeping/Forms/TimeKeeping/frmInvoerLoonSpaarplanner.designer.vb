<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoerLoonSpaarplanner
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dteBetaling = New System.Windows.Forms.DateTimePicker
        Me.txtBedrag = New NumericBox.NumericBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.cmdAdd = New System.Windows.Forms.ToolStripButton
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblNaam = New DevComponents.DotNetBar.LabelX
        Me.cmbLoonboek = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbShift = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.dgvBetalingen = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.lblSaldo = New DevComponents.DotNetBar.Controls.ReflectionLabel
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgvBetalingen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dteBetaling
        '
        Me.dteBetaling.Location = New System.Drawing.Point(13, 95)
        Me.dteBetaling.Name = "dteBetaling"
        Me.dteBetaling.Size = New System.Drawing.Size(200, 20)
        Me.dteBetaling.TabIndex = 1
        '
        'txtBedrag
        '
        Me.txtBedrag.BackColor = System.Drawing.SystemColors.Window
        Me.txtBedrag.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtBedrag.Location = New System.Drawing.Point(348, 95)
        Me.txtBedrag.Name = "txtBedrag"
        Me.txtBedrag.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtBedrag.Size = New System.Drawing.Size(85, 20)
        Me.txtBedrag.TabIndex = 3
        Me.txtBedrag.Text = "0,00"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAdd, Me.cmdDelete})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(787, 25)
        Me.ToolStrip1.TabIndex = 4
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = Global.IMT_Timekeeping.My.Resources.Resources.AddTableHS
        Me.cmdAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(49, 22)
        Me.cmdAdd.Text = "Add"
        '
        'cmdDelete
        '
        Me.cmdDelete.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Loonboek"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Datum"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(216, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Shift"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(345, 77)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Bedrag"
        '
        'lblNaam
        '
        Me.lblNaam.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNaam.Location = New System.Drawing.Point(138, 50)
        Me.lblNaam.Name = "lblNaam"
        Me.lblNaam.Size = New System.Drawing.Size(295, 23)
        Me.lblNaam.TabIndex = 10
        '
        'cmbLoonboek
        '
        Me.cmbLoonboek.DisplayMember = "Text"
        Me.cmbLoonboek.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbLoonboek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLoonboek.FormattingEnabled = True
        Me.cmbLoonboek.ItemHeight = 14
        Me.cmbLoonboek.Location = New System.Drawing.Point(16, 50)
        Me.cmbLoonboek.Name = "cmbLoonboek"
        Me.cmbLoonboek.Size = New System.Drawing.Size(324, 20)
        Me.cmbLoonboek.TabIndex = 0
        '
        'cmbShift
        '
        Me.cmbShift.DisplayMember = "Text"
        Me.cmbShift.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShift.FormattingEnabled = True
        Me.cmbShift.ItemHeight = 14
        Me.cmbShift.Location = New System.Drawing.Point(219, 95)
        Me.cmbShift.Name = "cmbShift"
        Me.cmbShift.Size = New System.Drawing.Size(121, 20)
        Me.cmbShift.TabIndex = 2
        '
        'dgvBetalingen
        '
        Me.dgvBetalingen.AllowUserToAddRows = False
        Me.dgvBetalingen.AllowUserToDeleteRows = False
        Me.dgvBetalingen.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBetalingen.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvBetalingen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBetalingen.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBetalingen.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvBetalingen.Location = New System.Drawing.Point(0, 121)
        Me.dgvBetalingen.MultiSelect = False
        Me.dgvBetalingen.Name = "dgvBetalingen"
        Me.dgvBetalingen.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBetalingen.Size = New System.Drawing.Size(787, 370)
        Me.dgvBetalingen.TabIndex = 11
        '
        'lblSaldo
        '
        Me.lblSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldo.Location = New System.Drawing.Point(439, 33)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(348, 40)
        Me.lblSaldo.TabIndex = 12
        Me.lblSaldo.Text = "saldo:"
        '
        'frmInvoerLoonSpaarplanner
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(787, 488)
        Me.Controls.Add(Me.lblSaldo)
        Me.Controls.Add(Me.dgvBetalingen)
        Me.Controls.Add(Me.cmbShift)
        Me.Controls.Add(Me.cmbLoonboek)
        Me.Controls.Add(Me.lblNaam)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.txtBedrag)
        Me.Controls.Add(Me.dteBetaling)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInvoerLoonSpaarplanner"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Looninvoer spaarplanners"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgvBetalingen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dteBetaling As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtBedrag As NumericBox.NumericBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblNaam As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbLoonboek As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbShift As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents lblSaldo As DevComponents.DotNetBar.Controls.ReflectionLabel
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvBetalingen As DevComponents.DotNetBar.Controls.DataGridViewX
End Class
