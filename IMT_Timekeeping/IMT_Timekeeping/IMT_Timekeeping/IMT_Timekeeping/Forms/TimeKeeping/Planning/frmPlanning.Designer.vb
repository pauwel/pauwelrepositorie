<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanning
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DotNetBarManager1 = New DevComponents.DotNetBar.DotNetBarManager(Me.components)
        Me.DockSite4 = New DevComponents.DotNetBar.DockSite
        Me.DockSite1 = New DevComponents.DotNetBar.DockSite
        Me.DockSite2 = New DevComponents.DotNetBar.DockSite
        Me.DockSite8 = New DevComponents.DotNetBar.DockSite
        Me.DockSite5 = New DevComponents.DotNetBar.DockSite
        Me.DockSite6 = New DevComponents.DotNetBar.DockSite
        Me.DockSite7 = New DevComponents.DotNetBar.DockSite
        Me.DockSite3 = New DevComponents.DotNetBar.DockSite
        Me.DockContainerItem1 = New DevComponents.DotNetBar.DockContainerItem
        Me.tabPrestaties = New System.Windows.Forms.TabControl
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.ToolstripMenu = New System.Windows.Forms.ToolStrip
        Me.cmdAdd = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.cmbEdit = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdUpdate = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdDelete = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdReport = New System.Windows.Forms.ToolStripButton
        Me.dtePrestatie = New System.Windows.Forms.DateTimePicker
        Me.Label4 = New System.Windows.Forms.Label
        Me.chkAllMachines = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPloeg = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.chkWordtUitbetaald = New System.Windows.Forms.CheckBox
        Me.chkSpaarplanner = New System.Windows.Forms.CheckBox
        Me.cmbShift = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbWerknemers = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbMachine = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.cmbVoyage = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.ToolstripMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'DotNetBarManager1
        '
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins)
        Me.DotNetBarManager1.BottomDockSite = Me.DockSite4
        Me.DotNetBarManager1.DefinitionName = ""
        Me.DotNetBarManager1.EnableFullSizeDock = False
        Me.DotNetBarManager1.LeftDockSite = Me.DockSite1
        Me.DotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.DotNetBarManager1.ParentForm = Me
        Me.DotNetBarManager1.RightDockSite = Me.DockSite2
        Me.DotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003
        Me.DotNetBarManager1.ToolbarBottomDockSite = Me.DockSite8
        Me.DotNetBarManager1.ToolbarLeftDockSite = Me.DockSite5
        Me.DotNetBarManager1.ToolbarRightDockSite = Me.DockSite6
        Me.DotNetBarManager1.ToolbarTopDockSite = Me.DockSite7
        Me.DotNetBarManager1.TopDockSite = Me.DockSite3
        '
        'DockSite4
        '
        Me.DockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DockSite4.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer
        Me.DockSite4.Location = New System.Drawing.Point(0, 456)
        Me.DockSite4.Name = "DockSite4"
        Me.DockSite4.Size = New System.Drawing.Size(885, 0)
        Me.DockSite4.TabIndex = 3
        Me.DockSite4.TabStop = False
        '
        'DockSite1
        '
        Me.DockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite1.Dock = System.Windows.Forms.DockStyle.Left
        Me.DockSite1.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer
        Me.DockSite1.Location = New System.Drawing.Point(0, 0)
        Me.DockSite1.Name = "DockSite1"
        Me.DockSite1.Size = New System.Drawing.Size(0, 456)
        Me.DockSite1.TabIndex = 0
        Me.DockSite1.TabStop = False
        '
        'DockSite2
        '
        Me.DockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite2.Dock = System.Windows.Forms.DockStyle.Right
        Me.DockSite2.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer
        Me.DockSite2.Location = New System.Drawing.Point(885, 0)
        Me.DockSite2.Name = "DockSite2"
        Me.DockSite2.Size = New System.Drawing.Size(0, 456)
        Me.DockSite2.TabIndex = 1
        Me.DockSite2.TabStop = False
        '
        'DockSite8
        '
        Me.DockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite8.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DockSite8.Location = New System.Drawing.Point(0, 456)
        Me.DockSite8.Name = "DockSite8"
        Me.DockSite8.Size = New System.Drawing.Size(885, 0)
        Me.DockSite8.TabIndex = 7
        Me.DockSite8.TabStop = False
        '
        'DockSite5
        '
        Me.DockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite5.Dock = System.Windows.Forms.DockStyle.Left
        Me.DockSite5.Location = New System.Drawing.Point(0, 0)
        Me.DockSite5.Name = "DockSite5"
        Me.DockSite5.Size = New System.Drawing.Size(0, 456)
        Me.DockSite5.TabIndex = 4
        Me.DockSite5.TabStop = False
        '
        'DockSite6
        '
        Me.DockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite6.Dock = System.Windows.Forms.DockStyle.Right
        Me.DockSite6.Location = New System.Drawing.Point(885, 0)
        Me.DockSite6.Name = "DockSite6"
        Me.DockSite6.Size = New System.Drawing.Size(0, 456)
        Me.DockSite6.TabIndex = 5
        Me.DockSite6.TabStop = False
        '
        'DockSite7
        '
        Me.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite7.Dock = System.Windows.Forms.DockStyle.Top
        Me.DockSite7.Location = New System.Drawing.Point(0, 0)
        Me.DockSite7.Name = "DockSite7"
        Me.DockSite7.Size = New System.Drawing.Size(885, 0)
        Me.DockSite7.TabIndex = 6
        Me.DockSite7.TabStop = False
        '
        'DockSite3
        '
        Me.DockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite3.Dock = System.Windows.Forms.DockStyle.Top
        Me.DockSite3.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer
        Me.DockSite3.Location = New System.Drawing.Point(0, 0)
        Me.DockSite3.Name = "DockSite3"
        Me.DockSite3.Size = New System.Drawing.Size(885, 0)
        Me.DockSite3.TabIndex = 2
        Me.DockSite3.TabStop = False
        '
        'DockContainerItem1
        '
        Me.DockContainerItem1.Name = "DockContainerItem1"
        Me.DockContainerItem1.Text = "DockContainerItem1"
        '
        'tabPrestaties
        '
        Me.tabPrestaties.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabPrestaties.Location = New System.Drawing.Point(1, 120)
        Me.tabPrestaties.Name = "tabPrestaties"
        Me.tabPrestaties.SelectedIndex = 0
        Me.tabPrestaties.Size = New System.Drawing.Size(881, 336)
        Me.tabPrestaties.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(528, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Werknemer"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(365, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Machine"
        '
        'ToolstripMenu
        '
        Me.ToolstripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdAdd, Me.ToolStripSeparator2, Me.cmbEdit, Me.ToolStripSeparator4, Me.cmdUpdate, Me.ToolStripSeparator3, Me.cmdDelete, Me.ToolStripSeparator1, Me.cmdReport})
        Me.ToolstripMenu.Location = New System.Drawing.Point(0, 0)
        Me.ToolstripMenu.Name = "ToolstripMenu"
        Me.ToolstripMenu.Size = New System.Drawing.Size(885, 25)
        Me.ToolstripMenu.TabIndex = 15
        Me.ToolstripMenu.Text = "Add"
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = Global.IMT_Timekeeping.My.Resources.Resources.AddTableHS
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(49, 22)
        Me.cmdAdd.Text = "Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'cmbEdit
        '
        Me.cmbEdit.Image = Global.IMT_Timekeeping.My.Resources.Resources.EditInformationHS
        Me.cmbEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmbEdit.Name = "cmbEdit"
        Me.cmbEdit.Size = New System.Drawing.Size(47, 22)
        Me.cmbEdit.Text = "Edit"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdUpdate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(65, 22)
        Me.cmdUpdate.Text = "Update"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = Global.IMT_Timekeeping.My.Resources.Resources.DeleteHS
        Me.cmdDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(60, 22)
        Me.cmdDelete.Text = "Delete"
        Me.cmdDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'cmdReport
        '
        Me.cmdReport.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdReport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(62, 22)
        Me.cmdReport.Text = "Report"
        '
        'dtePrestatie
        '
        Me.dtePrestatie.Location = New System.Drawing.Point(12, 49)
        Me.dtePrestatie.Name = "dtePrestatie"
        Me.dtePrestatie.Size = New System.Drawing.Size(200, 20)
        Me.dtePrestatie.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Prestatiedatum"
        '
        'chkAllMachines
        '
        Me.chkAllMachines.AutoSize = True
        Me.chkAllMachines.Location = New System.Drawing.Point(256, 93)
        Me.chkAllMachines.Name = "chkAllMachines"
        Me.chkAllMachines.Size = New System.Drawing.Size(103, 17)
        Me.chkAllMachines.TabIndex = 5
        Me.chkAllMachines.Text = "Alleen machines"
        Me.chkAllMachines.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(215, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Shift"
        '
        'txtPloeg
        '
        Me.txtPloeg.Location = New System.Drawing.Point(368, 48)
        Me.txtPloeg.MaxLength = 20
        Me.txtPloeg.Name = "txtPloeg"
        Me.txtPloeg.Size = New System.Drawing.Size(157, 20)
        Me.txtPloeg.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(365, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Ploeg"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 79)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 13)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Toon voyage gegevens"
        '
        'chkWordtUitbetaald
        '
        Me.chkWordtUitbetaald.AutoSize = True
        Me.chkWordtUitbetaald.Checked = True
        Me.chkWordtUitbetaald.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWordtUitbetaald.Location = New System.Drawing.Point(704, 93)
        Me.chkWordtUitbetaald.Name = "chkWordtUitbetaald"
        Me.chkWordtUitbetaald.Size = New System.Drawing.Size(104, 17)
        Me.chkWordtUitbetaald.TabIndex = 25
        Me.chkWordtUitbetaald.Text = "Wordt uitbetaald"
        Me.chkWordtUitbetaald.UseVisualStyleBackColor = True
        '
        'chkSpaarplanner
        '
        Me.chkSpaarplanner.AutoSize = True
        Me.chkSpaarplanner.Enabled = False
        Me.chkSpaarplanner.Location = New System.Drawing.Point(531, 74)
        Me.chkSpaarplanner.Name = "chkSpaarplanner"
        Me.chkSpaarplanner.Size = New System.Drawing.Size(89, 17)
        Me.chkSpaarplanner.TabIndex = 26
        Me.chkSpaarplanner.Text = "Spaarplanner"
        Me.chkSpaarplanner.UseVisualStyleBackColor = True
        '
        'cmbShift
        '
        Me.cmbShift.DisplayMember = "Text"
        Me.cmbShift.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShift.FormattingEnabled = True
        Me.cmbShift.ItemHeight = 14
        Me.cmbShift.Location = New System.Drawing.Point(218, 49)
        Me.cmbShift.Name = "cmbShift"
        Me.cmbShift.Size = New System.Drawing.Size(141, 20)
        Me.cmbShift.TabIndex = 27
        '
        'cmbWerknemers
        '
        Me.cmbWerknemers.DisplayMember = "Text"
        Me.cmbWerknemers.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbWerknemers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWerknemers.FormattingEnabled = True
        Me.cmbWerknemers.ItemHeight = 14
        Me.cmbWerknemers.Location = New System.Drawing.Point(531, 48)
        Me.cmbWerknemers.Name = "cmbWerknemers"
        Me.cmbWerknemers.Size = New System.Drawing.Size(171, 20)
        Me.cmbWerknemers.TabIndex = 28
        '
        'cmbMachine
        '
        Me.cmbMachine.DisplayMember = "Text"
        Me.cmbMachine.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbMachine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMachine.FormattingEnabled = True
        Me.cmbMachine.ItemHeight = 14
        Me.cmbMachine.Location = New System.Drawing.Point(368, 93)
        Me.cmbMachine.Name = "cmbMachine"
        Me.cmbMachine.Size = New System.Drawing.Size(330, 20)
        Me.cmbMachine.TabIndex = 29
        '
        'cmbVoyage
        '
        Me.cmbVoyage.DisplayMember = "Text"
        Me.cmbVoyage.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbVoyage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVoyage.FormattingEnabled = True
        Me.cmbVoyage.ItemHeight = 14
        Me.cmbVoyage.Location = New System.Drawing.Point(15, 95)
        Me.cmbVoyage.Name = "cmbVoyage"
        Me.cmbVoyage.Size = New System.Drawing.Size(159, 20)
        Me.cmbVoyage.TabIndex = 30
        '
        'frmPlanning
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(885, 456)
        Me.Controls.Add(Me.cmbVoyage)
        Me.Controls.Add(Me.cmbMachine)
        Me.Controls.Add(Me.cmbWerknemers)
        Me.Controls.Add(Me.cmbShift)
        Me.Controls.Add(Me.chkSpaarplanner)
        Me.Controls.Add(Me.chkWordtUitbetaald)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtPloeg)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.chkAllMachines)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtePrestatie)
        Me.Controls.Add(Me.ToolstripMenu)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tabPrestaties)
        Me.Controls.Add(Me.DockSite2)
        Me.Controls.Add(Me.DockSite1)
        Me.Controls.Add(Me.DockSite3)
        Me.Controls.Add(Me.DockSite4)
        Me.Controls.Add(Me.DockSite5)
        Me.Controls.Add(Me.DockSite6)
        Me.Controls.Add(Me.DockSite7)
        Me.Controls.Add(Me.DockSite8)
        Me.Name = "frmPlanning"
        Me.Text = "Prestaties"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolstripMenu.ResumeLayout(False)
        Me.ToolstripMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DotNetBarManager1 As DevComponents.DotNetBar.DotNetBarManager
    Friend WithEvents DockSite4 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite1 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite2 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite3 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite5 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite6 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite7 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite8 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockContainerItem1 As DevComponents.DotNetBar.DockContainerItem
    Friend WithEvents tabPrestaties As System.Windows.Forms.TabControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolstripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtePrestatie As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkAllMachines As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPloeg As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkWordtUitbetaald As System.Windows.Forms.CheckBox
    Friend WithEvents chkSpaarplanner As System.Windows.Forms.CheckBox
    Friend WithEvents cmbShift As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbMachine As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbWerknemers As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmbVoyage As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdReport As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmbEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdUpdate As System.Windows.Forms.ToolStripButton
End Class
