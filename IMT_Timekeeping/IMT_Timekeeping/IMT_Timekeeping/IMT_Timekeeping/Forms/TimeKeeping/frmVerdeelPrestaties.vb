Option Compare Text

Imports System.Threading


Public Class frmVerdeelPrestaties


    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean

    Dim loaddata As New LoadDataKosten
    Private Sub frmVerdeelPrestaties_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LockWindowUpdate(Me.Handle.ToInt32)
        Me.WindowState = FormWindowState.Maximized

        'datagridview verplaatsen en verbreden
        Dim br As Integer = Me.Width
        br = br / 2
        dgvOrg.Left = 0
        dgvOrg.Width = br

        dgvTo.Left = br
        dgvTo.Width = br

        'comboboxen verplaatsen

        cmbVoyageTo.Left = br + 5 '+5 of  - 5 pixels om scheding tussen controls te waarborgen
        txtUren.Left = (br - 65)
        cmbVoyage.Left = (br - 65 - 120 - 5)

        'buttons verplaatsen

        cmdForward.Left = br - 75 - 5
        'labels
        Label5.Left = cmbVoyageTo.Left
        Label4.Left = txtUren.Left
        Label3.Left = cmbVoyage.Left + cmbVoyage.Width - Label3.Width

        loaddata.Datum = dtePrestatieDatum.Value.Date
        cmbShift.DataSource = loaddata.PrestatieShiften
        cmbShift.DisplayMember = "omschrijving"
        cmbShift.ValueMember = "shiftid"
        cmbShift.SelectedIndex = -1

        LockWindowUpdate(0)
    End Sub
    Private Sub dtePrestatieDatum_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtePrestatieDatum.ValueChanged
        LockWindowUpdate(Me.Handle.ToInt32)

        loaddata.Datum = dtePrestatieDatum.Value.Date
        cmbShift.DataSource = loaddata.PrestatieShiften
        cmbShift.DisplayMember = "omschrijving"
        cmbShift.ValueMember = "shiftid"
        cmbShift.Text = ""
        cmbShift.SelectedIndex = -1

        cmbVoyage.SelectedIndex = -1
        cmbVoyage.Text = ""

        cmbVoyageTo.SelectedIndex = -1
        cmbVoyageTo.Text = ""

        LockWindowUpdate(0)

    End Sub

    Private Sub cmbShift_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbShift.SelectedValueChanged
        LockWindowUpdate(Me.Handle.ToInt32)
        Try
            loaddata.Shift = cmbShift.SelectedValue

            cmbVoyage.DataSource = loaddata.Prestatievoyages
            cmbVoyage.DisplayMember = "Voyagenbr"
            cmbVoyage.ValueMember = "VoyageID"
            cmbVoyage.SelectedIndex = -1
            cmbVoyage.Text = ""

           
        Catch ex As Exception

        End Try
        LockWindowUpdate(0)
    End Sub


    Private Sub cmbVoyage_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVoyage.SelectedIndexChanged
        Try
            loaddata.SelectedVoyage = cmbVoyage.SelectedValue
            loaddata.SelectVoyageto()

            cmbVoyageTo.DataSource = loaddata.VoyagesTo
            cmbVoyageTo.DisplayMember = "Voyagenbr"
            cmbVoyageTo.ValueMember = "VoyageID"
            cmbVoyageTo.SelectedIndex = -1
            cmbVoyageTo.Text = ""

            refreshOrg()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub refreshOrg()

        loaddata.VoyageSelected = cmbVoyage.SelectedValue

        If loaddata.Werknemers.Rows.Count = 0 Then
            dgvOrg.DataSource = Nothing
        Else
            dgvOrg.DataSource = loaddata.Werknemers
        End If


        For Each c As DataGridViewColumn In dgvOrg.Columns

            Select Case c.Name
                Case "ploeg", "loonboek", "naam", "uren", "Machine", "Machinekost"
                Case Else
                    c.Visible = False
            End Select

        Next
    End Sub
    Private Sub refreshTo()

        loaddata.VoyageToSelected = cmbVoyageTo.SelectedValue

        If loaddata.WerknemersTO.Rows.Count = 0 Then
            dgvTo.DataSource = Nothing
        Else
            dgvTo.DataSource = loaddata.WerknemersTO

        End If


        For Each c As DataGridViewColumn In dgvTo.Columns

            Select Case c.Name
                Case "ploeg", "loonboek", "naam", "uren", "Machine", "Machinekost"
                Case Else
                    c.Visible = False
            End Select


        Next
    End Sub

    Private Sub cmbVoyageTo_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVoyageTo.SelectedValueChanged
        Try
            refreshTo()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub cmdForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdForward.Click

        Dim orgVoyage As Int16 = cmbVoyage.SelectedValue
        Dim voyageto As Integer = cmbVoyageTo.SelectedValue

        splitter(dgvOrg, dgvTo, cmbVoyageTo)

        cmbVoyage.SelectedValue = orgVoyage
        cmbVoyageTo.SelectedValue = voyageto
    End Sub

   
    Private Sub splitter(ByVal Origineel As DataGridView, ByVal ToUpdate As DataGridView, ByVal Voyage As ComboBox)
        Dim vt As Integer = cmbVoyageTo.SelectedValue

        loaddata.Uren = txtUren.Text

        For Each r As DataGridViewRow In Origineel.SelectedRows
            loaddata.OrgUren = Origineel.Item("uren", r.Index).Value

            If loaddata.validate Then
                With loaddata
                    Dim s As New BLL.Splitter
                    s.Prestatieid = dgvOrg.Item("prestatieid", dgvOrg.CurrentCellAddress.Y).Value
                    s.SetProperties()
                    s.inputUren = txtUren.Text
                    s.VoyageTo = loaddata.VoyageToSelected
                    s.insert()

                End With
                loaddata.loadvoyages()
                cmbVoyage.DataSource = loaddata.Prestatievoyages
                cmbVoyageTo.SelectedValue = vt
            End If
        Next
      

    End Sub

   
    Private Sub chkMachines_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMachines.CheckedChanged
        LockWindowUpdate(Me.Handle.ToInt32)
        loaddata.MAchinesOnly = chkMachines.Checked


        loaddata.Datum = dtePrestatieDatum.Value.Date
        cmbShift.DataSource = loaddata.PrestatieShiften
        cmbShift.DisplayMember = "omschrijving"
        cmbShift.ValueMember = "shiftid"
        cmbShift.Text = ""
        cmbShift.SelectedIndex = -1

        cmbVoyage.SelectedIndex = -1
        cmbVoyage.Text = ""

        cmbVoyageTo.SelectedIndex = -1
        cmbVoyageTo.Text = ""

        LockWindowUpdate(0)

    End Sub

    Private Sub dgvOrg_CellEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOrg.CellEnter
        Try
            dgvOrg.Rows(dgvOrg.CurrentCellAddress.Y).Selected = True
        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgvTo_CellEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTo.CellEnter
        Try
            dgvTo.Rows(dgvTo.CurrentCellAddress.Y).Selected = True
        Catch ex As Exception

        End Try
    End Sub

 
End Class