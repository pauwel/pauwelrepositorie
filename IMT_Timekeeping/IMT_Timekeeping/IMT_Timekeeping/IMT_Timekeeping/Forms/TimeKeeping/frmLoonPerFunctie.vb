Option Compare Text

Imports BLL
Imports System.Windows.Forms
Imports System.Data
Imports System.Threading

Imports System.Globalization.CultureInfo
Imports System.Configuration

Public Class frmLoonPerFunctie

    Private clsLPF As New clsLPF

    Private TagEmployeeTypes As Integer
    Private TagShift As Integer

    Private id As Nullable(Of Integer)
    Private rijen As New ArrayList
    Private shiftrijen As New ArrayList, employeerijen As New ArrayList
    Private shiftrijenvalue As New ArrayList, employeerijenvalue As New ArrayList

    Private dgvLPF As DataGridView
    Private tabShiften As TabControl
    Private tabEmployees As TabControl



    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean

    Private Sub frmLoonPerFunctie_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LockWindowUpdate(Me.Handle.ToInt64)


        Me.WindowState = FormWindowState.Maximized
        LoadCatCombobox()
        LoadFunctieCombobox()

        ' Tabcontroluitbreiden met Shiften
        Dim t As Integer = 0
        tabShiften = tabdatagrid

        For Each dr As DataRow In clsPreload.shift.Rows ' lstShift.Items

            tabShiften.TabPages.Add(dr("omschrijving").ToString)

            tabShiften.TabPages(t).Tag = dr("shiftid").ToString

            Dim dg As New DataGridView

            tabShiften.TabPages(t).Controls.Add(dg)
            dg.Dock = DockStyle.Fill
            dg.AllowUserToAddRows = False
            dg.AllowUserToResizeRows = False
            dg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells


            Dim ADUser As Boolean = False 'maakt de gebruiker gebruik van AD? indien niet, beperkte weergave

            Try
                ADUser = ConfigurationManager.AppSettings("UseActiveDirectory")
            Catch ex As Exception
                ADUser = False
            End Try

            If ADUser Then
                Dim s As New BLL.isInRole
                If s.IsAdmin Then

                    'Datagrdiview eventhandler
                    AddHandler dg.KeyDown, AddressOf dgvLPF_KeyDown
                    AddHandler dg.KeyUp, AddressOf dgvLPF_KeyUp

                    AddHandler dg.CellValueChanged, AddressOf dgvLPF_CellValueChanged
                    AddHandler dg.CellLeave, AddressOf dgvLPF_CellLeave
                    AddHandler dg.DataError, AddressOf dgvLPF_dataerror
                    AddHandler dg.CellBeginEdit, AddressOf dg_CellBeginEdit
                    fraLPF.Enabled = True
                    cmdAdd.Enabled = True
                    cmdDelete.Enabled = True
                    cmdCategory.Enabled = True
                    cmdJob.Enabled = True
                    cmdEmployees.Enabled = True
                    cmdShiften.Enabled = True
                    dg.AllowUserToDeleteRows = True
                Else
                    dg.ReadOnly = True
                    fraLPF.Enabled = False
                    cmdAdd.Enabled = False
                    cmdDelete.Enabled = False
                    cmdCategory.Enabled = False
                    cmdJob.Enabled = False
                    cmdEmployees.Enabled = False
                    cmdShiften.Enabled = False
                    dg.AllowUserToDeleteRows = False
                End If
            End If


            t += 1

        Next
        Try
            tabShiften.SelectedIndex = 0
            TagShift = tabdatagrid.SelectedTab.Tag

            refreshgrid(TagShift)
        Catch ex As Exception

        End Try

        cmdUpdate.Enabled = False
        LockWindowUpdate(0)
    End Sub



    Private _cellvalue As Decimal
    Public Property cellvalue() As Decimal
        Get
            Return _cellvalue
        End Get
        Set(ByVal value As Decimal)
            _cellvalue = value
        End Set
    End Property

    Private _edit As Boolean
    Public Property edit() As Boolean
        Get
            Return _edit
        End Get
        Set(ByVal value As Boolean)
            _edit = value
        End Set
    End Property


    Private Sub dg_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs)

        Try
            cellvalue = dgvLPF.Item(e.ColumnIndex, e.RowIndex).Value
            edit = True
        Catch ex As InvalidCastException

        End Try

    End Sub

    Dim cat As Integer = 0
    Dim funct As Integer = 0
    Dim formula As String = Nothing
    Dim stdloon As Decimal = 0
    Dim totsupplement As Decimal = 0
    Dim totbedragOU As Decimal = 0
    Dim eindejaar As Decimal = 0
    Dim supplementdubbeleshift As Decimal = 0
    Dim _shid As Integer = 0

    Private Sub dgvLPF_CellLeave(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

        Select Case dgvLPF.Columns(e.ColumnIndex).Name
            Case "stdloon", "overuren", "supplementen", "eindejaarspremie"
                cmdUpdate.Enabled = True
                cmdAdd.Enabled = False
        End Select
        If edit Then
            dgvLPF.EndEdit()
            Dim nieuwewaarde As Decimal = 0
            nieuwewaarde = dgvLPF.Item(e.ColumnIndex, e.RowIndex).Value
            If cellvalue <> nieuwewaarde Then
                'update


                cat = dgvLPF.Item("catid", e.RowIndex).Value
                funct = dgvLPF.Item("Functieid", e.RowIndex).Value
                formula = dgvLPF.Item("Formule", e.RowIndex).Value


                Try
                    stdloon = dgvLPF.Item("Stdloon", e.RowIndex).Value.ToString()
                Catch ex As InvalidCastException
                End Try


                Try
                    totsupplement = dgvLPF.Item("Supplement", e.RowIndex).Value.ToString
                Catch ex As InvalidCastException
                End Try


                Try
                    totbedragOU = dgvLPF.Item("OverUren", e.RowIndex).Value.ToString
                Catch ex As InvalidCastException
                End Try


                Try
                    eindejaar = dgvLPF.Item("eindejaarspremie", e.RowIndex).Value.ToString
                Catch ex As InvalidCastException
                End Try

                Try
                    supplementdubbeleshift = dgvLPF.Item("supplementdubbeleshift", e.RowIndex).Value.ToString
                Catch ex As InvalidCastException

                End Try

                _shid = dgvLPF.Item("shiftid", e.RowIndex).Value

                Select Case tabdatagrid.SelectedTab.Tag
                    Case 36
                        UpdateShift(True)
                    Case Else
                        UpdateShift(False)

                End Select

            End If
            edit = False
        End If


    End Sub



#Region "Load data"


    Private Sub LoadCatCombobox()
        With clsPreload
            cmbCat.DataSource = .category
            cmbCat.DisplayMember = .categorydisplaymember
            cmbCat.ValueMember = .categoryvaluemember
            cmbCat.SelectedIndex = -1
        End With

    End Sub
    Private Sub LoadFunctieCombobox()
        With clsPreload
            cmbFunctie.DataSource = .functies
            cmbFunctie.DisplayMember = .functiesdisplaymember
            cmbFunctie.ValueMember = .functiesvaluemember
            cmbFunctie.SelectedIndex = -1
        End With


    End Sub


#End Region



    Private vw_clBLL_LPF As New vwLoonPerFunctie
    Private tshift As Integer
    Private temployee As Integer

#Region "Asynchronous handling"


    Dim thExecuteTaskAsync As Thread = Nothing
    Private Sub StartExecuteTaskAsync()
        'clear existing thread
        If Not thExecuteTaskAsync Is Nothing Then
            thExecuteTaskAsync.Abort()
            thExecuteTaskAsync.Join()
            thExecuteTaskAsync = Nothing
        End If
        'start a new thread to execute the task asynchronously
        thExecuteTaskAsync = New Thread(AddressOf ExecuteTaskAsync)
        thExecuteTaskAsync.Start()
    End Sub



    Private Sub ExecuteTaskAsync()
        clsLPF.Data = clBLL_getLoonPerFunctie.GetLoonPerFunctie(tshift)
        'clsLPF.Data.Columns.Add("totaal")
    End Sub


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'this is necessary if the form is trying to close, even before the completion of task
        If Not thExecuteTaskAsync Is Nothing Then thExecuteTaskAsync.Abort()

        'CheckUpdate()
    End Sub

#End Region

#Region "Update comboboxen"

    Private Sub cmbCat_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

        LoadCatCombobox()
    End Sub

    Private Sub cmbJob_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

        LoadFunctieCombobox()
    End Sub





#End Region

#Region "topmenu"

    Private Sub ToolStripEmployees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEmployees.Click
        frmEmployeeTypes.ShowDialog()
        'update na aanpassing
        clsPreload.employeetypes = clBLL_TypeWerknemers.GetWerknemersType


    End Sub
    Private Sub cmdShiften_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShiften.Click
        frmShifts.ShowDialog()

        clsPreload.shift = clBLL_Shiften.GetShift

    End Sub
    Private Sub cmdCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCategory.Click
        frmCategory.ShowDialog()
        'update na aanpassing
        clsPreload.category = clBLL_Category.GetCategories
        LoadCatCombobox()
    End Sub

    Private Sub cmdJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdJob.Click
        frmFunctions.ShowDialog()
        'update na aanpassing
        clsPreload.functies = clBLL_Functies.GetFuncties
        LoadFunctieCombobox()
    End Sub

#End Region


    'Private Sub CheckUpdate()
    '    If cmdUpdate.Enabled = True Then
    '        Dim answer As Integer = MsgBox("Er werden wijzigingen aangebracht in de loondetail welke niet werden geupdate, wens je deze gegevens te bewaren?", MsgBoxStyle.YesNo)

    '        If answer = 6 Then
    '            Select Case tabdatagrid.SelectedTab.Tag
    '                Case 36
    '                    UpdateShift(True)
    '                Case Else
    '                    UpdateShift(False)

    '            End Select
    '        End If
    '    End If
    'End Sub


    Private Sub refreshgrid(ByVal t As Integer) ', ByVal e As Integer)


        Me.Cursor = Cursors.WaitCursor

        tshift = t
        StartExecuteTaskAsync()

        Do While thExecuteTaskAsync.ThreadState = ThreadState.Running
            Thread.Sleep(0)
        Loop

        For Each c As Control In tabdatagrid.SelectedTab.Controls

            If c.GetType.ToString = "System.Windows.Forms.DataGridView" Then
                Try
                    Dim dg As DataGridView = c

                    dgvLPF = dg
                    dg.DataSource = clsLPF.Data
                    dg.Columns("loonid").Visible = False
                    dg.Columns("shiftid").Visible = False
                    dg.Columns("typewerknemerid").Visible = False
                    dg.Columns("catid").Visible = False
                    dg.Columns("functieid").Visible = False

                    Dim colexist As Boolean = False
                    For Each col As DataGridViewColumn In dg.Columns
                        If col.HeaderText = "totaal" Then
                            colexist = True
                            Exit For
                        End If
                    Next
                    'If Not colexist Then
                    '    Dim dc As New DataGridViewColumn
                    '    dg.Columns.Add("Totaal", "totaal")

                    'End If
                    'dg.Columns("totaal").DisplayIndex = 8

                    '' column opvullen
                    'For Each r As DataGridViewRow In dg.Rows
                    '    Dim tot As Decimal = 0
                    '    Dim i As Integer = -1
                    '    For Each cel As DataGridViewCell In r.Cells
                    '        i += 1
                    '        Select Case dg.Columns(i).Name

                    '            Case "stdloon", "walking", "premie", "overuren", "supplement", "eindejaarspremie", "rsz", "vakopleiding", _
                    '                "CEPA", "siwha", "preventie", "spec.fonds", "structuur", "hvd", "p.vorming", "basis", "sanering", "rsz/jaar"
                    '                If IsNumeric(r.Cells(i).Value) Then
                    '                    '  MsgBox(dg.Columns(i).Name)
                    '                    tot = tot + Convert.ToDecimal(cel.Value)
                    '                End If
                    '        End Select
                    '        r.Cells("totaal").Value = Math.Round(tot, 2).ToString

                    '    Next
                    'Next

                    For Each col As DataGridViewColumn In dg.Columns
                        Select Case col.Name
                            Case "Catcode", "Functiecode", "Formule"
                            Case Else
                                dg.Columns(col.Name).DefaultCellStyle.Format = "N2"
                                dg.Columns(col.Name).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                                If col.Name = "stdloon" Or col.Name = "Overuren" Or _
                                col.Name = "Supplement" Or col.Name = "eindejaarspremie" Or col.Name = "supplementdubbeleshift" Then
                                    col.ReadOnly = False
                                    col.DefaultCellStyle.BackColor = Color.LightGray
                                Else
                                    col.ReadOnly = True
                                End If
                        End Select
                    Next

                Catch ex As Exception

                End Try


            End If

        Next


        clear()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub clear()
        cmbCat.SelectedIndex = -1
        cmbFunctie.SelectedIndex = -1


        txtFormule.Clear()
        txtStdLoon.Text = 0
        txtTotaalOverUren.Text = 0
        txtTotBedragSupp.Text = 0
        txtEindejaarspremie.Text = 0
        txtSupplDubbeleShift.Text = 0

        cmbCat.Select()
    End Sub

    Private Sub cmdadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim cat As Nullable(Of Integer) = cmbCat.SelectedValue
        Dim funct As Nullable(Of Integer) = cmbFunctie.SelectedValue

        If cat.HasValue And funct.HasValue Then

            If clBLL_LoonPF.CheckBestaande(cat, funct) Then
                MsgBox("meervoudige records van dit type zijn niet toegelaten!", MsgBoxStyle.Information)
                Exit Sub
            End If

            For Each dr As DataRow In clsPreload.employeetypes.Rows


                Dim wnType As Integer = dr("TypeWerknemerID").ToString()

                For Each sh As DataRow In clsPreload.shift.Rows 'lstShift.SelectedItems

                    Dim formula As String = txtFormule.Text
                    Dim stdloon As Decimal = txtStdLoon.Text

                    Dim totsupplement As Decimal = txtTotBedragSupp.Text

                    Dim totbedragOU As Decimal = txtTotaalOverUren.Text
                    Dim eindejaar As Decimal = txtEindejaarspremie.Text

                    Dim supplementDubbeleShift As Decimal = Me.txtSupplDubbeleShift.Text

                    Dim shid As Integer = sh("shiftid").ToString

                    Dim ShiftUplift As Decimal = clBLL_Shiften.GetVergoedingByID(shid)
                    ShiftUplift = (ShiftUplift + 100) / 100

                    stdloon = stdloon * ShiftUplift
                    totbedragOU = totbedragOU * ShiftUplift
                    totsupplement = totsupplement * ShiftUplift


                    clBLL_LoonPF.InsertLoonPerFunctie(cat, funct, stdloon, totbedragOU, totsupplement, formula, eindejaar, wnType, shid, supplementDubbeleShift)

                Next
            Next

        End If

        GetTabSelection()
        refreshgrid(TagShift)
    End Sub



    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click

        For Each dr As DataGridViewRow In dgvLPF.SelectedRows

            Dim catid As Integer = dgvLPF.Item("catid", dgvLPF.CurrentCellAddress.Y).Value
            Dim functieid As Integer = dgvLPF.Item("functieid", dgvLPF.CurrentCellAddress.Y).Value
            clBLL_LoonPF.DeleteLoonPerFunctie(catid, functieid)
        Next

        GetTabSelection()
        refreshgrid(TagShift)
    End Sub

    Private Sub dgvLPF_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)


        Select Case dgvLPF.Columns(e.ColumnIndex).Name.ToString
            Case "stdloon", "overuren", "supplementen", "eindejaarspremie"
                cmdUpdate.Enabled = True
                cmdAdd.Enabled = False
        End Select



    End Sub

    Private Sub dgvLPF_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        Try
            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    Dim catid As Integer = rijen(dr)
                    dr = dr + 1
                    Dim functieid As Integer = rijen(dr)

                    clBLL_LoonPF.DeleteLoonPerFunctie(catid, functieid)
                Next

                GetTabSelection()
                refreshgrid(TagShift)

            End If


        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgvLPF_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            rijen.Clear()
            For Each dr As DataGridViewRow In dgvLPF.SelectedRows
                rijen.Add(dgvLPF.Item("catid", dr.Index).Value)
                rijen.Add(dgvLPF.Item("functieid", dr.Index).Value)
            Next
        Catch ex As Exception

        End Try


    End Sub


    Private Sub dgvLPF_dataerror(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs)
        cmdUpdate.Enabled = False
    End Sub



    Private Sub tabdatagrid_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabdatagrid.SelectedIndexChanged
        Try
            GetTabSelection()
        Catch ex As Exception
        End Try

    End Sub

    Private Sub tabShift_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            GetTabSelection()
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetTabSelection()

        TagShift = tabdatagrid.SelectedTab.Tag
        If tshift <> TagShift Then
            refreshgrid(TagShift)
            cmdUpdate.Enabled = False
            cmdAdd.Enabled = False
        End If
    End Sub




    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Select Case tabdatagrid.SelectedTab.Tag
            Case 36
                UpdateShift(True)
            Case Else
                UpdateShift(False)

        End Select
    End Sub


    Private Sub UpdateShift(ByVal all As Boolean)

        Me.Cursor = Cursors.WaitCursor
        LockWindowUpdate(Me.Handle.ToInt64)

        If all Then


            For Each sh As DataRow In clsPreload.shift.Rows
                Dim shid As Integer = sh("shiftid")
                Dim ShiftUplift As Decimal = clBLL_Shiften.GetVergoedingByID(shid)

                ShiftUplift = (ShiftUplift + 100) / 100
                Dim lp As New BLL.UpdateLoonperfunctie

                lp.UpdateLPF(stdloon * ShiftUplift, totbedragOU * ShiftUplift, totsupplement * ShiftUplift, eindejaar, Now.Date, supplementdubbeleshift, shid, cat, funct)

            Next
            'Next
        Else
            ' Dim wnType As Integer = dgvLPF.Item("TypeWerknemerID", r.Index).Value


            Dim lp As New BLL.UpdateLoonperfunctie

            lp.UpdateLPF(stdloon, totbedragOU, totsupplement, eindejaar, Now.Date, supplementdubbeleshift, _shid, cat, funct)


        End If
        '  Next

        cmdUpdate.Enabled = False
        'refreshgrid(tabdatagrid.SelectedTab.Tag)
        LockWindowUpdate(0)

        Me.Cursor = Cursors.Default
    End Sub



    Private Sub cmbCat_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Controleer()
    End Sub


    Private Sub cmbFunctie_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Controleer()
    End Sub

    Private Sub txtFormule_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Controleer()
    End Sub

    Private Function Controleer() As Boolean
        Try
            If cmbCat.SelectedValue > 0 And cmbFunctie.SelectedValue > 0 And Not String.IsNullOrEmpty(Trim(txtFormule.Text)) Then
                cmdAdd.Enabled = True
            End If
        Catch ex As Exception
            'opstart
            cmdAdd.Enabled = False
        End Try

    End Function

    Private Sub txtEindejaarspremie_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        _frmReports.Reportnaam = "loonkost.rpt"
        _frmReports.ShowDialog()
    End Sub


   
    Private Sub buttonFormule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonFormule.Click
        Dim f As New frmLoonPerFunctieNew
        f.StartPosition = FormStartPosition.CenterScreen
        Dim r As DialogResult = f.ShowDialog()

        If r = Windows.Forms.DialogResult.OK Then
            GetTabSelection()
            refreshgrid(TagShift)
        End If
    End Sub
End Class
