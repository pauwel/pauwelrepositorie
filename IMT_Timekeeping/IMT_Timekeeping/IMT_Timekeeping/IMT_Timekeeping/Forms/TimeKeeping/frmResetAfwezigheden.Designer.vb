<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResetAfwezigheden
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvReset = New DevComponents.DotNetBar.Controls.DataGridViewX
        CType(Me.dgvReset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvReset
        '
        Me.dgvReset.AllowUserToAddRows = False
        Me.dgvReset.AllowUserToDeleteRows = False
        Me.dgvReset.AllowUserToResizeRows = False
        Me.dgvReset.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvReset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReset.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvReset.Location = New System.Drawing.Point(0, 0)
        Me.dgvReset.Name = "dgvReset"
        Me.dgvReset.ReadOnly = True
        Me.dgvReset.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReset.Size = New System.Drawing.Size(634, 514)
        Me.dgvReset.TabIndex = 0
        '
        'frmResetAfwezigheden
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(634, 514)
        Me.Controls.Add(Me.dgvReset)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(400, 400)
        Me.Name = "frmResetAfwezigheden"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Overzicht afwezigheden"
        CType(Me.dgvReset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvReset As System.Windows.Forms.DataGridView
End Class
