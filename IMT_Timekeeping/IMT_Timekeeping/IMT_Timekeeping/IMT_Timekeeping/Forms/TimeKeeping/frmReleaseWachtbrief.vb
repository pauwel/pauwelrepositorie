﻿Public Class frmReleaseWachtbrief
    'dit scherm wordt gebruikt om de blokkering van briefjes welke reeds waren doorgestuurd naar CEPA op te heffen  
    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim release As New BLL.BLReleaseWBrief
        With cmbWerknemers
            .DataSource = release.GetNietSpaarplanners
            .ValueMember = "WerknemerID"
            .DisplayMember = "Naam"
        End With
        
    End Sub

    Private Sub btnUitvoeren_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUitvoeren.Click
        MsgBox("Met deze actie worden de geselecteerde loonbriefjes VERWIJDERD uit de database." & vbCrLf & "Doorgaan?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, "IMT")
        If MsgBoxResult.Yes Then
            For Each dr As DataGridViewRow In dgvPrestaties.SelectedRows
                Try
                    Dim release As New BLL.BLReleaseWBrief
                    If Not release.ReleaseWBBRIEF(dr.Cells("betaalid").Value, dr.Cells("wbbrief").Value) Then
                        MsgBox("een fout deed zich voor bij de aanpassing van loonbrief" & dr.Cells("WBBrief").Value.ToString & ", de situatie werd in zijn oorspronkelijke staat hersteld", MsgBoxStyle.Information, "IMT")
                    End If
                    Dim log As New BLL.BLLogging
                    log.InsertLogging(dr.Cells("betaalid").Value, cmbWerknemers.SelectedValue, BLL.BLLogging.logActieid.OpheffenBlokeringVanWachtbriefNaDoorsturingNaarCepa, String.Empty)

                Catch ex As Exception
                    MsgBox(ex.Message)
                    Dim log As New BLL.BLLogging
                    log.InsertLogging(dr.Cells("betaalid").Value, cmbWerknemers.SelectedValue, BLL.BLLogging.logActieid.OpheffenBlokeringVanWachtbriefNaDoorsturingNaarCepa, ex.Message)
                End Try

            Next
            GETDATA()
        End If
    End Sub

    Private Sub cmbWerknemers_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWerknemers.SelectedValueChanged
        GETDATA()
    End Sub
    Private Function GETDATA() As Boolean

        Try
            Dim release As New BLL.BLReleaseWBrief
            With dgvPrestaties
                .DataSource = release.GetLockedPrestaties(cmbWerknemers.SelectedValue)
                .Columns("betaalid").Visible = False
                .Columns("wachtid").Visible = False
            End With

        Catch ic As InvalidCastException
            'laden
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


End Class