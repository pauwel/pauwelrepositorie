﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNietSpaarplannerOvergang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNietSpaarplannerOvergang))
        Me.Wizard1 = New DevComponents.DotNetBar.Wizard
        Me.WizardPage1 = New DevComponents.DotNetBar.WizardPage
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblOmschrijving = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.WizardPage2 = New DevComponents.DotNetBar.WizardPage
        Me.lstSpaarplanners = New System.Windows.Forms.ListBox
        Me.WizardPage3 = New DevComponents.DotNetBar.WizardPage
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblWachtbestand = New System.Windows.Forms.Label
        Me.lblSom = New DevComponents.DotNetBar.LabelX
        Me.Wizard1.SuspendLayout()
        Me.WizardPage1.SuspendLayout()
        Me.WizardPage2.SuspendLayout()
        Me.WizardPage3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Wizard1
        '
        Me.Wizard1.BackButtonText = "< Vorige"
        Me.Wizard1.BackColor = System.Drawing.SystemColors.Control
        Me.Wizard1.CancelButtonText = "Annuleren"
        Me.Wizard1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Wizard1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Wizard1.FinishButtonTabIndex = 3
        Me.Wizard1.FinishButtonText = "Uitvoeren"
        '
        '
        '
        Me.Wizard1.FooterStyle.BackColor = System.Drawing.SystemColors.Control
        Me.Wizard1.FooterStyle.BackColorGradientAngle = 90
        Me.Wizard1.FooterStyle.BorderBottomWidth = 1
        Me.Wizard1.FooterStyle.BorderColor = System.Drawing.SystemColors.Control
        Me.Wizard1.FooterStyle.BorderLeftWidth = 1
        Me.Wizard1.FooterStyle.BorderRightWidth = 1
        Me.Wizard1.FooterStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Etched
        Me.Wizard1.FooterStyle.BorderTopColor = System.Drawing.SystemColors.Control
        Me.Wizard1.FooterStyle.BorderTopWidth = 1
        Me.Wizard1.FooterStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.Wizard1.FooterStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.Wizard1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Wizard1.HeaderCaptionFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        '
        '
        '
        Me.Wizard1.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Wizard1.HeaderStyle.BackColorGradientAngle = 90
        Me.Wizard1.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Etched
        Me.Wizard1.HeaderStyle.BorderBottomWidth = 1
        Me.Wizard1.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control
        Me.Wizard1.HeaderStyle.BorderLeftWidth = 1
        Me.Wizard1.HeaderStyle.BorderRightWidth = 1
        Me.Wizard1.HeaderStyle.BorderTopWidth = 1
        Me.Wizard1.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.Wizard1.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.Wizard1.HelpButtonVisible = False
        Me.Wizard1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.Wizard1.Location = New System.Drawing.Point(0, 0)
        Me.Wizard1.Name = "Wizard1"
        Me.Wizard1.NextButtonText = "Volgende >"
        Me.Wizard1.Size = New System.Drawing.Size(647, 559)
        Me.Wizard1.TabIndex = 0
        Me.Wizard1.WizardPages.AddRange(New DevComponents.DotNetBar.WizardPage() {Me.WizardPage1, Me.WizardPage2, Me.WizardPage3})
        '
        'WizardPage1
        '
        Me.WizardPage1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WizardPage1.BackColor = System.Drawing.Color.White
        Me.WizardPage1.Controls.Add(Me.Label1)
        Me.WizardPage1.Controls.Add(Me.lblOmschrijving)
        Me.WizardPage1.Controls.Add(Me.Label3)
        Me.WizardPage1.InteriorPage = False
        Me.WizardPage1.Location = New System.Drawing.Point(0, 0)
        Me.WizardPage1.Name = "WizardPage1"
        Me.WizardPage1.Size = New System.Drawing.Size(647, 513)
        '
        '
        '
        Me.WizardPage1.Style.BackColor = System.Drawing.Color.White
        Me.WizardPage1.Style.BackgroundImage = CType(resources.GetObject("WizardPage1.Style.BackgroundImage"), System.Drawing.Image)
        Me.WizardPage1.Style.BackgroundImagePosition = DevComponents.DotNetBar.eStyleBackgroundImage.TopLeft
        Me.WizardPage1.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 16.0!)
        Me.Label1.Location = New System.Drawing.Point(210, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(423, 66)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Overgang van spaarplanner naar niet spaarplanner"
        '
        'lblOmschrijving
        '
        Me.lblOmschrijving.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOmschrijving.BackColor = System.Drawing.Color.Transparent
        Me.lblOmschrijving.Location = New System.Drawing.Point(210, 100)
        Me.lblOmschrijving.Name = "lblOmschrijving"
        Me.lblOmschrijving.Size = New System.Drawing.Size(422, 381)
        Me.lblOmschrijving.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(210, 490)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(185, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "om verder te gaan, druk volgende"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'WizardPage2
        '
        Me.WizardPage2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WizardPage2.AntiAlias = False
        Me.WizardPage2.Controls.Add(Me.lstSpaarplanners)
        Me.WizardPage2.Location = New System.Drawing.Point(7, 72)
        Me.WizardPage2.Name = "WizardPage2"
        Me.WizardPage2.PageDescription = "Lijst van de actieve spaarplanners"
        Me.WizardPage2.PageTitle = "Spaarplanners"
        Me.WizardPage2.Size = New System.Drawing.Size(633, 429)
        Me.WizardPage2.TabIndex = 8
        '
        'lstSpaarplanners
        '
        Me.lstSpaarplanners.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstSpaarplanners.FormattingEnabled = True
        Me.lstSpaarplanners.Location = New System.Drawing.Point(0, 0)
        Me.lstSpaarplanners.Name = "lstSpaarplanners"
        Me.lstSpaarplanners.Size = New System.Drawing.Size(633, 420)
        Me.lstSpaarplanners.TabIndex = 0
        '
        'WizardPage3
        '
        Me.WizardPage3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WizardPage3.AntiAlias = False
        Me.WizardPage3.Controls.Add(Me.Panel1)
        Me.WizardPage3.Location = New System.Drawing.Point(7, 72)
        Me.WizardPage3.Name = "WizardPage3"
        Me.WizardPage3.PageDescription = "over te dragen bedrag naar het niet spaarplanner wachtbestand"
        Me.WizardPage3.PageTitle = "Wachtbestand"
        Me.WizardPage3.Size = New System.Drawing.Size(633, 429)
        Me.WizardPage3.TabIndex = 9
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblWachtbestand)
        Me.Panel1.Controls.Add(Me.lblSom)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(633, 429)
        Me.Panel1.TabIndex = 1
        '
        'lblWachtbestand
        '
        Me.lblWachtbestand.AutoSize = True
        Me.lblWachtbestand.Location = New System.Drawing.Point(64, 161)
        Me.lblWachtbestand.Name = "lblWachtbestand"
        Me.lblWachtbestand.Size = New System.Drawing.Size(112, 13)
        Me.lblWachtbestand.TabIndex = 1
        Me.lblWachtbestand.Text = "bedrag over te dragen"
        '
        'lblSom
        '
        Me.lblSom.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSom.Location = New System.Drawing.Point(67, 186)
        Me.lblSom.Name = "lblSom"
        Me.lblSom.Size = New System.Drawing.Size(263, 24)
        Me.lblSom.TabIndex = 0
        '
        'frmNietSpaarplannerOvergang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(647, 559)
        Me.Controls.Add(Me.Wizard1)
        Me.Name = "frmNietSpaarplannerOvergang"
        Me.Text = "Wizard"
        Me.Wizard1.ResumeLayout(False)
        Me.WizardPage1.ResumeLayout(False)
        Me.WizardPage2.ResumeLayout(False)
        Me.WizardPage3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Wizard1 As DevComponents.DotNetBar.Wizard
    Friend WithEvents WizardPage2 As DevComponents.DotNetBar.WizardPage
    Friend WithEvents WizardPage3 As DevComponents.DotNetBar.WizardPage
    Friend WithEvents lstSpaarplanners As System.Windows.Forms.ListBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblWachtbestand As System.Windows.Forms.Label
    Friend WithEvents lblSom As DevComponents.DotNetBar.LabelX
    Friend WithEvents WizardPage1 As DevComponents.DotNetBar.WizardPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblOmschrijving As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
