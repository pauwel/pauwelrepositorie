﻿Public Class frmNietSpaarplannerOvergang

    Dim DTSpaarplanners As New DataTable
    Private werknemerid As Integer = 0
    Private saldo As Decimal = 0

    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'wizard omschrijving    

        Dim tekst As String = String.Concat("Deze wizard helpt u met overgangsproces van spaarplanner naar niet spaarplanner.", vbCrLf, vbCrLf, vbCrLf, "Volgende stappen werden in de procedure opgenomen", vbCrLf, vbCrLf, _
                                           "- weergave van alle spaarplanners", vbCrLf, _
                                           "- tonen van het bedrag in het wachtbestand van de geselecteerde spaarplanner")


        lblOmschrijving.Text = tekst

        'ophalen lijst van spaarplanners 
        Dim BLSpaarplanners As New BLL.BLWizardSpaarplannerConvertie
        DTSpaarplanners = BLSpaarplanners.BLGetSpaarplanners
        With lstSpaarplanners
            .DataSource = DTSpaarplanners
            .DisplayMember = "naam"
            .ValueMember = "WerknemerID"
        End With


    End Sub
 

    Private Sub WizardPage3_BeforePageDisplayed(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.WizardCancelPageChangeEventArgs) Handles WizardPage3.BeforePageDisplayed
        werknemerid = lstSpaarplanners.SelectedValue
        With WizardPage3
            Dim b As New BLL.InsertBetalingenSpaarplanner
            saldo = String.Concat((Math.Round(b.getsaldo(werknemerid), 2).ToString), " €")
            lblSom.Text = saldo
        End With
    End Sub

    Private Sub WizardPage3_FinishButtonClick(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles WizardPage3.FinishButtonClick

        'procedure uitvoeren
        Dim BLSpaarplanners As New BLL.BLWizardSpaarplannerConvertie
        If BLSpaarplanners.BLOvergangNaarNietSpaarplanner(werknemerid, saldo) Then
            MsgBox("de overgang werd successvol uitgevoerd", MsgBoxStyle.Information, "IMT Spaarplanner overgang")
            'update in de tijdelijke tabel
            BLSpaarplanners.BLUpdateStatuut(werknemerid)

            Me.Close()
        Else
            MsgBox("de overgang is gefaald", MsgBoxStyle.Critical, "IMT Spaarplanner overgang")
        End If
    End Sub
End Class