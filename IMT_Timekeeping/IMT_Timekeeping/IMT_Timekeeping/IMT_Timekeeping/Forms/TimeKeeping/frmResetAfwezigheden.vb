Public Class frmResetAfwezigheden


    Private Sub frmResetAfwezigheden_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        RefreshGrid()
    End Sub

    Private Sub RefreshGrid()
        Dim resetAfwezig As New BLL.ResetAfwezigheden
        dgvReset.DataSource = resetAfwezig.GetAfwezigheden
        dgvReset.Columns("prestatieid").Visible = False

        For Each c As DataGridViewColumn In dgvReset.Columns
            Select Case c.Name
                Case "is_afwezig"
                    c.ReadOnly = False
                Case Else
                    c.ReadOnly = True
            End Select
        Next



    End Sub

    

    Private Sub dgvReset_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReset.CellClick
        dgvReset.EndEdit()
        Try
            Dim id As Integer = dgvReset.Item("prestatieid", dgvReset.CurrentCellAddress.Y).Value
            Dim resetvalidatie As New BLL.UpdateAfwezig

            resetvalidatie.SetAfwezigheid(False, Nothing, False, False, id)
            RefreshGrid()
        Catch ex As Exception

        End Try

    End Sub
End Class