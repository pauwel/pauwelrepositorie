Option Compare Text
Public Delegate Sub NotifyProgress(ByVal Message As String, ByVal PercentComplete As Integer)
Public Class frmTimekeepingOverzicht

    Dim spwacht As New BLL.GetNotValidated
    Dim start As Boolean = False
    Private m_clsNotifyDelegate As NotifyProgress

    Dim thread As Threading.Thread



    Private Sub frmTimekeepingOverzicht_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start = True
        Me.WindowState = FormWindowState.Maximized

        Dim s As New BLL.isInRole
        If s.IsAdmin Then
            buttonHervalidatie.Enabled = True
        Else
            buttonHervalidatie.Enabled = False
        End If

        Dim lb As New BLL.GetLoonBoekInWachtbestand
        cmbLoonBoek.DataSource = lb.Getdata
        cmbLoonBoek.DisplayMember = "Loonboek"
        cmbLoonBoek.ValueMember = "werknemerid"
        cmbLoonBoek.SelectedIndex = -1


        dteBegin.Value = DateAdd(DateInterval.Month, -1, Now)
        dteEind.Value = Now

        
        Me.ProgressBarBatch.Visible = False
        start = False
        Me.refreshGrid()
    End Sub


    ''' <summary>
    ''' De data in de overzichts datagrid vernieuwen 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub refreshGrid()
        If Not start Then
            Dim b As New BindingSource

            Dim LB As Integer = cmbLoonBoek.SelectedValue
            Dim bdate As DateTime = dteBegin.Value.Date
            Dim edate As DateTime = dteEind.Value.Date

            b.DataSource = spwacht.Getdata(LB, bdate, edate)


            If cmbLoonBoek.SelectedValue > 0 Then
                If chkValidated.Checked Then
                    b.Filter = "validated='true' and werknemerid=" & cmbLoonBoek.SelectedValue

                End If
                If chkTeBetalen.Checked Then 'wachtbestand
                    b.Filter = "validated='true' And wordtuitbetaald='false' and werknemerid=" & cmbLoonBoek.SelectedValue
                End If
                If Not chkTeBetalen.Checked And Not chkValidated.Checked Then

                    b.Filter = "validated='false' and werknemerid=" & cmbLoonBoek.SelectedValue

                End If
                b.Sort = "datum asc"
            Else

                If chkValidated.Checked Then
                    b.Filter = "validated='true'"
                    b.Sort = "datum asc"
                End If
                If chkTeBetalen.Checked Then 'wachtbestand
                    b.Filter = "validated='true' And wordtuitbetaald='false' and (spaarplan='false' or spaarplan is null)"
                    b.Sort = "loonboek asc, datum asc"
                End If

                If chkWachtSpaarplanners.Checked Then 'wachtbestand
                    b.Filter = "validated='true' And wordtuitbetaald='false' and (spaarplan='true' or spaarplan is null)"
                    b.Sort = "loonboek asc, datum asc"
                End If

                If Not chkTeBetalen.Checked And Not chkValidated.Checked And Not chkWachtSpaarplanners.Checked Then
                    'b.Filter = "validated='false' And wordtuitbetaald='false'"
                    b.Filter = "validated='false'"
                    b.Sort = "datum asc"
                End If

            End If

            Me.dgvNotValidated.DataSource = b

            For Each c As DataGridViewColumn In dgvNotValidated.Columns
                Select Case c.Name
                    Case "Datum", "omschrijving", "VoyageNbr", "loonboek", "naam", "voornaam", "shift", "wbbrief", "voyagenbr", "wordtuitbetaald", "spaarplan", "catcode", "functiecode"
                    Case Else
                        c.Visible = False
                End Select

            Next
        End If

    End Sub

    ''' <summary>
    ''' Venster openen met de detailgegevens van het geselecteerde record
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadDetailForm()
        Dim id As Integer

        id = Me.dgvNotValidated.Item("WachtID", Me.dgvNotValidated.SelectedRows(0).Index).Value
       
        'id = 142
        Try
            Dim form As frmTimekeepingDetail = New frmTimekeepingDetail(id)
            form.ShowDialog()
            'enkel refresh indien er een actie gebeurde in het detail formulier
            If form.DialogResult = Windows.Forms.DialogResult.OK Then
                Me.refreshGrid()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub cmdViewDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdViewDetails.Click
        Me.loadDetailForm()
    End Sub

    Private Sub dgvNotValidated_RowHeaderMouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvNotValidated.CellMouseDoubleClick
        Me.loadDetailForm()
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        Me.refreshGrid()
    End Sub

    Private Sub dgvNotValidated_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvNotValidated.SelectionChanged

        Try
            dgvNotValidated.Rows(dgvNotValidated.CurrentCellAddress.Y).Selected = True
        Catch ex As ArgumentOutOfRangeException

        End Try

    End Sub

   

    Private Sub cmdLoonType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoonType.Click
        frmLoonType.ShowDialog()
    End Sub

    Private Sub chkValidated_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkValidated.CheckedChanged
        refreshGrid()
    End Sub

    Private Sub chkTeBetalen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTeBetalen.CheckedChanged
        If chkTeBetalen.Checked Then
            chkWachtSpaarplanners.Checked = False
            cmdVerlof.Enabled = True
        Else
            cmdVerlof.Enabled = False
        End If
        refreshGrid()
    End Sub

    Private Sub cmbLoonBoek_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbLoonBoek.KeyUp
        If e.KeyCode = Keys.Delete Then
            cmbLoonBoek.DroppedDown = False
            cmbLoonBoek.Text = Nothing
            cmbLoonBoek.SelectedIndex = -1

        End If
    End Sub

    Private Sub cmbLoonBoek_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLoonBoek.SelectedValueChanged
        Try
            refreshGrid()
        Catch ex As Exception

        End Try

    End Sub

  
    Private Sub dteBegin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteBegin.ValueChanged
        dteBegin.MaxDate = dteEind.Value
        dteEind.MinDate = dteBegin.Value


        refreshGrid()
    End Sub

    Private Sub dteEind_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteEind.ValueChanged
        dteBegin.MaxDate = dteEind.Value
        dteEind.MinDate = dteBegin.Value


              refreshGrid()
    End Sub

    Private Sub cmdVerlof_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVerlof.Click

        Dim naam As String = Nothing
        Dim voornaam As String = Nothing
        Dim WID As Integer = 0

        Dim dt As New DataTable
        For Each c As DataGridViewColumn In dgvNotValidated.Columns
            dt.Columns.Add(c.Name)
        Next

        Dim selectieOK As Boolean = True
       

        For Each r As DataGridViewRow In dgvNotValidated.SelectedRows
            If r.Selected Then
                Dim dr As DataRow = dt.NewRow

                For Each c As DataGridViewColumn In dgvNotValidated.Columns
                    
                    dr(c.Name) = dgvNotValidated.Item(c.Name, r.Index).Value.ToString
                    'check op geselecteerde records
                    If c.Name = "werknemerid" Then
                        If WID = 0 Then
                            WID = dgvNotValidated.Item("werknemerid", r.Index).Value.ToString
                        Else
                            If WID <> dgvNotValidated.Item("werknemerid", r.Index).Value Then
                                'FOUTE SELECTIE
                                MsgBox("meervoudige selectie van verschillende werknemers is NIET toegelaten", MsgBoxStyle.Exclamation)
                                dt.Clear()
                                selectieOK = False
                                Exit For
                            End If
                        End If
                    End If

                Next
                If selectieOK Then
                    dt.Rows.Add(dr)
                Else
                    Exit For
                End If

            End If

        Next

        If dt.Rows.Count > 0 Then
            naam = dgvNotValidated.Item("naam", dgvNotValidated.CurrentCellAddress.Y).Value.ToString
            voornaam = dgvNotValidated.Item("voornaam", dgvNotValidated.CurrentCellAddress.Y).Value.ToString
        Else
            naam = "Geen selectie"
        End If
        
        Try
            Dim form As Form = New frmVerlof(dt, voornaam & " " & naam)
            form.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub cmdNaarBetaling_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNaarBetaling.Click

        Dim b As New BindingSource

        Dim LB As Integer = cmbLoonBoek.SelectedValue
        Dim bdate As DateTime = DateAdd(DateInterval.Year, -1, dteEind.Value.Date)
        Dim edate As DateTime = dteEind.Value.Date



        Dim dt As DataTable = spwacht.Getdata(LB, bdate, edate) 'b.DataSource
        Dim x As Integer = 0



        For Each r As DataRow In dt.Select("datumUit is null and wordtuitbetaald=1 and validated=1")
            Dim wordtuitbetaald As Boolean
            Try
                wordtuitbetaald = r("wordtuitbetaald")
            Catch ex As Exception
                wordtuitbetaald = False
            End Try
            Dim gevalideerd As Boolean
            Try
                gevalideerd = r("validated")
            Catch ex As Exception
                gevalideerd = False
            End Try


            Try

                Dim d As DateTime = r("datumUit")
            Catch ex As InvalidCastException

                If wordtuitbetaald And gevalideerd Then
                    Dim br As New BLL.InsertBetalingenSpaarplanner

                    br.InsertBetalingsregels(r("wachtid"))

                End If
            End Try





        Next

        MsgBox("De gegevens van de NIET spaarplanners werden verzonden naar het betalingsbestand")


    End Sub

    Private Sub chkWachtSpaarplanners_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkWachtSpaarplanners.CheckedChanged


        If chkWachtSpaarplanners.Checked Then
            cmdVerlof.Enabled = False
            chkTeBetalen.Checked = False

        Else
            cmdVerlof.Enabled = False
        End If
        refreshGrid()

    End Sub
    Public Event onProgress(ByVal teller As Integer)

    Private Sub btnValideer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValideer.Click

        Dim aantal As Integer = dgvNotValidated.SelectedRows.Count
        Me.ProgressBarBatch.Maximum = aantal
        Me.ProgressBarBatch.Value = 0
        Me.ProgressBarBatch.Visible = True

        'thread = New Threading.Thread(AddressOf ValideerProcess)
        'thread.IsBackground = True
        'thread.Start()
        Me.ValideerProcess()
        Me.refreshGrid()
        Me.ProgressBarBatch.Visible = False
    End Sub

    Private Sub ValideerProcess()
        Dim valideer As BLL.WachtregelValidatie
        Dim wachtid As Integer
        Dim teller As Integer = 0
        For Each r As DataGridViewRow In Me.dgvNotValidated.SelectedRows
            Try
                wachtid = Me.dgvNotValidated.Item("wachtID", r.Index).Value.ToString
                valideer = New BLL.WachtregelValidatie(wachtid)
                valideer.valideerWachtRegel()
                teller += 1
                'NotifyUI(teller)
                ' Threading.Thread.Sleep(10)
                RaiseEvent onProgress(teller)

            Catch ex As Exception
                ' bij fouten gewoon doorgaan naar volgende regel.  
            End Try

        Next
    End Sub

    Private Sub NotifyUI(ByVal perc As Integer) Handles Me.onProgress
        Me.ProgressBarBatch.Value = perc
        'MsgBox(perc & " / " & Me.dgvNotValidated.SelectedRows.Count)
    End Sub

    Private Sub cmdWBbrief_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdWBbrief.Click


        frmPrintLoonbrief.ShowDialog()
        If frmPrintLoonbrief.DialogResult = Windows.Forms.DialogResult.OK Then
            _frmReports.Datum = dteEind.Value.Date

            _frmReports.Reportnaam = "WBALLNEW.rpt"
            _frmReports.ShowDialog()
        End If

    End Sub

   
    Private Sub buttonHervalidatie_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonHervalidatie.Click
        Dim f As New frmReleaseWachtbrief
        f.StartPosition = FormStartPosition.CenterScreen
        f.ShowDialog()
    End Sub
End Class