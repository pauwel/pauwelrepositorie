<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimekeepingOverzicht
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimekeepingOverzicht))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.cmdRefresh = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdLoonType = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdViewDetails = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdVerlof = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdNaarBetaling = New System.Windows.Forms.ToolStripButton
        Me.btnValideer = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.cmdWBbrief = New System.Windows.Forms.ToolStripButton
        Me.dgvNotValidated = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.fraFilter = New System.Windows.Forms.GroupBox
        Me.chkWachtSpaarplanners = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dteEind = New System.Windows.Forms.DateTimePicker
        Me.dteBegin = New System.Windows.Forms.DateTimePicker
        Me.chkTeBetalen = New System.Windows.Forms.CheckBox
        Me.chkValidated = New System.Windows.Forms.CheckBox
        Me.cmbLoonBoek = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.Label1 = New System.Windows.Forms.Label
        Me.ProgressBarBatch = New DevComponents.DotNetBar.Controls.ProgressBarX
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.buttonHervalidatie = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgvNotValidated, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraFilter.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdRefresh, Me.ToolStripSeparator1, Me.cmdLoonType, Me.ToolStripSeparator2, Me.cmdViewDetails, Me.ToolStripSeparator3, Me.cmdVerlof, Me.ToolStripSeparator4, Me.cmdNaarBetaling, Me.btnValideer, Me.ToolStripSeparator5, Me.cmdWBbrief, Me.ToolStripSeparator6, Me.buttonHervalidatie, Me.ToolStripSeparator7})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(987, 25)
        Me.ToolStrip1.TabIndex = 6
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Image = Global.IMT_Timekeeping.My.Resources.Resources.Refresh
        Me.cmdRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(66, 22)
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator1.Visible = False
        '
        'cmdLoonType
        '
        Me.cmdLoonType.Image = Global.IMT_Timekeeping.My.Resources.Resources.Link
        Me.cmdLoonType.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdLoonType.Name = "cmdLoonType"
        Me.cmdLoonType.Size = New System.Drawing.Size(80, 22)
        Me.cmdLoonType.Text = "LoonType"
        Me.cmdLoonType.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'cmdViewDetails
        '
        Me.cmdViewDetails.Image = Global.IMT_Timekeeping.My.Resources.Resources.EditInformationHS
        Me.cmdViewDetails.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdViewDetails.Name = "cmdViewDetails"
        Me.cmdViewDetails.Size = New System.Drawing.Size(62, 22)
        Me.cmdViewDetails.Text = "Details"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'cmdVerlof
        '
        Me.cmdVerlof.Enabled = False
        Me.cmdVerlof.Image = Global.IMT_Timekeeping.My.Resources.Resources.Home
        Me.cmdVerlof.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdVerlof.Name = "cmdVerlof"
        Me.cmdVerlof.Size = New System.Drawing.Size(58, 22)
        Me.cmdVerlof.Text = "Verlof"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'cmdNaarBetaling
        '
        Me.cmdNaarBetaling.Image = Global.IMT_Timekeeping.My.Resources.Resources.euro
        Me.cmdNaarBetaling.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdNaarBetaling.Name = "cmdNaarBetaling"
        Me.cmdNaarBetaling.Size = New System.Drawing.Size(174, 22)
        Me.cmdNaarBetaling.Text = "Stuur naar betalingsbestand"
        '
        'btnValideer
        '
        Me.btnValideer.Image = Global.IMT_Timekeeping.My.Resources.Resources.DatabaseEdit
        Me.btnValideer.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnValideer.Name = "btnValideer"
        Me.btnValideer.Size = New System.Drawing.Size(69, 22)
        Me.btnValideer.Text = "Valideer"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'cmdWBbrief
        '
        Me.cmdWBbrief.Image = Global.IMT_Timekeeping.My.Resources.Resources.Document
        Me.cmdWBbrief.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdWBbrief.Name = "cmdWBbrief"
        Me.cmdWBbrief.Size = New System.Drawing.Size(93, 22)
        Me.cmdWBbrief.Text = "Loonbrieven"
        '
        'dgvNotValidated
        '
        Me.dgvNotValidated.AllowUserToAddRows = False
        Me.dgvNotValidated.AllowUserToDeleteRows = False
        Me.dgvNotValidated.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvNotValidated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvNotValidated.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvNotValidated.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvNotValidated.Location = New System.Drawing.Point(0, 93)
        Me.dgvNotValidated.Name = "dgvNotValidated"
        Me.dgvNotValidated.ReadOnly = True
        Me.dgvNotValidated.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNotValidated.Size = New System.Drawing.Size(987, 513)
        Me.dgvNotValidated.TabIndex = 7
        '
        'fraFilter
        '
        Me.fraFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fraFilter.Controls.Add(Me.chkWachtSpaarplanners)
        Me.fraFilter.Controls.Add(Me.Label3)
        Me.fraFilter.Controls.Add(Me.Label2)
        Me.fraFilter.Controls.Add(Me.dteEind)
        Me.fraFilter.Controls.Add(Me.dteBegin)
        Me.fraFilter.Controls.Add(Me.chkTeBetalen)
        Me.fraFilter.Controls.Add(Me.chkValidated)
        Me.fraFilter.Controls.Add(Me.cmbLoonBoek)
        Me.fraFilter.Controls.Add(Me.Label1)
        Me.fraFilter.Location = New System.Drawing.Point(0, 28)
        Me.fraFilter.Name = "fraFilter"
        Me.fraFilter.Size = New System.Drawing.Size(985, 59)
        Me.fraFilter.TabIndex = 8
        Me.fraFilter.TabStop = False
        Me.fraFilter.Text = "Filter"
        '
        'chkWachtSpaarplanners
        '
        Me.chkWachtSpaarplanners.AutoSize = True
        Me.chkWachtSpaarplanners.Location = New System.Drawing.Point(788, 33)
        Me.chkWachtSpaarplanners.Name = "chkWachtSpaarplanners"
        Me.chkWachtSpaarplanners.Size = New System.Drawing.Size(165, 17)
        Me.chkWachtSpaarplanners.TabIndex = 8
        Me.chkWachtSpaarplanners.Text = "Wachtbestand spaarplanners"
        Me.chkWachtSpaarplanners.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(253, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Einddatum"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(49, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Begindatum"
        '
        'dteEind
        '
        Me.dteEind.Location = New System.Drawing.Point(256, 30)
        Me.dteEind.Name = "dteEind"
        Me.dteEind.Size = New System.Drawing.Size(200, 20)
        Me.dteEind.TabIndex = 5
        '
        'dteBegin
        '
        Me.dteBegin.Location = New System.Drawing.Point(50, 30)
        Me.dteBegin.Name = "dteBegin"
        Me.dteBegin.Size = New System.Drawing.Size(200, 20)
        Me.dteBegin.TabIndex = 4
        '
        'chkTeBetalen
        '
        Me.chkTeBetalen.AutoSize = True
        Me.chkTeBetalen.Location = New System.Drawing.Point(686, 33)
        Me.chkTeBetalen.Name = "chkTeBetalen"
        Me.chkTeBetalen.Size = New System.Drawing.Size(96, 17)
        Me.chkTeBetalen.TabIndex = 3
        Me.chkTeBetalen.Text = "Wachtbestand"
        Me.chkTeBetalen.UseVisualStyleBackColor = True
        '
        'chkValidated
        '
        Me.chkValidated.AutoSize = True
        Me.chkValidated.Location = New System.Drawing.Point(597, 33)
        Me.chkValidated.Name = "chkValidated"
        Me.chkValidated.Size = New System.Drawing.Size(83, 17)
        Me.chkValidated.TabIndex = 2
        Me.chkValidated.Text = "Gevalideerd"
        Me.chkValidated.UseVisualStyleBackColor = True
        '
        'cmbLoonBoek
        '
        Me.cmbLoonBoek.DisplayMember = "Text"
        Me.cmbLoonBoek.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbLoonBoek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLoonBoek.FormattingEnabled = True
        Me.cmbLoonBoek.ItemHeight = 14
        Me.cmbLoonBoek.Location = New System.Drawing.Point(462, 30)
        Me.cmbLoonBoek.Name = "cmbLoonBoek"
        Me.cmbLoonBoek.Size = New System.Drawing.Size(121, 20)
        Me.cmbLoonBoek.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(459, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Loonboek"
        '
        'ProgressBarBatch
        '
        Me.ProgressBarBatch.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ProgressBarBatch.Location = New System.Drawing.Point(0, 612)
        Me.ProgressBarBatch.Name = "ProgressBarBatch"
        Me.ProgressBarBatch.Size = New System.Drawing.Size(987, 23)
        Me.ProgressBarBatch.TabIndex = 9
        Me.ProgressBarBatch.TabStop = False
        Me.ProgressBarBatch.Text = "Bezig met valideren... "
        Me.ProgressBarBatch.TextVisible = True
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 25)
        '
        'buttonHervalidatie
        '
        Me.buttonHervalidatie.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.buttonHervalidatie.Image = CType(resources.GetObject("buttonHervalidatie.Image"), System.Drawing.Image)
        Me.buttonHervalidatie.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.buttonHervalidatie.Name = "buttonHervalidatie"
        Me.buttonHervalidatie.Size = New System.Drawing.Size(133, 22)
        Me.buttonHervalidatie.Text = "Hervalideer na betaling"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'frmTimekeepingOverzicht
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(987, 635)
        Me.Controls.Add(Me.ProgressBarBatch)
        Me.Controls.Add(Me.fraFilter)
        Me.Controls.Add(Me.dgvNotValidated)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmTimekeepingOverzicht"
        Me.Text = "frmTimekeepingOverzicht"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgvNotValidated, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraFilter.ResumeLayout(False)
        Me.fraFilter.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdRefresh As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdLoonType As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdViewDetails As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvNotValidated As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents fraFilter As System.Windows.Forms.GroupBox
    Friend WithEvents cmbLoonBoek As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkTeBetalen As System.Windows.Forms.CheckBox
    Friend WithEvents chkValidated As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dteEind As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteBegin As System.Windows.Forms.DateTimePicker
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdVerlof As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdNaarBetaling As System.Windows.Forms.ToolStripButton
    Friend WithEvents chkWachtSpaarplanners As System.Windows.Forms.CheckBox
    Friend WithEvents btnValideer As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdWBbrief As System.Windows.Forms.ToolStripButton
    Friend WithEvents ProgressBarBatch As DevComponents.DotNetBar.Controls.ProgressBarX
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents buttonHervalidatie As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
End Class
