

Public Class frmSetCEPA


    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim update As New BLL.UpdateWachtbestand
        update.UpdateGegevens(dteCEPA.Value.Date)

        CEPAdatum = dteCEPA.Value.Date
        prestatieperiode = dtePrestatie.Value.Date
        frmWeekOverzicht.LoadGrid()
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub frmSetCEPA_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dteCEPA.MaxDate = Now
        dtePrestatie.MaxDate = Now
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

   
End Class