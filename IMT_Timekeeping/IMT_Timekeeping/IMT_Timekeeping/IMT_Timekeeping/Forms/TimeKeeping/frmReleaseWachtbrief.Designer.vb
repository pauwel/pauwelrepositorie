﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReleaseWachtbrief
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.cmbWerknemers = New System.Windows.Forms.ComboBox
        Me.dgvPrestaties = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.btnUitvoeren = New System.Windows.Forms.Button
        Me.btnSluiten = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.dgvPrestaties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbWerknemers
        '
        Me.cmbWerknemers.FormattingEnabled = True
        Me.cmbWerknemers.Location = New System.Drawing.Point(12, 25)
        Me.cmbWerknemers.Name = "cmbWerknemers"
        Me.cmbWerknemers.Size = New System.Drawing.Size(332, 21)
        Me.cmbWerknemers.TabIndex = 0
        '
        'dgvPrestaties
        '
        Me.dgvPrestaties.AllowUserToAddRows = False
        Me.dgvPrestaties.AllowUserToDeleteRows = False
        Me.dgvPrestaties.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvPrestaties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPrestaties.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPrestaties.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvPrestaties.Location = New System.Drawing.Point(0, 64)
        Me.dgvPrestaties.Name = "dgvPrestaties"
        Me.dgvPrestaties.ReadOnly = True
        Me.dgvPrestaties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPrestaties.Size = New System.Drawing.Size(814, 500)
        Me.dgvPrestaties.TabIndex = 1
        '
        'btnUitvoeren
        '
        Me.btnUitvoeren.Location = New System.Drawing.Point(360, 23)
        Me.btnUitvoeren.Name = "btnUitvoeren"
        Me.btnUitvoeren.Size = New System.Drawing.Size(75, 23)
        Me.btnUitvoeren.TabIndex = 2
        Me.btnUitvoeren.Text = "Uitvoeren"
        Me.btnUitvoeren.UseVisualStyleBackColor = True
        '
        'btnSluiten
        '
        Me.btnSluiten.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSluiten.Location = New System.Drawing.Point(452, 23)
        Me.btnSluiten.Name = "btnSluiten"
        Me.btnSluiten.Size = New System.Drawing.Size(75, 23)
        Me.btnSluiten.TabIndex = 3
        Me.btnSluiten.Text = "Sluiten"
        Me.btnSluiten.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "werknemers"
        '
        'frmReleaseWachtbrief
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnSluiten
        Me.ClientSize = New System.Drawing.Size(815, 564)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSluiten)
        Me.Controls.Add(Me.btnUitvoeren)
        Me.Controls.Add(Me.dgvPrestaties)
        Me.Controls.Add(Me.cmbWerknemers)
        Me.Name = "frmReleaseWachtbrief"
        Me.Text = "Verwijderen van Loonbriefjes na betaling "
        CType(Me.dgvPrestaties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbWerknemers As System.Windows.Forms.ComboBox
    Friend WithEvents dgvPrestaties As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents btnUitvoeren As System.Windows.Forms.Button
    Friend WithEvents btnSluiten As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
