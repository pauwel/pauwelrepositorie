
Option Compare Text
Public Class frmInvoerLoonSpaarplanner

    Private catid As Integer = 0
    Private minLoon As Decimal = 0

    Dim betaalregel As New BLL.BetaalRegel

    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean

    Private Sub frmInvoerLoonSpaarplanner_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        LockWindowUpdate(Me.Handle.ToInt64)

        Me.WindowState = FormWindowState.Maximized
        Dim sp As New BLL.GetSpaarplanners

        With cmbLoonboek
            .DataSource = sp.GetSpaarplanners
            .DisplayMember = sp.GetSpaarplanners.naamColumn.ColumnName.ToString
            .ValueMember = "WerknemerID"

        End With

        Dim sh As New BLL.GetShiftenToonInPLanning ' BLL.Shiften
        With cmbShift
            .DataSource = sh.GetShiften
            .DisplayMember = "omschrijving"
            .ValueMember = "shiftid"
        End With
        LockWindowUpdate(0)

    End Sub


    Private Sub cmbLoonboek_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLoonboek.SelectedValueChanged
        Dim naam As New BLL.clsWerknemersnaam
        Dim b As New BLL.InsertBetalingenSpaarplanner
        Try
            lblNaam.Text = (naam.GetNaam(cmbLoonboek.SelectedValue).ToString)

            Dim cat As New BLL.GetCategory
            catid = cat.GetCategory(cmbLoonboek.SelectedValue)

            lblSaldo.Text = "Saldo wachtbestand: � " & Math.Round(b.getsaldo(cmbLoonboek.SelectedValue), 2)
            refreshgrid()
        Catch ex As InvalidCastException
            lblNaam.Text = Nothing
            catid = 0
        Catch n As NullReferenceException
            lblNaam.Text = Nothing
            catid = 0
        End Try

    End Sub



    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        InsertBetaling()

    End Sub

    Dim stdloon As Decimal

    Private Sub InsertBetaling()
        Dim b As New BLL.InsertBetalingenSpaarplanner
        If navalideren() Then


            b.InsertBetalingSpaarplanner(dteBetaling.Value.Date, txtBedrag.Text, cmbShift.SelectedValue, cmbLoonboek.SelectedValue, stdloon)

            dteBetaling.Value = DateAdd(DateInterval.Day, 1, dteBetaling.Value)
            cmbShift.Select()

            lblSaldo.Text = "Saldo wachtbestand: � " & Math.Round(b.getsaldo(cmbLoonboek.SelectedValue), 2)
            refreshgrid()

        End If
    End Sub

    Private Function navalideren() As Boolean
        Dim b As New BLL.InsertBetalingenSpaarplanner

        With betaalregel
            .Saldo = b.getsaldo(cmbLoonboek.SelectedValue)
            .Werknemerid = cmbLoonboek.SelectedValue
            .datum = dteBetaling.Value.Date
            .Shift = cmbShift.SelectedValue
            .gevraagdloon = txtBedrag.Text
            If .validate Then
                stdloon = .StdLoon
                Return True

            Else
                Return False
            End If


        End With

       
    End Function


    Private Sub refreshgrid()
        LockWindowUpdate(Me.Handle.ToInt64)
        If cmbLoonboek.SelectedValue > 0 Then

            Dim d As New BLL.getBetalingPerWerknemer
            dgvBetalingen.DataSource = d.GetBetalingGegevens(cmbLoonboek.SelectedValue)
            dgvBetalingen.Columns("betaalid").Visible = False
        End If

        LockWindowUpdate(0)
    End Sub

    Private Sub txtBedrag_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBedrag.KeyUp
        If e.KeyCode = Keys.Enter Then
            InsertBetaling()
            txtBedrag.Text = 0
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click


        Dim betaalid As Integer = dgvBetalingen.Item("betaalid", dgvBetalingen.CurrentCellAddress.Y).Value.ToString
        Dim d As BLL.InsertBetalingenSpaarplanner = New BLL.InsertBetalingenSpaarplanner

        Dim i As Integer = d.DeleteBetalingsregel(betaalid)
        If i = 0 Then
            MsgBox("De betaling werd reeds uitgevoerd, record kon niet verwijdert worden", MsgBoxStyle.Information)
        Else
            Dim b As New BLL.InsertBetalingenSpaarplanner
            lblSaldo.Text = "Saldo wachtbestand: � " & Math.Round(b.getsaldo(cmbLoonboek.SelectedValue), 2)
            refreshgrid()
        End If

    End Sub


End Class