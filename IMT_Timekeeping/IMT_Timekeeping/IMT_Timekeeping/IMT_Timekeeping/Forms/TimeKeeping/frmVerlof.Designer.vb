<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVerlof
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dteVerlof = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtBedrag = New NumericBox.NumericBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblNaam = New DevComponents.DotNetBar.PanelEx
        Me.Bar1 = New DevComponents.DotNetBar.Bar
        Me.cmdSave = New DevComponents.DotNetBar.ButtonItem
        Me.lblSaldo = New DevComponents.DotNetBar.Controls.ReflectionLabel
        Me.dgvVerlof = New DevComponents.DotNetBar.Controls.DataGridViewX
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVerlof, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dteVerlof
        '
        Me.dteVerlof.Location = New System.Drawing.Point(12, 57)
        Me.dteVerlof.Name = "dteVerlof"
        Me.dteVerlof.Size = New System.Drawing.Size(200, 20)
        Me.dteVerlof.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Verlofdatum"
        '
        'txtBedrag
        '
        Me.txtBedrag.BackColor = System.Drawing.SystemColors.Window
        Me.txtBedrag.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtBedrag.Location = New System.Drawing.Point(218, 57)
        Me.txtBedrag.Name = "txtBedrag"
        Me.txtBedrag.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtBedrag.Size = New System.Drawing.Size(80, 20)
        Me.txtBedrag.TabIndex = 6
        Me.txtBedrag.Text = "0,00"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(215, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "bedrag"
        '
        'lblNaam
        '
        Me.lblNaam.CanvasColor = System.Drawing.SystemColors.Control
        Me.lblNaam.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.lblNaam.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblNaam.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNaam.Location = New System.Drawing.Point(0, 374)
        Me.lblNaam.Name = "lblNaam"
        Me.lblNaam.Size = New System.Drawing.Size(647, 31)
        Me.lblNaam.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.lblNaam.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.lblNaam.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.lblNaam.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.lblNaam.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.lblNaam.Style.GradientAngle = 90
        Me.lblNaam.TabIndex = 8
        Me.lblNaam.Text = "Naam onbekend"
        '
        'Bar1
        '
        Me.Bar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bar1.DockTabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Top
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdSave})
        Me.Bar1.Location = New System.Drawing.Point(0, 0)
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(647, 25)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.Bar1.TabIndex = 9
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'cmdSave
        '
        Me.cmdSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdSave.Image = Global.IMT_Timekeeping.My.Resources.Resources.saveHS
        Me.cmdSave.ImagePaddingHorizontal = 8
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS)
        Me.cmdSave.Text = "Opslaan"
        '
        'lblSaldo
        '
        Me.lblSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldo.ForeColor = System.Drawing.Color.Green
        Me.lblSaldo.Location = New System.Drawing.Point(364, 31)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(283, 46)
        Me.lblSaldo.TabIndex = 10
        Me.lblSaldo.Text = "Saldo onbekend"
        '
        'dgvVerlof
        '
        Me.dgvVerlof.AllowUserToAddRows = False
        Me.dgvVerlof.AllowUserToDeleteRows = False
        Me.dgvVerlof.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvVerlof.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvVerlof.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvVerlof.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvVerlof.Location = New System.Drawing.Point(0, 83)
        Me.dgvVerlof.Name = "dgvVerlof"
        Me.dgvVerlof.ReadOnly = True
        Me.dgvVerlof.Size = New System.Drawing.Size(647, 291)
        Me.dgvVerlof.TabIndex = 11
        '
        'frmVerlof
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(647, 405)
        Me.Controls.Add(Me.dgvVerlof)
        Me.Controls.Add(Me.lblSaldo)
        Me.Controls.Add(Me.Bar1)
        Me.Controls.Add(Me.lblNaam)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBedrag)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dteVerlof)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVerlof"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Invoer verlof"
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVerlof, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dteVerlof As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtBedrag As NumericBox.NumericBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblNaam As DevComponents.DotNetBar.PanelEx
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdSave As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents lblSaldo As DevComponents.DotNetBar.Controls.ReflectionLabel
    Friend WithEvents dgvVerlof As DevComponents.DotNetBar.Controls.DataGridViewX
End Class
