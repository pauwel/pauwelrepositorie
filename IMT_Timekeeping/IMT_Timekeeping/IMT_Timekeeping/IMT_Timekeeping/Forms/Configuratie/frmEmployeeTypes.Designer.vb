<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeTypes
    Inherits IMT_Timekeeping.frmBaseConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtSort = New NumericBox.NumericBox
        Me.ChkVaste = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtStelsel = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(342, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Sorteersleutel"
        '
        'txtSort
        '
        Me.txtSort.BackColor = System.Drawing.SystemColors.Window
        Me.txtSort.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSort.Location = New System.Drawing.Point(345, 94)
        Me.txtSort.Name = "txtSort"
        Me.txtSort.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSort.NoDigitsAfterDecimalSymbol = 0
        Me.txtSort.Size = New System.Drawing.Size(43, 20)
        Me.txtSort.TabIndex = 19
        Me.txtSort.Text = "0"
        '
        'ChkVaste
        '
        Me.ChkVaste.AutoSize = True
        Me.ChkVaste.Location = New System.Drawing.Point(208, 94)
        Me.ChkVaste.Name = "ChkVaste"
        Me.ChkVaste.Size = New System.Drawing.Size(111, 17)
        Me.ChkVaste.TabIndex = 16
        Me.ChkVaste.Text = "Vaste Werknemer"
        Me.ChkVaste.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 78)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Type Werknemer"
        '
        'txtStelsel
        '
        Me.txtStelsel.Location = New System.Drawing.Point(12, 94)
        Me.txtStelsel.MaxLength = 20
        Me.txtStelsel.Name = "txtStelsel"
        Me.txtStelsel.Size = New System.Drawing.Size(190, 20)
        Me.txtStelsel.TabIndex = 15
        '
        'frmEmployeeTypes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(584, 464)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtSort)
        Me.Controls.Add(Me.ChkVaste)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtStelsel)
        Me.Name = "frmEmployeeTypes"
        Me.Text = "Configuratie - Type Werknemers -"
        Me.Controls.SetChildIndex(Me.txtStelsel, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.ChkVaste, 0)
        Me.Controls.SetChildIndex(Me.txtSort, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSort As NumericBox.NumericBox
    Friend WithEvents ChkVaste As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtStelsel As System.Windows.Forms.TextBox

End Class
