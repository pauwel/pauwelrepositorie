

Option Compare Text

Public Class frmShifts

    Private menubar As clsMenuBar

    Private rijen As New ArrayList

    Private shift As String = Nothing, vergoeding As Decimal = 0, volgorde As Int16 = 0, tooninplanning As Boolean = False, verwijderen As Boolean = False
    Private id As Int16
    Private blnEdit As Boolean = False

    Private Sub frmTest_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        clear()
        refreshgrid()


    End Sub
    Public Overrides Sub initform()

    End Sub

    Protected Overrides Sub onCmdReportClick()
        MyBase.onCmdReportClick()
        _frmReports.Reportnaam = "shiften.rpt"
        _frmReports.ShowDialog()
    End Sub

    Private Sub clear()
        txtShift.Clear()
        txtShift.Select()
        txtVergoeding.Text = 0
        txtSort.Text = 0

        chkToonInPlanning.Checked = False
    End Sub

    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()

        With Me.dgView
            .DataSource = clBLL_Shiften.GetShift
            .Columns("shiftid").Visible = False
            .Columns("verwijderen").Visible = False
            If .Rows.Count > 0 Then
                .Rows(0).Selected = True
            End If
        End With
    End Sub

    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()


        shift = txtShift.Text
        vergoeding = txtVergoeding.Text
        volgorde = txtSort.Text
        tooninplanning = chkToonInPlanning.CheckState
        Try
            If Not String.IsNullOrEmpty(shift) Then
                clBLL_Shiften.InsertShift(shift, vergoeding, volgorde, tooninplanning, False)

                clsPreload.Shift = clBLL_Shiften.GetShift
                refreshgrid()
            End If

        Catch ex As Exception

        End Try
        clear()

    End Sub

    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()
        GetSelectedRows()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                Try
                    clBLL_Shiften.DeleteShift(id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next

            clsPreload.Shift = clBLL_Shiften.GetShift
        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()
        menubar.Update()
        blnEdit = True

        id = dgView.Item(clBLL_Shiften.GetShift.ShiftIDColumn.ColumnName, dgView.CurrentCellAddress.Y).Value

        shift = dgView.Item(clBLL_Shiften.GetShift.OmschrijvingColumn.ColumnName, dgView.CurrentCellAddress.Y).Value
        vergoeding = dgView.Item(clBLL_Shiften.GetShift.VergoedingColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()
        volgorde = dgView.Item(clBLL_Shiften.GetShift.SorteervolgordeColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()
        tooninplanning = dgView.Item(clBLL_Shiften.GetShift.ToonInPlanningColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()
        verwijderen = dgView.Item(clBLL_Shiften.GetShift.verwijderenColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()

        txtShift.Text = shift
        txtVergoeding.Text = vergoeding
        txtSort.Text = volgorde
        chkToonInPlanning.Checked = tooninplanning


    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()
        menubar.Load()

        shift = txtShift.Text
        vergoeding = txtVergoeding.Text
        volgorde = txtSort.Text
        tooninplanning = chkToonInPlanning.CheckState
        Try
            clBLL_Shiften.UpdateShift(shift, vergoeding, volgorde, tooninplanning, verwijderen, id)
            clsPreload.Shift = clBLL_Shiften.GetShift
            clear()
        Catch ex As Exception

        End Try
    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try

    End Sub




    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item(clsPreload.shiftvaluemember, dr.Index).Value)
        Next
        'TODO boodschap weergeven, indien er geen rijen geselecteerd werden
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    clBLL_Shiften.DeleteShift(id)
                Next

                clsPreload.Shift = clBLL_Shiften.GetShift
            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub txtShift_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtShift.TextChanged


        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtShift.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If


    End Sub

    Private Sub txtSort_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSort.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub
End Class
