Public Class frmFeestdagen
    Private menubar As clsMenuBar

    Private rijen As New ArrayList
    Private feestomschrijving As String, datum As DateTime
    Private id As Int16

    Private blnEdit As Boolean = False

    Private Sub frmFeestdagen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        clear()
        refreshgrid()
    End Sub

    
    Public Overrides Sub initform()

    End Sub

    Private Sub clear()
        txtFeestdag.Clear()
       
    End Sub

    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()

        With Me.dgView
            .DataSource = clBLL_feestdagen.getdata
            .Columns("feestid").Visible = False
        End With
    End Sub

    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()

        feestomschrijving = txtFeestdag.Text
        datum = dteFeestdag.Value
        Try
            If Not String.IsNullOrEmpty(Trim(feestomschrijving)) Then
                clBLL_feestdagen.InsertFeestdagen(feestomschrijving, datum)


                refreshgrid()
            End If

        Catch ex As Exception

        End Try
        clear()

    End Sub
    Protected Overrides Sub onCmdReportClick()
        MyBase.onCmdReportClick()

        _frmReports.Reportnaam = "feestdagen.rpt"
        _frmReports.ShowDialog()
    End Sub
    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()
        GetSelectedRows()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                clBLL_feestdagen.deleteFeestdagen(id)
            Next


        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()



        id = dgView.Item(clBLL_feestdagen.getdata.FeestIDColumn.ColumnName, dgView.CurrentCellAddress.Y).Value

        feestomschrijving = dgView.Item("feestdag", dgView.CurrentCellAddress.Y).Value
        dteFeestdag.Value = dgView.Item("datum", dgView.CurrentCellAddress.Y).Value
        txtFeestdag.Text = feestomschrijving

        menubar.Update()
        blnEdit = True
    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()
        menubar.Load()


        feestomschrijving = txtFeestdag.Text
        datum = dteFeestdag.Value

        Try
            clBLL_feestdagen.UpdateFeestdagen(feestomschrijving, datum, id)

            clear()
        Catch ex As Exception

        End Try
    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try

    End Sub



    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item("feestid", dr.Index).Value)
        Next
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    clBLL_feestdagen.deleteFeestdagen(id)
                Next


            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub txtFeestdag_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFeestdag.TextChanged

        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtFeestdag.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If

    End Sub
    
    Private Sub dteFeestdag_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dteFeestdag.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub
End Class
