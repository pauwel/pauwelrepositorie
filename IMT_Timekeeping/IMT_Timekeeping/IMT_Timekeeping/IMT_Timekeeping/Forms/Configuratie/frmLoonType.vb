
Option Compare Text

Public Class frmLoonType
    Private menubar As clsMenuBar

    Private rijen As New ArrayList
    Private omschrijving As String
    Private id As Int16

    Private blnEdit As Boolean = False


    Dim LoonType As New BLL.LoonType

    Private Sub frmLoonType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        clear()
        refreshgrid()
    End Sub

    Public Overrides Sub initform()

    End Sub

    Private Sub clear()
        txtOmschrijving.Clear()
        chkActief.Checked = True
        chkCEPAverplicht.Checked = False
        blnEdit = False
    End Sub

    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()

        With Me.dgView
            .DataSource = LoonType.getLoontype
            .Columns("special").Visible = False
            .Columns("loontypeid").Visible = False
        End With
    End Sub

    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()

        omschrijving = Trim(txtOmschrijving.Text)

        Try
            If Not String.IsNullOrEmpty(omschrijving) Then
                LoonType.insertloonType(omschrijving, False, chkActief.Checked, chkCEPAverplicht.Checked, chkUrenInvoer.Checked)

                refreshgrid()
            End If

        Catch ex As Exception

        End Try
        clear()

    End Sub

    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()
        GetSelectedRows()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                LoonType.DeleteloonType(id)
            Next


        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()



        id = dgView.Item("loontypeid", dgView.CurrentCellAddress.Y).Value

        omschrijving = dgView.Item("omschrijving", dgView.CurrentCellAddress.Y).Value

        txtOmschrijving.Text = omschrijving
        chkActief.Checked = dgView.Item("actief", dgView.CurrentCellAddress.Y).Value
        chkCEPAverplicht.Checked = dgView.Item("codeverplicht", dgView.CurrentCellAddress.Y).Value
        chkUrenInvoer.Checked = dgView.Item("UrenInvoer", dgView.CurrentCellAddress.Y).Value
        chkcode30Weergave.Checked = dgView.Item("code30", dgView.CurrentCellAddress.Y).Value
        menubar.Update()
        blnEdit = True
    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()
        menubar.Load()


        omschrijving = txtOmschrijving.Text

        Try

            LoonType.UpdateloonType(omschrijving, False, chkActief.Checked, chkCEPAverplicht.Checked, chkUrenInvoer.Checked, id)
            clear()
        Catch ex As Exception

        End Try
    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try

    End Sub



    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item("loontypeid", dr.Index).Value)
        Next
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    LoonType.DeleteloonType(id)
                Next


            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub txtomschrijving_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOmschrijving.TextChanged

        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtOmschrijving.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If

    End Sub

End Class
