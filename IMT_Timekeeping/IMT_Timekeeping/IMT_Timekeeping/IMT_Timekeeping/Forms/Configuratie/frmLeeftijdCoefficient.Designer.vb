<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeeftijdCoefficient
    Inherits IMT_Timekeeping.frmBaseConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtLeeftijd = New NumericBox.NumericBox
        Me.txtCoefficient = New NumericBox.NumericBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Leeftijd"
        '
        'txtLeeftijd
        '
        Me.txtLeeftijd.BackColor = System.Drawing.SystemColors.Window
        Me.txtLeeftijd.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtLeeftijd.Location = New System.Drawing.Point(12, 72)
        Me.txtLeeftijd.Name = "txtLeeftijd"
        Me.txtLeeftijd.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtLeeftijd.NoDigitsAfterDecimalSymbol = 0
        Me.txtLeeftijd.Size = New System.Drawing.Size(53, 20)
        Me.txtLeeftijd.TabIndex = 7
        Me.txtLeeftijd.Text = "0"
        '
        'txtCoefficient
        '
        Me.txtCoefficient.BackColor = System.Drawing.SystemColors.Window
        Me.txtCoefficient.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtCoefficient.Location = New System.Drawing.Point(71, 72)
        Me.txtCoefficient.Name = "txtCoefficient"
        Me.txtCoefficient.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtCoefficient.Size = New System.Drawing.Size(87, 20)
        Me.txtCoefficient.TabIndex = 8
        Me.txtCoefficient.Text = "0,00"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(68, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Co�fficient"
        '
        'frmLeeftijdCoefficient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(584, 464)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtCoefficient)
        Me.Controls.Add(Me.txtLeeftijd)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmLeeftijdCoefficient"
        Me.Text = "Leeftijd co�fficient"
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.txtLeeftijd, 0)
        Me.Controls.SetChildIndex(Me.txtCoefficient, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtLeeftijd As NumericBox.NumericBox
    Friend WithEvents txtCoefficient As NumericBox.NumericBox
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
