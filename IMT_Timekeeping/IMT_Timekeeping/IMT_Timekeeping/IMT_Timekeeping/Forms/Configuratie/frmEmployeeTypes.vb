Public Class frmEmployeeTypes
    Private menubar As clsMenuBar

    Private rijen As New ArrayList

    Private stelsel As String = Nothing, volgorde As Int16 = 0, vaste As Boolean = False
    Private id As Int16

    Private blnEdit As Boolean = False


    Private Sub frmEmployeeTypes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mb As New clsMenuBar(cmdAdd, cmdEdit, cmdUpdate, cmdDelete)
        menubar = mb
        menubar.Load()
        clear()
        refreshgrid()
    End Sub

   
    Protected Overrides Sub onCmdReportClick()
        MyBase.onCmdReportClick()

        _frmReports.Reportnaam = "employeetypes.rpt"
        _frmReports.ShowDialog()
    End Sub

    Public Overrides Sub initform()

    End Sub

    Private Sub clear()
        txtStelsel.Clear()

        txtSort.Text = 0
        ChkVaste.Checked = False
        blnEdit = False
    End Sub

    Protected Overrides Sub refreshgrid()
        MyBase.refreshgrid()

        With Me.dgView
            .DataSource = clsPreload.employeetypes
            .Columns(clsPreload.employeetypesvaluemember).Visible = False
        End With
    End Sub

    Protected Overrides Sub onBtnAddClick()
        MyBase.onBtnAddClick()
        menubar.Load()

        blnEdit = True
        stelsel = txtStelsel.Text

        volgorde = txtSort.Text
        vaste = ChkVaste.CheckState
        Try
            If Not String.IsNullOrEmpty(stelsel) Then
                clBLL_TypeWerknemers.InsertWerknemersType(stelsel, volgorde, vaste)

                clsPreload.employeetypes = clBLL_TypeWerknemers.GetWerknemersType
                refreshgrid()
            End If

        Catch ex As Exception
        Finally
            clear()
        End Try


    End Sub

    Protected Overrides Sub onBtnDeleteClick()
        MyBase.onBtnDeleteClick()
        GetSelectedRows()

        Try
            For dr As Integer = 0 To rijen.Count - 1
                id = rijen(dr)
                Try
                    clBLL_TypeWerknemers.DeleteWerknemersType(id)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next

            clsPreload.employeetypes = clBLL_TypeWerknemers.GetWerknemersType

        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub onBtnEditClick()
        MyBase.onBtnEditClick()
        

        id = dgView.Item(clBLL_TypeWerknemers.GetWerknemersType.TypeWerknemerIDColumn.ColumnName, dgView.CurrentCellAddress.Y).Value

        stelsel = dgView.Item(clBLL_TypeWerknemers.GetWerknemersType.StelselColumn.ColumnName, dgView.CurrentCellAddress.Y).Value
        '   vergoeding = dgView.Item(clBLL_TypeWerknemers.GetWerknemersType.ProcVergoedingColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()
        volgorde = dgView.Item(clBLL_TypeWerknemers.GetWerknemersType.SorteersleutelColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()
        vaste = dgView.Item(clBLL_TypeWerknemers.GetWerknemersType.VastIndienstColumn.ColumnName, dgView.CurrentCellAddress.Y).Value()


        txtStelsel.Text = stelsel

        txtSort.Text = volgorde
        ChkVaste.Checked = vaste


        menubar.Update()
        blnEdit = True
    End Sub
    Protected Overrides Sub onBtnUpdateClick()
        MyBase.onBtnUpdateClick()
        menubar.Load()

        stelsel = txtStelsel.Text
        volgorde = txtSort.Text
        vaste = ChkVaste.CheckState
        Try
            clBLL_TypeWerknemers.UpdateWerknemersType(stelsel, volgorde, id, vaste)

            clsPreload.employeetypes = clBLL_TypeWerknemers.GetWerknemersType


        Catch ex As Exception

        Finally
            clear()
        End Try
    End Sub
    Protected Overrides Sub onDgvClick()
        MyBase.onDgvClick()
        Try
            menubar.edit()
        Catch ex As Exception

        End Try

    End Sub

    


    Protected Overrides Sub onDgvKeyDown()
        MyBase.onDgvKeyDown()

        GetSelectedRows()
    End Sub

    Sub GetSelectedRows()

        rijen.Clear()
        For Each dr As DataGridViewRow In dgView.SelectedRows
            rijen.Add(dgView.Item(clsPreload.employeetypesvaluemember, dr.Index).Value)
        Next

        'TODO boodschap weergeven, indien er geen rijen geselecteerd werden
    End Sub



    Protected Overrides Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.onDgvKeyUp(e)

        Try

            If e.KeyCode = Keys.Delete Then

                For dr As Integer = 0 To rijen.Count - 1
                    id = rijen(dr)
                    Try
                        clBLL_TypeWerknemers.DeleteWerknemersType(id)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                Next

                clsPreload.employeetypes = clBLL_TypeWerknemers.GetWerknemersType

            End If
        Catch ex As Exception

        End Try
    End Sub



    Private Sub txtStelsel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStelsel.TextChanged



        If blnEdit Then
            menubar.Update()
        Else
            If String.IsNullOrEmpty(Trim(txtStelsel.Text)) Then
                menubar.Load()
            Else
                menubar.Add()
            End If
        End If



    End Sub

    Private Sub txtSort_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSort.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAdd.PerformClick()
        End If
    End Sub
End Class
