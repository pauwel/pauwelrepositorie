<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShifts
    Inherits IMT_Timekeeping.frmBaseConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtVergoeding = New NumericBox.NumericBox
        Me.txtShift = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtSort = New NumericBox.NumericBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.chkToonInPlanning = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(198, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Vergoeding (%)"
        '
        'txtVergoeding
        '
        Me.txtVergoeding.BackColor = System.Drawing.SystemColors.Window
        Me.txtVergoeding.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtVergoeding.Location = New System.Drawing.Point(198, 92)
        Me.txtVergoeding.Name = "txtVergoeding"
        Me.txtVergoeding.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtVergoeding.Size = New System.Drawing.Size(74, 20)
        Me.txtVergoeding.TabIndex = 1
        Me.txtVergoeding.Text = "0,00"
        '
        'txtShift
        '
        Me.txtShift.Location = New System.Drawing.Point(12, 92)
        Me.txtShift.MaxLength = 20
        Me.txtShift.Name = "txtShift"
        Me.txtShift.Size = New System.Drawing.Size(180, 20)
        Me.txtShift.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Shift"
        '
        'txtSort
        '
        Me.txtSort.BackColor = System.Drawing.SystemColors.Window
        Me.txtSort.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtSort.Location = New System.Drawing.Point(279, 92)
        Me.txtSort.Name = "txtSort"
        Me.txtSort.NegativeNumberColor = System.Drawing.Color.Red
        Me.txtSort.NoDigitsAfterDecimalSymbol = 0
        Me.txtSort.Size = New System.Drawing.Size(62, 20)
        Me.txtSort.TabIndex = 2
        Me.txtSort.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(282, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Sorteer"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(282, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "volgorde"
        '
        'chkToonInPlanning
        '
        Me.chkToonInPlanning.AutoSize = True
        Me.chkToonInPlanning.Location = New System.Drawing.Point(348, 94)
        Me.chkToonInPlanning.Name = "chkToonInPlanning"
        Me.chkToonInPlanning.Size = New System.Drawing.Size(139, 17)
        Me.chkToonInPlanning.TabIndex = 3
        Me.chkToonInPlanning.Text = "Toon in planningscherm"
        Me.chkToonInPlanning.UseVisualStyleBackColor = True
        '
        'frmShifts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(584, 464)
        Me.Controls.Add(Me.chkToonInPlanning)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtSort)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtVergoeding)
        Me.Controls.Add(Me.txtShift)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmShifts"
        Me.Text = "Configuratie - Shiften -"
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.txtShift, 0)
        Me.Controls.SetChildIndex(Me.txtVergoeding, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtSort, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.chkToonInPlanning, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtVergoeding As NumericBox.NumericBox
    Friend WithEvents txtShift As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSort As NumericBox.NumericBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkToonInPlanning As System.Windows.Forms.CheckBox

End Class
