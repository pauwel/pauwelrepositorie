Public Class frmBaseConfig

    Private Sub frmBaseConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.initform()
        Me.refreshgrid()

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Me.onBtnAddClick()

        Me.refreshgrid()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Me.onBtnDeleteClick()

        Me.refreshgrid()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Me.onBtnEditClick()

    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Me.onBtnUpdateClick()

        Me.refreshgrid()
    End Sub

    Private Sub dgView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgView.Click, dgView.SelectionChanged
        Try
            Me.dgView.Rows(dgView.CurrentCellAddress.Y).Selected = True
        Catch ex As Exception

        End Try

        Me.onDgvClick()

    End Sub

    Private Sub dgView_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgView.KeyDown
        Me.onDgvKeyDown()
    End Sub

    Private Sub dgView_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgView.KeyUp
        Me.onDgvKeyUp(e)
    End Sub


    ''' <summary>
    ''' Methode die wordt aangeroepen bij het laden van het forumier 
    ''' </summary>
    ''' <remarks>Kan overschreven worden in de afgeleide forms</remarks>
    Public Overridable Sub initform()

    End Sub
    ''' <summary>
    ''' Methode die de datagridview zal opvullen bij het laden en vernieuwen bij Add/Update/Delete actions
    ''' Hier komt de code die nodig is om de juiste gegevens uit de databse in de grid te steken
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub refreshgrid()
        ' geen basisfunctionaliteit nodig
    End Sub

    Protected Overridable Sub onBtnAddClick()
        ' code here
    End Sub

    Protected Overridable Sub onBtnUpdateClick()
        'code here
    End Sub

    Protected Overridable Sub onBtnDeleteClick()
        'code here
    End Sub
    Protected Overridable Sub onBtnEditClick()
        'code here 
    End Sub
   
    Protected Overridable Sub onDgvClick()

    End Sub

   

    Protected Overridable Sub onDgvKeyUp(ByVal e As System.Windows.Forms.KeyEventArgs)

    End Sub

    Protected Overridable Sub onDgvKeyDown()

    End Sub

    Protected Overridable Sub onCmdReportClick()

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        onCmdReportClick()
    End Sub
End Class