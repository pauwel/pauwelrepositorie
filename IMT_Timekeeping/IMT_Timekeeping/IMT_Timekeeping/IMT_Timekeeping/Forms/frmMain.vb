
Option Compare Text

Imports System.Windows.Forms
Imports System.Threading
Imports System.Configuration


Public Class frmMain

    Private hoogte As Integer = 0

  

    Private Sub frmMain_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        End
    End Sub



#Region "Asynchronous handling"


    Dim thExecuteTaskAsync As Thread = Nothing
    Private Sub StartExecuteTaskAsync()
        'clear existing thread
        If Not thExecuteTaskAsync Is Nothing Then
            thExecuteTaskAsync.Abort()
            thExecuteTaskAsync.Join()
            thExecuteTaskAsync = Nothing
        End If
        'start a new thread to execute the task asynchronously
        thExecuteTaskAsync = New Thread(AddressOf ExecuteTaskAsync)
        thExecuteTaskAsync.Start()
    End Sub

    Private Sub ExecuteTaskAsync()
        PRELOAD()
    End Sub


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'this is necessary if the form is trying to close, even before the completion of task
        If Not thExecuteTaskAsync Is Nothing Then thExecuteTaskAsync.Abort()
    End Sub

#End Region

    Friend Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hWndLock As IntPtr) As Boolean



    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Try
            LockWindowUpdate(Me.Handle.ToInt32)

            'PRELOAD()
            StartExecuteTaskAsync()

            Dim ChildForm As New frmPlanning

            Try
                ChildForm.Hide()
                ChildForm.MdiParent = Me


                ChildForm.Show()
                ' ChildForm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
                ChildForm.Dock = DockStyle.Fill

            Catch ex As Exception

            End Try
            cmdButtonPlanning.Checked = True
            Me.Update()
            checkSecurity()
            LockWindowUpdate(0)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try




    End Sub

    

    Private Sub checkSecurity()

        Dim ADUser As Boolean = False 'maakt de gebruiker gebruik van AD? indien niet, beperkte weergave


        Try
            ADUser = ConfigurationManager.AppSettings("UseActiveDirectory")
        Catch ex As Exception
            ADUser = False
        End Try

        'security
        If ADUser Then
            Dim s As New BLL.isInRole
            'indien in geen enkele group, dan laagste profile=> planner

            If (Not s.IsAdmin And Not s.IsPlanner And Not s.IsTimekeeper) Or s.IsPlanner Then
                cmdButtonLoonPerFunctie.Enabled = True
                cmdButtonWerknemers.Enabled = True
                cmdButtonPlanning.Enabled = True
                cmdConfig.Enabled = True
                Timekeeping.Enabled = False
                cmdMail.Enabled = False
                cmdMailActive.Enabled = False
                ButtonItemRederijen.Enabled = True
                ButtonItemTransportmiddel.Enabled = True
                ButtonItemKostenplaats.Enabled = True
                ButtonItem1.Enabled = True
                btnItemCategories.Enabled = True
                btnItemFuncties.Enabled = True
                btnItemEmployeeTypes.Enabled = False
                ButtonItemLoonkost.Enabled = False
                ButtonItemFeestdagen.Enabled = False
                ButtonItem2.Enabled = False
                btnItemShiften.Enabled = False
                btnItemMachines.Enabled = True
                ButtonItemMachinesPerFuncties.Enabled = True

            End If


            'MsgBox(My.User.Name)

            If s.IsTimekeeper Then
                cmdButtonLoonPerFunctie.Enabled = True
                cmdButtonWerknemers.Enabled = True
                cmdButtonPlanning.Enabled = True
                cmdConfig.Enabled = True
                Timekeeping.Enabled = True
                cmdMail.Enabled = False
                cmdMailActive.Enabled = False
                ButtonItemRederijen.Enabled = True
                ButtonItemTransportmiddel.Enabled = True
                ButtonItemKostenplaats.Enabled = True
                ButtonItem1.Enabled = True
                btnItemCategories.Enabled = True
                btnItemFuncties.Enabled = True
                btnItemEmployeeTypes.Enabled = False
                ButtonItemLoonkost.Enabled = False
                ButtonItemFeestdagen.Enabled = True
                ButtonItem2.Enabled = True
                btnItemShiften.Enabled = True
                btnItemMachines.Enabled = True
                ButtonItemMachinesPerFuncties.Enabled = True
                Exit Sub
            End If

            If s.IsAdmin Then
                cmdButtonLoonPerFunctie.Enabled = True
                cmdButtonWerknemers.Enabled = True
                cmdButtonPlanning.Enabled = True
                cmdConfig.Enabled = True
                Timekeeping.Enabled = True
                cmdMail.Enabled = True
                cmdMailActive.Enabled = True
                ButtonItemRederijen.Enabled = True
                ButtonItemTransportmiddel.Enabled = True
                ButtonItemKostenplaats.Enabled = True
                ButtonItem1.Enabled = True
                btnItemCategories.Enabled = True
                btnItemFuncties.Enabled = True
                btnItemEmployeeTypes.Enabled = True
                ButtonItemLoonkost.Enabled = True
                ButtonItemFeestdagen.Enabled = True
                ButtonItem2.Enabled = True
                btnItemShiften.Enabled = True
                btnItemMachines.Enabled = True
                ButtonItemMachinesPerFuncties.Enabled = True

            End If
        End If

    End Sub



    Private Sub PRELOAD()
        With clsPreload

            .shift = clBLL_Shiften.GetShift
            .shiftdisplaymember = clBLL_Shiften.GetShift.OmschrijvingColumn.ToString
            .shiftvaluemember = clBLL_Shiften.GetShift.ShiftIDColumn.ToString


            .category = clBLL_Category.GetCategories
            .categorydisplaymember = clBLL_Category.GetCategories.CatCodeColumn.ToString
            .categoryvaluemember = clBLL_Category.GetCategories.CatIDColumn.ToString


            .employeetypes = clBLL_TypeWerknemers.GetWerknemersType
            .employeetypesdisplaymember = clBLL_TypeWerknemers.GetWerknemersType.StelselColumn.ToString()
            .employeetypesvaluemember = clBLL_TypeWerknemers.GetWerknemersType.TypeWerknemerIDColumn.ToString()


            .functies = clBLL_Functies.GetFuncties
            .functiesdisplaymember = clBLL_Functies.GetFuncties.FunctieCodeColumn.ToString
            .functiesvaluemember = clBLL_Functies.GetFuncties.FunctieIDColumn.ToString

            .employees = clBLL_werknemers.GetWerknemers
            .employeesdisplaymember = clBLL_werknemers.GetWerknemers.NaamColumn.ColumnName.ToString
            .employeesvaluemember = clBLL_werknemers.GetWerknemers.WerknemerIDColumn.ToString

            .land = clBLL_landen.GetLanden
            .landdisplaymember = clBLL_landen.GetLanden.LandColumn.ToString
            .landvaluemember = clBLL_landen.GetLanden.LandIDColumn.ToString


            .Machines = clBLL_Machines.GetMAchine
            .MachineDisplayMember = clBLL_Machines.GetMAchine.CodeWithOmschrijvingColumn.ToString
            .MachineValueMember = clBLL_Machines.GetMAchine.MachineIDColumn.ToString

            ' properies voor dropdowns met rederijen.  
            .rederij = clBLL_Rederijen.GetRederijen
            .rederijDisplayMember = clBLL_Rederijen.GetRederijen.RederijnaamColumn.ToString
            .rederijValueMember = clBLL_Rederijen.GetRederijen.rederijIDColumn.ToString
        End With
    End Sub
#Region "loon per functie"
    Private Sub cmdButtonLoonPerFunctie_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdButtonLoonPerFunctie.Click
        LockWindowUpdate(Me.Handle.ToInt64)


        CleanMDI()
        If Not CheckOpenform(frmLoonPerFunctie) Then

            Dim ChildForm As New frmLoonPerFunctie
            ChildForm.Dock = DockStyle.Fill
            ChildForm.MdiParent = Me

            ChildForm.Show()
        Else
            frmLoonPerFunctie.MdiParent = Me
            frmLoonPerFunctie.Dock = DockStyle.Fill

            BringOnTop(frmLoonPerFunctie)
            frmLoonPerFunctie.WindowState = FormWindowState.Maximized

        End If
        Selectedfunction.Text = ""
        Selectedfunction.BackColor = Color.LightGray
        SelectedVoyage.Text = ""
        SelectedVoyage.BackColor = Color.LightGray


        LockWindowUpdate(0)
    End Sub
#End Region

    Private Sub BringOnTop(ByVal f As Form)

        For Each frm As Form In MdiChildren
            If f.Name.ToString = frm.Name.ToString Then
                frm.BringToFront()
            End If
        Next
    End Sub

    Function CheckOpenform(ByVal f As Form) As Boolean
        CheckOpenform = False
        Try
            For x As Integer = 0 To Me.MdiChildren.Length - 1
                Dim tempChild As Form = CType(Me.MdiChildren(x), Form)
                If tempChild.Name = f.Name Then
                    CheckOpenform = True
                    Exit For
               
                End If
            Next
        Catch ex As Exception

        End Try

    End Function

#Region "Werknemers"
    Private Sub cmdButtonWerknemers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdButtonWerknemers.Click
        LockWindowUpdate(Me.Handle.ToInt64)

        CleanMDI()
        If Not CheckOpenform(frmWerkNemers) Then
            Dim ChildForm As New frmWerkNemers
            ChildForm.MdiParent = Me
            ChildForm.Dock = DockStyle.Fill
            ChildForm.WindowState = FormWindowState.Maximized
            ChildForm.Show()
        Else

            BringOnTop(frmWerkNemers)
        End If
     
        Selectedfunction.Text = ""
        Selectedfunction.BackColor = Color.LightGray
        SelectedVoyage.Text = ""
        SelectedVoyage.BackColor = Color.LightGray
        LockWindowUpdate(0)
    End Sub

#End Region


#Region "Planningscherm"
    Private Sub cmdButtonPlanning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdButtonPlanning.Click
        LockWindowUpdate(Me.Handle.ToInt64)
        CleanMDI()
        If Not CheckOpenform(frmPlanning) Then
            Dim ChildForm As New frmPlanning
            ChildForm.MdiParent = Me
            ChildForm.Dock = DockStyle.Fill
            ChildForm.Show()
        Else

            BringOnTop(frmPlanning)
        End If
        PlanningPanelLayout()
        
        LockWindowUpdate(0)
    End Sub


   
    Private Sub PlanningPanelLayout()

        hoogte = NavigationPanePanel3.Height
        'eerste panel top => 10
        hoogte = ((hoogte - 10) / 2)


        With panelKosten
            .Top = 10
            .Height = hoogte
            .Left = 0
            .Width = NavigationPanePanel3.Width

        End With
        With panelFuncties
            .Top = 15 + hoogte
            .Height = hoogte - 15
            .Left = 0
            .Width = NavigationPanePanel3.Width
        End With
        loadKostenTree()
        loadFunctieTree()
    End Sub

    Private Sub loadKostenTree()
        treeKosten.Nodes.Clear()
        treeKosten.ImageList = TreekostenImageList


        Dim t As Integer = 0
        For Each d As DataRow In clBLL_getTreeKostenplaats.GetTreeKosten
            Dim node As New TreeNode


            node.Text = d.Item("kostenplaats").ToString
            node.Tag = "KOST" & d.Item("kostid").ToString
            node.ImageIndex = 0
            node.SelectedImageIndex = 0
            Dim strTag As String = node.Tag
            t = strTag.Substring(4)

            For Each dr As DataRow In clBLL_Voyages.GetVoyagesByKostID(t)

                Dim child As New TreeNode
                child.Text = dr.Item("voyagenbr").ToString 
                If Trim(child.Text) <> "" Then
                    child.Tag = dr.Item("voyageid").ToString 
                    child.ImageIndex = 1
                    child.SelectedImageIndex = 2
                    node.Nodes.Add(child)
                End If

            Next

            treeKosten.Nodes.Add(node)
        Next
        treeKosten.ExpandAll()
    End Sub
    Private Sub loadFunctieTree()
        treeFuncties.Nodes.Clear()
        treeFuncties.ImageList = treefunctieImagelist


        Dim t As Integer = 0
        For Each d As DataRow In clBLL_GetTreeFuncties.GetTreeFuncties
            Dim node As New TreeNode


            node.Text = d.Item("functiecode").ToString
            node.Tag = d.Item("functieID").ToString
            node.ImageIndex = 0
            node.SelectedImageIndex = 1
            t = node.Tag

            treeFuncties.Nodes.Add(node)
        Next
    End Sub

    Private Sub NavigationPanePanel3_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NavigationPanePanel3.Resize
        PlanningPanelLayout()
    End Sub



    Private Sub treeKosten_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles treeKosten.AfterSelect
        'in try blok geplaats,Bij klik op kostendrager ipv voyage sluit de applicatie zich bij IMT,
        'deze fout is lokaal niet reproduceerbaar.....
        Try
            Dim strTag As String = treeKosten.SelectedNode.Tag.ToString

            Try
                If strTag.Substring(0, 4) = "KOST" Then
                    MsgBox("Selecteer een voyage uit de kostenplaats", MsgBoxStyle.Information)

                End If
                Try
                    SelectedVoyage.Text = "Selecteer een voyage uit de kostenplaats"
                Catch ex As Exception

                End Try

            Catch ex As Exception

                clsPlanner.SelectedVoyage = strTag
                SelectedVoyage.Text = "Voyage :" & treeKosten.SelectedNode.Text
                SelectedVoyage.BackColor = Color.DarkBlue
                SelectedVoyage.ForeColor = Color.Gold

                SelectedVoyage.Visible = True
            End Try
        Catch ex As Exception

        End Try

    End Sub

    Private Sub treeFuncties_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles treeFuncties.AfterSelect
        Dim strTag As String = treeFuncties.SelectedNode.Tag.ToString

        Try
            clsPlanner.SelectedFunctie = strTag
            Selectedfunction.Text = "Functie :" & treeFuncties.SelectedNode.Text
            Selectedfunction.BackColor = Color.DarkBlue
            Selectedfunction.ForeColor = Color.Gold
            Selectedfunction.Visible = True

        Catch ex As Exception
            Selectedfunction.Text = ""
            Selectedfunction.Visible = False
        End Try
    End Sub

#End Region

   
#Region "configuratiescherm"
    Private Sub ExplorerBar1_ItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExplorerBar1.ItemClick
        Dim formname As String
        If TypeOf sender Is DevComponents.DotNetBar.ButtonItem Then
            formname = sender.Tag.ToString
            FormFunctions.GetFormByName(formname).ShowDialog()
        End If

    End Sub
#End Region



    
   
#Region "Timekeeping"
    Private Sub cmdDagOverzicht_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDagOverzicht.Click
        LockWindowUpdate(Me.Handle.ToInt64)

        If Not CheckOpenform(frmTimekeepingOverzicht) Then
            Dim ChildForm As New frmTimekeepingOverzicht
            ChildForm.MdiParent = Me
            ChildForm.Dock = DockStyle.Fill
            ChildForm.Show()
        Else

            BringOnTop(frmTimekeepingOverzicht)
        End If



        LockWindowUpdate(0)
    End Sub


    Private Sub Timekeeping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timekeeping.Click
        CleanMDI()
        LockWindowUpdate(Me.Handle.ToInt64)

        If Not CheckOpenform(frmTimekeepingOverzicht) Then
            Dim ChildForm As New frmTimekeepingOverzicht
            ChildForm.MdiParent = Me
            ChildForm.Dock = DockStyle.Fill
            ChildForm.Show()
        Else

            BringOnTop(frmTimekeepingOverzicht)
        End If
        LockWindowUpdate(0)
    End Sub


    Private Sub CleanMDI()
        LockWindowUpdate(Me.Handle.ToInt64)
        For Each f As Form In MdiChildren
            f.Close()
        Next
        Selectedfunction.Text = ""
        Selectedfunction.BackColor = Color.LightGray
        SelectedVoyage.Text = ""
        SelectedVoyage.BackColor = Color.LightGray
        LockWindowUpdate(0)
    End Sub

    Private Sub cmdKostenAnalyse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKostenAnalyse.Click
        LockWindowUpdate(Me.Handle.ToInt64)
        CleanMDI()
        If Not CheckOpenform(frmVerdeelPrestaties) Then
            Dim ChildForm As New frmVerdeelPrestaties
            ChildForm.MdiParent = Me
            ChildForm.Dock = DockStyle.Fill
            ChildForm.Show()
        Else

            BringOnTop(frmVerdeelPrestaties)
        End If
        LockWindowUpdate(0)
    End Sub


#End Region


    Private Sub cmdAfwezigheden_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAfwezigheden.Click
        frmResetAfwezigheden.ShowDialog()
    End Sub



    Private Sub cmdConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConfig.Click
        CleanMDI()
    End Sub



    Private Sub cmdOverzicht_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOverzicht.Click

        LockWindowUpdate(Me.Handle.ToInt64)

        CleanMDI()
        If Not CheckOpenform(frmWeekOverzicht) Then
            Dim ChildForm As New frmWeekOverzicht
            ChildForm.MdiParent = Me
            ChildForm.Dock = DockStyle.Fill
            ChildForm.Show()
        Else

            BringOnTop(frmWeekOverzicht)
        End If
        LockWindowUpdate(0)

    End Sub

   
    Private Sub ButtonItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem3.Click
       

        LockWindowUpdate(Me.Handle.ToInt64)

        CleanMDI()
        If Not CheckOpenform(frmInvoerLoonSpaarplanner) Then
            Dim ChildForm As New frmInvoerLoonSpaarplanner
            ChildForm.MdiParent = Me
            ChildForm.Dock = DockStyle.Fill
            ChildForm.Show()
        Else

            BringOnTop(frmInvoerLoonSpaarplanner)
        End If
        LockWindowUpdate(0)
    End Sub

    
    Private Sub cmdKosten_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKosten.Click
        CleanMDI()
        frmReportKosten.ShowDialog()
    End Sub

   

    Private Sub cmdRederijKosten_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRederijKosten.Click
        FrmKostenPerRederij.ShowDialog()
    End Sub


End Class
