

Public Class clsMenuBar

   

    Private _AddButton As ToolStripButton
    Public Property AddButton() As ToolStripButton
        Get
            Return _AddButton
        End Get
        Set(ByVal value As ToolStripButton)
            _AddButton = value
        End Set
    End Property



    Private _EditButton As ToolStripButton
    Public Property EditButton() As ToolStripButton
        Get
            Return _EditButton
        End Get
        Set(ByVal value As ToolStripButton)
            _EditButton = value

        End Set
    End Property


    Private _UpdateButton As ToolStripButton
    Public Property UpdateButton() As ToolStripButton
        Get
            Return _UpdateButton
        End Get
        Set(ByVal value As ToolStripButton)
            _UpdateButton = value
        End Set
    End Property


    Private _DeleteButton As ToolStripButton
    Public Property DeleteButton() As ToolStripButton
        Get
            Return _DeleteButton
        End Get
        Set(ByVal value As ToolStripButton)
            _DeleteButton = value
        End Set
    End Property


    Private _datagridview As DataGridView
    Public Property Datagridview() As DataGridView
        Get
            Return _datagridview
        End Get
        Set(ByVal value As DataGridView)
            _datagridview = value
        End Set
    End Property

    Sub New()

    End Sub

    Sub New(ByVal addbutton As ToolStripButton, ByVal edit As ToolStripButton, ByVal update As ToolStripButton, ByVal delete As ToolStripButton)
        _AddButton = addbutton
        _EditButton = edit
        _UpdateButton = update
        _DeleteButton = delete
    End Sub



    ''' <summary>
    ''' Laden van het formulier
    ''' </summary>
    ''' <remarks></remarks>
    Sub Load()

        AddButton.Enabled = False
        EditButton.Enabled = False
        UpdateButton.Enabled = False
        DeleteButton.Enabled = False


    End Sub


    ''' <summary>
    ''' Editeren , selectie van een rij in de datagrid
    ''' </summary>
    ''' <remarks></remarks>
    Sub edit()
        AddButton.Enabled = True
        EditButton.Enabled = True
        UpdateButton.Enabled = False
        DeleteButton.Enabled = True
    End Sub
    
    ''' <summary>
    ''' Update, na selectie van edit
    ''' </summary>
    ''' <remarks></remarks>
    Sub Update()
        AddButton.Enabled = False
        EditButton.Enabled = False
        UpdateButton.Enabled = True
        DeleteButton.Enabled = True
    End Sub
    ''' <summary>
    ''' Add, na wijziging invoerveld(en)
    ''' </summary>
    ''' <remarks></remarks>

    Sub Add()
        AddButton.Enabled = True
        EditButton.Enabled = False
        UpdateButton.Enabled = False
        DeleteButton.Enabled = False
    End Sub








End Class
