
Imports System.Data
Public Class clsWachtbestand



    Private _notValidated As DataTable
    Public Property Notvalidated() As DataTable
        Get
            Return _notValidated
        End Get
        Set(ByVal value As DataTable)
            _notValidated = value
        End Set
    End Property

    Private _Validated As DataTable
    Public Property Validated() As DataTable
        Get
            Return _Validated
        End Get
        Set(ByVal value As DataTable)
            _Validated = value
        End Set
    End Property



    Private id As Integer
    Public Property prestatieid() As Integer
        Get
            Return id
        End Get
        Set(ByVal value As Integer)
            id = value
        End Set
    End Property



    Private _wd As Boolean
    Public Property weekdag() As Boolean
        Get
            Return _wd
        End Get
        Set(ByVal value As Boolean)
            _wd = value
        End Set
    End Property


    Private _weekend As Boolean
    Public Property Weekend() As Boolean
        Get
            Return _weekend
        End Get
        Set(ByVal value As Boolean)
            _weekend = value
        End Set
    End Property


    Private fd As Boolean
    Public Property Feestdag() As Boolean
        Get
            Return fd
        End Get
        Set(ByVal value As Boolean)
            fd = value
        End Set
    End Property


    Private pdatum As Date
    Public Property PrestatieDatum() As Date
        Get
            Return pdatum
        End Get
        Set(ByVal value As Date)
            pdatum = value
        End Set
    End Property


    Private _ploeg As Integer
    Public Property Ploeg() As Integer
        Get
            Return _ploeg
        End Get
        Set(ByVal value As Integer)
            _ploeg = value
        End Set
    End Property

    Sub SetCheckboxes()

        Select Case PrestatieDatum.DayOfWeek

            Case DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday
                If Ploeg = 39 Then
                    weekdag = False
                    Weekend = True
                Else
                    weekdag = True
                    Weekend = False
                End If

            Case DayOfWeek.Saturday, DayOfWeek.Sunday
                weekdag = False
                Weekend = True

        End Select
        If Feestdag Then
            Feestdag = True
        End If

    End Sub



    Private _afwezig As Boolean
    Public Property Afwezigheid() As Boolean
        Get
            Return _afwezig
        End Get
        Set(ByVal value As Boolean)
            _afwezig = value
        End Set
    End Property


    Private _redenafwezig As String
    Public Property RedenAfwezig() As String
        Get
            Return _redenafwezig
        End Get
        Set(ByVal value As String)
            _redenafwezig = value
        End Set
    End Property


    Private _wordtbetaald As Boolean
    Public Property WordtUitbetaald() As Boolean
        Get
            Return _wordtbetaald
        End Get
        Set(ByVal value As Boolean)
            _wordtbetaald = value
        End Set
    End Property



    Function validate() As Boolean
        Dim afwezig As New BLL.UpdateAfwezig
        If Afwezigheid Then
            If String.IsNullOrEmpty(Trim(RedenAfwezig)) Then
                MsgBox("Reden van afwezigheid invullen aub!!", MsgBoxStyle.Exclamation)
                Return False
            Else


                afwezig.SetAfwezigheid(Afwezigheid, RedenAfwezig, True, False, prestatieid)
                Return True
            End If

        Else
            afwezig.SetAfwezigheid(False, Nothing, True, WordtUitbetaald, prestatieid)
        End If
        Return True
    End Function

End Class
