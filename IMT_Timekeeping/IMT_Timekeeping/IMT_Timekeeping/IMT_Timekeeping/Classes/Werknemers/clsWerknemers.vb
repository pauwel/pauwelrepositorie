Option Compare Text

Imports System.Data.SqlTypes

Public Class clsWerknemers
    Private _isvast As Boolean
    Private _loonboek As String
    Private _naam As String, _voornaam As String
    Private _WNType As Nullable(Of Integer), _CAT As Nullable(Of Integer)
    Private _adres As String, _postcode As String, _gemeente As String, _land As Integer
    Private _mail As String, _telefoon As String, _mobiel As String, _id As Int32

    Private _datumvastindienst As DateTime, _datumgeboorte As DateTime, _datumspaarplan As DateTime
    Private _spaarplan As Boolean = False



    Private wn As DataTable
    Public Property WerknemersData() As DataTable
        Get
            Return wn
        End Get
        Set(ByVal value As DataTable)
            wn = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Public Property IsVast() As Boolean
        Get
            Return _isvast
        End Get
        Set(ByVal value As Boolean)
            _isvast = value
        End Set
    End Property
    Public Property loonboek() As String
        Get
            Return _loonboek
        End Get
        Set(ByVal value As String)
            _loonboek = value
        End Set
    End Property
    Public Property naam() As String
        Get
            Return _naam
        End Get
        Set(ByVal value As String)
            _naam = value
        End Set
    End Property
    Public Property voornaam() As String
        Get
            Return _voornaam
        End Get
        Set(ByVal value As String)
            _voornaam = value
        End Set
    End Property

    Public Property Werknemerstype() As Nullable(Of Integer)
        Get
            Return _WNType
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _WNType = value
        End Set
    End Property
    Public Property category() As Nullable(Of Integer)
        Get
            Return _CAT
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CAT = value
        End Set
    End Property

    Public Property adres() As String
        Get
            Return _adres
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Or Trim(value) = "" Then
                _adres = Nothing
            Else
                _adres = value
            End If
        End Set
    End Property
    Public Property postcode() As String
        Get
            Return _postcode
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Or Trim(value) = "" Then
                _postcode = Nothing
            Else
                _postcode = value
            End If
        End Set
    End Property
    Public Property gemeente() As String
        Get
            Return _gemeente
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Or Trim(value) = "" Then
                _gemeente = Nothing
            Else
                _gemeente = value
            End If
        End Set
    End Property
    Public Property land() As Integer
        Get
            Return _land
        End Get
        Set(ByVal value As Integer)
            If IsNumeric(value) Then
                _land = value
            Else
                _land = 0
            End If
        End Set
    End Property

    Public Property email() As String
        Get
            Return _mail
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Or Trim(value) = "" Then
                _mail = Nothing
            Else
                _mail = value
            End If
        End Set
    End Property
    Public Property telefoon() As String
        Get
            Return _telefoon
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Or Trim(value) = "" Then
                _telefoon = Nothing
            Else
                _telefoon = value
            End If
        End Set
    End Property
    Public Property mobiel() As String
        Get
            Return _mobiel
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Or Trim(value) = "" Then
                _mobiel = Nothing
            Else
                _mobiel = value
            End If
        End Set
    End Property
    Public Property DatumVastInDienst() As DateTime
        Get
            Return _datumvastindienst
        End Get
        Set(ByVal value As DateTime)
            If _isvast Then
                _datumvastindienst = value
            Else
                _datumvastindienst = SqlDateTime.Null
            End If

        End Set
    End Property

    Public Property DatumGeboorte() As DateTime
        Get
            Return _datumgeboorte
        End Get
        Set(ByVal value As DateTime)
            If _spaarplan = True Then
                _datumgeboorte = value
            Else
                _datumgeboorte = SqlDateTime.Null
            End If

        End Set
    End Property
    Public Property DatumSpaarplan() As DateTime
        Get
            Return _datumspaarplan
        End Get
        Set(ByVal value As DateTime)
            If _spaarplan = True Then
                _datumspaarplan = value
            Else
                _datumspaarplan = SqlDateTime.Null
            End If
        End Set
    End Property

    Public Property spaarplan() As Boolean
        Get
            Return _spaarplan
        End Get
        Set(ByVal value As Boolean)
            _spaarplan = value

        End Set
    End Property

    Public Function VALIDATE() As Boolean
        Dim ok As Boolean = True

        If String.IsNullOrEmpty(_loonboek) Or _loonboek = "_____/__" Then
            ok = False
            MsgBox("Loonboek is een verplicht veld", MsgBoxStyle.Information, APPLICATIENAAM)
        End If

        If String.IsNullOrEmpty(_naam) Or Trim(_naam) = "" Then
            ok = False
            MsgBox("naam is een verplicht veld", MsgBoxStyle.Information, APPLICATIENAAM)
        End If
        If String.IsNullOrEmpty(_voornaam) Or Trim(_voornaam) = "" Then
            ok = False
            MsgBox("voornaam is een verplicht veld", MsgBoxStyle.Information, APPLICATIENAAM)
        End If
        If Not _WNType.HasValue Then
            ok = False
            MsgBox("Werknemer Type is een verplicht veld", MsgBoxStyle.Information, APPLICATIENAAM)
        End If
        If Not _CAT.HasValue Then
            ok = False
            MsgBox("Category is een verplicht veld", MsgBoxStyle.Information, APPLICATIENAAM)
        End If


        'controle indien de werknemer vast in dienst is
        If _isvast Then
            If Not _datumvastindienst = SqlDateTime.Null Then
                ok = False
                MsgBox("Datum vast in dienst is een verplicht veld voor vaste werknemers", MsgBoxStyle.Information, APPLICATIENAAM)
            End If
        End If
        'controle indien spaarplan
        If _spaarplan Then
            If Not _datumgeboorte = SqlDateTime.Null Then
                ok = False
                MsgBox("Datum geboorte is een verplicht veld voor werknemers met spaarplan", MsgBoxStyle.Information, APPLICATIENAAM)
            End If
            If Not _datumspaarplan = SqlDateTime.Null Then
                ok = False
                MsgBox("Datum spaarplan is een verplicht veld voor werknemers met spaarplan", MsgBoxStyle.Information, APPLICATIENAAM)
            End If

            If _datumvastindienst.Date > DatumSpaarplan.Date Then
                ok = False
                MsgBox("Datum spaarplan kan nooit voor datum vast in dienst zijn", MsgBoxStyle.Information, APPLICATIENAAM)
            End If
        End If

        Return ok
    End Function

    Public Function LoonboekIsUnique() As Boolean

        If clBLL_werknemers.LoonboekIsUnique(_loonboek) Then
            Return True
        Else
            Return False
        End If

    End Function
End Class


