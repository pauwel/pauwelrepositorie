Namespace My

    Partial Friend Class MyApplication

        Protected Overrides Function OnInitialize(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            'Set the splash screen timeout. 

            Me.MinimumSplashScreenDisplayTime = 5000

            Return MyBase.OnInitialize(commandLineArgs)

        End Function

    End Class
    
End Namespace

