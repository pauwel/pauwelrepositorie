
Imports BLL

Module ClassVAR

    'BLL classes
    Public clBLL_LoonPF As New BLL.LoonPerFuncties
    Public clBLL_Functies As New BLL.Functions
    Public clBLL_Category As New BLL.Category

    Public clBLL_TypeWerknemers As New BLL.TypeWerknemers
    Public clBLL_Shiften As New BLL.Shiften

    Public clBLL_landen As New BLL.Landen
    Public clBLL_werknemers As New BLL.Werknemers

    Public clBLL_loonkost As New BLL.Loonkost
    Public clsPreload As New startup

    Public clBLL_TransportMiddel As New BLL.TransportMiddel
    Public clBLL_Voyages As New BLL.Voyages
    'Public clBLL_Kostenplaats As New BLL.Kosten

    Public clBLL_Machines As New BLL.Machines
    Public clBLL_FunctiePerMachine As New BLL.FunctiePerMachine

    Public clBLL_PrestatiesWN As New BLL.PrestatiesWerknemers
    Public clBLL_feestdagen As New BLL.Feestdagen

    Public clBLL_Rederijen As New BLL.Rederij
    Public clBLL_Kostenplaats As New BLL.Kostenplaats

    Public clBLL_Leeftijd As New BLL.LeeftijdCoefficient


    Public clBLL_Wachtbestand As New BLL.Wachtbestand
    Public clBLL_Mail As New BLL.Mail

    
    'Stored Procedures
    Public clBLL_GetTreeFuncties As New BLL.TreeFuncties
    Public clBLL_getTreeKostenplaats As New BLL.TreeKostenPlaats
    Public clBLL_GetWerknemersByFunctie As New BLL.BLLGetWerknemersByFunctie
    Public clBLL_GetFunctiePerMachine As New BLL.FunctiesPerMachine
    Public clBLL_GetMachinesPerSelectedFunctie As New BLL.MachinesPerSelectedFunctie
    Public cllBLL_getVoyageByDate As New BLL.GetVoyagesPerDate
    Public clBLL_getPrestatieBydate As New BLL.GetPrestatieByDate
    Public clBLL_getLoonPerFunctie As New BLL.GetLoonPerFunctieDetail
    Public clBLL_getTransportmiddelen As New BLL.SP_Transporten



    'Classes
    Public clsPlanner As New clsPlanning




End Module
