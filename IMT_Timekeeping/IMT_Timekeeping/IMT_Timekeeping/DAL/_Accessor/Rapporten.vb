﻿Imports System.Data.SqlClient
Imports System.Configuration

Public Class Rapporten


    Public Function DLreportSelector(ByVal werknemerid As Integer) As DataTable

        Dim con As New SqlConnection
        con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString

        Try
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand
            With com
                .CommandText = _
                    "select ws.werknemerid,ws.statuutid, ws.datefrom as begindatum, ws.dateuntil as einddatum, s.statuut from WerknemerStatuut ws " & _
                    "inner join  Statuten s on s.statuutid=ws.statuutid " & _
                    "where(ws.werknemerid = @werknemerid) order by 3"

                Dim p(0) As SqlParameter
                p(0) = New SqlParameter
                With p(0)
                    .ParameterName = "@werknemerid"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = werknemerid
                End With

                .Parameters.Add(p(0))
                .CommandType = CommandType.Text
                .Connection = con
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds.Tables(0)

        Catch ex As Exception
            Return Nothing
        Finally

            con.Close()
        End Try

    End Function


End Class
