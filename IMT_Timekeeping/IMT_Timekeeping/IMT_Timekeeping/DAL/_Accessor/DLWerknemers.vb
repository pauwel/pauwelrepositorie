﻿Imports System.Data.SqlClient
Imports System.Configuration

Public Class DLWerknemers
   

    Public Function InsertWerknemers(ByVal loonboek As String, ByVal naam As String, ByVal voornaam As String, ByVal geboorte As DateTime, ByVal adres As String, ByVal postcode As String, _
        ByVal gemeente As String, ByVal land As Int32, ByVal tel As String, ByVal mobiel As String, ByVal mail As String, ByVal datumvast As DateTime, ByVal spaarplan As Boolean, ByVal datumspaarplan As DateTime, _
        ByVal catid As Int32, ByVal typewn As Int32) As Boolean


        Dim con As New SqlConnection
        con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString

        Try
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand
            With com
                .CommandText = "InsertWerknemer"
                .CommandType = CommandType.StoredProcedure

                Dim p(15) As SqlParameter
                p(0) = New SqlParameter
                With p(0)
                    .ParameterName = "@loonboek"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = loonboek
                End With
                .Parameters.Add(p(0))

                p(1) = New SqlParameter
                With p(1)
                    .ParameterName = "@Naam"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = naam
                End With
                .Parameters.Add(p(1))

                p(2) = New SqlParameter
                With p(2)
                    .ParameterName = "@Voornaam"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = voornaam
                End With
                .Parameters.Add(p(2))

                p(3) = New SqlParameter
                With p(3)
                    .ParameterName = "@Geboortedatum"
                    .SqlDbType = SqlDbType.DateTime
                    .SqlValue = geboorte
                End With
                .Parameters.Add(p(3))

                p(4) = New SqlParameter
                With p(4)
                    .ParameterName = "@Adres"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(adres Is Nothing, DBNull.Value, adres)
                End With
                .Parameters.Add(p(4))

                p(5) = New SqlParameter
                With p(5)
                    .ParameterName = "@Postcode"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(postcode Is Nothing, DBNull.Value, postcode)
                End With
                .Parameters.Add(p(5))

                p(6) = New SqlParameter
                With p(6)
                    .ParameterName = "@Gemeente"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(gemeente Is Nothing, DBNull.Value, gemeente)
                End With
                .Parameters.Add(p(6))

                p(7) = New SqlParameter
                With p(7)
                    .ParameterName = "@Land"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = land
                End With
                .Parameters.Add(p(7))

                p(8) = New SqlParameter
                With p(8)
                    .ParameterName = "@Telefoon"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(tel Is Nothing, DBNull.Value, tel)
                End With
                .Parameters.Add(p(8))

                p(9) = New SqlParameter
                With p(9)
                    .ParameterName = "@Mobiel"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(mobiel Is Nothing, DBNull.Value, mobiel)
                End With
                .Parameters.Add(p(9))

                p(10) = New SqlParameter
                With p(10)
                    .ParameterName = "@Email"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(mail Is Nothing, DBNull.Value, mail)
                End With
                .Parameters.Add(p(10))

                p(11) = New SqlParameter
                With p(11)
                    .ParameterName = "@DatumVastIndienst"
                    .SqlDbType = SqlDbType.DateTime
                    .SqlValue = datumvast
                End With
                .Parameters.Add(p(11))

                p(12) = New SqlParameter
                With p(12)
                    .ParameterName = "@Spaarplan"
                    .SqlDbType = SqlDbType.Bit
                    .SqlValue = IIf(Not spaarplan, 0, 1)
                End With
                .Parameters.Add(p(12))

                p(13) = New SqlParameter
                With p(13)
                    .ParameterName = "@DatumSpaarplan"
                    .SqlDbType = SqlDbType.DateTime
                    .SqlValue = datumspaarplan
                End With
                .Parameters.Add(p(13))

                p(14) = New SqlParameter
                With p(14)
                    .ParameterName = "@CatID"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = catid
                End With
                .Parameters.Add(p(14))

                p(15) = New SqlParameter
                With p(15)
                    .ParameterName = "@TypeWerknemerID"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = typewn
                End With
                .Parameters.Add(p(15))

                .Connection = con

            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0) = 1 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally

            con.Close()
        End Try



    End Function



    Public Function UpdateWerknemers(ByVal loonboek As String, ByVal naam As String, ByVal voornaam As String, ByVal geboorte As DateTime, ByVal adres As String, ByVal postcode As String, _
        ByVal gemeente As String, ByVal land As Int32, ByVal tel As String, ByVal mobiel As String, ByVal mail As String, ByVal datumvast As DateTime, ByVal spaarplan As Boolean, ByVal datumspaarplan As DateTime, _
        ByVal catid As Int32, ByVal typewn As Int32, ByVal werknemerid As Integer) As Boolean


        Dim con As New SqlConnection
        con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString

        Try
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand
            With com
                .CommandText = "UpdateWerknemer"
                .CommandType = CommandType.StoredProcedure

                Dim p(16) As SqlParameter
                p(0) = New SqlParameter
                With p(0)
                    .ParameterName = "@loonboek"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = loonboek
                End With
                .Parameters.Add(p(0))

                p(1) = New SqlParameter
                With p(1)
                    .ParameterName = "@Naam"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = naam
                End With
                .Parameters.Add(p(1))

                p(2) = New SqlParameter
                With p(2)
                    .ParameterName = "@Voornaam"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = voornaam
                End With
                .Parameters.Add(p(2))

                p(3) = New SqlParameter
                With p(3)
                    .ParameterName = "@Geboortedatum"
                    .SqlDbType = SqlDbType.DateTime
                    .SqlValue = geboorte
                End With
                .Parameters.Add(p(3))

                p(4) = New SqlParameter
                With p(4)
                    .ParameterName = "@Adres"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(adres Is Nothing, DBNull.Value, adres)
                End With
                .Parameters.Add(p(4))

                p(5) = New SqlParameter
                With p(5)
                    .ParameterName = "@Postcode"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(postcode Is Nothing, DBNull.Value, postcode)
                End With
                .Parameters.Add(p(5))

                p(6) = New SqlParameter
                With p(6)
                    .ParameterName = "@Gemeente"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(gemeente Is Nothing, DBNull.Value, gemeente)
                End With
                .Parameters.Add(p(6))

                p(7) = New SqlParameter
                With p(7)
                    .ParameterName = "@Land"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = land
                End With
                .Parameters.Add(p(7))

                p(8) = New SqlParameter
                With p(8)
                    .ParameterName = "@Telefoon"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(tel Is Nothing, DBNull.Value, tel)
                End With
                .Parameters.Add(p(8))

                p(9) = New SqlParameter
                With p(9)
                    .ParameterName = "@Mobiel"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(mobiel Is Nothing, DBNull.Value, mobiel)
                End With
                .Parameters.Add(p(9))

                p(10) = New SqlParameter
                With p(10)
                    .ParameterName = "@Email"
                    .SqlDbType = SqlDbType.VarChar
                    .SqlValue = IIf(mail Is Nothing, DBNull.Value, mail)
                End With
                .Parameters.Add(p(10))

                p(11) = New SqlParameter
                With p(11)
                    .ParameterName = "@DatumVastIndienst"
                    .SqlDbType = SqlDbType.DateTime
                    .SqlValue = datumvast
                End With
                .Parameters.Add(p(11))

                p(12) = New SqlParameter
                With p(12)
                    .ParameterName = "@Spaarplan"
                    .SqlDbType = SqlDbType.Bit
                    .SqlValue = IIf(Not spaarplan, 0, 1)
                End With
                .Parameters.Add(p(12))

                p(13) = New SqlParameter
                With p(13)
                    .ParameterName = "@DatumSpaarplan"
                    .SqlDbType = SqlDbType.DateTime
                    .SqlValue = datumspaarplan
                End With
                .Parameters.Add(p(13))

                p(14) = New SqlParameter
                With p(14)
                    .ParameterName = "@CatID"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = catid
                End With
                .Parameters.Add(p(14))

                p(15) = New SqlParameter
                With p(15)
                    .ParameterName = "@TypeWerknemerID"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = typewn
                End With
                .Parameters.Add(p(15))

                p(16) = New SqlParameter
                With p(16)
                    .ParameterName = "@werknemerid"
                    .SqlDbType = SqlDbType.Int
                    .SqlValue = werknemerid
                End With
                .Parameters.Add(p(16))

                .Connection = con

            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0) = 1 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally

            con.Close()
        End Try



    End Function

End Class
