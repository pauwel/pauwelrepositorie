﻿
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Configuration.ConfigurationManager

Public Class wizardSpaarplannerAccessor

    Public Function GetSpaarplanners() As DataTable

        Try

            Dim con As New SqlConnection
            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand
            With com
                .CommandType = CommandType.StoredProcedure
                .CommandText = "GetSpaarplanners"
                .Connection = con
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            con.Close()
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function OvergangNietSpaarplanner(ByVal werknemerid As Integer, ByVal wachtbedrag As Decimal) As Boolean
        Try

            Dim con As New SqlConnection
            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand

            Dim p(1) As SqlParameter
            p(0) = New SqlParameter
            With p(0)
                .ParameterName = "@werknemerid"
                .SqlDbType = SqlDbType.Int
                .SqlValue = werknemerid
            End With
            p(1) = New SqlParameter
            With p(1)
                .ParameterName = "@wachtbedrag"
                .SqlDbType = SqlDbType.Decimal
                .SqlValue = wachtbedrag
            End With
            With com
                .CommandType = CommandType.StoredProcedure
                .CommandText = "UpdateNaarNietSpaarplanner"
                .Connection = con
                .Parameters.Add(p(0))
                .Parameters.Add(p(1))
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            con.Close()
            If ds.Tables(0).Rows(0).Item(0) = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Public Function UpdateStatuut(ByVal werknemerid As Integer) As Boolean
        Try

            Dim con As New SqlConnection
            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand

            Dim p(1) As SqlParameter
            p(0) = New SqlParameter
            With p(0)
                .ParameterName = "@werknemerid"
                .SqlDbType = SqlDbType.Int
                .SqlValue = werknemerid
            End With
            p(1) = New SqlParameter
            With p(1)
                .ParameterName = "@StatuutID"
                .SqlDbType = SqlDbType.Int
                .SqlValue = 0
            End With
            With com
                .CommandType = CommandType.StoredProcedure
                .CommandText = "UpdateStatuut"
                .Connection = con
                .Parameters.Add(p(0))
                .Parameters.Add(p(1))
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            con.Close()
            If ds.Tables(0).Rows(0).Item(0) = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try


    End Function

End Class
