﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports DAL.entities

Public Class DLLoonPerFunctie

    Public Function GetLoonPerFunctie() As DataTable

        Try

            Dim strSQL As String = String.Empty


            strSQL = "select distinct c.catcode,f.Functiecode,TW.Stelsel, s.Omschrijving, lpf.Formule, lpf.LoonID, lpf.CatID, lpf.FunctieID, lpf.stdLoon, " & _
                    "lpf.TotBedragOveruren, lpf.TotSupplement, lpf.eindejaarspremie, lpf.TypeWerknemerID, lpf.DatumBijdrageBerekeningCEPA, lpf.ShiftID, lpf.SupplementDubbeleShift  " & _
                    "from tblLoonPerFunctie lpf  " & _
                    "inner join tblCategories c on c.CatID= lpf.catid  " & _
                    "inner join tblFuncties f on f.FunctieID= lpf.functieid  " & _
                    "inner join tblShiften s on s.ShiftID=lpf.shiftid  " & _
                    "inner join tblTypeWerknemer TW  on TW.TypeWerknemerID=lpf.TypeWerknemerID " & _
                    "order by  1 ,2 ,3,4,14"


            Dim con As New SqlConnection
            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand
            With com
                .CommandType = CommandType.Text
                .CommandText = strSQL
                .Connection = con
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            con.Close()
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function UpdateLoonPerFunctie(ByVal data As LoonPerFunctie_Entity) As Boolean


        Dim con As New SqlConnection
        Try

            Dim strSQL As String = String.Empty

            strSQL = "UPDATE tblLoonPerFunctie " & _
                      " SET CatID =@CatId " & _
                          ",FunctieID = @FunctieID  " & _
                          ",Formule = @Formule " & _
                          ",stdLoon = @stdLoon " & _
                          ",TotBedragOveruren = @TotBedragOveruren " & _
                          ",TotSupplement = @TotSupplement " & _
                          ",eindejaarspremie = @eindejaarspremie " & _
                          ",TypeWerknemerID = @TypeWerknemerID " & _
                          ",DatumBijdrageBerekeningCEPA = @DatumBijdrageBerekeningCEPA " & _
                          ",ShiftID = @ShiftID " & _
                          ",SupplementDubbeleShift = @SupplementDubbeleShift " & _
                     "WHERE loonid= @LoonId"

            Dim p(11) As SqlParameter
            p(0) = New SqlParameter
            With p(0)
                .ParameterName = "@CatID"
                .SqlDbType = SqlDbType.Int
                .SqlValue = data.CatID
            End With
            p(1) = New SqlParameter
            With p(1)
                .ParameterName = "@FunctieID"
                .SqlDbType = SqlDbType.Int
                .SqlValue = data.FunctieID
            End With
            p(2) = New SqlParameter
            With p(2)
                .ParameterName = "@Formule"
                .SqlDbType = SqlDbType.VarChar
                .SqlValue = data.Formule
            End With
            p(3) = New SqlParameter
            With p(3)
                .ParameterName = "@stdLoon"
                .SqlDbType = SqlDbType.Decimal
                .SqlValue = data.stdLoon
            End With
            p(4) = New SqlParameter
            With p(4)
                .ParameterName = "@TotBedragOveruren"
                .SqlDbType = SqlDbType.Decimal
                .SqlValue = data.TotBedragOveruren
            End With
            p(5) = New SqlParameter
            With p(5)
                .ParameterName = "@TotSupplement"
                .SqlDbType = SqlDbType.Decimal
                .SqlValue = data.TotSupplement
            End With
            p(6) = New SqlParameter
            With p(6)
                .ParameterName = "@eindejaarspremie"
                .SqlDbType = SqlDbType.Decimal
                .SqlValue = data.eindejaarspremie
            End With
            p(7) = New SqlParameter
            With p(7)
                .ParameterName = "@TypeWerknemerID"
                .SqlDbType = SqlDbType.Int
                .SqlValue = data.TypeWerknemerID
            End With
            p(8) = New SqlParameter
            With p(8)
                .ParameterName = "@DatumBijdrageBerekeningCEPA"
                .SqlDbType = SqlDbType.DateTime
                .SqlValue = data.DatumBijdrageBerekeningCEPA
            End With
            p(9) = New SqlParameter
            With p(9)
                .ParameterName = "@ShiftID"
                .SqlDbType = SqlDbType.Int
                .SqlValue = data.ShiftID
            End With
            p(10) = New SqlParameter
            With p(10)
                .ParameterName = "@SupplementDubbeleShift"
                .SqlDbType = SqlDbType.Decimal
                .SqlValue = data.SupplementDubbeleShift
            End With

            p(11) = New SqlParameter
            With p(11)
                .ParameterName = "@LoonId"
                .SqlDbType = SqlDbType.Int
                .SqlValue = data.LoonID
            End With

       

            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand
            With com
                .CommandType = CommandType.Text
                .CommandText = strSQL
                For Each par As SqlParameter In p
                    .Parameters.Add(par)
                Next

                .Connection = con
                .ExecuteNonQuery()
            End With

        Catch ex As Exception
            Throw ex
        Finally
            con.Close()
        End Try
        Return True
    End Function



End Class
