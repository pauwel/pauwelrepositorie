﻿
Namespace entities
    Public Class WerknemerStatuut_entity

        Public Sub WerknemerStatuut_entity()
            werknemerid = 0
            statuutid = 0
            begindatum = DateTime.MinValue
            einddatum = DateTime.MaxValue
            statuut = String.Empty
        End Sub

        Private _werknemerid As Integer
        Public Property werknemerid() As Integer
            Get
                Return _werknemerid
            End Get
            Set(ByVal value As Integer)
                _werknemerid = value
            End Set
        End Property


        Private _statuutid As Integer
        Public Property statuutid() As Integer
            Get
                Return _statuutid
            End Get
            Set(ByVal value As Integer)
                _statuutid = value
            End Set
        End Property

        Private _begindatum As DateTime
        Public Property begindatum() As DateTime
            Get
                Return _begindatum
            End Get
            Set(ByVal value As DateTime)
                _begindatum = value
            End Set
        End Property

        Private _einddatum As DateTime
        Public Property einddatum() As DateTime
            Get
                Return _einddatum
            End Get
            Set(ByVal value As DateTime)
                _einddatum = value
            End Set
        End Property

        Private _statuut As String
        Public Property statuut() As String
            Get
                Return _statuut
            End Get
            Set(ByVal value As String)
                _statuut = value
            End Set
        End Property





    End Class

End Namespace
