﻿Imports System.Collections.Generic
Imports DAL.entities


Public Class werknemerstatuut_convertor

    Public Function ConvertWerknemerstatuut(ByVal dt As DataTable) As List(Of WerknemerStatuut_entity)
        Try
            Dim wn As New List(Of WerknemerStatuut_entity)

            For Each r As DataRow In dt.Rows
                Dim temp As WerknemerStatuut_entity = New WerknemerStatuut_entity
                With temp
                    .werknemerid = r("werknemerid")
                    .statuutid = r("statuutid")
                    .begindatum = r("begindatum")
                    .einddatum = IIf(r.IsNull("einddatum"), Today.AddDays(1), r("einddatum"))
                    .statuut = r("statuut")
                End With
                wn.Add(temp)
            Next

            Return wn
        Catch ex As Exception
            Throw ex
        End Try

    End Function


End Class
