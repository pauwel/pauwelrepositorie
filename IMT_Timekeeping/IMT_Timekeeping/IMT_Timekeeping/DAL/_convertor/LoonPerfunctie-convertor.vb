﻿
Imports System.Collections.Generic
Imports DAL.entities
'Imports DAL.DAL.entities

Public Class LoonPerfunctie_convertor

    Public Function ConvertLoonPerFunctie(ByVal dt As DataTable) As List(Of LoonPerFunctie_Entity)
        Try
            Dim wn As New List(Of LoonPerFunctie_Entity)

            For Each r As DataRow In dt.Rows
                Dim temp As LoonPerFunctie_Entity = New LoonPerFunctie_Entity
                With temp
                    .CatID = r("CatID")
                    .DatumBijdrageBerekeningCEPA = r("DatumBijdrageBerekeningCEPA")
                    .eindejaarspremie = r("eindejaarspremie")
                    .Formule = r("Formule")
                    .FunctieID = r("FunctieID")
                    .LoonID = r("LoonID")
                    .ShiftID = r("ShiftID")
                    .stdLoon = r("stdLoon")
                    .SupplementDubbeleShift = r("SupplementDubbeleShift")
                    .TotBedragOveruren = r("TotBedragOveruren")
                    .TotSupplement = r("TotSupplement")
                    .TypeWerknemerID = r("TypeWerknemerID")
                    .CatCode = r("Catcode")
                    .FunctieCode = r("Functiecode")
                    .Omschrijving = r("Omschrijving")
                    .Stelsel = r("stelsel")

                End With
                wn.Add(temp)
            Next

            Return wn
        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Class
