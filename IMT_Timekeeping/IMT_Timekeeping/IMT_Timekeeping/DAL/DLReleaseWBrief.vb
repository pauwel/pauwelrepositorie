﻿Imports System.Data.SqlClient
Imports System.Configuration

Public Class DLReleaseWBrief

    Public Function DLGetNietSpaarplanner() As DataTable
        Dim con As New SqlConnection
        Try

            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If

            Dim com As New SqlCommand
            With com
                .CommandText = "select distinct w.WerknemerID, (w.loonboek + ' ' + w.naam + ' ' + w.voornaam) as 'Naam' " & _
                                " from tblWerknemers w " & _
                                " inner join tblbetalingen b on b.werknemerid=w.werknemerid" & _
                                " order by (w.loonboek + ' ' + w.naam + ' ' + w.voornaam)"

                .CommandType = CommandType.Text
                Dim p As New SqlParameter
                p.ParameterName = "@datum"
                p.SqlDbType = SqlDbType.DateTime
                p.SqlValue = Now.Date.AddYears(-1)

                .Parameters.Add(p)
                .Connection = con
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds.Tables(0)


        Catch ex As Exception
            Throw ex
        Finally
            con.Close()
        End Try

    End Function

    Private Function IsSpaarplanner(ByVal werknemerid As Integer) As Boolean
        Dim con As New SqlConnection
        Try

            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If

            Dim com As New SqlCommand
            With com

                Dim sqlStr As String = "select isnull(spaarplan,0) as spaarplan from tblwerknemers where werknemerid=@werknemerid"
                .CommandText = sqlStr
                .CommandType = CommandType.Text

                Dim p(0) As SqlParameter
                p(0) = New SqlParameter
                p(0).ParameterName = "werknemerid"
                p(0).SqlDbType = SqlDbType.Int
                p(0).SqlValue = werknemerid

                .Parameters.Add(p(0))

        

                .Connection = con
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0) = 0 Then
                Return False
            Else
                Return True
            End If


        Catch ex As Exception
            Throw ex
        Finally
            con.Close()
        End Try
    End Function

    Public Function DLGetPrestatiesSendedToCEPA(ByVal werknemerid As Integer) As DataTable
        Dim con As New SqlConnection
        Try

            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If

            Dim com As New SqlCommand
            With com
                Dim sqlStr As String = Nothing

                sqlStr = "select  b.betaalID,  b.datum,   b.cepaBedrag,  b.code30, b.wachtid,  " & _
                        "case when isnull( b.wachtid,0)=0 then 0 else (select distinct w.wbbrief from tblwachtbestand w where w.wachtid = b.wachtid) end as 'WBBrief' " & _
                        ",   b.Walking,  b.Premie,  b.kledijvergoeding,   b.CEPADatum from tblbetalingen b where werknemerid=@werknemerid order by  b.CEPADatum desc, b.datum desc"



                .CommandText = sqlStr
                .CommandType = CommandType.Text
                Dim p(0) As SqlParameter
                p(0) = New SqlParameter
                p(0).ParameterName = "@werknemerid"
                p(0).SqlDbType = SqlDbType.Int
                p(0).SqlValue = werknemerid

                .Parameters.Add(p(0))


                .Connection = con
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds.Tables(0)


        Catch ex As Exception
            Throw ex
        Finally
            con.Close()
        End Try

    End Function

    Public Function DLReleaseWachtid(ByVal betaalid As Integer, ByVal wbbrief As Integer) As Boolean
        Dim con As New SqlConnection
        Try

            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If

            Dim com As New SqlCommand
            With com

                .CommandText = "ReleaseWBBRIEF"
                .CommandType = CommandType.StoredProcedure
                Dim p(1) As SqlParameter

                p(0) = New SqlParameter
                p(0).ParameterName = "@betaalid"
                p(0).SqlDbType = SqlDbType.Int
                p(0).SqlValue = betaalid
                .Parameters.Add(p(0))


                p(1) = New SqlParameter
                p(1).ParameterName = "@wbbrief"
                p(1).SqlDbType = SqlDbType.Int
                p(1).SqlValue = wbbrief
                .Parameters.Add(p(1))

                .Connection = con
            End With
            Dim da As New SqlDataAdapter(com)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0) = 1 Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            Throw ex
        Finally
            con.Close()
        End Try

    End Function

   
End Class
