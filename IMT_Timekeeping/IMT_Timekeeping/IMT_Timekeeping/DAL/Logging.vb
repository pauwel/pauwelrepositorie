﻿Imports System.Data.SqlClient
Imports System.Configuration

Public Class Logging
    

    Public Function Log(ByVal betaalid As Integer, ByVal werknemerid As Integer, ByVal actieid As Integer, ByVal memo As String) As Boolean
        Dim con As New SqlConnection
        Try

            con.ConnectionString = ConfigurationManager.ConnectionStrings("DAL.My.MySettings.IMT_TimekeepingConnectionString").ConnectionString
            If Not con.State = ConnectionState.Open Then
                con.Open()
            End If
            Dim com As New SqlCommand

            Dim p(4) As SqlParameter
            p(0) = New SqlParameter
            With p(0)
                .ParameterName = "@userid"
                .SqlDbType = SqlDbType.VarChar
                .SqlValue = Environment.UserName
            End With
            p(1) = New SqlParameter
            With p(1)
                .ParameterName = "@actieid"
                .SqlDbType = SqlDbType.Int
                .SqlValue = actieid
            End With
            p(2) = New SqlParameter
            With p(2)
                .ParameterName = "@memo"
                .SqlDbType = SqlDbType.VarChar
                .SqlValue = memo
            End With
            p(3) = New SqlParameter
            With p(3)
                .ParameterName = "@wachtid"
                .SqlDbType = SqlDbType.Int
                .SqlValue = betaalid
            End With

            p(4) = New SqlParameter
            With p(4)
                .ParameterName = "@werknemerid"
                .SqlDbType = SqlDbType.Int
                .SqlValue = werknemerid
            End With
            With com
                .CommandType = CommandType.StoredProcedure
                .CommandText = "InsertLog"
                .Connection = con
                .Parameters.Add(p(0))
                .Parameters.Add(p(1))
                .Parameters.Add(p(2))
                .Parameters.Add(p(3))
                .Parameters.Add(p(4))

            End With

            com.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        Finally
            con.Close()
        End Try


    End Function


End Class
